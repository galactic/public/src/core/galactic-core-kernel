**GALACTIC** core kernel instructions
=====================================

Prerequisite
------------

*galactic-core-kernel* requires `python 3.10`_,
a programming language that comes pre-installed on linux and Mac OS X,
and which is easily installed `on Windows`_;

Installation
------------

Install *galactic-core-kernel* using the bash command

.. code-block:: shell-session

    $ pip install \
        --index-url https://gitlab.univ-lr.fr/api/v4/groups/galactic/-/packages/pypi/simple \
        galactic-core-kernel[docs]

The ``docs`` extra needs an install of `graphviz`_. Don't forget to add
the ``--pre`` flag if you want the latest unstable build.

Contributing
------------

Build
~~~~~

Building *galactic-core-kernel* requires

* `hatch`_, which is a tool for dependency management and packaging in Python;
* `graphviz`_, which is a graph visualization software;
* `plantuml`_, which is a software able to produce schema from uml sources.
  This software is only useful for producing the documentation.

Build *galactic-core-kernel* using the bash command

.. code-block:: shell-session

    $ hatch build

Testing
~~~~~~~

Test *galactic-core-kernel* using the bash commands:

.. code-block:: shell-session

    $ hatch test

for running the tests.

.. code-block:: shell-session

    $ hatch test --cover

for running the tests with the coverage.

.. code-block:: shell-session

    $ hatch test --doctest-modules src

for running the `doctest`.

Linting
~~~~~~~

Lint *galactic-core-kernel* using the bash commands:

.. code-block:: shell-session

    $ hatch fmt --check

for running static linting.

.. code-block:: shell-session

    $ hatch fmt

for automatic fixing of static linting issues.

.. code-block:: shell-session

    $ hatch run lint:check

for running dynamic linting.

Documentation
~~~~~~~~~~~~~

Build the documentation using the bash command:

.. code-block:: shell-session

    $ hatch run docs:build

Getting Help
~~~~~~~~~~~~

.. important::

    If you have any difficulties with *galactic-core-kernel*, please feel welcome to
    `file an issue`_ on gitlab so that we can help.

.. _file an issue: https://gitlab.univ-lr.fr/galactic/public/src/core/galactic-core-kernel/-/issues
.. _python 3.10: http://www.python.org
.. _on Windows: https://www.python.org/downloads/windows
.. _hatch: https://hatch.pypa.io/
.. _graphviz: http://www.graphviz.org
.. _plantuml: https://plantuml.com
