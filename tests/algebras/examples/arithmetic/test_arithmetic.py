"""Arithmetic test module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import factors, gcd, lcm


class ArithmeticTestCase(TestCase):
    def test_lcm(self):
        self.assertEqual(lcm(6, 4), 12)
        self.assertEqual(lcm(), 1)
        self.assertEqual(lcm(0), 0)
        self.assertEqual(lcm(0, 6, 4), 0)
        self.assertEqual(lcm(6, 4, None), None)

    def test_gcd(self):
        self.assertEqual(gcd(6, 4), 2)
        self.assertEqual(gcd(), 0)
        self.assertEqual(gcd(0), 0)
        self.assertEqual(gcd(0, 6, 4), 2)
        self.assertEqual(gcd(0, 2, None), None)

    def test_factors(self):
        self.assertEqual(list(factors(12)), [2, 2, 3])
        self.assertEqual(list(factors(1)), [])
        self.assertEqual(list(factors(0)), [0])
