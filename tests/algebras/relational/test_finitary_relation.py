"""The :mod:`test_finitary_relation` module."""

import copy
from unittest import TestCase

from galactic.algebras.relational import (
    AbstractFiniteRelation,
    FiniteProjection,
    FiniteSelection,
    FrozenFiniteRelation,
    MutableFiniteRelation,
)


class RelationTestCase(TestCase):
    def test_is_instance(self):
        self.assertIsInstance(
            MutableFiniteRelation(
                [1, 2, 3],
                [2, 3, 4],
                [3, 4, 5],
            ),
            AbstractFiniteRelation,
        )
        self.assertIsInstance(
            FrozenFiniteRelation(
                [1, 2, 3],
                [2, 3, 4],
                [3, 4, 5],
            ),
            AbstractFiniteRelation,
        )

    def test___init__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
        )
        self.assertEqual(
            list(relation),
            [],
        )
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(
            list(relation),
            [(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(
            tuple(list(universe) for universe in relation.universes),
            ([1, 2, 3], [2, 3, 4], [3, 4, 5]),
        )
        with self.assertRaises(ValueError):
            _ = MutableFiniteRelation(
                [1, 2, 3],
                [2, 3, 4],
                [3, 4, 5],
                elements=[(1, 2, 3, 4)],
            )
        with self.assertRaises(ValueError):
            _ = MutableFiniteRelation(
                [1, 2, 3],
                [2, 3, 4],
                [3, 4, 5],
                elements=[(1, 5, 3)],
            )

    def test___eq__(self):
        relation1 = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )

        relation2 = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(
            relation1,
            relation2,
        )
        relation3 = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 5)],
        )
        self.assertNotEqual(
            relation1,
            relation3,
        )
        relation4 = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3)],
        )
        self.assertNotEqual(
            relation1,
            relation4,
        )
        relation5 = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            elements=[(1, 2)],
        )
        self.assertNotEqual(
            relation1,
            relation5,
        )

    def test___hash__(self):
        relation1 = FrozenFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation2 = FrozenFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(
            hash(relation1),
            hash(relation2),
        )

    def test___contains__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertIn((1, 2, 3), relation)
        self.assertNotIn({1}, relation)

    def test_universes(self):
        relation = FrozenFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(list(relation.universes[0]), [1, 2, 3])
        self.assertEqual(len(relation.universes[0]), 3)
        self.assertIn(1, relation.universes[0])
        self.assertNotIn({1}, relation.universes[0])

    def test_copy(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        clone = copy.copy(relation)
        self.assertEqual(list(relation), list(clone))
        self.assertEqual(list(relation), list(relation.copy()))
        self.assertEqual(
            list(clone),
            [(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(
            tuple(list(universe) for universe in clone.universes),
            ([1, 2, 3], [2, 3, 4], [3, 4, 5]),
        )

    def test_isdisjoint(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertFalse(relation.isdisjoint([(1, 2, 3), (2, 3, 4)]))

    def test_issubset(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertTrue(relation.issubset([(1, 2, 3), (2, 3, 4), (1, 2, 5)]))

    def test_issuperset(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertTrue(relation.issuperset([(1, 2, 3)]))

    def test_union(self):
        result = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        ).union([(3, 4, 5)])
        self.assertEqual(list(result), [(1, 2, 3), (2, 3, 4), (3, 4, 5)])

    def test_intersection(self):
        result = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        ).intersection([(1, 2, 3), (2, 3, 5)])
        self.assertEqual(list(result), [(1, 2, 3)])

    def test_difference(self):
        result = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        ).difference([(1, 2, 3), (2, 3, 5)])
        self.assertEqual(list(result), [(2, 3, 4)])

    def test_symmetric_difference(self):
        result = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        ).symmetric_difference([(1, 2, 3), (2, 3, 5)])
        self.assertEqual(list(result), [(2, 3, 4), (2, 3, 5)])

    def test_add(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.add((3, 3, 5))
        self.assertEqual(list(relation), [(1, 2, 3), (2, 3, 4), (3, 3, 5)])
        with self.assertRaises(ValueError):
            relation.add((4, 2, 3))

    def test_remove(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.remove((1, 2, 3))
        self.assertEqual(
            list(relation),
            [(2, 3, 4)],
        )

    def test_discard(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.discard((3, 3, 5))
        self.assertEqual(list(relation), [(1, 2, 3), (2, 3, 4)])
        relation.discard((1, 2, 3))
        self.assertEqual(list(relation), [(2, 3, 4)])

    def test_pop(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(
            relation.pop(),
            (1, 2, 3),
        )
        self.assertEqual(list(relation), [(2, 3, 4)])

    def test_clear(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.clear()
        self.assertEqual(list(relation), [])

    def test_extend(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.extend([(2, 3, 5)])
        self.assertEqual(
            list(relation),
            [(1, 2, 3), (2, 3, 4), (2, 3, 5)],
        )

    def test_update(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.update([(2, 3, 5)])
        self.assertEqual(
            list(relation),
            [(1, 2, 3), (2, 3, 4), (2, 3, 5)],
        )

    def test_intersection_update(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.intersection_update([(1, 2, 3), (2, 3, 5)])
        self.assertEqual(list(relation), [(1, 2, 3)])

    def test_difference_update(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.difference_update([(1, 2, 3), (2, 3, 5)])
        self.assertEqual(list(relation), [(2, 3, 4)])

    def test_symmetric_difference_update(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.symmetric_difference_update([(1, 2, 3), (2, 3, 5)])
        self.assertEqual(list(relation), [(2, 3, 4), (2, 3, 5)])

    def test_universe_add(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.universes[0].add(4)
        self.assertEqual(list(relation), [(1, 2, 3), (2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [1, 2, 3, 4])

    def test_universe_remove(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.universes[0].remove(3)
        self.assertEqual(list(relation), [(1, 2, 3), (2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [1, 2])
        relation.universes[0].remove(1)
        self.assertEqual(list(relation), [(2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [2])
        with self.assertRaises(KeyError):
            relation.universes[0].remove(1)

    def test_universe_discard(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.universes[0].discard(3)
        self.assertEqual(list(relation), [(1, 2, 3), (2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [1, 2])
        relation.universes[0].discard(1)
        self.assertEqual(list(relation), [(2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [2])
        relation.universes[0].discard(1)

    def test_universe_clear(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.universes[0].clear()
        self.assertEqual(list(relation), [])
        self.assertEqual(list(relation.universes[0]), [])

    def test_universe_pop(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        self.assertEqual(relation.universes[0].pop(), 1)
        self.assertEqual(list(relation), [(2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [2, 3])
        relation.universes[0].pop()
        relation.universes[0].pop()
        self.assertEqual(list(relation.universes[0]), [])
        with self.assertRaises(KeyError):
            relation.universes[0].pop()

    def test_universe_extend(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        relation.universes[0].extend([4, 5])
        self.assertEqual(list(relation), [(1, 2, 3), (2, 3, 4)])
        self.assertEqual(list(relation.universes[0]), [1, 2, 3, 4, 5])


class FiniteSelectionTestCase(TestCase):
    def test_is_instance(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4), (1, 3, 4)],
        )
        self.assertIsInstance(
            FiniteSelection(relation, {0: 1}),
            AbstractFiniteRelation,
        )

    def test___init__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4), (1, 3, 4)],
        )
        self.assertTrue(FiniteSelection(relation, {0: 1}))
        self.assertEqual(
            list(FiniteSelection(relation, {0: 1})),
            [(1, 2, 3), (1, 3, 4)],
        )
        self.assertEqual(list(FiniteSelection(relation, {0: 1, 2: 3})), [(1, 2, 3)])

    def test___eq__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4), (1, 3, 4)],
        )
        selection = FiniteSelection(relation, {0: 1})
        self.assertEqual(selection, selection)
        self.assertEqual(selection, FiniteSelection(relation, {0: 1}))
        self.assertEqual(list(selection), [(1, 2, 3), (1, 3, 4)])
        self.assertNotEqual(selection, {1})

    def test___contains__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        selection = FiniteSelection(relation, {0: 1})
        self.assertIn((1, 2, 3), selection)
        self.assertNotIn((2, 3, 4), selection)
        self.assertNotIn((3, 4, 5), selection)

    def test_universes(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        selection = FiniteSelection(relation, {0: 1})
        self.assertEqual(
            [list(universe) for universe in selection.universes],
            [[1, 2, 3], [2, 3, 4], [3, 4, 5]],
        )


class FiniteProjectionTestCase(TestCase):
    def test_is_instance(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4), (1, 3, 4)],
        )
        self.assertIsInstance(
            FiniteProjection(relation, [0, 1]),
            AbstractFiniteRelation,
        )

    def test___init__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4), (1, 3, 4)],
        )
        self.assertTrue(FiniteProjection(relation, [0, 1]))
        self.assertEqual(
            list(FiniteProjection(relation, [0, 1])),
            [(1, 2), (2, 3), (1, 3)],
        )
        self.assertEqual(list(FiniteProjection(relation, [2])), [(3,), (4,)])

    def test___eq__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4), (1, 3, 4)],
        )
        projection = FiniteProjection(relation, [0, 1])
        self.assertEqual(projection, projection)
        self.assertEqual(projection, FiniteProjection(relation, [0, 1]))
        self.assertEqual(list(projection), [(1, 2), (2, 3), (1, 3)])
        self.assertNotEqual(projection, {1})

    def test___contains__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        projection = FiniteProjection(relation, [0, 1])
        self.assertIn((1, 2), projection)
        self.assertNotIn((2, 4), projection)

    def test__len__(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        projection = FiniteProjection(relation, [2])
        self.assertEqual(len(projection), 2)

    def test_universes(self):
        relation = MutableFiniteRelation(
            [1, 2, 3],
            [2, 3, 4],
            [3, 4, 5],
            elements=[(1, 2, 3), (2, 3, 4)],
        )
        projection = FiniteProjection(relation, [0, 1])
        self.assertEqual(
            [list(universe) for universe in projection.universes],
            [[1, 2, 3], [2, 3, 4]],
        )
