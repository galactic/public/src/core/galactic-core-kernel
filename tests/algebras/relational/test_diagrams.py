"""The :mod:`test_diagrams` module."""

from contextlib import suppress
from unittest import TestCase

from galactic.algebras.relational import (
    MutableFiniteBinaryRelation,
)
from galactic.algebras.relational.renderer import (
    SagittalDiagram,
)


class SagittalDiagramTestCase(TestCase):
    def test_course(self):
        with suppress(ModuleNotFoundError):
            relation = MutableFiniteBinaryRelation[int, int](
                domain=[1, 2, 3],
                co_domain=[2, 3, 4],
                elements=[(1, 2), (1, 3), (2, 3)],
            )
            diagram = SagittalDiagram(relation)
            self.assertEqual(
                diagram.source.strip().replace("\t", "    "),
                """
digraph {
    graph [rankdir=LR]
    {
        graph [rank=same]
        D0_0 [label=1 shape=circle]
        D0_1 [label=2 shape=circle]
        D0_2 [label=3 shape=circle]
    }
    {
        graph [rank=same]
        D1_0 [label=2 shape=circle]
        D1_1 [label=3 shape=circle]
        D1_2 [label=4 shape=circle]
    }
    D0_0 -> D1_0
    D0_0 -> D1_1
    D0_1 -> D1_1
}            """.strip(),
            )
