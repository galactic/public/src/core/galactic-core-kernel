"""The :mod:`test_binary_relation` module."""

import copy
from unittest import TestCase

from galactic.algebras.relational import (
    AbstractBinaryRelation,
    AbstractFiniteBinaryRelation,
    AbstractFiniteRelation,
    AbstractLeftFiniteBinaryRelation,
    AbstractRelation,
    AbstractRightFiniteBinaryRelation,
    FrozenFiniteBinaryRelation,
    FrozenFiniteEndoRelation,
    MutableFiniteBinaryRelation,
    MutableFiniteEndoRelation,
)


class BinaryRelationTestCase(TestCase):
    def test_is_instance(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertIsInstance(
            relation,
            AbstractRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractLeftFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractRightFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteBinaryRelation,
        )
        relation = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertIsInstance(
            relation,
            AbstractRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractLeftFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractRightFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteBinaryRelation,
        )
        relation = MutableFiniteEndoRelation[int](
            domain=[1, 2, 3],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertIsInstance(
            relation,
            AbstractRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractLeftFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractRightFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteBinaryRelation,
        )
        relation = FrozenFiniteEndoRelation[int](
            domain=[1, 2, 3],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertIsInstance(
            relation,
            AbstractRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractLeftFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractRightFiniteBinaryRelation,
        )
        self.assertIsInstance(
            relation,
            AbstractFiniteBinaryRelation,
        )

    def test___init__(self):
        with self.assertRaises(TypeError):
            _ = MutableFiniteBinaryRelation[int, int](1, 2)
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertTrue(relation)
        self.assertEqual(
            list(relation),
            [(1, 2), (1, 3), (2, 3)],
        )
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2],
            co_domain=[2, 3],
        )
        self.assertEqual(list(relation.domain), [1, 2])
        self.assertEqual(list(relation.co_domain), [2, 3])

    def test___hash__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(
            hash(relation1),
            hash(relation2),
        )

    def test__check(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3, 4],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        self.assertNotEqual(relation1, relation2)
        relation3 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 4],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        self.assertNotEqual(relation1, relation3)

    def test___eq__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(
            relation1,
            relation2,
        )
        other = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (2, 3)],
        )
        self.assertNotEqual(
            relation1,
            other,
        )

    def test___lt__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertLess(
            relation1,
            relation2,
        )
        relation3 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (2, 3)],
        )
        self.assertFalse(
            relation1 < relation3,
        )

    def test___le__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertLessEqual(
            relation1,
            relation1,
        )
        self.assertLessEqual(
            relation1,
            relation2,
        )
        relation3 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (2, 3)],
        )
        self.assertFalse(
            relation1 <= relation3,
        )
        relation4 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2)],
        )
        self.assertFalse(
            relation1 <= relation4,
        )

    def test___gt__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        self.assertGreater(
            relation1,
            relation2,
        )
        other = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 4)],
        )
        self.assertFalse(
            relation1 > other,
        )

    def test___ge__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        self.assertGreaterEqual(
            relation1,
            relation1,
        )
        self.assertGreaterEqual(
            relation1,
            relation2,
        )
        relation3 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 4)],
        )
        self.assertFalse(
            relation1 >= relation3,
        )
        relation4 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3), (2, 4)],
        )
        self.assertFalse(
            relation1 >= relation4,
        )

    def test___or__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        self.assertEqual(
            list(relation1 | relation2),
            [(1, 2), (1, 3), (2, 3)],
        )

    def test___ior__(self):
        relation1 = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        relation1 |= relation2
        self.assertEqual(
            list(relation1),
            [(1, 2), (1, 3), (2, 3)],
        )

    def test___and__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        self.assertEqual(
            list(relation1 & relation2),
            [(1, 3)],
        )

    def test___iand__(self):
        relation1 = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        relation1 &= relation2
        self.assertEqual(
            list(relation1),
            [(1, 3)],
        )

    def test___sub__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        self.assertEqual(
            list(relation1 - relation2),
            [(1, 2)],
        )

    def test___isub__(self):
        relation1 = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        relation1 -= relation2
        self.assertEqual(
            list(relation1),
            [(1, 2)],
        )

    def test___xor__(self):
        relation1 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        self.assertEqual(
            list(relation1 ^ relation2),
            [(1, 2), (2, 3)],
        )

    def test___ixor__(self):
        relation1 = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3)],
        )
        relation2 = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 3), (2, 3)],
        )
        relation1 ^= relation2
        self.assertEqual(
            list(relation1),
            [(1, 2), (2, 3)],
        )

    def test___contains__(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertIn((1, 2), relation)
        self.assertNotIn((1, 2, 3), relation)
        self.assertNotIn(({1, 2}, 2), relation)

    def test_universes(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        # noinspection PyTypeChecker
        self.assertEqual(
            tuple(list(universe) for universe in relation.universes),
            ([1, 2, 3], [2, 3, 4]),
        )

    def test_domain(self):
        relation = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(list(relation.domain), [1, 2, 3])
        self.assertIn(1, relation.domain)
        self.assertNotIn(4, relation.domain)
        self.assertNotIn({1}, relation.domain)

        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(list(relation.domain), [1, 2, 3])
        self.assertIn(1, relation.domain)
        self.assertNotIn(4, relation.domain)
        self.assertNotIn({1}, relation.domain)

    def test_domain_add(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.domain.add(4)
        self.assertEqual(list(relation.domain), [1, 2, 3, 4])
        self.assertIn(1, relation.domain)
        self.assertIn(4, relation.domain)
        self.assertNotIn(5, relation.domain)
        self.assertNotIn({1}, relation.domain)

    def test_domain_remove(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.domain.remove(1)
        self.assertEqual(list(relation.domain), [2, 3])
        self.assertEqual(list(relation), [(2, 3)])
        with self.assertRaises(KeyError):
            relation.domain.remove(1)
        relation.domain.discard(1)

    def test_domain_pop(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(relation.domain.pop(), 1)
        self.assertEqual(list(relation.domain), [2, 3])
        self.assertEqual(list(relation), [(2, 3)])
        relation.domain.pop()
        relation.domain.pop()
        with self.assertRaises(KeyError):
            relation.domain.pop()

    def test_domain_clear(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.domain.clear()
        self.assertEqual(list(relation.domain), [])
        self.assertEqual(list(relation), [])

    def test_domain_extend(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.domain.extend([4, 5])
        self.assertEqual(list(relation.domain), [1, 2, 3, 4, 5])
        self.assertEqual(list(relation), [(1, 2), (1, 3), (2, 3)])

    def test_co_domain(self):
        relation = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(list(relation.co_domain), [2, 3, 4])
        self.assertIn(2, relation.co_domain)
        self.assertNotIn(1, relation.co_domain)
        self.assertNotIn({2}, relation.co_domain)

        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(list(relation.co_domain), [2, 3, 4])
        self.assertIn(2, relation.co_domain)
        self.assertNotIn(1, relation.co_domain)
        self.assertNotIn({2}, relation.co_domain)

    def test_co_domain_add(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.co_domain.add(5)
        self.assertEqual(list(relation.co_domain), [2, 3, 4, 5])
        self.assertIn(2, relation.co_domain)
        self.assertIn(5, relation.co_domain)
        self.assertNotIn(6, relation.co_domain)
        self.assertNotIn({1}, relation.co_domain)

    def test_co_domain_remove(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.co_domain.remove(2)
        self.assertEqual(list(relation.co_domain), [3, 4])
        self.assertEqual(list(relation), [(1, 3), (2, 3)])
        with self.assertRaises(KeyError):
            relation.co_domain.remove(1)
        relation.co_domain.discard(1)

    def test_co_domain_pop(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(relation.co_domain.pop(), 2)
        self.assertEqual(list(relation.co_domain), [3, 4])
        self.assertEqual(list(relation), [(1, 3), (2, 3)])
        relation.co_domain.pop()
        relation.co_domain.pop()
        with self.assertRaises(KeyError):
            relation.co_domain.pop()

    def test_co_domain_clear(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.co_domain.clear()
        self.assertEqual(list(relation.co_domain), [])
        self.assertEqual(list(relation), [])

    def test_co_domain_extend(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.co_domain.extend([4, 5])
        self.assertEqual(list(relation.co_domain), [2, 3, 4, 5])
        self.assertEqual(list(relation), [(1, 2), (1, 3), (2, 3)])

    def test_successors(self):
        relation = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        with self.assertRaises(ValueError):
            _ = relation.successors(0)
        successors = relation.successors(1)
        self.assertEqual(list(successors), [2, 3])
        self.assertIn(2, successors)
        self.assertNotIn(4, successors)
        self.assertNotIn({1}, successors)

        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        with self.assertRaises(ValueError):
            _ = relation.successors(0)
        successors = relation.successors(1)
        self.assertEqual(list(successors), [2, 3])
        self.assertIn(2, successors)
        self.assertNotIn(4, successors)
        self.assertNotIn({1}, successors)
        relation.domain.remove(1)
        with self.assertRaises(RuntimeError):
            repr(successors)

    def test_successors_add(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        successors = relation.successors(1)
        successors.add(4)
        self.assertEqual(list(successors), [2, 3, 4])
        self.assertEqual(list(relation), [(1, 2), (1, 3), (1, 4), (2, 3)])
        with self.assertRaises(KeyError):
            successors.add(5)

    def test_successors_remove(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        successors = relation.successors(1)
        successors.remove(2)
        self.assertEqual(list(successors), [3])
        self.assertEqual(list(relation), [(1, 3), (2, 3)])
        with self.assertRaises(KeyError):
            successors.remove(5)
        successors.discard(5)

    def test_successors_pop(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        successors = relation.successors(1)
        self.assertEqual(successors.pop(), 2)
        self.assertEqual(list(successors), [3])
        self.assertEqual(list(relation), [(1, 3), (2, 3)])
        successors.pop()
        with self.assertRaises(KeyError):
            successors.pop()

    def test_successors_clear(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        successors = relation.successors(1)
        successors.clear()
        self.assertEqual(list(successors), [])
        self.assertEqual(list(relation), [(2, 3)])

    def test_successors_extend(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        successors = relation.successors(1)
        successors.extend([4])
        self.assertEqual(list(successors), [2, 3, 4])
        self.assertEqual(list(relation), [(1, 2), (1, 3), (1, 4), (2, 3)])

    def test_predecessors(self):
        relation = FrozenFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        with self.assertRaises(ValueError):
            _ = relation.predecessors(0)
        predecessors = relation.predecessors(3)
        self.assertEqual(list(predecessors), [1, 2])
        self.assertIn(1, predecessors)

        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        with self.assertRaises(ValueError):
            _ = relation.predecessors(0)
        predecessors = relation.predecessors(3)
        self.assertEqual(list(predecessors), [1, 2])
        self.assertIn(1, predecessors)

    def test_predecessors_add(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        predecessors = relation.predecessors(3)
        predecessors.add(3)
        self.assertEqual(list(predecessors), [1, 2, 3])
        self.assertEqual(list(relation), [(1, 2), (1, 3), (2, 3), (3, 3)])
        with self.assertRaises(KeyError):
            predecessors.add(5)

    def test_predecessors_remove(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        predecessors = relation.predecessors(3)
        predecessors.remove(2)
        self.assertEqual(list(predecessors), [1])
        self.assertEqual(list(relation), [(1, 2), (1, 3)])
        with self.assertRaises(KeyError):
            predecessors.remove(5)
        predecessors.discard(5)

    def test_predecessors_pop(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        predecessors = relation.predecessors(3)
        self.assertEqual(predecessors.pop(), 1)
        self.assertEqual(list(predecessors), [2])
        self.assertEqual(list(relation), [(1, 2), (2, 3)])
        predecessors.pop()
        with self.assertRaises(KeyError):
            predecessors.pop()

    def test_predecessors_clear(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        predecessors = relation.predecessors(3)
        predecessors.clear()
        self.assertEqual(list(predecessors), [])
        self.assertEqual(list(relation), [(1, 2)])

    def test_predecessors_extend(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        predecessors = relation.predecessors(3)
        predecessors.extend([3])
        self.assertEqual(list(predecessors), [1, 2, 3])
        self.assertEqual(list(relation), [(1, 2), (1, 3), (2, 3), (3, 3)])

    def test_copy(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertEqual(list(relation), list(copy.copy(relation)))
        self.assertEqual(list(relation), list(relation.copy()))

    def test_isdisjoint(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertFalse(relation.isdisjoint([(1, 2), (1, 3), (2, 3)]))

    def test_issubset(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertTrue(relation.issubset([(1, 2), (1, 3), (2, 3), (2, 5)]))

    def test_issuperset(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        self.assertTrue(
            relation.issuperset(
                [
                    (1, 2),
                    (1, 3),
                ],
            ),
        )

    def test_union(self):
        result = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        ).union([(3, 4)])
        self.assertEqual(list(result), [(1, 2), (1, 3), (2, 3), (3, 4)])

    def test_intersection(self):
        result = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        ).intersection([(1, 3), (2, 3), (3, 4)])
        self.assertEqual(list(result), [(1, 3), (2, 3)])

    def test_difference(self):
        result = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        ).difference([(1, 3), (2, 3), (3, 4)])
        self.assertEqual(list(result), [(1, 2)])

    def test_symmetric_difference(self):
        result = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        ).symmetric_difference([(1, 3), (2, 3), (3, 4)])
        self.assertEqual(list(result), [(1, 2), (3, 4)])

    def test_add(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.add((1, 4))
        self.assertEqual(
            list(relation),
            [(1, 2), (1, 3), (1, 4), (2, 3)],
        )
        self.assertEqual(list(relation.successors(1)), [2, 3, 4])
        self.assertEqual(list(relation.predecessors(4)), [1])

    def test_remove(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.remove((1, 3))
        self.assertEqual(
            list(relation),
            [(1, 2), (2, 3)],
        )
        self.assertEqual(list(relation.successors(1)), [2])
        self.assertEqual(list(relation.predecessors(3)), [2])

    def test_pop(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.pop()
        self.assertEqual(len(relation), 2)
        relation.pop()
        self.assertEqual(len(relation), 1)
        relation.pop()
        self.assertEqual(len(relation), 0)
        with self.assertRaises(StopIteration):
            relation.pop()

    def test_discard(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.discard((1, 3))
        self.assertEqual(
            list(relation),
            [(1, 2), (2, 3)],
        )
        self.assertEqual(list(relation.successors(1)), [2])
        self.assertEqual(list(relation.predecessors(3)), [2])

    def test_clear(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.clear()
        self.assertEqual(
            list(relation),
            [],
        )

    def test_update(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.update([(1, 4)])
        self.assertEqual(
            list(relation),
            [(1, 2), (1, 3), (1, 4), (2, 3)],
        )
        self.assertEqual(list(relation.successors(1)), [2, 3, 4])
        self.assertEqual(list(relation.predecessors(4)), [1])

    def test_intersection_update(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.intersection_update([(1, 3), (2, 3), (3, 4)])
        self.assertEqual(list(relation), [(1, 3), (2, 3)])

    def test_difference_update(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.difference_update([(1, 3), (2, 3), (3, 4)])
        self.assertEqual(list(relation), [(1, 2)])

    def test_symmetric_difference_update(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        relation.symmetric_difference_update([(1, 3), (2, 3), (3, 4)])
        self.assertEqual(list(relation), [(1, 2), (3, 4)])
