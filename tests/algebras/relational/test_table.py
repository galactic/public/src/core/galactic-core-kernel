"""The :mod:`test_table` module."""

from unittest import TestCase

from IPython.core.formatters import HTMLFormatter, MarkdownFormatter

from galactic.algebras.relational import (
    MutableFiniteBinaryRelation,
)
from galactic.algebras.relational.renderer import (
    BinaryTable,
)


class BinaryTableTestCase(TestCase):
    def test__repr_markdown_(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        table = BinaryTable(relation)
        self.assertEqual(
            MarkdownFormatter()(table),
            r"""
| |2|3|4|
|-:|-:|-:|-:|
|1| $\checkmark$| $\checkmark$|  |
|2|  | $\checkmark$|  |
|3|  |  |  |
            """.strip(),
        )

    def test__repr_html_(self):
        relation = MutableFiniteBinaryRelation[int, int](
            domain=[1, 2, 3],
            co_domain=[2, 3, 4],
            elements=[(1, 2), (1, 3), (2, 3)],
        )
        table = BinaryTable(relation)
        self.assertEqual(
            HTMLFormatter()(table),
            r"""
<table>
  <thead>
    <tr>
      <th align="right"></th>
      <th align="right">2</th>
      <th align="right">3</th>
      <th align="right">4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td align="right">1</td>
      <td align="right">$\checkmark$</td>
      <td align="right">$\checkmark$</td>
      <td align="right"></td>
    </tr>
    <tr>
      <td align="right">2</td>
      <td align="right"></td>
      <td align="right">$\checkmark$</td>
      <td align="right"></td>
    </tr>
    <tr>
      <td align="right">3</td>
      <td align="right"></td>
      <td align="right"></td>
      <td align="right"></td>
    </tr>
  </tbody>
</table>
            """.strip(),
        )
