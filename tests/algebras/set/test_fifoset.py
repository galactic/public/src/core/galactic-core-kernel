"""The :mod:`test_fifoset` module."""

import copy
from unittest import TestCase

from galactic.algebras.set import FIFOSet, FrozenFIFOSet


class FIFOTest(TestCase):
    def test___init__(self):
        with self.assertRaises(TypeError):
            _ = FIFOSet[int](1, 2)

    def test___hash__(self):
        a = FrozenFIFOSet[int]([1, 2, 3, 4])
        b = FrozenFIFOSet[int]([1, 3, 4, 2])
        self.assertEqual(hash(a), hash(b))

    def test___copy__(self):
        a = FrozenFIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a, copy.copy(a))

    def test___reversed__(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(list(reversed(a)), [4, 3, 2, 1])

    def test_move_to_end(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(list(a), [1, 2, 3, 4])
        a.move_to_end(1)
        self.assertEqual(list(a), [2, 3, 4, 1])
        a.move_to_end(1, False)
        self.assertEqual(list(a), [1, 2, 3, 4])

    def test_pop(self):
        with self.assertRaises(KeyError):
            _ = FIFOSet[int]().pop()

        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.pop(), 1)
        self.assertEqual(list(a), [2, 3, 4])

    def test_head(self):
        with self.assertRaises(KeyError):
            _ = FIFOSet[int]().head

        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.head, 1)

    def test_popitem(self):
        with self.assertRaises(KeyError):
            _ = FIFOSet[int]().popitem()

        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.popitem(), 1)
        self.assertEqual(list(a), [2, 3, 4])
        self.assertEqual(a.popitem(last=True), 4)
        self.assertEqual(list(a), [2, 3])

    def test_tail(self):
        with self.assertRaises(KeyError):
            _ = FIFOSet[int]().tail

        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.tail, 4)

    def test_clear(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        a.clear()
        self.assertEqual(list(a), [])

    def test_copy(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a, a.copy())

    def test_union(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.union([2, 3, 5], [3, 4, 6]), {1, 2, 3, 4, 5, 6})

    def test_intersection(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.intersection([2, 3, 5], [3, 4, 6]), {3})

    def test_difference(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.difference([2, 5], [4]), {1, 3})

    def test_symmetric_difference(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertEqual(a.symmetric_difference([2, 5]), {1, 3, 4, 5})

    def test_isdisjoint(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertTrue(a.isdisjoint([5, 6]))
        self.assertFalse(a.isdisjoint([1, 5]))

    def test_issubset(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertTrue(a.issubset([1, 2, 3, 4, 5, 6]))
        self.assertFalse(a.issubset([1, 2, 3, 5, 6]))

    def test_issuperset(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        self.assertTrue(a.issuperset([1, 2, 3]))
        self.assertFalse(a.issuperset([1, 2, 3, 5]))

    def test_update(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        a.update([3, 4, 5, 6], [6, 7, 8])
        self.assertEqual(a, {1, 2, 3, 4, 5, 6, 7, 8})

    def test_intersection_update(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        a.intersection_update([3, 4, 5, 6], [3, 6, 7, 8])
        self.assertEqual(a, {3})

    def test_difference_update(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        a.difference_update([3, 4, 5, 6], [2, 6, 7, 8])
        self.assertEqual(a, {1})

    def test_symmetric_difference_update(self):
        a = FIFOSet[int]([1, 2, 3, 4])
        a.symmetric_difference_update([3, 4, 5, 6])
        self.assertEqual(a, {1, 2, 5, 6})
