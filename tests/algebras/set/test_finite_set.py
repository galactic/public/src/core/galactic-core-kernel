"""The :mod:`test_finite_set` module."""

import copy
import itertools
from unittest import TestCase

from galactic.algebras.set import FiniteSubSet, FiniteUniverse, IntegerSet


class FiniteUniverseTest(TestCase):
    def test___init__(self):
        universe = FiniteUniverse[str]("")
        self.assertEqual(list(universe), [])

    def test___len__(self):
        universe = FiniteUniverse[str]("012")

        self.assertEqual(len(universe), 3)

    def test___iter__(self):
        universe = FiniteUniverse[str]("012")

        self.assertEqual(list(universe), ["0", "1", "2"])

    def test___reversed__(self):
        universe = FiniteUniverse[str]("012")

        self.assertEqual(list(reversed(universe)), ["2", "1", "0"])

    def test___contains__(self):
        universe = FiniteUniverse[str]("012")

        self.assertIn("0", universe)
        self.assertNotIn("3", universe)
        self.assertNotIn([1], universe)

    def test_index(self):
        universe = FiniteUniverse[str]("012")

        self.assertEqual(universe.index("1"), 1)

        with self.assertRaises(ValueError):
            _ = universe.index("3")
        with self.assertRaises(ValueError):
            _ = universe.index("0", 1)
        with self.assertRaises(ValueError):
            # noinspection PyArgumentEqualDefault
            _ = universe.index("1", 0, 0)
        with self.assertRaises(TypeError):
            _ = universe.index([1])

    def test_count(self):
        universe = FiniteUniverse[str]("012")

        self.assertEqual(universe.count("0"), 1)
        self.assertEqual(universe.count("3"), 0)

    def test_parts(self):
        universe = FiniteUniverse[str]("012")
        self.assertEqual(
            [list(part) for part in universe.parts()],
            [
                [],
                ["0"],
                ["1"],
                ["2"],
                ["0", "1"],
                ["0", "2"],
                ["1", "2"],
                ["0", "1", "2"],
            ],
        )

    def test_singletons(self):
        universe = FiniteUniverse[str]("012")
        self.assertEqual(
            [list(part) for part in universe.singletons()],
            [["0"], ["1"], ["2"]],
        )


class FiniteSubSetTest(TestCase):
    def test___init__(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, "013")
        self.assertEqual(list(subset), ["0", "1", "3"])
        subset = FiniteSubSet[str](universe, elements="0134679AD56")
        self.assertEqual(
            list(subset),
            ["0", "1", "3", "4", "5", "6", "7", "9", "A", "D"],
        )

    def test_from_intervals(self):
        universe = FiniteUniverse[str]("0123")
        subset = FiniteSubSet[str].from_intervals(universe, [(0, 2), (3, 4)])
        self.assertEqual(list(subset), ["0", "1", "3"])

        with self.assertRaises(ValueError):
            _ = FiniteSubSet[str].from_intervals(universe, [(-1, 2)])
        with self.assertRaises(ValueError):
            _ = FiniteSubSet[str].from_intervals(universe, [(1, 2), (0, 1)])
        with self.assertRaises(ValueError):
            _ = FiniteSubSet[str].from_intervals(universe, [(1, 1)])
        with self.assertRaises(ValueError):
            _ = FiniteSubSet[str].from_intervals(universe, [(1, 10)])

    def test___copy__(self):
        universe = FiniteUniverse[str]("0123")
        subset = FiniteSubSet[str].from_intervals(universe, [(0, 2), (3, 4)])
        self.assertEqual(subset, copy.copy(subset))
        self.assertEqual(subset, subset.copy())

    def test___len__(self):
        universe = FiniteUniverse[str]("0123")

        subset = FiniteSubSet[str](universe, elements="023")
        self.assertEqual(len(subset), 3)

    def test___contains__(self):
        universe = FiniteUniverse[str]("0123")

        subset = FiniteSubSet[str](universe, elements="023")
        self.assertIn("0", subset)
        self.assertNotIn("1", subset)
        self.assertNotIn("4", subset)

    def test___eq__(self):
        universe = FiniteUniverse[str]("0123")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="023")
        self.assertEqual(subset1, subset2)
        self.assertEqual(subset1, set("023"))

    def test___ne__(self):
        universe = FiniteUniverse[str]("0123")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="123")
        self.assertNotEqual(subset1, subset2)
        self.assertNotEqual(subset1, set("123"))

    def test___lt__(self):
        universe = FiniteUniverse[str]("0123")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="02")
        self.assertFalse(subset1 < subset2)
        self.assertLess(subset2, subset1)
        self.assertFalse(subset1 < set("023"))

    def test___le__(self):
        universe = FiniteUniverse[str]("0123456789")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="02")
        self.assertFalse(subset1 <= subset2)
        self.assertLessEqual(subset2, subset1)
        self.assertLessEqual(subset1, set("023"))

        subset3 = FiniteSubSet[str](universe, elements="3")
        self.assertFalse(subset1 <= subset3)
        self.assertLessEqual(subset3, subset1)
        self.assertFalse(subset2 <= subset3)
        self.assertFalse(subset3 <= subset2)

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                self.assertEqual(
                    subset1 <= subset2,
                    subset1 <= frozenset(iterable2),
                )

    def test_issubset(self):
        universe = FiniteUniverse[str]("0123456789")

        subset1 = FiniteSubSet[str](universe, elements="023")
        self.assertTrue(subset1.issubset(subset1))
        self.assertFalse(subset1.issubset("02"))
        self.assertTrue(subset1.issubset("0234"))

    def test___gt__(self):
        universe = FiniteUniverse[str]("0123")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="02")
        self.assertGreater(subset1, subset2)
        self.assertFalse(subset2 > subset1)
        self.assertFalse(subset1 > set("023"))

    def test___ge__(self):
        universe = FiniteUniverse[str]("0123456789")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="02")
        self.assertGreaterEqual(subset1, subset2)
        self.assertFalse(subset2 >= subset1)
        self.assertGreaterEqual(subset1, set("023"))

        subset3 = FiniteSubSet[str](universe, elements="3")
        self.assertGreaterEqual(subset1, subset3)
        self.assertFalse(subset3 >= subset1)
        self.assertFalse(subset2 >= subset3)
        self.assertFalse(subset3 >= subset2)

        subset1 = FiniteSubSet[str](universe, elements="012")
        subset2 = FiniteSubSet[str](universe, elements="12")
        self.assertGreaterEqual(subset1, subset2)
        self.assertFalse(subset2 >= subset1)
        self.assertGreaterEqual(subset1, set("12"))

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                self.assertEqual(
                    subset1 >= subset2,
                    subset1 >= frozenset(iterable2),
                )

    def test_issuperset(self):
        universe = FiniteUniverse[str]("0123456789")

        subset1 = FiniteSubSet[str](universe, elements="023")
        self.assertTrue(subset1.issuperset(subset1))
        self.assertTrue(subset1.issuperset("02"))
        self.assertFalse(subset1.issuperset("0234"))

    def test_isdisjoint(self):
        universe = FiniteUniverse[str]("0123456789")

        subset1 = FiniteSubSet[str](universe, elements="023")
        subset2 = FiniteSubSet[str](universe, elements="02")
        self.assertFalse(subset1.isdisjoint(subset2))
        self.assertFalse(subset2.isdisjoint(subset1))
        subset3 = FiniteSubSet[str](universe, elements="02")
        subset4 = FiniteSubSet[str](universe, elements="13")
        self.assertTrue(subset3.isdisjoint(subset4))
        self.assertTrue(subset4.isdisjoint(subset3))
        self.assertTrue(subset3.isdisjoint(set("13")))

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                self.assertEqual(
                    subset1.isdisjoint(subset2),
                    subset1.isdisjoint(frozenset(iterable2)),
                )

    def test___or__(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")

        subset1 = FiniteSubSet[str](universe, elements="0134679AD")
        subset2 = FiniteSubSet[str](universe, elements="2789CD")
        subset = subset1 | subset2
        self.assertEqual(list(subset), list("012346789ACD"))
        subset = subset2 | subset1
        self.assertEqual(list(subset), list("012346789ACD"))

        subset3 = FiniteSubSet[str](universe, elements="0268B")
        subset4 = FiniteSubSet[str](universe, elements="1479C")
        subset = subset3 | subset4
        self.assertEqual(list(subset), list("01246789BC"))
        subset = subset4 | subset3
        self.assertEqual(list(subset), list("01246789BC"))

        # noinspection PyTypeChecker
        subset = subset1 | "56"
        self.assertEqual(list(subset), list("01345679AD"))
        # noinspection PyUnresolvedReferences
        subset = "56" | subset1
        self.assertEqual(list(subset), list("01345679AD"))

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                expected = subset1 | iterable2
                result = subset1 | subset2
                self.assertEqual(list(result), list(expected))
                self.assertEqual(len(result), len(expected))
                self.assertEqual(hash(result), hash(expected))

    def test_union(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AD").union("2789CD")
        self.assertEqual(list(subset), list("012346789ACD"))

    def test___and__(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")

        subset1 = FiniteSubSet[str](universe, elements="0134679A")
        subset2 = FiniteSubSet[str](universe, elements="014679")
        subset = subset1 & subset2
        self.assertEqual(list(subset), list("014679"))

        subset3 = FiniteSubSet[str](universe, elements="01345679A")
        subset4 = FiniteSubSet[str](universe, elements="0145679A")
        subset = subset3 & subset4
        self.assertEqual(list(subset), list("0145679A"))
        subset = subset4 & subset3
        self.assertEqual(list(subset), list("0145679A"))

        subset5 = FiniteSubSet[str](universe, elements="0134679A")
        subset6 = FiniteSubSet[str](universe, elements="24579")
        subset = subset5 & subset6
        self.assertEqual(list(subset), list("479"))

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                expected = subset1 & iterable2
                result = subset1 & subset2
                self.assertEqual(list(result), list(expected))
                self.assertEqual(len(result), len(expected))
                self.assertEqual(hash(result), hash(expected))

    def test_intersection(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AD").intersection(
            "2789CD",
        )
        self.assertEqual(list(subset), list("79D"))

    def test___sub__(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")

        subset = FiniteSubSet[str](universe, elements="34679AD")

        subset1 = FiniteSubSet[str](universe, elements="")
        self.assertEqual(list(subset - subset1), list("34679AD"))
        self.assertEqual(list(subset1 - subset), list(""))

        subset2 = FiniteSubSet[str](universe, elements="01")
        self.assertEqual(list(subset - subset2), list("34679AD"))
        self.assertEqual(list(subset2 - subset), list("01"))

        subset3 = FiniteSubSet[str](universe, elements="0123")
        self.assertEqual(list(subset - subset3), list("4679AD"))
        self.assertEqual(list(subset3 - subset), list("012"))

        subset4 = FiniteSubSet[str](universe, elements="01234")
        self.assertEqual(list(subset - subset4), list("679AD"))
        self.assertEqual(list(subset4 - subset), list("012"))

        subset5 = FiniteSubSet[str](universe, elements="012345")
        self.assertEqual(list(subset - subset5), list("679AD"))
        self.assertEqual(list(subset5 - subset), list("0125"))

        subset6 = FiniteSubSet[str](universe, elements="012346")
        self.assertEqual(list(subset - subset6), list("79AD"))

        subset7 = FiniteSubSet[str](universe, elements="0123467")
        self.assertEqual(list(subset - subset7), list("9AD"))
        self.assertEqual(list(subset7 - subset), list("012"))

        subset8 = FiniteSubSet[str](universe, elements="01234678")
        self.assertEqual(list(subset - subset8), list("9AD"))
        self.assertEqual(list(subset8 - subset), list("0128"))

        subset9 = "56"
        # noinspection PyUnresolvedReferences,PyTypeChecker
        self.assertEqual(list(subset - subset9), list("3479AD"))
        # noinspection PyUnresolvedReferences
        self.assertEqual(list(subset9 - subset), list("5"))

        subset1 = FiniteSubSet[str](universe, elements="0134679AD")
        subset2 = FiniteSubSet[str](universe, elements="2789CD")
        self.assertEqual(list(subset1 - subset2), list("01346A"))
        self.assertEqual(list(subset2 - subset1), list("28C"))

        subset3 = FiniteSubSet[str](universe, elements="0268B")
        subset4 = FiniteSubSet[str](universe, elements="1479C")
        self.assertEqual(list(subset3 - subset4), list("0268B"))
        self.assertEqual(list(subset4 - subset3), list("1479C"))

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                expected = subset1 - iterable2
                result = subset1 - subset2
                self.assertEqual(list(result), list(expected))
                self.assertEqual(len(result), len(expected))
                self.assertEqual(hash(result), hash(expected))

    def test_difference(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AD").difference("2789CD")
        self.assertEqual(list(subset), list("01346A"))

    def test___xor__(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")

        subset = FiniteSubSet[str](universe, elements="34679AD")

        subset1 = FiniteSubSet[str](universe, elements="")
        self.assertEqual(list(subset ^ subset1), list("34679AD"))
        self.assertEqual(list(subset1 ^ subset), list("34679AD"))

        subset2 = FiniteSubSet[str](universe, elements="01")
        self.assertEqual(list(subset ^ subset2), list("0134679AD"))
        self.assertEqual(list(subset2 ^ subset), list("0134679AD"))

        subset1 = FiniteSubSet[str](universe, elements="0134679AD")

        # noinspection PyTypeChecker
        subset = subset1 ^ "56"
        self.assertEqual(list(subset), list("0134579AD"))
        # noinspection PyUnresolvedReferences
        subset = "56" ^ subset1
        self.assertEqual(list(subset), list("0134579AD"))

        self.assertEqual(
            list(
                FiniteSubSet[str](universe, elements="1")
                ^ FiniteSubSet[str](universe, elements="01"),
            ),
            list("0"),
        )

        self.assertEqual(
            list(
                FiniteSubSet[str](universe, elements="2")
                ^ FiniteSubSet[str](universe, elements="02"),
            ),
            list("0"),
        )

        self.assertEqual(
            list(
                FiniteSubSet[str](universe, elements="02")
                ^ FiniteSubSet[str](universe, elements="0"),
            ),
            list("2"),
        )

        elements = "01234567"

        for iterable1 in itertools.chain.from_iterable(
            itertools.combinations(elements, n) for n in range(len(elements) + 1)
        ):
            for iterable2 in itertools.chain.from_iterable(
                itertools.combinations(elements, n) for n in range(len(elements) + 1)
            ):
                subset1 = FiniteSubSet[str](universe, elements=iterable1)
                subset2 = FiniteSubSet[str](universe, elements=iterable2)
                expected = subset1 ^ iterable2
                result = subset1 ^ subset2
                self.assertEqual(list(result), list(expected))
                self.assertEqual(len(result), len(expected))
                self.assertEqual(hash(result), hash(expected))

    def test_symmetric_difference(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AD").symmetric_difference(
            "2789CD",
        )
        self.assertEqual(list(subset), list("0123468AC"))

    def test___invert__(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")

        subset = FiniteSubSet[str](universe, elements="0134679AF")
        self.assertEqual(list(~subset), list("258BCDE"))
        self.assertEqual(list(~~subset), list(subset))

        self.assertEqual(
            list(~FiniteSubSet[str](universe)),
            list(universe),
        )
        self.assertEqual(list(~~FiniteSubSet[str](universe)), [])

    def test_universe(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AF")
        self.assertIs(subset.universe, universe)

    def test_intervals(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AF")
        self.assertEqual(
            list(subset.intervals()),
            [(0, 2), (3, 5), (6, 8), (9, 11), (15, 16)],
        )

    def test_ranges(self):
        universe = FiniteUniverse[str]("0123456789ABCDEF")
        subset = FiniteSubSet[str](universe, elements="0134679AF")
        self.assertEqual(
            list(itertools.chain.from_iterable(subset.ranges())),
            [0, 1, 3, 4, 6, 7, 9, 10, 15],
        )

    def test_subsets(self):
        universe = FiniteUniverse[str]("01234")
        set1 = FiniteSubSet[str](universe, elements="01")
        self.assertEqual(
            [list(part) for part in set1.subsets()],
            [[], ["0"], ["1"], ["0", "1"]],
        )
        self.assertEqual(
            [list(part) for part in set1.subsets(True)],
            [[], ["0"], ["1"]],
        )

    def test_supersets(self):
        universe = FiniteUniverse[str]("01234")
        set1 = FiniteSubSet[str](universe, elements="01")
        self.assertEqual(
            [list(part) for part in set1.supersets()],
            [
                ["0", "1"],
                ["0", "1", "2"],
                ["0", "1", "3"],
                ["0", "1", "4"],
                ["0", "1", "2", "3"],
                ["0", "1", "2", "4"],
                ["0", "1", "3", "4"],
                ["0", "1", "2", "3", "4"],
            ],
        )
        self.assertEqual(
            [list(part) for part in set1.supersets(True)],
            [
                ["0", "1", "2"],
                ["0", "1", "3"],
                ["0", "1", "4"],
                ["0", "1", "2", "3"],
                ["0", "1", "2", "4"],
                ["0", "1", "3", "4"],
                ["0", "1", "2", "3", "4"],
            ],
        )


class IntegerSetTestCase(TestCase):
    def test___init__(self):
        subset = IntegerSet([(0, 2), (3, 4)])
        self.assertEqual(list(subset), [0, 1, 3])
        subset = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11)])
        self.assertEqual(list(subset), [0, 1, 3, 4, 6, 7, 9, 10])

        subset = IntegerSet([(0, (1 << 8) - 1)])
        self.assertEqual(list(subset.intervals()), [(0, (1 << 8) - 1)])

        subset = IntegerSet([(0, (1 << 16) - 1)])
        self.assertEqual(list(subset.intervals()), [(0, (1 << 16) - 1)])

        subset = IntegerSet([(0, (1 << 32) - 1)])
        self.assertEqual(list(subset.intervals()), [(0, (1 << 32) - 1)])

        subset = IntegerSet([(0, (1 << 64) - 1)])
        self.assertEqual(list(subset.intervals()), [(0, (1 << 64) - 1)])

        subset = IntegerSet([(0, 1 << 64)])
        self.assertEqual(list(subset.intervals()), [(0, 1 << 64)])

        subset = IntegerSet([(-1 << 7, (1 << 7) - 1)])
        self.assertEqual(list(subset.intervals()), [(-1 << 7, (1 << 7) - 1)])

        subset = IntegerSet([(-1 << 15, (1 << 15) - 1)])
        self.assertEqual(list(subset.intervals()), [(-1 << 15, (1 << 15) - 1)])

        subset = IntegerSet([(-1 << 31, (1 << 31) - 1)])
        self.assertEqual(list(subset.intervals()), [(-1 << 31, (1 << 31) - 1)])

        subset = IntegerSet([(-1 << 63, (1 << 63) - 1)])
        self.assertEqual(list(subset.intervals()), [(-1 << 63, (1 << 63) - 1)])

        subset = IntegerSet([(-1 << 64, 1 << 64)])
        self.assertEqual(list(subset.intervals()), [(-1 << 64, 1 << 64)])

    def test_from_iterable(self):
        subset = IntegerSet.from_iterable([0, 1, 3, 4, 6, 7, 9, 10])
        self.assertEqual(list(subset.intervals()), [(0, 2), (3, 5), (6, 8), (9, 11)])
        subset = IntegerSet.from_iterable()
        self.assertEqual(list(subset.intervals()), [])

    def test___len__(self):
        subset = IntegerSet([(0, 2), (3, 4)])
        self.assertEqual(len(subset), 3)

    def test___contains__(self):
        subset = IntegerSet([(0, 2), (3, 4)])
        self.assertIn(0, subset)
        self.assertNotIn(4, subset)
        self.assertNotIn("dummy", subset)

    def test___eq__(self):
        subset1 = IntegerSet([(0, 2), (3, 4)])
        subset2 = IntegerSet([(0, 2), (3, 4)])
        self.assertEqual(subset1, subset2)
        self.assertEqual(subset1, {0, 1, 3})

    def test___ne__(self):
        subset1 = IntegerSet([(0, 2), (3, 4)])
        subset2 = IntegerSet([(0, 2), (3, 5)])
        self.assertNotEqual(subset1, subset2)
        self.assertNotEqual(subset1, {0, 1, 3, 4})

    def test___lt__(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertFalse(subset1 < subset2)
        self.assertLess(subset2, subset1)
        self.assertFalse(subset1 < {0, 2, 3})

    def test___le__(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertFalse(subset1 <= subset2)
        self.assertLessEqual(subset2, subset1)
        self.assertLessEqual(subset1, {0, 2, 3})

    def test_issubset(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertFalse(subset1.issubset(subset2))
        self.assertTrue(subset2.issubset(subset1))
        self.assertTrue(subset1.issubset([0, 2, 3]))

    def test___gt__(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertGreater(subset1, subset2)
        self.assertFalse(subset2 > subset1)
        self.assertFalse(subset1 > {0, 2, 3})

    def test___ge__(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertGreaterEqual(subset1, subset2)
        self.assertFalse(subset2 >= subset1)
        self.assertGreaterEqual(subset1, {0, 2, 3})

    def test_issuperset(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertTrue(subset1.issuperset(subset2))
        self.assertFalse(subset2.issuperset(subset1))
        self.assertTrue(subset1.issuperset([0, 2, 3]))

    def test_isdisjoint(self):
        subset1 = IntegerSet([(0, 1), (2, 4)])
        subset2 = IntegerSet([(0, 1), (2, 3)])
        self.assertFalse(subset1.isdisjoint(subset2))
        self.assertTrue(subset1.isdisjoint(set()))

    def test___or__(self):
        subset1 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11), (13, 14)])
        subset2 = IntegerSet([(2, 3), (7, 10), (12, 14)])
        subset = subset1 | subset2
        self.assertEqual(list(subset), [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13])
        subset = subset2 | subset1
        self.assertEqual(list(subset), [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13])

        subset3 = IntegerSet([(0, 1), (2, 3), (6, 7), (8, 9), (11, 12)])
        subset4 = IntegerSet([(1, 2), (4, 5), (7, 8), (9, 10), (12, 13)])
        subset = subset3 | subset4
        self.assertEqual(list(subset), [0, 1, 2, 4, 6, 7, 8, 9, 11, 12])
        subset = subset4 | subset3
        self.assertEqual(list(subset), [0, 1, 2, 4, 6, 7, 8, 9, 11, 12])

    def test_union(self):
        subset1 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11), (13, 14)])
        subset2 = IntegerSet([(2, 3), (7, 10), (12, 14)])
        subset = subset1.union(subset2)
        self.assertEqual(list(subset), [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13])
        subset = subset1.union(list(subset2))
        self.assertEqual(list(subset), [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13])
        subset = subset2.union(subset1)
        self.assertEqual(list(subset), [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13])
        subset = subset2.union(list(subset1))
        self.assertEqual(list(subset), [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13])

        subset3 = IntegerSet([(0, 1), (2, 3), (6, 7), (8, 9), (11, 12)])
        subset4 = IntegerSet([(1, 2), (4, 5), (7, 8), (9, 10), (12, 13)])
        subset = subset3.union(subset4)
        self.assertEqual(list(subset), [0, 1, 2, 4, 6, 7, 8, 9, 11, 12])
        subset = subset3.union(list(subset4))
        self.assertEqual(list(subset), [0, 1, 2, 4, 6, 7, 8, 9, 11, 12])
        subset = subset4.union(subset3)
        self.assertEqual(list(subset), [0, 1, 2, 4, 6, 7, 8, 9, 11, 12])
        subset = subset4.union(list(subset3))
        self.assertEqual(list(subset), [0, 1, 2, 4, 6, 7, 8, 9, 11, 12])

    def test___and__(self):
        subset1 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11)])
        subset2 = IntegerSet([(0, 2), (4, 5), (6, 8), (9, 10)])
        subset = subset1 & subset2
        self.assertEqual(list(subset), [0, 1, 4, 6, 7, 9])

        subset3 = IntegerSet([(0, 2), (3, 8), (9, 11)])
        subset4 = IntegerSet([(0, 2), (4, 8), (9, 11)])
        subset = subset3 & subset4
        self.assertEqual(list(subset), [0, 1, 4, 5, 6, 7, 9, 10])
        subset = subset4 & subset3
        self.assertEqual(list(subset), [0, 1, 4, 5, 6, 7, 9, 10])

        subset5 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11)])
        subset6 = IntegerSet([(2, 3), (4, 6), (7, 8), (9, 10)])
        subset = subset5 & subset6
        self.assertEqual(list(subset), [4, 7, 9])

    def test_intersection(self):
        subset1 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11)])
        subset2 = IntegerSet([(0, 2), (4, 5), (6, 8), (9, 10)])
        subset = subset1.intersection(subset2)
        self.assertEqual(list(subset), [0, 1, 4, 6, 7, 9])
        subset = subset1.intersection(list(subset2))
        self.assertEqual(list(subset), [0, 1, 4, 6, 7, 9])

        subset3 = IntegerSet([(0, 2), (3, 8), (9, 11)])
        subset4 = IntegerSet([(0, 2), (4, 8), (9, 11)])
        subset = subset3.intersection(subset4)
        self.assertEqual(list(subset), [0, 1, 4, 5, 6, 7, 9, 10])
        subset = subset3.intersection(list(subset4))
        self.assertEqual(list(subset), [0, 1, 4, 5, 6, 7, 9, 10])
        subset = subset4.intersection(subset3)
        self.assertEqual(list(subset), [0, 1, 4, 5, 6, 7, 9, 10])
        subset = subset4.intersection(list(subset3))
        self.assertEqual(list(subset), [0, 1, 4, 5, 6, 7, 9, 10])

        subset5 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11)])
        subset6 = IntegerSet([(2, 3), (4, 6), (7, 8), (9, 10)])
        subset = subset5.intersection(subset6)
        self.assertEqual(list(subset), [4, 7, 9])
        subset = subset5.intersection(list(subset6))
        self.assertEqual(list(subset), [4, 7, 9])

    def test___sub__(self):
        subset = IntegerSet([(3, 5), (6, 8), (9, 11), (13, 14)])

        subset1 = IntegerSet()
        self.assertEqual(list(subset - subset1), [3, 4, 6, 7, 9, 10, 13])
        self.assertEqual(list(subset1 - subset), [])

        subset2 = IntegerSet([(0, 2)])
        self.assertEqual(list(subset - subset2), [3, 4, 6, 7, 9, 10, 13])
        self.assertEqual(list(subset2 - subset), [0, 1])

        subset3 = IntegerSet([(0, 4)])
        self.assertEqual(list(subset - subset3), [4, 6, 7, 9, 10, 13])
        self.assertEqual(list(subset3 - subset), [0, 1, 2])

        subset4 = IntegerSet([(0, 5)])
        self.assertEqual(list(subset - subset4), [6, 7, 9, 10, 13])
        self.assertEqual(list(subset4 - subset), [0, 1, 2])

        subset5 = IntegerSet([(0, 6)])
        self.assertEqual(list(subset - subset5), [6, 7, 9, 10, 13])
        self.assertEqual(list(subset5 - subset), [0, 1, 2, 5])

        subset6 = IntegerSet([(0, 5), (6, 7)])
        self.assertEqual(list(subset - subset6), [7, 9, 10, 13])

        subset7 = IntegerSet([(0, 5), (6, 8)])
        self.assertEqual(list(subset - subset7), [9, 10, 13])
        self.assertEqual(list(subset7 - subset), [0, 1, 2])

        subset8 = IntegerSet([(0, 5), (6, 9)])
        self.assertEqual(list(subset - subset8), [9, 10, 13])
        self.assertEqual(list(subset8 - subset), [0, 1, 2, 8])

        subset1 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11), (13, 14)])
        subset2 = IntegerSet([(2, 3), (7, 10), (12, 14)])
        self.assertEqual(list(subset1 - subset2), [0, 1, 3, 4, 6, 10])
        self.assertEqual(list(subset2 - subset1), [2, 8, 12])

        subset3 = IntegerSet([(0, 1), (2, 3), (6, 7), (8, 9), (11, 12)])
        subset4 = IntegerSet([(1, 2), (4, 5), (7, 8), (9, 10), (12, 13)])
        self.assertEqual(list(subset3 - subset4), [0, 2, 6, 8, 11])
        self.assertEqual(list(subset4 - subset3), [1, 4, 7, 9, 12])

    def test_difference(self):
        subset = IntegerSet([(3, 5), (6, 8), (9, 11), (13, 14)])

        subset1 = IntegerSet()
        self.assertEqual(list(subset.difference(subset1)), [3, 4, 6, 7, 9, 10, 13])
        self.assertEqual(
            list(subset.difference(list(subset1))),
            [3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(list(subset1.difference(subset)), [])
        self.assertEqual(list(subset1.difference(list(subset))), [])

        subset2 = IntegerSet([(0, 2)])
        self.assertEqual(
            list(subset.difference(subset2)),
            [3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset2))),
            [3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(list(subset2.difference(subset)), [0, 1])
        self.assertEqual(list(subset2.difference(list(subset))), [0, 1])

        subset3 = IntegerSet([(0, 4)])
        self.assertEqual(
            list(subset.difference(subset3)),
            [4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset3))),
            [4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(list(subset3.difference(subset)), [0, 1, 2])
        self.assertEqual(list(subset3.difference(list(subset))), [0, 1, 2])

        subset4 = IntegerSet([(0, 5)])
        self.assertEqual(
            list(subset.difference(subset4)),
            [6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset4))),
            [6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset4.difference(subset)),
            [0, 1, 2],
        )
        self.assertEqual(
            list(subset4.difference(list(subset))),
            [0, 1, 2],
        )

        subset5 = IntegerSet([(0, 6)])
        self.assertEqual(
            list(subset.difference(subset5)),
            [6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset5))),
            [6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset5.difference(subset)),
            [0, 1, 2, 5],
        )
        self.assertEqual(
            list(subset5.difference(list(subset))),
            [0, 1, 2, 5],
        )

        subset6 = IntegerSet([(0, 5), (6, 7)])
        self.assertEqual(
            list(subset.difference(subset6)),
            [7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset6))),
            [7, 9, 10, 13],
        )

        subset7 = IntegerSet([(0, 5), (6, 8)])
        self.assertEqual(
            list(subset.difference(subset7)),
            [9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset7))),
            [9, 10, 13],
        )
        self.assertEqual(
            list(subset7.difference(subset)),
            [0, 1, 2],
        )
        self.assertEqual(
            list(subset7.difference(list(subset))),
            [0, 1, 2],
        )

        subset8 = IntegerSet([(0, 5), (6, 9)])
        self.assertEqual(
            list(subset.difference(subset8)),
            [9, 10, 13],
        )
        self.assertEqual(
            list(subset.difference(list(subset8))),
            [9, 10, 13],
        )
        self.assertEqual(
            list(subset8.difference(subset)),
            [0, 1, 2, 8],
        )
        self.assertEqual(
            list(subset8.difference(list(subset))),
            [0, 1, 2, 8],
        )

        subset9 = [5, 6]
        self.assertEqual(
            list(subset.difference(subset9)),
            [3, 4, 7, 9, 10, 13],
        )

        subset1 = IntegerSet([(0, 2), (3, 5), (6, 8), (9, 11), (13, 14)])
        subset2 = IntegerSet([(2, 3), (7, 10), (12, 14)])
        self.assertEqual(
            list(subset1.difference(subset2)),
            [0, 1, 3, 4, 6, 10],
        )
        self.assertEqual(
            list(subset1.difference(list(subset2))),
            [0, 1, 3, 4, 6, 10],
        )
        self.assertEqual(
            list(subset2.difference(subset1)),
            [2, 8, 12],
        )
        self.assertEqual(
            list(subset2.difference(list(subset1))),
            [2, 8, 12],
        )

        subset3 = IntegerSet([(0, 1), (2, 3), (6, 7), (8, 9), (11, 12)])
        subset4 = IntegerSet([(1, 2), (4, 5), (7, 8), (9, 10), (12, 13)])
        self.assertEqual(
            list(subset3.difference(subset4)),
            [0, 2, 6, 8, 11],
        )
        self.assertEqual(
            list(subset3.difference(list(subset4))),
            [0, 2, 6, 8, 11],
        )
        self.assertEqual(
            list(subset4.difference(subset3)),
            [1, 4, 7, 9, 12],
        )
        self.assertEqual(
            list(subset4.difference(list(subset3))),
            [1, 4, 7, 9, 12],
        )

    def test___xor__(self):
        subset = IntegerSet([(3, 5), (6, 8), (9, 11), (13, 14)])

        subset1 = IntegerSet()
        self.assertEqual(list(subset ^ subset1), [3, 4, 6, 7, 9, 10, 13])
        self.assertEqual(list(subset1 ^ subset), [3, 4, 6, 7, 9, 10, 13])

        subset2 = IntegerSet([(0, 2)])
        self.assertEqual(list(subset ^ subset2), [0, 1, 3, 4, 6, 7, 9, 10, 13])
        self.assertEqual(list(subset2 ^ subset), [0, 1, 3, 4, 6, 7, 9, 10, 13])

        self.assertEqual(list(IntegerSet([(1, 2)]) ^ IntegerSet([(0, 2)])), [0])
        self.assertEqual(list(IntegerSet([(2, 3)]) ^ IntegerSet([(0, 1), (2, 3)])), [0])
        self.assertEqual(list(IntegerSet([(0, 1), (2, 3)]) ^ IntegerSet([(0, 1)])), [2])

    def test_symmetric_difference(self):
        subset = IntegerSet([(3, 5), (6, 8), (9, 11), (13, 14)])

        subset1 = IntegerSet()
        self.assertEqual(
            list(subset.symmetric_difference(subset1)),
            [3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.symmetric_difference(list(subset1))),
            [3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset1.symmetric_difference(subset)),
            [3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset1.symmetric_difference(list(subset))),
            [3, 4, 6, 7, 9, 10, 13],
        )

        subset2 = IntegerSet([(0, 2)])
        self.assertEqual(
            list(subset.symmetric_difference(subset2)),
            [0, 1, 3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset.symmetric_difference(list(subset2))),
            [0, 1, 3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset2.symmetric_difference(subset)),
            [0, 1, 3, 4, 6, 7, 9, 10, 13],
        )
        self.assertEqual(
            list(subset2.symmetric_difference(list(subset))),
            [0, 1, 3, 4, 6, 7, 9, 10, 13],
        )

        self.assertEqual(
            list(IntegerSet([(1, 2)]).symmetric_difference(IntegerSet([(0, 2)]))),
            [0],
        )
        self.assertEqual(
            list(IntegerSet([(1, 2)]).symmetric_difference(list(IntegerSet([(0, 2)])))),
            [0],
        )
        self.assertEqual(
            list(
                IntegerSet([(2, 3)]).symmetric_difference(IntegerSet([(0, 1), (2, 3)])),
            ),
            [0],
        )
        self.assertEqual(
            list(
                IntegerSet([(2, 3)]).symmetric_difference(
                    list(IntegerSet([(0, 1), (2, 3)])),
                ),
            ),
            [0],
        )
        self.assertEqual(
            list(
                IntegerSet([(0, 1), (2, 3)]).symmetric_difference(IntegerSet([(0, 1)])),
            ),
            [2],
        )
        self.assertEqual(
            list(
                IntegerSet([(0, 1), (2, 3)]).symmetric_difference(
                    list(IntegerSet([(0, 1)])),
                ),
            ),
            [2],
        )
