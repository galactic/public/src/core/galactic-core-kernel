"""The :mod:`test_meet_semi_lattice_view` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import (
    ExtensibleFiniteMeetSemiLattice,
    FiniteMeetSemiLatticeView,
)
from galactic.algebras.poset import bottom


class BoundedMeetSemiLatticeTestCase(TestCase):
    def test___init__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        view = FiniteMeetSemiLatticeView[PrimeFactors](semi_lattice)
        # noinspection PyTypeChecker
        self.assertEqual(set(semi_lattice), set(view))

        bounded = semi_lattice.ideal(PrimeFactors(24))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {element for element in semi_lattice if element <= PrimeFactors(24)},
        )
        bounded = semi_lattice.filter(PrimeFactors(6))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {element for element in semi_lattice if element >= PrimeFactors(6)},
        )

    # noinspection PyTypeChecker
    def test___contains__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.ideal(PrimeFactors(6))
        elements = set(bounded)
        for element in semi_lattice:
            if element in elements:
                self.assertIn(element, bounded)
            else:
                self.assertNotIn(element, bounded)
        bounded = semi_lattice.filter(PrimeFactors(6))
        elements = set(bounded)
        for element in semi_lattice:
            if element in elements:
                self.assertIn(element, bounded)
            else:
                self.assertNotIn(element, bounded)

    # noinspection PyTypeChecker
    def test___len__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.ideal(PrimeFactors(6))
        elements = set(bounded)
        self.assertEqual(len(elements), len(bounded))
        bounded = semi_lattice.filter(PrimeFactors(6))
        elements = set(bounded)
        self.assertEqual(len(elements), len(bounded))

    def test___iter__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                element
                for element in semi_lattice
                if PrimeFactors(24) >= element >= PrimeFactors(6)
            },
        )

    def test___lt__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertLess(
            bounded,
            semi_lattice,
        )

    def test___gt__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(12))
        # noinspection PyTypeChecker
        self.assertGreater(
            semi_lattice.filter(PrimeFactors(6)),
            bounded,
        )
        # noinspection PyTypeChecker
        self.assertGreater(
            semi_lattice.ideal(PrimeFactors(24)),
            bounded,
        )

    def test_minimum(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertNotIn(1, semi_lattice.bottom)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(12))
        self.assertEqual(bounded.minimum, PrimeFactors(6))
        bounded = semi_lattice.ideal(PrimeFactors(12))
        self.assertEqual(bounded.minimum, semi_lattice.minimum)

    def test_maximum(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(12))
        self.assertEqual(bounded.maximum, PrimeFactors(12))
        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertIsNone(bounded.maximum)

    def test_order(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(3))
            .ideal(PrimeFactors(12))
        )
        self.assertEqual(len(bounded.order), 6)
        self.assertEqual(
            list(bounded.order),
            [
                (PrimeFactors(12), PrimeFactors(12)),
                (PrimeFactors(6), PrimeFactors(12)),
                (PrimeFactors(6), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(12)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(3)),
            ],
        )
        for couple in bounded.order:
            self.assertIn(couple, bounded.order)
        self.assertNotIn(2, bounded.order)
        self.assertNotIn((PrimeFactors(3), PrimeFactors(7)), bounded.order)

    def test_order_universes(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(3))
            .ideal(PrimeFactors(12))
        )
        self.assertEqual(
            bounded.order.universes,
            (bounded.order.domain, bounded.order.domain),
        )
        self.assertEqual(
            set(bounded.order.domain),
            {PrimeFactors(3), PrimeFactors(6), PrimeFactors(12)},
        )
        self.assertEqual(
            set(bounded.order.co_domain),
            {PrimeFactors(3), PrimeFactors(6), PrimeFactors(12)},
        )

    def test_order_successors(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(3))
            .ideal(PrimeFactors(12))
        )
        self.assertEqual(
            set(bounded.order.successors(PrimeFactors(3))),
            {
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(12),
            },
        )
        self.assertEqual(
            len(bounded.order.successors(PrimeFactors(3))),
            3,
        )

        self.assertIn(PrimeFactors(6), bounded.order.successors(PrimeFactors(3)))
        self.assertNotIn(6, bounded.order.successors(PrimeFactors(3)))

    def test_order_predecessors(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(3))
            .ideal(PrimeFactors(12))
        )
        self.assertEqual(
            set(bounded.order.predecessors(PrimeFactors(6))),
            {
                PrimeFactors(3),
                PrimeFactors(6),
            },
        )
        self.assertEqual(
            len(bounded.order.predecessors(PrimeFactors(6))),
            2,
        )

        self.assertIn(PrimeFactors(3), bounded.order.predecessors(PrimeFactors(6)))
        self.assertIn(PrimeFactors(6), bounded.order.predecessors(PrimeFactors(6)))
        self.assertNotIn(6, bounded.order.predecessors(PrimeFactors(6)))

    def test_order_sinks_sources(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(3))
            .ideal(PrimeFactors(12))
        )

        self.assertEqual(
            set(bounded.order.sinks),
            {PrimeFactors(12)},
        )
        self.assertEqual(
            set(bounded.order.sources),
            {PrimeFactors(3)},
        )

    def test_order_domain(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(3))
            .ideal(PrimeFactors(12))
        )
        self.assertEqual(
            set(bounded.order.domain),
            {
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(12),
            },
        )

        self.assertEqual(len(bounded.order.domain), 3)

        self.assertIn(PrimeFactors(3), bounded.order.domain)
        self.assertIn(PrimeFactors(6), bounded.order.domain)
        self.assertIn(PrimeFactors(12), bounded.order.domain)
        self.assertNotIn(PrimeFactors(11), bounded.order.domain)

    def test_cover(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(18), PrimeFactors(36)),
                (PrimeFactors(12), PrimeFactors(24)),
                (PrimeFactors(12), PrimeFactors(36)),
                (PrimeFactors(6), PrimeFactors(18)),
                (PrimeFactors(6), PrimeFactors(12)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

        bounded = semi_lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(12), PrimeFactors(24)),
                (PrimeFactors(6), PrimeFactors(12)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(1), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(2)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.cover),
            {(PrimeFactors(12), PrimeFactors(24)), (PrimeFactors(6), PrimeFactors(12))},
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

    def test_cover_universes(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(6))
            .ideal(PrimeFactors(24))
        )
        self.assertEqual(
            bounded.cover.universes,
            (bounded.cover.domain, bounded.cover.domain),
        )
        self.assertEqual(
            set(bounded.cover.domain),
            {PrimeFactors(6), PrimeFactors(12), PrimeFactors(24)},
        )
        self.assertEqual(
            set(bounded.cover.co_domain),
            {PrimeFactors(6), PrimeFactors(12), PrimeFactors(24)},
        )

    def test_cover_successors(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(6))
            .ideal(PrimeFactors(24))
        )
        self.assertEqual(
            set(bounded.cover.successors(PrimeFactors(6))),
            {PrimeFactors(12)},
        )
        self.assertEqual(
            len(bounded.cover.successors(PrimeFactors(6))),
            1,
        )
        self.assertIn(PrimeFactors(12), bounded.cover.successors(PrimeFactors(6)))
        self.assertNotIn(PrimeFactors(3), bounded.cover.successors(PrimeFactors(6)))
        self.assertNotIn(3, bounded.cover.successors(PrimeFactors(6)))

    def test_cover_predecessors(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(6))
            .ideal(PrimeFactors(24))
        )
        self.assertEqual(
            set(bounded.cover.predecessors(PrimeFactors(12))),
            {PrimeFactors(6)},
        )
        self.assertEqual(
            len(bounded.cover.predecessors(PrimeFactors(12))),
            1,
        )
        self.assertIn(PrimeFactors(6), bounded.cover.predecessors(PrimeFactors(12)))
        self.assertNotIn(PrimeFactors(3), bounded.cover.predecessors(PrimeFactors(12)))
        self.assertNotIn(3, bounded.cover.predecessors(PrimeFactors(12)))

        for neighbourhood in bounded.cover.neighbourhoods():
            self.assertNotIn(2, bounded.cover.predecessors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.predecessors),
                len(bounded.cover.predecessors(neighbourhood.element)),
            )
            self.assertEqual(
                set(neighbourhood.predecessors),
                set(bounded.cover.predecessors(neighbourhood.element)),
            )
            # noinspection PyTypeChecker
            for element in bounded:
                if element in neighbourhood.predecessors:
                    self.assertIn(
                        element,
                        bounded.cover.predecessors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        bounded.cover.predecessors(neighbourhood.element),
                    )

    def test_cover_sinks_sources(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(24), PrimeFactors(36)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(6)},
        )

        bounded = semi_lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(24)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {
                PrimeFactors(1),
            },
        )

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(24)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(6)},
        )

    def test_cover_neighbourhoods(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(24),
                    frozenset(),
                    frozenset({PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(36),
                    frozenset(),
                    frozenset({PrimeFactors(18), PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(18),
                    frozenset({PrimeFactors(36)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(12),
                    frozenset({PrimeFactors(24), PrimeFactors(36)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(6),
                    frozenset({PrimeFactors(18), PrimeFactors(12)}),
                    frozenset(),
                ),
            },
        )

        bounded = semi_lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(24),
                    frozenset(),
                    frozenset({PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(12),
                    frozenset({PrimeFactors(24)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(6),
                    frozenset({PrimeFactors(12)}),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(6)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(2),
                    frozenset({PrimeFactors(6)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(1),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                    frozenset(),
                ),
            },
        )

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(24),
                    frozenset(),
                    frozenset({PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(12),
                    frozenset({PrimeFactors(24)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(6),
                    frozenset({PrimeFactors(12)}),
                    frozenset(),
                ),
            },
        )

    def test_top(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(24), PrimeFactors(36)},
        )
        self.assertEqual(len(bounded.top), 2)
        self.assertIn(PrimeFactors(24), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

        bounded = semi_lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(24)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(24), bounded.top)
        self.assertNotIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(24)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(24), bounded.top)
        self.assertNotIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

    def test_bottom(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(6)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(PrimeFactors(12), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

        bounded = semi_lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.bottom),
            {
                PrimeFactors(1),
            },
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(1), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(6)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(PrimeFactors(12), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

    def test_filter(self):
        bounded = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        ).filter(PrimeFactors(6))
        with self.assertRaises(ValueError):
            _ = bounded.filter(PrimeFactors(1))

    def test_ideal(self):
        bounded = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        ).ideal(PrimeFactors(24))
        with self.assertRaises(ValueError):
            _ = bounded.ideal(PrimeFactors(36))

    # noinspection PyTypeChecker
    def test_upper_limit(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        for bounded in (
            semi_lattice.filter(PrimeFactors(6)),
            semi_lattice.ideal(PrimeFactors(24)),
            semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24)),
        ):
            for limit in bounded:
                for element in bounded:
                    if element <= limit:
                        self.assertIn(element, bounded.upper_limit(limit))
                    else:
                        self.assertNotIn(element, bounded.upper_limit(limit))
                self.assertNotIn(2, bounded.upper_limit(limit))
                self.assertEqual(
                    set(bounded.upper_limit(limit)),
                    {element for element in bounded if element <= limit},
                )
                self.assertEqual(
                    len(bounded.upper_limit(limit)),
                    len({element for element in bounded if element <= limit}),
                )
                for element in bounded:
                    if element < limit:
                        self.assertIn(
                            element,
                            bounded.upper_limit(limit, strict=True),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.upper_limit(limit, strict=True),
                        )
                self.assertNotIn(2, bounded.upper_limit(limit, strict=True))
                self.assertEqual(
                    set(bounded.upper_limit(limit, strict=True)),
                    {element for element in bounded if element < limit},
                )
                self.assertEqual(
                    len(bounded.upper_limit(limit, strict=True)),
                    len({element for element in bounded if element < limit}),
                )

    # noinspection PyTypeChecker
    def test_lower_limit(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        for bounded in (
            semi_lattice.filter(PrimeFactors(6)),
            semi_lattice.ideal(PrimeFactors(24)),
            semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24)),
        ):
            for limit in bounded:
                for element in bounded:
                    if element >= limit:
                        self.assertIn(element, bounded.lower_limit(limit))
                    else:
                        self.assertNotIn(element, bounded.lower_limit(limit))
                self.assertNotIn(2, bounded.lower_limit(limit))
                self.assertEqual(
                    set(bounded.lower_limit(limit)),
                    {element for element in bounded if element >= limit},
                )
                self.assertEqual(
                    len(bounded.lower_limit(limit)),
                    len({element for element in bounded if element >= limit}),
                )
                for element in bounded:
                    if element > limit:
                        self.assertIn(
                            element,
                            bounded.lower_limit(limit, strict=True),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.lower_limit(limit, strict=True),
                        )
                self.assertNotIn(2, bounded.lower_limit(limit, strict=True))
                self.assertEqual(
                    set(bounded.lower_limit(limit, strict=True)),
                    {element for element in bounded if element > limit},
                )
                self.assertEqual(
                    len(bounded.lower_limit(limit, strict=True)),
                    len({element for element in bounded if element > limit}),
                )

    # noinspection PyTypeChecker
    def test_meet_irreducible(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        for irreducible in bounded.meet_irreducible:
            self.assertLessEqual(
                len(bounded.cover.successors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.meet_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.successors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.meet_irreducible),
            {
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            },
        )
        self.assertEqual(
            len(bounded.meet_irreducible),
            3,
        )
        self.assertIn(PrimeFactors(18), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(24), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(36), bounded.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.meet_irreducible)
        self.assertNotIn(1, bounded.meet_irreducible)

        bounded = semi_lattice.ideal(PrimeFactors(24))
        for irreducible in bounded.meet_irreducible:
            self.assertLessEqual(
                len(bounded.cover.successors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.meet_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.successors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.meet_irreducible),
            {
                PrimeFactors(6),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(12),
                PrimeFactors(24),
            },
        )
        self.assertEqual(
            len(bounded.meet_irreducible),
            5,
        )
        self.assertIn(PrimeFactors(6), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(2), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(3), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(12), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(24), bounded.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.meet_irreducible)
        self.assertNotIn(1, bounded.meet_irreducible)

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        for irreducible in bounded.meet_irreducible:
            self.assertLessEqual(
                len(bounded.cover.successors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.meet_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.successors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.meet_irreducible),
            {PrimeFactors(6), PrimeFactors(12), PrimeFactors(24)},
        )
        self.assertEqual(
            len(bounded.meet_irreducible),
            3,
        )
        self.assertIn(PrimeFactors(6), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(12), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(24), bounded.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.meet_irreducible)
        self.assertNotIn(1, bounded.meet_irreducible)

        bounded = semi_lattice.filter(PrimeFactors(6))
        semi_lattice.extend([PrimeFactors(30)])
        self.assertEqual(
            set(bounded.meet_irreducible),
            {
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
                PrimeFactors(30),
            },
        )

    def test_atoms(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded.atoms),
            {
                PrimeFactors(18),
                PrimeFactors(12),
            },
        )
        self.assertEqual(len(bounded.atoms), 2)
        self.assertIn(PrimeFactors(18), bounded.atoms)
        self.assertNotIn(PrimeFactors(1), bounded.atoms)
        self.assertNotIn(1, bounded.atoms)

        bounded = semi_lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.atoms),
            {
                PrimeFactors(2),
                PrimeFactors(3),
            },
        )
        self.assertEqual(len(bounded.atoms), 2)
        self.assertNotIn(PrimeFactors(1), bounded.atoms)
        self.assertIn(PrimeFactors(2), bounded.atoms)
        self.assertNotIn(1, bounded.atoms)

        bounded = semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.atoms),
            {
                PrimeFactors(12),
            },
        )
        self.assertEqual(len(bounded.atoms), 1)
        self.assertNotIn(PrimeFactors(6), bounded.atoms)
        self.assertIn(PrimeFactors(12), bounded.atoms)
        self.assertNotIn(1, bounded.atoms)

    def test_smallest_meet_irreducible(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for bounded in (
            semi_lattice.filter(PrimeFactors(6)),
            semi_lattice.ideal(PrimeFactors(24)),
            semi_lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24)),
        ):
            for value in semi_lattice:
                self.assertEqual(
                    set(bounded.smallest_meet_irreducible(value)),
                    set(
                        bottom(
                            irreducible
                            for irreducible in bounded.meet_irreducible
                            if irreducible >= value
                        ),
                    ),
                )
                for element in semi_lattice:
                    if element in bottom(
                        irreducible
                        for irreducible in bounded.meet_irreducible
                        if irreducible >= value
                    ):
                        self.assertIn(
                            element,
                            bounded.smallest_meet_irreducible(value),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.smallest_meet_irreducible(value),
                        )
                self.assertEqual(
                    len(bounded.smallest_meet_irreducible(value)),
                    len(
                        set(
                            bottom(
                                irreducible
                                for irreducible in bounded.meet_irreducible
                                if irreducible >= value
                            ),
                        ),
                    ),
                )
                self.assertEqual(
                    set(bounded.smallest_meet_irreducible(value, strict=True)),
                    set(
                        bottom(
                            irreducible
                            for irreducible in bounded.meet_irreducible
                            if irreducible > value
                        ),
                    ),
                )
                self.assertEqual(
                    len(bounded.smallest_meet_irreducible(value, strict=True)),
                    len(
                        set(
                            bottom(
                                irreducible
                                for irreducible in bounded.meet_irreducible
                                if irreducible > value
                            ),
                        ),
                    ),
                )
                for element in semi_lattice:
                    if element in bottom(
                        irreducible
                        for irreducible in bounded.meet_irreducible
                        if irreducible > value
                    ):
                        self.assertIn(
                            element,
                            bounded.smallest_meet_irreducible(
                                value,
                                strict=True,
                            ),
                        )
                    else:
                        self.assertNotIn(
                            value,
                            bounded.smallest_meet_irreducible(
                                value,
                                strict=True,
                            ),
                        )
