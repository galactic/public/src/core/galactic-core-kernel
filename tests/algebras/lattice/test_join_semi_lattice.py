"""The :mod:`test_join_semi_lattice` module."""

import copy
from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import (
    ExtensibleFiniteJoinSemiLattice,
    FrozenFiniteJoinSemiLattice,
)
from galactic.algebras.poset import MutableFinitePartiallyOrderedSet


class ExtensibleFiniteJoinSemiLatticeTestCase(TestCase):
    def test___init__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertFalse(semi_lattice)
        self.assertEqual(list(semi_lattice), [])
        self.assertEqual(len(semi_lattice), 0)

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(96)],
        )
        self.assertTrue(semi_lattice)
        self.assertEqual(
            list(semi_lattice),
            [PrimeFactors(96)],
        )
        self.assertEqual(len(semi_lattice), 1)

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(96),
                PrimeFactors(72),
            ],
        )
        self.assertEqual(
            list(semi_lattice),
            [PrimeFactors(96), PrimeFactors(72), PrimeFactors(288)],
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(96),
                PrimeFactors(72),
                PrimeFactors(12),
            ],
        )
        self.assertEqual(
            list(semi_lattice),
            [
                PrimeFactors(12),
                PrimeFactors(96),
                PrimeFactors(72),
                PrimeFactors(288),
            ],
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(96),
                PrimeFactors(72),
                PrimeFactors(12),
                PrimeFactors(32),
            ],
        )
        self.assertEqual(
            semi_lattice,
            {
                PrimeFactors(12),
                PrimeFactors(32),
                PrimeFactors(72),
                PrimeFactors(96),
                PrimeFactors(288),
            },
        )
        self.assertEqual(
            list(semi_lattice.join_irreducible),
            [
                PrimeFactors(12),
                PrimeFactors(32),
                PrimeFactors(72),
            ],
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(2)],
        )
        semi_lattice.extend([PrimeFactors(3)])
        semi_lattice.extend([PrimeFactors(6)])
        semi_lattice.extend([PrimeFactors(24)])
        semi_lattice.extend([PrimeFactors(36)])
        semi_lattice.extend([PrimeFactors(18)])
        self.assertEqual(
            set(semi_lattice),
            {
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(24),
                PrimeFactors(18),
                PrimeFactors(36),
                PrimeFactors(72),
            },
        )
        self.assertEqual(
            len(
                ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                    elements=[
                        PrimeFactors(1),
                        PrimeFactors(2),
                    ],
                ),
            ),
            2,
        )
        self.assertEqual(
            len(
                ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                    elements=[PrimeFactors(1)],
                ),
            ),
            1,
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        # noinspection PyUnresolvedReferences
        self.assertEqual(
            semi_lattice._irreducible,
            {
                PrimeFactors(3): set(),
                PrimeFactors(6): {PrimeFactors(3)},
                PrimeFactors(14): set(),
                PrimeFactors(15): {PrimeFactors(3)},
                PrimeFactors(18): {PrimeFactors(3), PrimeFactors(6)},
                PrimeFactors(24): {PrimeFactors(3), PrimeFactors(6)},
                PrimeFactors(36): {PrimeFactors(18), PrimeFactors(3), PrimeFactors(6)},
            },
        )
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(6),
                PrimeFactors(30),
            ],
        )
        # noinspection PyUnresolvedReferences
        self.assertEqual(
            semi_lattice._irreducible,
            {
                PrimeFactors(1): set(),
                PrimeFactors(2): {PrimeFactors(1)},
                PrimeFactors(6): {PrimeFactors(1), PrimeFactors(2)},
                PrimeFactors(30): {PrimeFactors(1), PrimeFactors(2), PrimeFactors(6)},
            },
        )

    def test___eq__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertNotEqual(semi_lattice, 1)

    def test___hash__(self):
        semi_lattice1 = FrozenFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        semi_lattice2 = FrozenFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(36),
                PrimeFactors(14),
                PrimeFactors(18),
                PrimeFactors(6),
                PrimeFactors(3),
                PrimeFactors(15),
                PrimeFactors(24),
            ],
        )
        self.assertEqual(hash(semi_lattice1), hash(semi_lattice2))

    def test___copy__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(semi_lattice, copy.copy(semi_lattice))
        self.assertEqual(semi_lattice, semi_lattice.copy())

    def test___contains__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertNotIn(2, semi_lattice)
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertNotIn(2, semi_lattice)
        self.assertIn(PrimeFactors(3), semi_lattice)
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertNotIn(2, semi_lattice)
        self.assertIn(PrimeFactors(2), semi_lattice)
        self.assertIn(PrimeFactors(3), semi_lattice)
        self.assertIn(PrimeFactors(6), semi_lattice)

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertIn(PrimeFactors(1260), semi_lattice)
        self.assertIn(PrimeFactors(6), semi_lattice)
        self.assertNotIn(PrimeFactors(11), semi_lattice)

    def test___len__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertEqual(len(semi_lattice), 0)
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(len(semi_lattice), 1)
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(len(semi_lattice), 3)

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(len(semi_lattice), 23)

    def test___iter__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertEqual(list(semi_lattice), [])
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(list(semi_lattice), [PrimeFactors(3)])
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(
            list(semi_lattice),
            [
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(6),
            ],
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(4),
                PrimeFactors(9),
            ],
        )
        self.assertEqual(
            semi_lattice,
            {
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(4),
                PrimeFactors(6),
                PrimeFactors(9),
                PrimeFactors(10),
                PrimeFactors(15),
                PrimeFactors(14),
                PrimeFactors(21),
                PrimeFactors(35),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(20),
                PrimeFactors(30),
                PrimeFactors(45),
                PrimeFactors(28),
                PrimeFactors(42),
                PrimeFactors(63),
                PrimeFactors(70),
                PrimeFactors(105),
                PrimeFactors(36),
                PrimeFactors(60),
                PrimeFactors(90),
                PrimeFactors(84),
                PrimeFactors(126),
                PrimeFactors(140),
                PrimeFactors(210),
                PrimeFactors(315),
                PrimeFactors(180),
                PrimeFactors(252),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(1260),
            },
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(semi_lattice),
            {
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(30),
                PrimeFactors(36),
                PrimeFactors(42),
                PrimeFactors(72),
                PrimeFactors(90),
                PrimeFactors(120),
                PrimeFactors(126),
                PrimeFactors(168),
                PrimeFactors(180),
                PrimeFactors(210),
                PrimeFactors(252),
                PrimeFactors(360),
                PrimeFactors(504),
                PrimeFactors(630),
                PrimeFactors(840),
                PrimeFactors(1260),
                PrimeFactors(2520),
            },
        )

    def test___lt__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertLess(
            semi_lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLess(
            semi_lattice,
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )

    def test___gt__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertGreater(
            semi_lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](),
        )
        self.assertGreater(
            semi_lattice,
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](),
        )

    def test___le__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertLessEqual(
            semi_lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](),
        )
        self.assertLessEqual(
            semi_lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](elements=[PrimeFactors(3)]),
        )

        self.assertLessEqual(
            semi_lattice,
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLessEqual(
            semi_lattice,
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](),
        )

    def test___ge__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertGreaterEqual(
            semi_lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](),
        )
        self.assertGreaterEqual(
            semi_lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[PrimeFactors(3)],
            ),
        )
        self.assertGreaterEqual(
            semi_lattice,
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](),
        )
        self.assertGreaterEqual(
            semi_lattice,
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[PrimeFactors(3)],
            ),
        )

    def test___or__(self):
        semi_lattice1 = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        semi_lattice2 = semi_lattice1 | [PrimeFactors(2)]
        self.assertLessEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )

    def test___ior__(self):
        semi_lattice1 = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        semi_lattice1 |= [PrimeFactors(2)]
        self.assertEqual(
            list(semi_lattice1),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )

    def test___and__(self):
        semi_lattice1 = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(2), PrimeFactors(5)],
        )
        semi_lattice2 = semi_lattice1 & [
            PrimeFactors(2),
            PrimeFactors(5),
            PrimeFactors(7),
        ]
        self.assertGreaterEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(2), PrimeFactors(5), PrimeFactors(10)],
        )

    def test_join(self):
        semi_lattice1 = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        semi_lattice2 = semi_lattice1.join(
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](elements=[PrimeFactors(2)]),
        )
        self.assertLessEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )

    def test_meet(self):
        semi_lattice1 = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(2), PrimeFactors(5)],
        )
        semi_lattice2 = semi_lattice1.meet(
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[PrimeFactors(2), PrimeFactors(5), PrimeFactors(7)],
            ),
        )
        self.assertGreaterEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(2), PrimeFactors(5), PrimeFactors(10)],
        )

    def test_isdisjoint(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertTrue(semi_lattice.isdisjoint([]))
        self.assertTrue(semi_lattice.isdisjoint([PrimeFactors(4)]))
        self.assertFalse(semi_lattice.isdisjoint([PrimeFactors(2)]))
        self.assertFalse(semi_lattice.isdisjoint([PrimeFactors(3)]))
        self.assertFalse(semi_lattice.isdisjoint([PrimeFactors(6)]))

    def test_issubset(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertFalse(semi_lattice.issubset([]))
        self.assertFalse(semi_lattice.issubset([PrimeFactors(4)]))
        self.assertFalse(semi_lattice.issubset([PrimeFactors(2), PrimeFactors(3)]))
        self.assertTrue(
            semi_lattice.issubset([PrimeFactors(2), PrimeFactors(3), PrimeFactors(6)]),
        )

    def test_issuperset(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertTrue(semi_lattice.issuperset([]))
        self.assertFalse(semi_lattice.issuperset([PrimeFactors(4)]))
        self.assertTrue(semi_lattice.issuperset([PrimeFactors(2), PrimeFactors(3)]))
        self.assertTrue(
            semi_lattice.issuperset(
                [PrimeFactors(2), PrimeFactors(3), PrimeFactors(6)],
            ),
        )
        self.assertFalse(
            semi_lattice.issuperset(
                [
                    PrimeFactors(2),
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(4),
                ],
            ),
        )

    def test_minimum(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertIsNone(semi_lattice.minimum)
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
            ],
        )
        self.assertEqual(semi_lattice.minimum, PrimeFactors(2))
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertIsNone(semi_lattice.minimum)

    def test_order(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
            ],
        )
        self.assertEqual(len(semi_lattice.order), 8)
        self.assertEqual(
            set(semi_lattice.order),
            {
                (PrimeFactors(3), PrimeFactors(3)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(42)),
                (PrimeFactors(14), PrimeFactors(14)),
                (PrimeFactors(14), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(6)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(42), PrimeFactors(42)),
            },
        )
        for couple in semi_lattice.order:
            self.assertIn(couple, semi_lattice.order)
        self.assertNotIn(2, semi_lattice.order)
        self.assertNotIn((PrimeFactors(3), PrimeFactors(7)), semi_lattice.order)

    def test_order_universes(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            semi_lattice.order.universes,
            (semi_lattice.order.domain, semi_lattice.order.domain),
        )
        self.assertEqual(
            list(semi_lattice.order.domain),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )
        self.assertEqual(
            list(semi_lattice.order.co_domain),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )

    def test_order_successors(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = semi_lattice.order.successors(3)
        self.assertTrue(semi_lattice.order.successors(PrimeFactors(72)))
        self.assertEqual(
            list(semi_lattice.order.successors(PrimeFactors(72))),
            [
                PrimeFactors(72),
                PrimeFactors(504),
                PrimeFactors(360),
                PrimeFactors(2520),
            ],
        )
        self.assertEqual(
            len(semi_lattice.order.successors(PrimeFactors(72))),
            4,
        )
        self.assertTrue(semi_lattice.order.successors(PrimeFactors(72)))

        self.assertIn(
            PrimeFactors(504),
            semi_lattice.order.successors(PrimeFactors(72)),
        )
        self.assertNotIn(6, semi_lattice.order.successors(PrimeFactors(72)))

    def test_order_predecessors(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = semi_lattice.order.predecessors(3)
        self.assertTrue(semi_lattice.order.predecessors(PrimeFactors(72)))
        self.assertEqual(
            list(semi_lattice.order.predecessors(PrimeFactors(72))),
            [
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
                PrimeFactors(72),
            ],
        )
        self.assertEqual(
            len(semi_lattice.order.predecessors(PrimeFactors(72))),
            6,
        )

        self.assertIn(
            PrimeFactors(3),
            semi_lattice.order.predecessors(PrimeFactors(72)),
        )
        self.assertNotIn(1, semi_lattice.order.predecessors(PrimeFactors(72)))

    def test_order_sinks_sources(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(semi_lattice.order.sinks),
            {PrimeFactors(2520)},
        )
        self.assertEqual(
            set(semi_lattice.order.sources),
            {
                PrimeFactors(14),
                PrimeFactors(3),
            },
        )

    def test_order_domain(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(semi_lattice.order.domain),
            {
                PrimeFactors(3),
                PrimeFactors(14),
                PrimeFactors(6),
                PrimeFactors(15),
                PrimeFactors(42),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(30),
                PrimeFactors(126),
                PrimeFactors(36),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(90),
                PrimeFactors(120),
                PrimeFactors(72),
                PrimeFactors(252),
                PrimeFactors(630),
                PrimeFactors(180),
                PrimeFactors(840),
                PrimeFactors(504),
                PrimeFactors(360),
                PrimeFactors(1260),
                PrimeFactors(2520),
            },
        )

        self.assertEqual(len(semi_lattice.order.domain), 23)

        self.assertIn(PrimeFactors(3), semi_lattice.order.domain)
        self.assertIn(PrimeFactors(6), semi_lattice.order.domain)
        self.assertNotIn(PrimeFactors(11), semi_lattice.order.domain)

    def test_cover(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertNotIn(2, semi_lattice.cover)
        self.assertEqual(len(semi_lattice.cover), 0)
        self.assertEqual(list(semi_lattice.cover), [])

        semi_lattice.extend([PrimeFactors(2)])
        self.assertNotIn(2, semi_lattice.cover)
        self.assertNotIn((2, 3), semi_lattice.cover)
        self.assertIn((PrimeFactors(2), PrimeFactors(6)), semi_lattice.cover)
        self.assertIn((PrimeFactors(3), PrimeFactors(6)), semi_lattice.cover)
        self.assertEqual(len(semi_lattice.cover), 2)
        self.assertEqual(
            list(semi_lattice.cover),
            [
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(6)),
            ],
        )
        semi_lattice.extend([PrimeFactors(12)])
        self.assertIn((PrimeFactors(6), PrimeFactors(12)), semi_lattice.cover)
        self.assertEqual(len(semi_lattice.cover), 3)

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(semi_lattice.cover),
            {
                (PrimeFactors(3), PrimeFactors(15)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(14), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(24)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(18)),
                (PrimeFactors(6), PrimeFactors(30)),
                (PrimeFactors(15), PrimeFactors(30)),
                (PrimeFactors(18), PrimeFactors(36)),
                (PrimeFactors(18), PrimeFactors(126)),
                (PrimeFactors(18), PrimeFactors(90)),
                (PrimeFactors(24), PrimeFactors(168)),
                (PrimeFactors(24), PrimeFactors(72)),
                (PrimeFactors(24), PrimeFactors(120)),
                (PrimeFactors(42), PrimeFactors(168)),
                (PrimeFactors(42), PrimeFactors(126)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(30), PrimeFactors(120)),
                (PrimeFactors(30), PrimeFactors(210)),
                (PrimeFactors(30), PrimeFactors(90)),
                (PrimeFactors(36), PrimeFactors(72)),
                (PrimeFactors(36), PrimeFactors(252)),
                (PrimeFactors(36), PrimeFactors(180)),
                (PrimeFactors(126), PrimeFactors(252)),
                (PrimeFactors(126), PrimeFactors(630)),
                (PrimeFactors(168), PrimeFactors(504)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(90), PrimeFactors(180)),
                (PrimeFactors(90), PrimeFactors(630)),
                (PrimeFactors(210), PrimeFactors(630)),
                (PrimeFactors(210), PrimeFactors(840)),
                (PrimeFactors(120), PrimeFactors(840)),
                (PrimeFactors(120), PrimeFactors(360)),
                (PrimeFactors(72), PrimeFactors(504)),
                (PrimeFactors(72), PrimeFactors(360)),
                (PrimeFactors(252), PrimeFactors(504)),
                (PrimeFactors(252), PrimeFactors(1260)),
                (PrimeFactors(180), PrimeFactors(360)),
                (PrimeFactors(180), PrimeFactors(1260)),
                (PrimeFactors(630), PrimeFactors(1260)),
                (PrimeFactors(840), PrimeFactors(2520)),
                (PrimeFactors(504), PrimeFactors(2520)),
                (PrimeFactors(360), PrimeFactors(2520)),
                (PrimeFactors(1260), PrimeFactors(2520)),
            },
        )
        for couple in semi_lattice.cover:
            self.assertIn(couple, semi_lattice.cover)
        relation = set(semi_lattice.cover)
        for couple in semi_lattice.order:
            if couple in relation:
                self.assertIn(couple, semi_lattice.cover)
            else:
                self.assertNotIn(couple, semi_lattice.cover)

    def test_cover_universes(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            semi_lattice.cover.universes,
            (semi_lattice.cover.domain, semi_lattice.cover.domain),
        )
        self.assertEqual(
            list(semi_lattice.cover.domain),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )
        self.assertEqual(
            list(semi_lattice.cover.co_domain),
            [PrimeFactors(3), PrimeFactors(2), PrimeFactors(6)],
        )

    def test_cover_successors(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.cover.successors(PrimeFactors(25))
        self.assertTrue(semi_lattice.cover.successors(PrimeFactors(18)))
        self.assertEqual(
            set(semi_lattice.cover.successors(PrimeFactors(18))),
            {PrimeFactors(36), PrimeFactors(126), PrimeFactors(90)},
        )
        self.assertEqual(
            len(semi_lattice.cover.successors(PrimeFactors(18))),
            3,
        )
        self.assertIn(PrimeFactors(36), semi_lattice.cover.successors(PrimeFactors(18)))
        self.assertNotIn(
            PrimeFactors(3),
            semi_lattice.cover.successors(PrimeFactors(18)),
        )
        self.assertNotIn(3, semi_lattice.cover.successors(PrimeFactors(18)))

    def test_cover_predecessors(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.cover.predecessors(PrimeFactors(25))
        self.assertTrue(semi_lattice.cover.predecessors(PrimeFactors(18)))
        self.assertEqual(
            set(semi_lattice.cover.predecessors(PrimeFactors(18))),
            {PrimeFactors(6)},
        )

        for neighbourhood in semi_lattice.cover.neighbourhoods():
            self.assertNotIn(2, semi_lattice.cover.predecessors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.predecessors),
                len(semi_lattice.cover.predecessors(neighbourhood.element)),
            )
            self.assertEqual(
                set(neighbourhood.predecessors),
                set(semi_lattice.cover.predecessors(neighbourhood.element)),
            )
            for element in semi_lattice:
                if element in neighbourhood.predecessors:
                    self.assertIn(
                        element,
                        semi_lattice.cover.predecessors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        semi_lattice.cover.predecessors(neighbourhood.element),
                    )

    def test_cover_sinks_sources(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(semi_lattice.cover.sinks),
            {PrimeFactors(2520)},
        )
        self.assertEqual(
            set(semi_lattice.cover.sources),
            {
                PrimeFactors(14),
                PrimeFactors(3),
            },
        )

    def test_cover_neighbourhoods(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in semi_lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(3),
                    frozenset(
                        [
                            PrimeFactors(6),
                            PrimeFactors(15),
                        ],
                    ),
                    frozenset(),
                ),
                (
                    PrimeFactors(14),
                    frozenset(
                        [
                            PrimeFactors(42),
                        ],
                    ),
                    frozenset(),
                ),
                (
                    PrimeFactors(6),
                    frozenset(
                        [
                            PrimeFactors(42),
                            PrimeFactors(30),
                            PrimeFactors(18),
                            PrimeFactors(24),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(3),
                        ],
                    ),
                ),
                (
                    PrimeFactors(15),
                    frozenset(
                        [
                            PrimeFactors(30),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(3),
                        ],
                    ),
                ),
                (
                    PrimeFactors(42),
                    frozenset(
                        [
                            PrimeFactors(210),
                            PrimeFactors(126),
                            PrimeFactors(168),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(14),
                            PrimeFactors(6),
                        ],
                    ),
                ),
                (
                    PrimeFactors(18),
                    frozenset(
                        [
                            PrimeFactors(126),
                            PrimeFactors(90),
                            PrimeFactors(36),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(6),
                        ],
                    ),
                ),
                (
                    PrimeFactors(24),
                    frozenset(
                        [
                            PrimeFactors(168),
                            PrimeFactors(120),
                            PrimeFactors(72),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(6),
                        ],
                    ),
                ),
                (
                    PrimeFactors(30),
                    frozenset(
                        [
                            PrimeFactors(210),
                            PrimeFactors(90),
                            PrimeFactors(120),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(6),
                            PrimeFactors(15),
                        ],
                    ),
                ),
                (
                    PrimeFactors(126),
                    frozenset(
                        [
                            PrimeFactors(630),
                            PrimeFactors(252),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(42),
                            PrimeFactors(18),
                        ],
                    ),
                ),
                (
                    PrimeFactors(36),
                    frozenset(
                        [
                            PrimeFactors(72),
                            PrimeFactors(252),
                            PrimeFactors(180),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(18),
                        ],
                    ),
                ),
                (
                    PrimeFactors(168),
                    frozenset(
                        [
                            PrimeFactors(840),
                            PrimeFactors(504),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(42),
                            PrimeFactors(24),
                        ],
                    ),
                ),
                (
                    PrimeFactors(210),
                    frozenset(
                        [
                            PrimeFactors(630),
                            PrimeFactors(840),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(42),
                            PrimeFactors(30),
                        ],
                    ),
                ),
                (
                    PrimeFactors(90),
                    frozenset(
                        [
                            PrimeFactors(630),
                            PrimeFactors(180),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(18),
                            PrimeFactors(30),
                        ],
                    ),
                ),
                (
                    PrimeFactors(120),
                    frozenset(
                        [
                            PrimeFactors(840),
                            PrimeFactors(360),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(24),
                            PrimeFactors(30),
                        ],
                    ),
                ),
                (
                    PrimeFactors(72),
                    frozenset(
                        [
                            PrimeFactors(504),
                            PrimeFactors(360),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(24),
                            PrimeFactors(36),
                        ],
                    ),
                ),
                (
                    PrimeFactors(252),
                    frozenset(
                        [
                            PrimeFactors(504),
                            PrimeFactors(1260),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(126),
                            PrimeFactors(36),
                        ],
                    ),
                ),
                (
                    PrimeFactors(630),
                    frozenset(
                        [
                            PrimeFactors(1260),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(126),
                            PrimeFactors(210),
                            PrimeFactors(90),
                        ],
                    ),
                ),
                (
                    PrimeFactors(180),
                    frozenset(
                        [
                            PrimeFactors(360),
                            PrimeFactors(1260),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(36),
                            PrimeFactors(90),
                        ],
                    ),
                ),
                (
                    PrimeFactors(840),
                    frozenset(
                        [
                            PrimeFactors(2520),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(168),
                            PrimeFactors(210),
                            PrimeFactors(120),
                        ],
                    ),
                ),
                (
                    PrimeFactors(504),
                    frozenset(
                        [
                            PrimeFactors(2520),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(168),
                            PrimeFactors(72),
                            PrimeFactors(252),
                        ],
                    ),
                ),
                (
                    PrimeFactors(360),
                    frozenset(
                        [
                            PrimeFactors(2520),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(120),
                            PrimeFactors(72),
                            PrimeFactors(180),
                        ],
                    ),
                ),
                (
                    PrimeFactors(1260),
                    frozenset(
                        [
                            PrimeFactors(2520),
                        ],
                    ),
                    frozenset(
                        [
                            PrimeFactors(252),
                            PrimeFactors(630),
                            PrimeFactors(180),
                        ],
                    ),
                ),
                (
                    PrimeFactors(2520),
                    frozenset(),
                    frozenset(
                        [
                            PrimeFactors(840),
                            PrimeFactors(504),
                            PrimeFactors(360),
                            PrimeFactors(1260),
                        ],
                    ),
                ),
            },
        )
        for neighbourhood in semi_lattice.cover.neighbourhoods():
            for successor in neighbourhood.successors:
                self.assertIn(successor, neighbourhood.successors)
            for predecessor in neighbourhood.predecessors:
                self.assertIn(predecessor, neighbourhood.predecessors)

    def test_top(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertEqual(list(semi_lattice.top), [])
        self.assertEqual(len(semi_lattice.top), 0)
        self.assertNotIn(1, semi_lattice.top)
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.top),
            [PrimeFactors(2520)],
        )

        self.assertIn(PrimeFactors(2520), semi_lattice.top)
        self.assertNotIn(PrimeFactors(6), semi_lattice.top)
        self.assertNotIn(6, semi_lattice.top)

    def test_bottom(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.bottom),
            [PrimeFactors(3), PrimeFactors(14)],
        )

        self.assertIn(PrimeFactors(3), semi_lattice.bottom)
        self.assertNotIn(PrimeFactors(6), semi_lattice.bottom)
        self.assertNotIn(6, semi_lattice.bottom)

    def test_filter(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(42))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(42),
                PrimeFactors(126),
                PrimeFactors(210),
                PrimeFactors(168),
                PrimeFactors(252),
                PrimeFactors(630),
                PrimeFactors(840),
                PrimeFactors(504),
                PrimeFactors(1260),
                PrimeFactors(2520),
            },
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.filter(PrimeFactors(1))

    def test_ideal(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.ideal(PrimeFactors(840))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(3),
                PrimeFactors(14),
                PrimeFactors(6),
                PrimeFactors(15),
                PrimeFactors(42),
                PrimeFactors(24),
                PrimeFactors(30),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(120),
                PrimeFactors(840),
            },
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.ideal(PrimeFactors(1))

    def test_filter_ideal(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {PrimeFactors(42), PrimeFactors(210), PrimeFactors(168), PrimeFactors(840)},
        )

    def test_ideal_filter(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .ideal(PrimeFactors(840))
            .filter(PrimeFactors(42))
        )
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {PrimeFactors(42), PrimeFactors(210), PrimeFactors(168), PrimeFactors(840)},
        )

    def test_upper_limit(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for limit in semi_lattice:
            for element in semi_lattice:
                if element <= limit:
                    self.assertTrue(semi_lattice.upper_limit(limit))
                    self.assertIn(element, semi_lattice.upper_limit(limit))
                else:
                    self.assertNotIn(element, semi_lattice.upper_limit(limit))
            self.assertNotIn(2, semi_lattice.upper_limit(limit))
            self.assertEqual(
                set(semi_lattice.upper_limit(limit)),
                {element for element in semi_lattice if element <= limit},
            )
            self.assertEqual(
                len(semi_lattice.upper_limit(limit)),
                len({element for element in semi_lattice if element <= limit}),
            )
            for element in semi_lattice:
                if element < limit:
                    self.assertIn(
                        element,
                        semi_lattice.upper_limit(limit, strict=True),
                    )
                else:
                    self.assertNotIn(
                        element,
                        semi_lattice.upper_limit(limit, strict=True),
                    )
            self.assertNotIn(2, semi_lattice.upper_limit(limit, strict=True))
            self.assertEqual(
                set(semi_lattice.upper_limit(limit, strict=True)),
                {element for element in semi_lattice if element < limit},
            )
            self.assertEqual(
                len(semi_lattice.upper_limit(limit, strict=True)),
                len({element for element in semi_lattice if element < limit}),
            )

    def test_lower_limit(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for limit in semi_lattice:
            for element in semi_lattice:
                if element >= limit:
                    self.assertTrue(semi_lattice.lower_limit(limit))
                    self.assertIn(element, semi_lattice.lower_limit(limit))
                else:
                    self.assertNotIn(element, semi_lattice.lower_limit(limit))
            self.assertNotIn(2, semi_lattice.lower_limit(limit))
            self.assertEqual(
                set(semi_lattice.lower_limit(limit)),
                {element for element in semi_lattice if element >= limit},
            )
            self.assertEqual(
                len(semi_lattice.lower_limit(limit)),
                len({element for element in semi_lattice if element >= limit}),
            )
            for element in semi_lattice:
                if element > limit:
                    self.assertIn(
                        element,
                        semi_lattice.lower_limit(limit, strict=True),
                    )
                else:
                    self.assertNotIn(
                        element,
                        semi_lattice.lower_limit(limit, strict=True),
                    )
            self.assertNotIn(2, semi_lattice.lower_limit(limit, strict=True))
            self.assertEqual(
                set(semi_lattice.lower_limit(limit, strict=True)),
                {element for element in semi_lattice if element > limit},
            )
            self.assertEqual(
                len(semi_lattice.lower_limit(limit, strict=True)),
                len({element for element in semi_lattice if element > limit}),
            )

    def test_join_irreducible(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.join_irreducible),
            [
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertIn(PrimeFactors(36), semi_lattice.join_irreducible)
        self.assertNotIn(PrimeFactors(1), semi_lattice.join_irreducible)
        self.assertNotIn(1, semi_lattice.join_irreducible)

    def test_co_atoms(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors]()
        self.assertEqual(list(semi_lattice.co_atoms), [])
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.co_atoms),
            [
                PrimeFactors(360),
                PrimeFactors(504),
                PrimeFactors(1260),
                PrimeFactors(840),
            ],
        )

        self.assertIn(PrimeFactors(840), semi_lattice.co_atoms)
        self.assertNotIn(PrimeFactors(1), semi_lattice.co_atoms)
        self.assertNotIn(1, semi_lattice.co_atoms)

    def test_greatest_join_irreducible(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(
                semi_lattice.greatest_join_irreducible(
                    PrimeFactors(21),
                    strict=True,
                ),
            ),
            [PrimeFactors(3)],
        )
        self.assertEqual(
            list(
                semi_lattice.greatest_join_irreducible(
                    PrimeFactors(6),
                    strict=True,
                ),
            ),
            [PrimeFactors(3)],
        )
        self.assertEqual(
            list(semi_lattice.greatest_join_irreducible(PrimeFactors(72))),
            [PrimeFactors(24), PrimeFactors(36)],
        )
        self.assertIn(
            PrimeFactors(36),
            semi_lattice.greatest_join_irreducible(PrimeFactors(72)),
        )
        self.assertNotIn(
            PrimeFactors(6),
            semi_lattice.greatest_join_irreducible(PrimeFactors(72)),
        )
        self.assertNotIn(14, semi_lattice.greatest_join_irreducible(PrimeFactors(72)))

    def test_floor(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            semi_lattice.floor(PrimeFactors(12)),
            PrimeFactors(6),
        )
        self.assertEqual(
            semi_lattice.floor(PrimeFactors(28)),
            PrimeFactors(14),
        )
        self.assertIsNone(semi_lattice.floor(PrimeFactors(7)))

    def test_extend(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(6), PrimeFactors(14)],
        )
        semi_lattice.extend(
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            ),
        )
        self.assertEqual(
            list(semi_lattice.join_irreducible),
            [
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(6), PrimeFactors(14)],
        )
        self.assertEqual(
            sorted([int(element) for element in semi_lattice]),
            [3, 6, 14, 42],
        )
        semi_lattice.extend([PrimeFactors(13)])
        self.assertEqual(
            sorted([int(element) for element in semi_lattice]),
            [3, 6, 13, 14, 39, 42, 78, 182, 546],
        )

        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(7),
                PrimeFactors(2 * 7),
                PrimeFactors(3 * 5),
            ],
        )

        self.assertEqual(
            {
                (item.element, frozenset(item.successors), frozenset(item.predecessors))
                for item in semi_lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(7),
                    frozenset({PrimeFactors(21), PrimeFactors(14)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(21),
                    frozenset({PrimeFactors(105), PrimeFactors(42)}),
                    frozenset({PrimeFactors(3), PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(14),
                    frozenset({PrimeFactors(42)}),
                    frozenset({PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(21), PrimeFactors(15)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210)}),
                    frozenset({PrimeFactors(21), PrimeFactors(14)}),
                ),
                (
                    PrimeFactors(15),
                    frozenset({PrimeFactors(105)}),
                    frozenset({PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(105),
                    frozenset({PrimeFactors(210)}),
                    frozenset({PrimeFactors(21), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(210),
                    frozenset(),
                    frozenset({PrimeFactors(105), PrimeFactors(42)}),
                ),
            },
        )

        semi_lattice.extend([PrimeFactors(2 * 11)])
        self.assertEqual(
            {
                (item.element, frozenset(item.successors), frozenset(item.predecessors))
                for item in semi_lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(66), PrimeFactors(21), PrimeFactors(15)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(15),
                    frozenset({PrimeFactors(105), PrimeFactors(330)}),
                    frozenset({PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(14),
                    frozenset({PrimeFactors(42), PrimeFactors(154)}),
                    frozenset({PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(154),
                    frozenset({PrimeFactors(462)}),
                    frozenset({PrimeFactors(22), PrimeFactors(14)}),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210), PrimeFactors(462)}),
                    frozenset({PrimeFactors(21), PrimeFactors(14)}),
                ),
                (
                    PrimeFactors(22),
                    frozenset({PrimeFactors(154), PrimeFactors(66)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(66),
                    frozenset({PrimeFactors(330), PrimeFactors(462)}),
                    frozenset({PrimeFactors(3), PrimeFactors(22)}),
                ),
                (
                    PrimeFactors(330),
                    frozenset({PrimeFactors(2310)}),
                    frozenset({PrimeFactors(66), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(7),
                    frozenset({PrimeFactors(21), PrimeFactors(14)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(21),
                    frozenset({PrimeFactors(105), PrimeFactors(42)}),
                    frozenset({PrimeFactors(3), PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(105),
                    frozenset({PrimeFactors(210)}),
                    frozenset({PrimeFactors(21), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(462),
                    frozenset({PrimeFactors(2310)}),
                    frozenset({PrimeFactors(66), PrimeFactors(42), PrimeFactors(154)}),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(2310)}),
                    frozenset({PrimeFactors(105), PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(2310),
                    frozenset(),
                    frozenset(
                        {PrimeFactors(210), PrimeFactors(330), PrimeFactors(462)},
                    ),
                ),
            },
        )

        semi_lattice.extend([PrimeFactors(11)])
        self.assertEqual(
            {
                (item.element, frozenset(item.successors), frozenset(item.predecessors))
                for item in semi_lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(15),
                    frozenset({PrimeFactors(105), PrimeFactors(165)}),
                    frozenset({PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(22),
                    frozenset({PrimeFactors(154), PrimeFactors(66)}),
                    frozenset({PrimeFactors(11)}),
                ),
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(33), PrimeFactors(21), PrimeFactors(15)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(33),
                    frozenset({PrimeFactors(66), PrimeFactors(165), PrimeFactors(231)}),
                    frozenset({PrimeFactors(11), PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(165),
                    frozenset({PrimeFactors(330), PrimeFactors(1155)}),
                    frozenset({PrimeFactors(33), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(66),
                    frozenset({PrimeFactors(330), PrimeFactors(462)}),
                    frozenset({PrimeFactors(33), PrimeFactors(22)}),
                ),
                (
                    PrimeFactors(330),
                    frozenset({PrimeFactors(2310)}),
                    frozenset({PrimeFactors(66), PrimeFactors(165)}),
                ),
                (
                    PrimeFactors(21),
                    frozenset({PrimeFactors(105), PrimeFactors(42), PrimeFactors(231)}),
                    frozenset({PrimeFactors(3), PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(11),
                    frozenset({PrimeFactors(33), PrimeFactors(77), PrimeFactors(22)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(7),
                    frozenset({PrimeFactors(77), PrimeFactors(21), PrimeFactors(14)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(77),
                    frozenset({PrimeFactors(154), PrimeFactors(231)}),
                    frozenset({PrimeFactors(11), PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(231),
                    frozenset({PrimeFactors(1155), PrimeFactors(462)}),
                    frozenset({PrimeFactors(33), PrimeFactors(77), PrimeFactors(21)}),
                ),
                (
                    PrimeFactors(105),
                    frozenset({PrimeFactors(210), PrimeFactors(1155)}),
                    frozenset({PrimeFactors(21), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(1155),
                    frozenset({PrimeFactors(2310)}),
                    frozenset(
                        {PrimeFactors(105), PrimeFactors(165), PrimeFactors(231)},
                    ),
                ),
                (
                    PrimeFactors(14),
                    frozenset({PrimeFactors(42), PrimeFactors(154)}),
                    frozenset({PrimeFactors(7)}),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210), PrimeFactors(462)}),
                    frozenset({PrimeFactors(21), PrimeFactors(14)}),
                ),
                (
                    PrimeFactors(154),
                    frozenset({PrimeFactors(462)}),
                    frozenset({PrimeFactors(77), PrimeFactors(14), PrimeFactors(22)}),
                ),
                (
                    PrimeFactors(462),
                    frozenset({PrimeFactors(2310)}),
                    frozenset(
                        {
                            PrimeFactors(42),
                            PrimeFactors(154),
                            PrimeFactors(66),
                            PrimeFactors(231),
                        },
                    ),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(2310)}),
                    frozenset({PrimeFactors(105), PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(2310),
                    frozenset(),
                    frozenset(
                        {
                            PrimeFactors(210),
                            PrimeFactors(1155),
                            PrimeFactors(330),
                            PrimeFactors(462),
                        },
                    ),
                ),
            },
        )
