"""The :mod:`test_meet_semi_lattice_element` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import infimum, infimum_generators


class MeetableTest(TestCase):
    def test__or(self):
        a = PrimeFactors(32)
        b = PrimeFactors(56)
        self.assertEqual(
            a & b,
            PrimeFactors(8),
            "The least common multiple of 32 and 56 is 8",
        )
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            _ = a & 5

    def test_meet(self):
        a = PrimeFactors(32)
        b = PrimeFactors(56)
        c = PrimeFactors(88)
        self.assertEqual(
            a.meet(b, c),
            PrimeFactors(8),
            "The least common multiple of 32, 56 and 88 is 8",
        )

        self.assertEqual(
            infimum([a, b, c]),
            PrimeFactors(8),
            "The least common multiple of 32, 56 and 88 is 8",
        )

    def test_infimum_generators(self):
        self.assertEqual(
            set(
                infimum_generators(
                    [PrimeFactors(32), PrimeFactors(56), PrimeFactors(8)],
                ),
            ),
            {PrimeFactors(32), PrimeFactors(56)},
        )
