"""The :mod:`test_meet_semi_lattice` module."""

import copy
from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import (
    ExtensibleFiniteMeetSemiLattice,
    FrozenFiniteMeetSemiLattice,
)


class FiniteMeetSemiLatticeTestCase(TestCase):
    def test___init__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertFalse(semi_lattice)
        self.assertEqual(list(semi_lattice), [])
        self.assertEqual(len(semi_lattice), 0)

        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(96)],
        )
        self.assertTrue(semi_lattice)
        self.assertEqual(
            semi_lattice,
            {PrimeFactors(96)},
        )
        self.assertEqual(len(semi_lattice), 1)

        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(96),
                PrimeFactors(72),
            ],
        )
        self.assertEqual(
            semi_lattice,
            {PrimeFactors(96), PrimeFactors(72), PrimeFactors(24)},
        )

        semi_lattice.extend([PrimeFactors(3)])
        semi_lattice.extend([PrimeFactors(6)])
        semi_lattice.extend([PrimeFactors(24)])
        semi_lattice.extend([PrimeFactors(36)])
        semi_lattice.extend([PrimeFactors(18)])
        self.assertEqual(
            set(semi_lattice),
            {
                PrimeFactors(96),
                PrimeFactors(72),
                PrimeFactors(36),
                PrimeFactors(24),
                PrimeFactors(18),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(3),
            },
        )

        self.assertEqual(
            len(
                ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                    elements=[PrimeFactors(1), PrimeFactors(2)],
                ),
            ),
            2,
        )
        self.assertEqual(
            len(
                ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                    elements=[PrimeFactors(1)],
                ),
            ),
            1,
        )

    def test___eq__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertNotEqual(semi_lattice, 1)

    def test___hash__(self):
        semi_lattice1 = FrozenFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        semi_lattice2 = FrozenFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(36),
                PrimeFactors(14),
                PrimeFactors(18),
                PrimeFactors(6),
                PrimeFactors(3),
                PrimeFactors(15),
                PrimeFactors(24),
            ],
        )
        self.assertEqual(hash(semi_lattice1), hash(semi_lattice2))

    def test___copy__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(semi_lattice, copy.copy(semi_lattice))
        self.assertEqual(semi_lattice, semi_lattice.copy())

    def test___contains__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertNotIn(2, semi_lattice)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertNotIn(2, semi_lattice)
        self.assertIn(PrimeFactors(3), semi_lattice)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertNotIn(2, semi_lattice)
        self.assertIn(PrimeFactors(2), semi_lattice)
        self.assertIn(PrimeFactors(3), semi_lattice)
        self.assertIn(PrimeFactors(1), semi_lattice)

    def test___len__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertEqual(len(semi_lattice), 0)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(len(semi_lattice), 1)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(len(semi_lattice), 3)

    def test___iter__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertEqual(list(semi_lattice), [])
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(list(semi_lattice), [PrimeFactors(3)])
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(
            semi_lattice,
            {
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(1),
            },
        )

    def test___lt__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertLess(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLess(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )

    def test___gt__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertGreater(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](),
        )
        self.assertGreater(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](),
        )

    def test___le__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertLessEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](),
        )
        self.assertLessEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )

        self.assertLessEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLessEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](),
        )

    def test___ge__(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertGreaterEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](),
        )
        self.assertGreaterEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[PrimeFactors(3)],
            ),
        )
        self.assertGreaterEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](),
        )
        self.assertGreaterEqual(
            semi_lattice,
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[PrimeFactors(3)],
            ),
        )

    def test___or__(self):
        semi_lattice1 = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        semi_lattice2 = semi_lattice1 | [PrimeFactors(2)]
        self.assertLessEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)],
        )

    def test___ior__(self):
        semi_lattice1 = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        semi_lattice1 |= [PrimeFactors(2)]
        self.assertEqual(
            list(semi_lattice1),
            [PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)],
        )

    def test___and__(self):
        semi_lattice1 = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(2), PrimeFactors(5)],
        )
        semi_lattice2 = semi_lattice1 & [
            PrimeFactors(2),
            PrimeFactors(5),
            PrimeFactors(7),
        ]
        self.assertGreaterEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(5), PrimeFactors(2), PrimeFactors(1)],
        )

    def test_join(self):
        semi_lattice1 = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        semi_lattice2 = semi_lattice1.join(
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](elements=[PrimeFactors(2)]),
        )
        self.assertLessEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)],
        )

    def test_meet(self):
        semi_lattice1 = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(2), PrimeFactors(5)],
        )
        semi_lattice2 = semi_lattice1.meet(
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[PrimeFactors(2), PrimeFactors(5), PrimeFactors(7)],
            ),
        )
        self.assertGreaterEqual(semi_lattice1, semi_lattice2)
        self.assertEqual(
            list(semi_lattice2),
            [PrimeFactors(2), PrimeFactors(5), PrimeFactors(1)],
        )

    def test_isdisjoint(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertTrue(semi_lattice.isdisjoint([]))
        self.assertTrue(semi_lattice.isdisjoint([PrimeFactors(4)]))
        self.assertFalse(semi_lattice.isdisjoint([PrimeFactors(2)]))
        self.assertFalse(semi_lattice.isdisjoint([PrimeFactors(3)]))
        self.assertFalse(semi_lattice.isdisjoint([PrimeFactors(1)]))

    def test_issubset(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertFalse(semi_lattice.issubset([]))
        self.assertFalse(semi_lattice.issubset([PrimeFactors(4)]))
        self.assertFalse(semi_lattice.issubset([PrimeFactors(2), PrimeFactors(3)]))
        self.assertTrue(
            semi_lattice.issubset([PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)]),
        )

    def test_issuperset(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertTrue(semi_lattice.issuperset([]))
        self.assertFalse(semi_lattice.issuperset([PrimeFactors(4)]))
        self.assertTrue(semi_lattice.issuperset([PrimeFactors(2), PrimeFactors(3)]))
        self.assertTrue(
            semi_lattice.issuperset(
                [PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)],
            ),
        )
        self.assertFalse(
            semi_lattice.issuperset(
                [
                    PrimeFactors(2),
                    PrimeFactors(3),
                    PrimeFactors(1),
                    PrimeFactors(4),
                ],
            ),
        )

    def test_maximum(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertIsNone(semi_lattice.maximum)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
            ],
        )
        self.assertEqual(semi_lattice.maximum, PrimeFactors(2))
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertIsNone(semi_lattice.maximum)

    def test_order(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
            ],
        )
        self.assertEqual(len(semi_lattice.order), 12)
        self.assertEqual(
            set(semi_lattice.order),
            {
                (PrimeFactors(14), PrimeFactors(14)),
                (PrimeFactors(6), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(14)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(2)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(14)),
                (PrimeFactors(1), PrimeFactors(6)),
                (PrimeFactors(1), PrimeFactors(2)),
                (PrimeFactors(1), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(1)),
            },
        )
        for couple in semi_lattice.order:
            self.assertIn(couple, semi_lattice.order)
        self.assertNotIn(2, semi_lattice.order)
        self.assertNotIn((PrimeFactors(3), PrimeFactors(7)), semi_lattice.order)

    def test_order_universes(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            semi_lattice.order.universes,
            (semi_lattice.order.domain, semi_lattice.order.domain),
        )
        self.assertEqual(
            set(semi_lattice.order.domain),
            {PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)},
        )
        self.assertEqual(
            set(semi_lattice.order.co_domain),
            {PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)},
        )

    def test_order_successors(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = semi_lattice.order.successors(3)
        self.assertEqual(
            set(semi_lattice.order.successors(PrimeFactors(12))),
            {
                PrimeFactors(24),
                PrimeFactors(36),
                PrimeFactors(12),
            },
        )
        self.assertEqual(
            len(semi_lattice.order.successors(PrimeFactors(12))),
            3,
        )

        self.assertIn(PrimeFactors(36), semi_lattice.order.successors(PrimeFactors(12)))
        self.assertNotIn(6, semi_lattice.order.successors(PrimeFactors(12)))

    def test_order_predecessors(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = semi_lattice.order.predecessors(3)
        self.assertEqual(
            list(semi_lattice.order.predecessors(PrimeFactors(12))),
            [
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(3),
                PrimeFactors(2),
                PrimeFactors(1),
            ],
        )

        self.assertIn(
            PrimeFactors(3),
            semi_lattice.order.predecessors(PrimeFactors(12)),
        )
        self.assertNotIn(1, semi_lattice.order.predecessors(PrimeFactors(12)))

    def test_order_sinks_sources(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(semi_lattice.order.sinks),
            {
                PrimeFactors(36),
                PrimeFactors(24),
                PrimeFactors(15),
                PrimeFactors(14),
            },
        )
        self.assertEqual(
            set(semi_lattice.order.sources),
            {
                PrimeFactors(1),
            },
        )

    def test_order_domain(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(semi_lattice.order.domain),
            {
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
                PrimeFactors(36),
                PrimeFactors(18),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(3),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

        self.assertEqual(len(semi_lattice.order.domain), 10)

        self.assertIn(PrimeFactors(3), semi_lattice.order.domain)
        self.assertIn(PrimeFactors(6), semi_lattice.order.domain)
        self.assertNotIn(PrimeFactors(11), semi_lattice.order.domain)

    def test_cover(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertNotIn(2, semi_lattice.cover)
        self.assertEqual(len(semi_lattice.cover), 0)
        self.assertEqual(list(semi_lattice.cover), [])

        semi_lattice.extend([PrimeFactors(2)])
        self.assertNotIn(2, semi_lattice.cover)
        self.assertNotIn((2, 3), semi_lattice.cover)
        self.assertIn((PrimeFactors(1), PrimeFactors(2)), semi_lattice.cover)
        self.assertIn((PrimeFactors(1), PrimeFactors(3)), semi_lattice.cover)
        self.assertEqual(len(semi_lattice.cover), 2)
        self.assertEqual(
            set(semi_lattice.cover),
            {
                (PrimeFactors(1), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(2)),
            },
        )

        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(semi_lattice.cover),
            {
                (PrimeFactors(18), PrimeFactors(36)),
                (PrimeFactors(12), PrimeFactors(24)),
                (PrimeFactors(12), PrimeFactors(36)),
                (PrimeFactors(6), PrimeFactors(18)),
                (PrimeFactors(6), PrimeFactors(12)),
                (PrimeFactors(3), PrimeFactors(15)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(14)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(1), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(2)),
            },
        )
        for couple in semi_lattice.cover:
            self.assertIn(couple, semi_lattice.cover)
        relation = set(semi_lattice.cover)
        for couple in semi_lattice.order:
            if couple in relation:
                self.assertIn(couple, semi_lattice.cover)
            else:
                self.assertNotIn(couple, semi_lattice.cover)

    def test_cover_universes(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            semi_lattice.cover.universes,
            (semi_lattice.cover.domain, semi_lattice.cover.domain),
        )
        self.assertEqual(
            set(semi_lattice.cover.domain),
            {PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)},
        )
        self.assertEqual(
            set(semi_lattice.cover.co_domain),
            {PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)},
        )

    def test_cover_successors(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.cover.successors(PrimeFactors(25))
        self.assertEqual(
            set(semi_lattice.cover.successors(PrimeFactors(12))),
            {PrimeFactors(24), PrimeFactors(36)},
        )
        self.assertTrue(semi_lattice.cover.successors(PrimeFactors(12)))
        self.assertEqual(
            len(semi_lattice.cover.successors(PrimeFactors(12))),
            2,
        )
        self.assertIn(PrimeFactors(36), semi_lattice.cover.successors(PrimeFactors(12)))
        self.assertNotIn(
            PrimeFactors(3),
            semi_lattice.cover.successors(PrimeFactors(12)),
        )
        self.assertNotIn(3, semi_lattice.cover.successors(PrimeFactors(12)))

    def test_cover_predecessors(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.cover.predecessors(PrimeFactors(25))
        self.assertEqual(
            set(semi_lattice.cover.predecessors(PrimeFactors(12))),
            {PrimeFactors(6)},
        )
        self.assertTrue(semi_lattice.cover.predecessors(PrimeFactors(12)))

        for neighbourhood in semi_lattice.cover.neighbourhoods():
            self.assertNotIn(2, semi_lattice.cover.predecessors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.predecessors),
                len(semi_lattice.cover.predecessors(neighbourhood.element)),
            )
            if len(neighbourhood.predecessors) > 0:
                self.assertTrue(neighbourhood.predecessors)
            self.assertEqual(
                set(neighbourhood.predecessors),
                set(semi_lattice.cover.predecessors(neighbourhood.element)),
            )
            for element in semi_lattice:
                if element in neighbourhood.predecessors:
                    self.assertIn(
                        element,
                        semi_lattice.cover.predecessors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        semi_lattice.cover.predecessors(neighbourhood.element),
                    )

    def test_cover_sinks_sources(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(semi_lattice.order.sinks),
            {
                PrimeFactors(36),
                PrimeFactors(24),
                PrimeFactors(15),
                PrimeFactors(14),
            },
        )
        self.assertEqual(
            set(semi_lattice.order.sources),
            {
                PrimeFactors(1),
            },
        )

    def test_cover_neighbourhoods(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in semi_lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(14),
                    frozenset(),
                    frozenset({PrimeFactors(2)}),
                ),
                (
                    PrimeFactors(15),
                    frozenset(),
                    frozenset({PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(24),
                    frozenset(),
                    frozenset({PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(36),
                    frozenset(),
                    frozenset({PrimeFactors(18), PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(18),
                    frozenset({PrimeFactors(36)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(12),
                    frozenset({PrimeFactors(24), PrimeFactors(36)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(6),
                    frozenset({PrimeFactors(18), PrimeFactors(12)}),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(6), PrimeFactors(15)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(2),
                    frozenset({PrimeFactors(14), PrimeFactors(6)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(1),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                    frozenset(),
                ),
            },
        )
        for neighbourhood in semi_lattice.cover.neighbourhoods():
            for successor in neighbourhood.successors:
                self.assertTrue(neighbourhood.successors)
                self.assertIn(successor, neighbourhood.successors)
            for predecessor in neighbourhood.predecessors:
                self.assertTrue(neighbourhood.predecessors)
                self.assertIn(predecessor, neighbourhood.predecessors)

    def test_top(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertEqual(list(semi_lattice.top), [])
        self.assertEqual(len(semi_lattice.top), 0)
        self.assertNotIn(1, semi_lattice.top)
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.top),
            [
                PrimeFactors(36),
                PrimeFactors(24),
                PrimeFactors(15),
                PrimeFactors(14),
            ],
        )

        self.assertIn(PrimeFactors(36), semi_lattice.top)
        self.assertNotIn(PrimeFactors(6), semi_lattice.top)
        self.assertNotIn(6, semi_lattice.top)

    def test_bottom(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.bottom),
            [PrimeFactors(1)],
        )

        self.assertIn(PrimeFactors(1), semi_lattice.bottom)
        self.assertNotIn(PrimeFactors(6), semi_lattice.bottom)
        self.assertNotIn(6, semi_lattice.bottom)

    def test_filter(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(6))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(24),
                PrimeFactors(36),
                PrimeFactors(18),
                PrimeFactors(12),
                PrimeFactors(6),
            },
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.filter(PrimeFactors(5))

    def test_ideal(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.ideal(PrimeFactors(24))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(24),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(3),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )
        with self.assertRaises(ValueError):
            _ = semi_lattice.ideal(PrimeFactors(5))

    def test_filter_ideal(self):
        bounded = (
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(6))
            .ideal(PrimeFactors(24))
        )
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(24),
                PrimeFactors(12),
                PrimeFactors(6),
            },
        )

    def test_upper_limit(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for limit in semi_lattice:
            for element in semi_lattice:
                if element <= limit:
                    self.assertTrue(semi_lattice.upper_limit(limit))
                    self.assertIn(element, semi_lattice.upper_limit(limit))
                else:
                    self.assertNotIn(element, semi_lattice.upper_limit(limit))
            self.assertNotIn(2, semi_lattice.upper_limit(limit))
            self.assertEqual(
                set(semi_lattice.upper_limit(limit)),
                {element for element in semi_lattice if element <= limit},
            )
            self.assertEqual(
                len(semi_lattice.upper_limit(limit)),
                len({element for element in semi_lattice if element <= limit}),
            )
            for element in semi_lattice:
                if element < limit:
                    self.assertIn(
                        element,
                        semi_lattice.upper_limit(limit, strict=True),
                    )
                else:
                    self.assertNotIn(
                        element,
                        semi_lattice.upper_limit(limit, strict=True),
                    )
            self.assertNotIn(2, semi_lattice.upper_limit(limit, strict=True))
            self.assertEqual(
                set(semi_lattice.upper_limit(limit, strict=True)),
                {element for element in semi_lattice if element < limit},
            )
            self.assertEqual(
                len(semi_lattice.upper_limit(limit, strict=True)),
                len({element for element in semi_lattice if element < limit}),
            )

    def test_lower_limit(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for limit in semi_lattice:
            for element in semi_lattice:
                if element >= limit:
                    self.assertTrue(semi_lattice.lower_limit(limit))
                    self.assertIn(element, semi_lattice.lower_limit(limit))
                else:
                    self.assertNotIn(element, semi_lattice.lower_limit(limit))
            self.assertNotIn(2, semi_lattice.lower_limit(limit))
            self.assertEqual(
                set(semi_lattice.lower_limit(limit)),
                {element for element in semi_lattice if element >= limit},
            )
            self.assertEqual(
                len(semi_lattice.lower_limit(limit)),
                len({element for element in semi_lattice if element >= limit}),
            )
            for element in semi_lattice:
                if element > limit:
                    self.assertIn(
                        element,
                        semi_lattice.lower_limit(limit, strict=True),
                    )
                else:
                    self.assertNotIn(
                        element,
                        semi_lattice.lower_limit(limit, strict=True),
                    )
            self.assertNotIn(2, semi_lattice.lower_limit(limit, strict=True))
            self.assertEqual(
                set(semi_lattice.lower_limit(limit, strict=True)),
                {element for element in semi_lattice if element > limit},
            )
            self.assertEqual(
                len(semi_lattice.lower_limit(limit, strict=True)),
                len({element for element in semi_lattice if element > limit}),
            )

    def test_meet_irreducible(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.meet_irreducible),
            [
                PrimeFactors(18),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertIn(PrimeFactors(36), semi_lattice.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), semi_lattice.meet_irreducible)
        self.assertNotIn(1, semi_lattice.meet_irreducible)

    def test_atoms(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors]()
        self.assertEqual(list(semi_lattice.atoms), [])
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(semi_lattice.atoms),
            [PrimeFactors(3), PrimeFactors(2)],
        )

        self.assertIn(PrimeFactors(3), semi_lattice.atoms)
        self.assertNotIn(PrimeFactors(1), semi_lattice.atoms)
        self.assertNotIn(1, semi_lattice.atoms)

    def test_smallest_meet_irreducible(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(
                semi_lattice.smallest_meet_irreducible(
                    PrimeFactors(6),
                ),
            ),
            [PrimeFactors(18), PrimeFactors(24)],
        )
        self.assertEqual(
            list(
                semi_lattice.smallest_meet_irreducible(
                    PrimeFactors(6),
                    strict=True,
                ),
            ),
            [PrimeFactors(18), PrimeFactors(24)],
        )
        self.assertEqual(
            list(semi_lattice.smallest_meet_irreducible(PrimeFactors(18))),
            [PrimeFactors(18)],
        )
        self.assertIn(
            PrimeFactors(24),
            semi_lattice.smallest_meet_irreducible(PrimeFactors(6)),
        )
        self.assertNotIn(
            PrimeFactors(6),
            semi_lattice.smallest_meet_irreducible(PrimeFactors(6)),
        )
        self.assertNotIn(14, semi_lattice.smallest_meet_irreducible(PrimeFactors(6)))

    def test_ceil(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            semi_lattice.ceil(PrimeFactors(6)),
            PrimeFactors(6),
        )
        self.assertEqual(
            semi_lattice.ceil(PrimeFactors(7)),
            PrimeFactors(14),
        )
        self.assertIsNone(semi_lattice.ceil(PrimeFactors(42)))

    def test_extend(self):
        semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(6), PrimeFactors(14)],
        )
        semi_lattice.extend(
            ExtensibleFiniteMeetSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            ),
        )
        self.assertEqual(
            list(semi_lattice.meet_irreducible),
            [
                PrimeFactors(18),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
