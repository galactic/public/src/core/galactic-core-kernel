"""The :mod:`test_lattice_view` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import ExtensibleFiniteLattice, FiniteLatticeView
from galactic.algebras.poset import bottom, top


class BoundedLatticeTestCase(TestCase):
    # noinspection PyTypeChecker
    def test___init__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        view = FiniteLatticeView[PrimeFactors](lattice)
        self.assertEqual(set(lattice), set(view))

        bounded = lattice.ideal(PrimeFactors(42))
        self.assertEqual(
            set(bounded),
            {element for element in lattice if element <= PrimeFactors(42)},
        )
        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded),
            {element for element in lattice if element >= PrimeFactors(42)},
        )

    # noinspection PyTypeChecker
    def test___contains__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.ideal(PrimeFactors(42))
        elements = set(bounded)
        for element in lattice:
            if element in elements:
                self.assertIn(element, bounded)
            else:
                self.assertNotIn(element, bounded)
        bounded = lattice.filter(PrimeFactors(42))
        elements = set(bounded)
        for element in lattice:
            if element in elements:
                self.assertIn(element, bounded)
            else:
                self.assertNotIn(element, bounded)

    # noinspection PyTypeChecker
    def test___len__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.ideal(PrimeFactors(42))
        elements = set(bounded)
        self.assertEqual(len(elements), len(bounded))
        bounded = lattice.filter(PrimeFactors(42))
        elements = set(bounded)
        self.assertEqual(len(elements), len(bounded))

    def test___iter__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                element
                for element in lattice
                if PrimeFactors(840) >= element >= PrimeFactors(42)
            },
        )

    def test___lt__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertLess(
            bounded,
            lattice,
        )

    def test___gt__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        # noinspection PyTypeChecker
        self.assertGreater(
            lattice.filter(PrimeFactors(42)),
            bounded,
        )
        # noinspection PyTypeChecker
        self.assertGreater(
            lattice.ideal(PrimeFactors(840)),
            bounded,
        )

    def test_minimum(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(bounded.minimum, PrimeFactors(42))
        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(bounded.minimum, PrimeFactors(1))

    def test_maximum(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(bounded.maximum, PrimeFactors(840))
        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(bounded.maximum, PrimeFactors(2520))

    def test_order(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(len(bounded.order), 18)
        self.assertEqual(
            set(bounded.order),
            {
                (PrimeFactors(840), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(168)),
                (PrimeFactors(420), PrimeFactors(840)),
                (PrimeFactors(420), PrimeFactors(420)),
                (PrimeFactors(84), PrimeFactors(840)),
                (PrimeFactors(84), PrimeFactors(168)),
                (PrimeFactors(84), PrimeFactors(420)),
                (PrimeFactors(84), PrimeFactors(84)),
                (PrimeFactors(210), PrimeFactors(840)),
                (PrimeFactors(210), PrimeFactors(420)),
                (PrimeFactors(210), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(840)),
                (PrimeFactors(42), PrimeFactors(168)),
                (PrimeFactors(42), PrimeFactors(420)),
                (PrimeFactors(42), PrimeFactors(84)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(42)),
            },
        )
        for couple in bounded.order:
            self.assertIn(couple, bounded.order)
        self.assertNotIn(2, bounded.order)
        self.assertNotIn((PrimeFactors(3), PrimeFactors(7)), bounded.order)

    def test_order_universes(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            bounded.order.universes,
            (bounded.order.domain, bounded.order.domain),
        )
        self.assertEqual(
            set(bounded.order.domain),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(840),
            },
        )
        self.assertEqual(
            set(bounded.order.co_domain),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(840),
            },
        )

    def test_order_successors(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.order.successors(PrimeFactors(42))),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(840),
            },
        )
        self.assertEqual(
            len(bounded.order.successors(PrimeFactors(42))),
            6,
        )

        self.assertIn(PrimeFactors(210), bounded.order.successors(PrimeFactors(42)))
        self.assertNotIn(6, bounded.order.successors(PrimeFactors(42)))

    def test_order_predecessors(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.order.predecessors(PrimeFactors(210))),
            {
                PrimeFactors(42),
                PrimeFactors(210),
            },
        )
        self.assertEqual(
            len(bounded.order.predecessors(PrimeFactors(210))),
            2,
        )

        self.assertIn(PrimeFactors(210), bounded.order.predecessors(PrimeFactors(210)))
        self.assertIn(PrimeFactors(42), bounded.order.predecessors(PrimeFactors(210)))
        self.assertNotIn(6, bounded.order.predecessors(PrimeFactors(210)))

    def test_order_sinks_sources(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )

        self.assertEqual(
            set(bounded.order.sinks),
            {PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.order.sources),
            {PrimeFactors(42)},
        )

    def test_order_domain(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.order.domain),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(840),
            },
        )

        self.assertEqual(len(bounded.order.domain), 6)

        self.assertIn(PrimeFactors(42), bounded.order.domain)
        self.assertIn(PrimeFactors(210), bounded.order.domain)
        self.assertIn(PrimeFactors(840), bounded.order.domain)
        self.assertNotIn(PrimeFactors(11), bounded.order.domain)

    def test_cover(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(840), PrimeFactors(2520)),
                (PrimeFactors(504), PrimeFactors(2520)),
                (PrimeFactors(1260), PrimeFactors(2520)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(504)),
                (PrimeFactors(420), PrimeFactors(840)),
                (PrimeFactors(420), PrimeFactors(1260)),
                (PrimeFactors(252), PrimeFactors(504)),
                (PrimeFactors(252), PrimeFactors(1260)),
                (PrimeFactors(630), PrimeFactors(1260)),
                (PrimeFactors(84), PrimeFactors(168)),
                (PrimeFactors(84), PrimeFactors(420)),
                (PrimeFactors(84), PrimeFactors(252)),
                (PrimeFactors(210), PrimeFactors(420)),
                (PrimeFactors(210), PrimeFactors(630)),
                (PrimeFactors(126), PrimeFactors(252)),
                (PrimeFactors(126), PrimeFactors(630)),
                (PrimeFactors(42), PrimeFactors(84)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(126)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(120), PrimeFactors(840)),
                (PrimeFactors(420), PrimeFactors(840)),
                (PrimeFactors(24), PrimeFactors(168)),
                (PrimeFactors(24), PrimeFactors(120)),
                (PrimeFactors(84), PrimeFactors(168)),
                (PrimeFactors(84), PrimeFactors(420)),
                (PrimeFactors(60), PrimeFactors(120)),
                (PrimeFactors(60), PrimeFactors(420)),
                (PrimeFactors(210), PrimeFactors(420)),
                (PrimeFactors(12), PrimeFactors(24)),
                (PrimeFactors(12), PrimeFactors(84)),
                (PrimeFactors(12), PrimeFactors(60)),
                (PrimeFactors(42), PrimeFactors(84)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(30), PrimeFactors(60)),
                (PrimeFactors(30), PrimeFactors(210)),
                (PrimeFactors(6), PrimeFactors(12)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(30)),
                (PrimeFactors(14), PrimeFactors(42)),
                (PrimeFactors(15), PrimeFactors(30)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(14)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(15)),
                (PrimeFactors(1), PrimeFactors(2)),
                (PrimeFactors(1), PrimeFactors(3)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(420), PrimeFactors(840)),
                (PrimeFactors(84), PrimeFactors(168)),
                (PrimeFactors(84), PrimeFactors(420)),
                (PrimeFactors(210), PrimeFactors(420)),
                (PrimeFactors(42), PrimeFactors(84)),
                (PrimeFactors(42), PrimeFactors(210)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

    def test_cover_universes(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            bounded.cover.universes,
            (bounded.cover.domain, bounded.cover.domain),
        )
        self.assertEqual(
            set(bounded.cover.domain),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(840),
            },
        )
        self.assertEqual(
            set(bounded.cover.co_domain),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(840),
            },
        )

    def test_cover_successors(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.cover.successors(PrimeFactors(42))),
            {PrimeFactors(210), PrimeFactors(84)},
        )
        self.assertEqual(
            len(bounded.cover.successors(PrimeFactors(42))),
            2,
        )
        self.assertIn(PrimeFactors(210), bounded.cover.successors(PrimeFactors(42)))
        self.assertNotIn(PrimeFactors(3), bounded.cover.successors(PrimeFactors(42)))
        self.assertNotIn(3, bounded.cover.successors(PrimeFactors(42)))

    # noinspection PyTypeChecker
    def test_cover_predecessors(self):
        bounded = (
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.cover.predecessors(PrimeFactors(840))),
            {PrimeFactors(168), PrimeFactors(420)},
        )
        self.assertEqual(
            len(bounded.cover.predecessors(PrimeFactors(840))),
            2,
        )
        self.assertIn(PrimeFactors(420), bounded.cover.predecessors(PrimeFactors(840)))
        self.assertNotIn(PrimeFactors(3), bounded.cover.predecessors(PrimeFactors(840)))
        self.assertNotIn(3, bounded.cover.predecessors(PrimeFactors(840)))

        for neighbourhood in bounded.cover.neighbourhoods():
            self.assertNotIn(2, bounded.cover.predecessors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.predecessors),
                len(bounded.cover.predecessors(neighbourhood.element)),
            )
            self.assertEqual(
                set(neighbourhood.predecessors),
                set(bounded.cover.predecessors(neighbourhood.element)),
            )
            for element in bounded:
                if element in neighbourhood.predecessors:
                    self.assertIn(
                        element,
                        bounded.cover.predecessors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        bounded.cover.predecessors(neighbourhood.element),
                    )

    def test_cover_sinks_sources(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(2520)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(42)},
        )

        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(1)},
        )

        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(42)},
        )

    def test_cover_neighbourhoods(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(2520),
                    frozenset(),
                    frozenset(
                        {PrimeFactors(840), PrimeFactors(504), PrimeFactors(1260)},
                    ),
                ),
                (
                    PrimeFactors(840),
                    frozenset({PrimeFactors(2520)}),
                    frozenset({PrimeFactors(168), PrimeFactors(420)}),
                ),
                (
                    PrimeFactors(504),
                    frozenset({PrimeFactors(2520)}),
                    frozenset({PrimeFactors(168), PrimeFactors(252)}),
                ),
                (
                    PrimeFactors(1260),
                    frozenset({PrimeFactors(2520)}),
                    frozenset(
                        {PrimeFactors(420), PrimeFactors(252), PrimeFactors(630)},
                    ),
                ),
                (
                    PrimeFactors(168),
                    frozenset({PrimeFactors(840), PrimeFactors(504)}),
                    frozenset({PrimeFactors(84)}),
                ),
                (
                    PrimeFactors(420),
                    frozenset({PrimeFactors(840), PrimeFactors(1260)}),
                    frozenset({PrimeFactors(210), PrimeFactors(84)}),
                ),
                (
                    PrimeFactors(252),
                    frozenset({PrimeFactors(504), PrimeFactors(1260)}),
                    frozenset({PrimeFactors(84), PrimeFactors(126)}),
                ),
                (
                    PrimeFactors(630),
                    frozenset({PrimeFactors(1260)}),
                    frozenset({PrimeFactors(210), PrimeFactors(126)}),
                ),
                (
                    PrimeFactors(84),
                    frozenset(
                        {PrimeFactors(168), PrimeFactors(420), PrimeFactors(252)},
                    ),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(420), PrimeFactors(630)}),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(126),
                    frozenset({PrimeFactors(252), PrimeFactors(630)}),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210), PrimeFactors(84), PrimeFactors(126)}),
                    frozenset(),
                ),
            },
        )

        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(840),
                    frozenset(),
                    frozenset(
                        {PrimeFactors(168), PrimeFactors(420), PrimeFactors(120)},
                    ),
                ),
                (
                    PrimeFactors(168),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(24), PrimeFactors(84)}),
                ),
                (
                    PrimeFactors(120),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(24), PrimeFactors(60)}),
                ),
                (
                    PrimeFactors(420),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(210), PrimeFactors(84), PrimeFactors(60)}),
                ),
                (
                    PrimeFactors(24),
                    frozenset({PrimeFactors(168), PrimeFactors(120)}),
                    frozenset({PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(84),
                    frozenset({PrimeFactors(168), PrimeFactors(420)}),
                    frozenset({PrimeFactors(42), PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(60),
                    frozenset({PrimeFactors(120), PrimeFactors(420)}),
                    frozenset({PrimeFactors(12), PrimeFactors(30)}),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(420)}),
                    frozenset({PrimeFactors(42), PrimeFactors(30)}),
                ),
                (
                    PrimeFactors(12),
                    frozenset({PrimeFactors(24), PrimeFactors(84), PrimeFactors(60)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210), PrimeFactors(84)}),
                    frozenset({PrimeFactors(14), PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(30),
                    frozenset({PrimeFactors(210), PrimeFactors(60)}),
                    frozenset({PrimeFactors(6), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(6),
                    frozenset({PrimeFactors(42), PrimeFactors(12), PrimeFactors(30)}),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(14),
                    frozenset({PrimeFactors(42)}),
                    frozenset({PrimeFactors(2)}),
                ),
                (
                    PrimeFactors(15),
                    frozenset({PrimeFactors(30)}),
                    frozenset({PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(2),
                    frozenset({PrimeFactors(6), PrimeFactors(14)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(6), PrimeFactors(15)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(1),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                    frozenset(),
                ),
            },
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(840),
                    frozenset(),
                    frozenset({PrimeFactors(168), PrimeFactors(420)}),
                ),
                (
                    PrimeFactors(168),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(84)}),
                ),
                (
                    PrimeFactors(420),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(210), PrimeFactors(84)}),
                ),
                (
                    PrimeFactors(84),
                    frozenset({PrimeFactors(168), PrimeFactors(420)}),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(420)}),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210), PrimeFactors(84)}),
                    frozenset(),
                ),
            },
        )

    def test_top(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(2520)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(840)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(840), bounded.top)
        self.assertNotIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(840)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(840), bounded.top)
        self.assertNotIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

    def test_bottom(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(42)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(42), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(1)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(1), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(42)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(42), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

    def test_filter(self):
        bounded = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        ).filter(PrimeFactors(42))
        with self.assertRaises(ValueError):
            _ = bounded.filter(PrimeFactors(1))

    def test_ideal(self):
        bounded = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        ).ideal(PrimeFactors(840))
        with self.assertRaises(ValueError):
            _ = bounded.ideal(PrimeFactors(11))

    # noinspection PyTypeChecker
    def test_upper_limit(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        for bounded in (
            lattice.filter(PrimeFactors(42)),
            lattice.ideal(PrimeFactors(840)),
            lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840)),
        ):
            for limit in bounded:
                for element in bounded:
                    if element <= limit:
                        self.assertIn(element, bounded.upper_limit(limit))
                    else:
                        self.assertNotIn(element, bounded.upper_limit(limit))
                self.assertNotIn(2, bounded.upper_limit(limit))
                self.assertEqual(
                    set(bounded.upper_limit(limit)),
                    {element for element in bounded if element <= limit},
                )
                self.assertEqual(
                    len(bounded.upper_limit(limit)),
                    len({element for element in bounded if element <= limit}),
                )
                for element in bounded:
                    if element < limit:
                        self.assertIn(
                            element,
                            bounded.upper_limit(limit, strict=True),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.upper_limit(limit, strict=True),
                        )
                self.assertNotIn(2, bounded.upper_limit(limit, strict=True))
                self.assertEqual(
                    set(bounded.upper_limit(limit, strict=True)),
                    {element for element in bounded if element < limit},
                )
                self.assertEqual(
                    len(bounded.upper_limit(limit, strict=True)),
                    len({element for element in bounded if element < limit}),
                )

    # noinspection PyTypeChecker
    def test_lower_limit(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        for bounded in (
            lattice.filter(PrimeFactors(42)),
            lattice.ideal(PrimeFactors(840)),
            lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840)),
        ):
            for limit in bounded:
                for element in bounded:
                    if element >= limit:
                        self.assertIn(element, bounded.lower_limit(limit))
                    else:
                        self.assertNotIn(element, bounded.lower_limit(limit))
                self.assertNotIn(2, bounded.lower_limit(limit))
                self.assertEqual(
                    set(bounded.lower_limit(limit)),
                    {element for element in bounded if element >= limit},
                )
                self.assertEqual(
                    len(bounded.lower_limit(limit)),
                    len({element for element in bounded if element >= limit}),
                )
                for element in bounded:
                    if element > limit:
                        self.assertIn(
                            element,
                            bounded.lower_limit(limit, strict=True),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.lower_limit(limit, strict=True),
                        )
                self.assertNotIn(2, bounded.lower_limit(limit, strict=True))
                self.assertEqual(
                    set(bounded.lower_limit(limit, strict=True)),
                    {element for element in bounded if element > limit},
                )
                self.assertEqual(
                    len(bounded.lower_limit(limit, strict=True)),
                    len({element for element in bounded if element > limit}),
                )

    # noinspection PyTypeChecker
    def test_join_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        for irreducible in bounded.join_irreducible:
            self.assertLessEqual(
                len(bounded.cover.predecessors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.join_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(126),
            },
        )
        self.assertEqual(
            len(bounded.join_irreducible),
            5,
        )
        self.assertIn(PrimeFactors(42), bounded.join_irreducible)
        self.assertIn(PrimeFactors(126), bounded.join_irreducible)
        self.assertIn(PrimeFactors(210), bounded.join_irreducible)
        self.assertIn(PrimeFactors(168), bounded.join_irreducible)
        self.assertIn(PrimeFactors(84), bounded.join_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.join_irreducible)
        self.assertNotIn(1, bounded.join_irreducible)

        bounded = lattice.ideal(PrimeFactors(840))
        for irreducible in bounded.join_irreducible:
            self.assertLessEqual(
                len(bounded.cover.predecessors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.join_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
            else:
                self.assertNotEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(12),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
            },
        )
        self.assertEqual(
            len(bounded.join_irreducible),
            6,
        )
        self.assertIn(PrimeFactors(3), bounded.join_irreducible)
        self.assertIn(PrimeFactors(2), bounded.join_irreducible)
        self.assertIn(PrimeFactors(12), bounded.join_irreducible)
        self.assertIn(PrimeFactors(14), bounded.join_irreducible)
        self.assertIn(PrimeFactors(15), bounded.join_irreducible)
        self.assertIn(PrimeFactors(24), bounded.join_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.join_irreducible)
        self.assertNotIn(1, bounded.join_irreducible)

        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        for irreducible in bounded.join_irreducible:
            self.assertLessEqual(
                len(bounded.cover.predecessors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.join_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(42),
                PrimeFactors(168),
                PrimeFactors(210),
                PrimeFactors(84),
            },
        )
        self.assertEqual(
            len(bounded.join_irreducible),
            4,
        )
        self.assertIn(PrimeFactors(168), bounded.join_irreducible)
        self.assertIn(PrimeFactors(210), bounded.join_irreducible)
        self.assertIn(PrimeFactors(42), bounded.join_irreducible)
        self.assertIn(PrimeFactors(84), bounded.join_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.join_irreducible)
        self.assertNotIn(1, bounded.join_irreducible)

        lattice.extend([PrimeFactors(12)])
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(210),
                PrimeFactors(168),
            },
        )

    # noinspection PyTypeChecker
    def test_meet_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(6))
        for irreducible in bounded.meet_irreducible:
            self.assertLessEqual(
                len(bounded.cover.successors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.meet_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
            else:
                self.assertNotEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.meet_irreducible),
            {
                PrimeFactors(840),
                PrimeFactors(360),
                PrimeFactors(630),
                PrimeFactors(1260),
                PrimeFactors(504),
            },
        )
        self.assertEqual(
            len(bounded.meet_irreducible),
            5,
        )
        self.assertIn(PrimeFactors(840), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(360), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(630), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(1260), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(504), bounded.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.meet_irreducible)
        self.assertNotIn(1, bounded.meet_irreducible)

        bounded = lattice.ideal(PrimeFactors(24))
        for irreducible in bounded.meet_irreducible:
            self.assertLessEqual(
                len(bounded.cover.successors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.meet_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.successors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.meet_irreducible),
            {
                PrimeFactors(6),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(12),
                PrimeFactors(24),
            },
        )
        self.assertEqual(
            len(bounded.meet_irreducible),
            5,
        )
        self.assertIn(PrimeFactors(6), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(2), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(3), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(12), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(24), bounded.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.meet_irreducible)
        self.assertNotIn(1, bounded.meet_irreducible)

        bounded = lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        for irreducible in bounded.meet_irreducible:
            self.assertLessEqual(
                len(bounded.cover.successors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.meet_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.successors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.successors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.meet_irreducible),
            {PrimeFactors(6), PrimeFactors(12), PrimeFactors(24)},
        )
        self.assertEqual(
            len(bounded.meet_irreducible),
            3,
        )
        self.assertIn(PrimeFactors(6), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(12), bounded.meet_irreducible)
        self.assertIn(PrimeFactors(24), bounded.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.meet_irreducible)
        self.assertNotIn(1, bounded.meet_irreducible)

        bounded = lattice.filter(PrimeFactors(6))
        lattice.extend([PrimeFactors(30)])
        self.assertEqual(
            set(bounded.meet_irreducible),
            {
                PrimeFactors(840),
                PrimeFactors(360),
                PrimeFactors(630),
                PrimeFactors(1260),
                PrimeFactors(504),
            },
        )

    def test_atoms(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded.atoms),
            {PrimeFactors(42), PrimeFactors(18), PrimeFactors(12), PrimeFactors(30)},
        )
        self.assertEqual(len(bounded.atoms), 4)
        self.assertIn(PrimeFactors(18), bounded.atoms)
        self.assertNotIn(PrimeFactors(1), bounded.atoms)
        self.assertNotIn(1, bounded.atoms)

        bounded = lattice.ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.atoms),
            {
                PrimeFactors(2),
                PrimeFactors(3),
            },
        )
        self.assertEqual(len(bounded.atoms), 2)
        self.assertNotIn(PrimeFactors(1), bounded.atoms)
        self.assertIn(PrimeFactors(2), bounded.atoms)
        self.assertNotIn(1, bounded.atoms)

        bounded = lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24))
        self.assertEqual(
            set(bounded.atoms),
            {
                PrimeFactors(12),
            },
        )
        self.assertEqual(len(bounded.atoms), 1)
        self.assertNotIn(PrimeFactors(6), bounded.atoms)
        self.assertIn(PrimeFactors(12), bounded.atoms)
        self.assertNotIn(1, bounded.atoms)

    def test_co_atoms(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.co_atoms),
            {
                PrimeFactors(504),
                PrimeFactors(1260),
                PrimeFactors(840),
            },
        )
        self.assertEqual(len(bounded.co_atoms), 3)
        self.assertIn(PrimeFactors(840), bounded.co_atoms)
        self.assertNotIn(PrimeFactors(1), bounded.co_atoms)
        self.assertNotIn(1, bounded.co_atoms)

        bounded = lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.co_atoms),
            {PrimeFactors(168), PrimeFactors(420), PrimeFactors(120)},
        )
        self.assertEqual(len(bounded.co_atoms), 3)
        self.assertNotIn(PrimeFactors(840), bounded.co_atoms)
        self.assertIn(PrimeFactors(168), bounded.co_atoms)
        self.assertNotIn(1, bounded.co_atoms)

        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.co_atoms),
            {
                PrimeFactors(168),
                PrimeFactors(420),
            },
        )
        self.assertEqual(len(bounded.co_atoms), 2)
        self.assertNotIn(PrimeFactors(840), bounded.co_atoms)
        self.assertIn(PrimeFactors(168), bounded.co_atoms)
        self.assertNotIn(1, bounded.co_atoms)

    def test_greatest_join_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for bounded in (
            lattice.filter(PrimeFactors(42)),
            lattice.ideal(PrimeFactors(840)),
            lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840)),
        ):
            for value in lattice:
                self.assertEqual(
                    set(bounded.greatest_join_irreducible(value)),
                    set(
                        top(
                            irreducible
                            for irreducible in bounded.join_irreducible
                            if irreducible <= value
                        ),
                    ),
                )
                for element in lattice:
                    if element in top(
                        irreducible
                        for irreducible in bounded.join_irreducible
                        if irreducible <= value
                    ):
                        self.assertIn(
                            element,
                            bounded.greatest_join_irreducible(value),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.greatest_join_irreducible(value),
                        )
                self.assertEqual(
                    len(bounded.greatest_join_irreducible(value)),
                    len(
                        set(
                            top(
                                irreducible
                                for irreducible in bounded.join_irreducible
                                if irreducible <= value
                            ),
                        ),
                    ),
                )
                self.assertEqual(
                    set(bounded.greatest_join_irreducible(value, strict=True)),
                    set(
                        top(
                            irreducible
                            for irreducible in bounded.join_irreducible
                            if irreducible < value
                        ),
                    ),
                )
                self.assertEqual(
                    len(bounded.greatest_join_irreducible(value, strict=True)),
                    len(
                        set(
                            top(
                                irreducible
                                for irreducible in bounded.join_irreducible
                                if irreducible < value
                            ),
                        ),
                    ),
                )
                for element in lattice:
                    if element in top(
                        irreducible
                        for irreducible in bounded.join_irreducible
                        if irreducible < value
                    ):
                        self.assertIn(
                            element,
                            bounded.greatest_join_irreducible(
                                value,
                                strict=True,
                            ),
                        )
                    else:
                        self.assertNotIn(
                            value,
                            bounded.greatest_join_irreducible(
                                value,
                                strict=True,
                            ),
                        )

    def test_smallest_meet_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for bounded in (
            lattice.filter(PrimeFactors(6)),
            lattice.ideal(PrimeFactors(24)),
            lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(24)),
        ):
            for value in lattice:
                self.assertEqual(
                    set(bounded.smallest_meet_irreducible(value)),
                    set(
                        bottom(
                            irreducible
                            for irreducible in bounded.meet_irreducible
                            if irreducible >= value
                        ),
                    ),
                )
                for element in lattice:
                    if element in bottom(
                        irreducible
                        for irreducible in bounded.meet_irreducible
                        if irreducible >= value
                    ):
                        self.assertIn(
                            element,
                            bounded.smallest_meet_irreducible(value),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.smallest_meet_irreducible(value),
                        )
                self.assertEqual(
                    len(bounded.smallest_meet_irreducible(value)),
                    len(
                        set(
                            bottom(
                                irreducible
                                for irreducible in bounded.meet_irreducible
                                if irreducible >= value
                            ),
                        ),
                    ),
                )
                self.assertEqual(
                    set(bounded.smallest_meet_irreducible(value, strict=True)),
                    set(
                        bottom(
                            irreducible
                            for irreducible in bounded.meet_irreducible
                            if irreducible > value
                        ),
                    ),
                )
                self.assertEqual(
                    len(bounded.smallest_meet_irreducible(value, strict=True)),
                    len(
                        set(
                            bottom(
                                irreducible
                                for irreducible in bounded.meet_irreducible
                                if irreducible > value
                            ),
                        ),
                    ),
                )
                for element in lattice:
                    if element in bottom(
                        irreducible
                        for irreducible in bounded.meet_irreducible
                        if irreducible > value
                    ):
                        self.assertIn(
                            element,
                            bounded.smallest_meet_irreducible(
                                value,
                                strict=True,
                            ),
                        )
                    else:
                        self.assertNotIn(
                            value,
                            bounded.smallest_meet_irreducible(
                                value,
                                strict=True,
                            ),
                        )
