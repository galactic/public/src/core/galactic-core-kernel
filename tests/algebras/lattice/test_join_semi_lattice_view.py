"""The :mod:`test_join_semi_lattice_view` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import (
    ExtensibleFiniteJoinSemiLattice,
    FiniteJoinSemiLatticeView,
)
from galactic.algebras.poset import top


class BoundedJoinSemiLatticeTestCase(TestCase):
    def test___init__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        view = FiniteJoinSemiLatticeView[PrimeFactors](semi_lattice)
        # noinspection PyTypeChecker
        self.assertEqual(set(semi_lattice), set(view))

        bounded = semi_lattice.ideal(PrimeFactors(42))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {element for element in semi_lattice if element <= PrimeFactors(42)},
        )
        bounded = semi_lattice.filter(PrimeFactors(42))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {element for element in semi_lattice if element >= PrimeFactors(42)},
        )

    def test___contains__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.ideal(PrimeFactors(42))
        # noinspection PyTypeChecker
        elements = set(bounded)
        for element in semi_lattice:
            if element in elements:
                # noinspection PyTypeChecker
                self.assertIn(element, bounded)
            else:
                # noinspection PyTypeChecker
                self.assertNotIn(element, bounded)
        bounded = semi_lattice.filter(PrimeFactors(42))
        # noinspection PyTypeChecker
        elements = set(bounded)
        for element in semi_lattice:
            if element in elements:
                # noinspection PyTypeChecker
                self.assertIn(element, bounded)
            else:
                # noinspection PyTypeChecker
                self.assertNotIn(element, bounded)

    def test___len__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.ideal(PrimeFactors(42))
        # noinspection PyTypeChecker
        elements = set(bounded)
        # noinspection PyTypeChecker
        self.assertEqual(len(elements), len(bounded))
        bounded = semi_lattice.filter(PrimeFactors(42))
        # noinspection PyTypeChecker
        elements = set(bounded)
        # noinspection PyTypeChecker
        self.assertEqual(len(elements), len(bounded))

    def test___iter__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        # noinspection PyTypeChecker
        self.assertEqual(
            set(bounded),
            {
                element
                for element in semi_lattice
                if PrimeFactors(840) >= element >= PrimeFactors(42)
            },
        )

    def test___lt__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertLess(
            bounded,
            semi_lattice,
        )

    def test___gt__(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        # noinspection PyTypeChecker
        self.assertGreater(
            semi_lattice.filter(PrimeFactors(42)),
            bounded,
        )
        # noinspection PyTypeChecker
        self.assertGreater(
            semi_lattice.ideal(PrimeFactors(840)),
            bounded,
        )

    def test_minimum(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(bounded.minimum, PrimeFactors(42))
        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertIsNone(bounded.minimum)

    def test_maximum(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(bounded.maximum, PrimeFactors(840))
        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(bounded.maximum, semi_lattice.maximum)

    def test_order(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(len(bounded.order), 9)
        self.assertEqual(
            list(bounded.order),
            [
                (PrimeFactors(42), PrimeFactors(42)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(168)),
                (PrimeFactors(42), PrimeFactors(840)),
                (PrimeFactors(210), PrimeFactors(210)),
                (PrimeFactors(210), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(168)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(840), PrimeFactors(840)),
            ],
        )
        for couple in bounded.order:
            self.assertIn(couple, bounded.order)
        self.assertNotIn(2, bounded.order)
        self.assertNotIn((PrimeFactors(3), PrimeFactors(7)), bounded.order)

    def test_order_universes(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            bounded.order.universes,
            (bounded.order.domain, bounded.order.domain),
        )
        self.assertEqual(
            set(bounded.order.domain),
            {PrimeFactors(42), PrimeFactors(210), PrimeFactors(168), PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.order.co_domain),
            {PrimeFactors(42), PrimeFactors(210), PrimeFactors(168), PrimeFactors(840)},
        )

    def test_order_successors(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.order.successors(PrimeFactors(42))),
            {
                PrimeFactors(42),
                PrimeFactors(210),
                PrimeFactors(168),
                PrimeFactors(840),
            },
        )
        self.assertEqual(
            len(bounded.order.successors(PrimeFactors(42))),
            4,
        )

        self.assertIn(PrimeFactors(210), bounded.order.successors(PrimeFactors(42)))
        self.assertNotIn(6, bounded.order.successors(PrimeFactors(42)))

    def test_order_predecessors(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.order.predecessors(PrimeFactors(210))),
            {
                PrimeFactors(42),
                PrimeFactors(210),
            },
        )
        self.assertEqual(
            len(bounded.order.predecessors(PrimeFactors(210))),
            2,
        )

        self.assertIn(PrimeFactors(210), bounded.order.predecessors(PrimeFactors(210)))
        self.assertIn(PrimeFactors(42), bounded.order.predecessors(PrimeFactors(210)))
        self.assertNotIn(6, bounded.order.predecessors(PrimeFactors(210)))

    def test_order_sinks_sources(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )

        self.assertEqual(
            set(bounded.order.sinks),
            {PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.order.sources),
            {PrimeFactors(42)},
        )

    def test_order_domain(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.order.domain),
            {
                PrimeFactors(42),
                PrimeFactors(210),
                PrimeFactors(168),
                PrimeFactors(840),
            },
        )

        self.assertEqual(len(bounded.order.domain), 4)

        self.assertIn(PrimeFactors(42), bounded.order.domain)
        self.assertIn(PrimeFactors(210), bounded.order.domain)
        self.assertIn(PrimeFactors(840), bounded.order.domain)
        self.assertNotIn(PrimeFactors(11), bounded.order.domain)

    def test_cover(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(42), PrimeFactors(126)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(168)),
                (PrimeFactors(126), PrimeFactors(630)),
                (PrimeFactors(126), PrimeFactors(252)),
                (PrimeFactors(210), PrimeFactors(630)),
                (PrimeFactors(210), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(504)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(252), PrimeFactors(504)),
                (PrimeFactors(252), PrimeFactors(1260)),
                (PrimeFactors(630), PrimeFactors(1260)),
                (PrimeFactors(840), PrimeFactors(2520)),
                (PrimeFactors(504), PrimeFactors(2520)),
                (PrimeFactors(1260), PrimeFactors(2520)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(15)),
                (PrimeFactors(14), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(30)),
                (PrimeFactors(6), PrimeFactors(24)),
                (PrimeFactors(15), PrimeFactors(30)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(168)),
                (PrimeFactors(24), PrimeFactors(120)),
                (PrimeFactors(24), PrimeFactors(168)),
                (PrimeFactors(30), PrimeFactors(210)),
                (PrimeFactors(30), PrimeFactors(120)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(210), PrimeFactors(840)),
                (PrimeFactors(120), PrimeFactors(840)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover),
            {
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(168)),
                (PrimeFactors(210), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(840)),
            },
        )
        for couple in bounded.cover:
            self.assertIn(couple, bounded.cover)
        relation = set(bounded.cover)
        for couple in bounded.order:
            if couple in relation:
                self.assertIn(couple, bounded.cover)
            else:
                self.assertNotIn(couple, bounded.cover)

    def test_cover_universes(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            bounded.cover.universes,
            (bounded.cover.domain, bounded.cover.domain),
        )
        self.assertEqual(
            set(bounded.cover.domain),
            {PrimeFactors(42), PrimeFactors(210), PrimeFactors(168), PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.cover.co_domain),
            {PrimeFactors(42), PrimeFactors(210), PrimeFactors(168), PrimeFactors(840)},
        )

    def test_cover_successors(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.cover.successors(PrimeFactors(42))),
            {PrimeFactors(210), PrimeFactors(168)},
        )
        self.assertEqual(
            len(bounded.cover.successors(PrimeFactors(42))),
            2,
        )
        self.assertIn(PrimeFactors(210), bounded.cover.successors(PrimeFactors(42)))
        self.assertNotIn(PrimeFactors(3), bounded.cover.successors(PrimeFactors(42)))
        self.assertNotIn(3, bounded.cover.successors(PrimeFactors(42)))

    def test_cover_predecessors(self):
        bounded = (
            ExtensibleFiniteJoinSemiLattice[PrimeFactors](
                elements=[
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(14),
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            )
            .filter(PrimeFactors(42))
            .ideal(PrimeFactors(840))
        )
        self.assertEqual(
            set(bounded.cover.predecessors(PrimeFactors(840))),
            {PrimeFactors(210), PrimeFactors(168)},
        )
        self.assertEqual(
            len(bounded.cover.predecessors(PrimeFactors(840))),
            2,
        )
        self.assertIn(PrimeFactors(210), bounded.cover.predecessors(PrimeFactors(840)))
        self.assertNotIn(PrimeFactors(3), bounded.cover.predecessors(PrimeFactors(840)))
        self.assertNotIn(3, bounded.cover.predecessors(PrimeFactors(840)))

        for neighbourhood in bounded.cover.neighbourhoods():
            self.assertNotIn(2, bounded.cover.predecessors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.predecessors),
                len(bounded.cover.predecessors(neighbourhood.element)),
            )
            self.assertEqual(
                set(neighbourhood.predecessors),
                set(bounded.cover.predecessors(neighbourhood.element)),
            )
            # noinspection PyTypeChecker
            for element in bounded:
                if element in neighbourhood.predecessors:
                    self.assertIn(
                        element,
                        bounded.cover.predecessors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        bounded.cover.predecessors(neighbourhood.element),
                    )

    def test_cover_sinks_sources(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(2520)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(42)},
        )

        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {
                PrimeFactors(3),
                PrimeFactors(14),
            },
        )

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.cover.sinks),
            {PrimeFactors(840)},
        )
        self.assertEqual(
            set(bounded.cover.sources),
            {PrimeFactors(42)},
        )

    def test_cover_neighbourhoods(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(42),
                    frozenset(
                        {
                            PrimeFactors(168),
                            PrimeFactors(210),
                            PrimeFactors(126),
                        },
                    ),
                    frozenset(),
                ),
                (
                    PrimeFactors(126),
                    frozenset(
                        {
                            PrimeFactors(252),
                            PrimeFactors(630),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(42),
                        },
                    ),
                ),
                (
                    PrimeFactors(210),
                    frozenset(
                        {
                            PrimeFactors(840),
                            PrimeFactors(630),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(42),
                        },
                    ),
                ),
                (
                    PrimeFactors(168),
                    frozenset(
                        {
                            PrimeFactors(504),
                            PrimeFactors(840),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(42),
                        },
                    ),
                ),
                (
                    PrimeFactors(252),
                    frozenset(
                        {
                            PrimeFactors(504),
                            PrimeFactors(1260),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(126),
                        },
                    ),
                ),
                (
                    PrimeFactors(630),
                    frozenset(
                        {
                            PrimeFactors(1260),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(210),
                            PrimeFactors(126),
                        },
                    ),
                ),
                (
                    PrimeFactors(840),
                    frozenset(
                        {
                            PrimeFactors(2520),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(168),
                            PrimeFactors(210),
                        },
                    ),
                ),
                (
                    PrimeFactors(504),
                    frozenset(
                        {
                            PrimeFactors(2520),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(168),
                            PrimeFactors(252),
                        },
                    ),
                ),
                (
                    PrimeFactors(1260),
                    frozenset(
                        {
                            PrimeFactors(2520),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(252),
                            PrimeFactors(630),
                        },
                    ),
                ),
                (
                    PrimeFactors(2520),
                    frozenset(),
                    frozenset(
                        {
                            PrimeFactors(840),
                            PrimeFactors(504),
                            PrimeFactors(1260),
                        },
                    ),
                ),
            },
        )

        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(3),
                    frozenset(
                        {
                            PrimeFactors(6),
                            PrimeFactors(15),
                        },
                    ),
                    frozenset(),
                ),
                (
                    PrimeFactors(14),
                    frozenset(
                        {
                            PrimeFactors(42),
                        },
                    ),
                    frozenset(),
                ),
                (
                    PrimeFactors(6),
                    frozenset(
                        {
                            PrimeFactors(24),
                            PrimeFactors(42),
                            PrimeFactors(30),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(3),
                        },
                    ),
                ),
                (
                    PrimeFactors(15),
                    frozenset(
                        {
                            PrimeFactors(30),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(3),
                        },
                    ),
                ),
                (
                    PrimeFactors(42),
                    frozenset(
                        {
                            PrimeFactors(168),
                            PrimeFactors(210),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(14),
                            PrimeFactors(6),
                        },
                    ),
                ),
                (
                    PrimeFactors(24),
                    frozenset(
                        {
                            PrimeFactors(120),
                            PrimeFactors(168),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(6),
                        },
                    ),
                ),
                (
                    PrimeFactors(30),
                    frozenset(
                        {
                            PrimeFactors(120),
                            PrimeFactors(210),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(6),
                            PrimeFactors(15),
                        },
                    ),
                ),
                (
                    PrimeFactors(168),
                    frozenset(
                        {
                            PrimeFactors(840),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(24),
                            PrimeFactors(42),
                        },
                    ),
                ),
                (
                    PrimeFactors(210),
                    frozenset(
                        {
                            PrimeFactors(840),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(42),
                            PrimeFactors(30),
                        },
                    ),
                ),
                (
                    PrimeFactors(120),
                    frozenset(
                        {
                            PrimeFactors(840),
                        },
                    ),
                    frozenset(
                        {
                            PrimeFactors(24),
                            PrimeFactors(30),
                        },
                    ),
                ),
                (
                    PrimeFactors(840),
                    frozenset(),
                    frozenset(
                        {
                            PrimeFactors(168),
                            PrimeFactors(210),
                            PrimeFactors(120),
                        },
                    ),
                ),
            },
        )

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(168), PrimeFactors(210)}),
                    frozenset(),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(168),
                    frozenset({PrimeFactors(840)}),
                    frozenset({PrimeFactors(42)}),
                ),
                (
                    PrimeFactors(840),
                    frozenset(),
                    frozenset({PrimeFactors(168), PrimeFactors(210)}),
                ),
            },
        )

    def test_top(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(2520)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(840)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(840), bounded.top)
        self.assertNotIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.top),
            {PrimeFactors(840)},
        )
        self.assertEqual(len(bounded.top), 1)
        self.assertIn(PrimeFactors(840), bounded.top)
        self.assertNotIn(PrimeFactors(2520), bounded.top)
        self.assertNotIn(PrimeFactors(6), bounded.top)
        self.assertNotIn(6, bounded.top)

    def test_bottom(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(42)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(42), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.bottom),
            {
                PrimeFactors(3),
                PrimeFactors(14),
            },
        )
        self.assertEqual(len(bounded.bottom), 2)
        self.assertIn(PrimeFactors(3), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.bottom),
            {PrimeFactors(42)},
        )
        self.assertEqual(len(bounded.bottom), 1)
        self.assertIn(PrimeFactors(42), bounded.bottom)
        self.assertNotIn(PrimeFactors(6), bounded.bottom)
        self.assertNotIn(6, bounded.bottom)

    def test_filter(self):
        bounded = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        ).filter(PrimeFactors(42))
        with self.assertRaises(ValueError):
            _ = bounded.filter(PrimeFactors(1))

    def test_ideal(self):
        bounded = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        ).ideal(PrimeFactors(840))
        with self.assertRaises(ValueError):
            _ = bounded.ideal(PrimeFactors(1))

    # noinspection PyTypeChecker
    def test_upper_limit(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        for bounded in (
            semi_lattice.filter(PrimeFactors(42)),
            semi_lattice.ideal(PrimeFactors(840)),
            semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840)),
        ):
            for limit in bounded:
                for element in bounded:
                    if element <= limit:
                        self.assertIn(element, bounded.upper_limit(limit))
                    else:
                        self.assertNotIn(element, bounded.upper_limit(limit))
                self.assertNotIn(2, bounded.upper_limit(limit))
                self.assertEqual(
                    set(bounded.upper_limit(limit)),
                    {element for element in bounded if element <= limit},
                )
                self.assertEqual(
                    len(bounded.upper_limit(limit)),
                    len({element for element in bounded if element <= limit}),
                )
                for element in bounded:
                    if element < limit:
                        self.assertIn(
                            element,
                            bounded.upper_limit(limit, strict=True),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.upper_limit(limit, strict=True),
                        )
                self.assertNotIn(2, bounded.upper_limit(limit, strict=True))
                self.assertEqual(
                    set(bounded.upper_limit(limit, strict=True)),
                    {element for element in bounded if element < limit},
                )
                self.assertEqual(
                    len(bounded.upper_limit(limit, strict=True)),
                    len({element for element in bounded if element < limit}),
                )

    # noinspection PyTypeChecker
    def test_lower_limit(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        for bounded in (
            semi_lattice.filter(PrimeFactors(42)),
            semi_lattice.ideal(PrimeFactors(840)),
            semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840)),
        ):
            for limit in bounded:
                for element in bounded:
                    if element >= limit:
                        self.assertIn(element, bounded.lower_limit(limit))
                    else:
                        self.assertNotIn(element, bounded.lower_limit(limit))
                self.assertNotIn(2, bounded.lower_limit(limit))
                self.assertEqual(
                    set(bounded.lower_limit(limit)),
                    {element for element in bounded if element >= limit},
                )
                self.assertEqual(
                    len(bounded.lower_limit(limit)),
                    len({element for element in bounded if element >= limit}),
                )
                for element in bounded:
                    if element > limit:
                        self.assertIn(
                            element,
                            bounded.lower_limit(limit, strict=True),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.lower_limit(limit, strict=True),
                        )
                self.assertNotIn(2, bounded.lower_limit(limit, strict=True))
                self.assertEqual(
                    set(bounded.lower_limit(limit, strict=True)),
                    {element for element in bounded if element > limit},
                )
                self.assertEqual(
                    len(bounded.lower_limit(limit, strict=True)),
                    len({element for element in bounded if element > limit}),
                )

    # noinspection PyTypeChecker
    def test_join_irreducible(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        for irreducible in bounded.join_irreducible:
            self.assertLessEqual(
                len(bounded.cover.predecessors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.join_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(42),
                PrimeFactors(126),
                PrimeFactors(210),
                PrimeFactors(168),
                PrimeFactors(252),
            },
        )
        self.assertEqual(
            len(bounded.join_irreducible),
            5,
        )
        self.assertIn(PrimeFactors(42), bounded.join_irreducible)
        self.assertIn(PrimeFactors(126), bounded.join_irreducible)
        self.assertIn(PrimeFactors(210), bounded.join_irreducible)
        self.assertIn(PrimeFactors(168), bounded.join_irreducible)
        self.assertIn(PrimeFactors(252), bounded.join_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.join_irreducible)
        self.assertNotIn(1, bounded.join_irreducible)

        bounded = semi_lattice.ideal(PrimeFactors(840))
        for irreducible in bounded.join_irreducible:
            self.assertLessEqual(
                len(bounded.cover.predecessors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.join_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
            },
        )
        self.assertEqual(
            len(bounded.join_irreducible),
            5,
        )
        self.assertIn(PrimeFactors(3), bounded.join_irreducible)
        self.assertIn(PrimeFactors(6), bounded.join_irreducible)
        self.assertIn(PrimeFactors(14), bounded.join_irreducible)
        self.assertIn(PrimeFactors(15), bounded.join_irreducible)
        self.assertIn(PrimeFactors(24), bounded.join_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.join_irreducible)
        self.assertNotIn(1, bounded.join_irreducible)

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        for irreducible in bounded.join_irreducible:
            self.assertLessEqual(
                len(bounded.cover.predecessors(irreducible)),
                1,
            )
        for element in bounded:
            if element in bounded.join_irreducible:
                self.assertLessEqual(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
            else:
                self.assertGreater(
                    len(bounded.cover.predecessors(element)),
                    1,
                )
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(42),
                PrimeFactors(168),
                PrimeFactors(210),
            },
        )
        self.assertEqual(
            len(bounded.join_irreducible),
            3,
        )
        self.assertIn(PrimeFactors(168), bounded.join_irreducible)
        self.assertIn(PrimeFactors(210), bounded.join_irreducible)
        self.assertIn(PrimeFactors(42), bounded.join_irreducible)
        self.assertNotIn(PrimeFactors(1), bounded.join_irreducible)
        self.assertNotIn(1, bounded.join_irreducible)

        semi_lattice.extend([PrimeFactors(12)])
        self.assertEqual(
            set(bounded.join_irreducible),
            {
                PrimeFactors(42),
                PrimeFactors(84),
                PrimeFactors(210),
                PrimeFactors(168),
            },
        )

    def test_co_atoms(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        bounded = semi_lattice.filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded.co_atoms),
            {
                PrimeFactors(504),
                PrimeFactors(1260),
                PrimeFactors(840),
            },
        )
        self.assertEqual(len(bounded.co_atoms), 3)
        self.assertIn(PrimeFactors(840), bounded.co_atoms)
        self.assertNotIn(PrimeFactors(1), bounded.co_atoms)
        self.assertNotIn(1, bounded.co_atoms)

        bounded = semi_lattice.ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.co_atoms),
            {
                PrimeFactors(120),
                PrimeFactors(168),
                PrimeFactors(210),
            },
        )
        self.assertEqual(len(bounded.co_atoms), 3)
        self.assertNotIn(PrimeFactors(840), bounded.co_atoms)
        self.assertIn(PrimeFactors(168), bounded.co_atoms)
        self.assertNotIn(1, bounded.co_atoms)

        bounded = semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded.co_atoms),
            {
                PrimeFactors(168),
                PrimeFactors(210),
            },
        )
        self.assertEqual(len(bounded.co_atoms), 2)
        self.assertNotIn(PrimeFactors(840), bounded.co_atoms)
        self.assertIn(PrimeFactors(168), bounded.co_atoms)
        self.assertNotIn(1, bounded.co_atoms)

    def test_greatest_join_irreducible(self):
        semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for bounded in (
            semi_lattice.filter(PrimeFactors(42)),
            semi_lattice.ideal(PrimeFactors(840)),
            semi_lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840)),
        ):
            for value in semi_lattice:
                self.assertEqual(
                    set(bounded.greatest_join_irreducible(value)),
                    set(
                        top(
                            irreducible
                            for irreducible in bounded.join_irreducible
                            if irreducible <= value
                        ),
                    ),
                )
                for element in semi_lattice:
                    if element in top(
                        irreducible
                        for irreducible in bounded.join_irreducible
                        if irreducible <= value
                    ):
                        self.assertIn(
                            element,
                            bounded.greatest_join_irreducible(value),
                        )
                    else:
                        self.assertNotIn(
                            element,
                            bounded.greatest_join_irreducible(value),
                        )
                self.assertEqual(
                    len(bounded.greatest_join_irreducible(value)),
                    len(
                        set(
                            top(
                                irreducible
                                for irreducible in bounded.join_irreducible
                                if irreducible <= value
                            ),
                        ),
                    ),
                )
                self.assertEqual(
                    set(bounded.greatest_join_irreducible(value, strict=True)),
                    set(
                        top(
                            irreducible
                            for irreducible in bounded.join_irreducible
                            if irreducible < value
                        ),
                    ),
                )
                self.assertEqual(
                    len(bounded.greatest_join_irreducible(value, strict=True)),
                    len(
                        set(
                            top(
                                irreducible
                                for irreducible in bounded.join_irreducible
                                if irreducible < value
                            ),
                        ),
                    ),
                )
                for element in semi_lattice:
                    if element in top(
                        irreducible
                        for irreducible in bounded.join_irreducible
                        if irreducible < value
                    ):
                        self.assertIn(
                            element,
                            bounded.greatest_join_irreducible(
                                value,
                                strict=True,
                            ),
                        )
                    else:
                        self.assertNotIn(
                            value,
                            bounded.greatest_join_irreducible(
                                value,
                                strict=True,
                            ),
                        )
