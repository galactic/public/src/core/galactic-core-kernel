"""The :mod:`test_lattice` module."""

import copy
from contextlib import suppress
from unittest import TestCase

from galactic.algebras.examples.arithmetic import (
    PrimeFactors,
)
from galactic.algebras.examples.arithmetic.renderer import (
    PrimeFactorsRenderer,
)
from galactic.algebras.lattice import (
    ExtensibleFiniteJoinSemiLattice,
    ExtensibleFiniteLattice,
    ExtensibleFiniteMeetSemiLattice,
    FrozenFiniteLattice,
    ReducedContext,
)
from galactic.algebras.lattice.renderer import (
    ReducedContextDiagram,
)
from galactic.algebras.poset import MutableFinitePartiallyOrderedSet


class FiniteLatticeTestCase(TestCase):
    def test___init___(self):
        lattice = FrozenFiniteLattice[PrimeFactors]()
        self.assertEqual(list(lattice), [])
        self.assertEqual(len(lattice), 0)
        self.assertEqual(set(lattice), set())
        self.assertEqual(lattice.minimum, None)
        self.assertEqual(lattice.maximum, None)
        self.assertEqual(set(lattice.meet_irreducible), set())
        self.assertEqual(set(lattice.join_irreducible), set())

        lattice = FrozenFiniteLattice[PrimeFactors](elements=[PrimeFactors(1)])
        self.assertEqual(set(lattice), {PrimeFactors(1)})
        self.assertEqual(len(lattice), 1)

        lattice = FrozenFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(1), PrimeFactors(2)],
        )
        self.assertEqual(set(lattice), {PrimeFactors(1), PrimeFactors(2)})
        self.assertEqual(len(lattice), 2)

        lattice = FrozenFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3)],
        )
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(6),
            },
        )
        self.assertEqual(len(lattice), 4)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(96),
                PrimeFactors(72),
                PrimeFactors(12),
                PrimeFactors(32),
            ],
        )
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(288),
                PrimeFactors(72),
                PrimeFactors(96),
                PrimeFactors(24),
                PrimeFactors(32),
                PrimeFactors(12),
                PrimeFactors(8),
                PrimeFactors(4),
            },
        )
        self.assertEqual(len(lattice), 8)

    def test___eq__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertNotEqual(lattice, 1)

    def test___hash__(self):
        lattice1 = FrozenFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        lattice2 = FrozenFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(36),
                PrimeFactors(14),
                PrimeFactors(18),
                PrimeFactors(6),
                PrimeFactors(3),
                PrimeFactors(15),
                PrimeFactors(24),
            ],
        )
        self.assertEqual(hash(lattice1), hash(lattice2))

    def test___copy__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(lattice, copy.copy(lattice))
        self.assertEqual(lattice, lattice.copy())

    def test___contains__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertNotIn(2, lattice)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertNotIn(2, lattice)
        self.assertIn(PrimeFactors(3), lattice)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertNotIn(2, lattice)
        self.assertIn(PrimeFactors(1), lattice)
        self.assertIn(PrimeFactors(2), lattice)
        self.assertIn(PrimeFactors(3), lattice)
        self.assertIn(PrimeFactors(6), lattice)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)],
        )
        self.assertIn(PrimeFactors(1), lattice)
        self.assertIn(PrimeFactors(2), lattice)
        self.assertIn(PrimeFactors(6), lattice)
        self.assertIn(PrimeFactors(30), lattice)
        self.assertNotIn(PrimeFactors(45), lattice)
        self.assertNotIn(1, lattice)

        for element in lattice:
            self.assertIn(element, lattice)

    def test___len__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertEqual(len(lattice), 0)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(len(lattice), 1)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(len(lattice), 4)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(len(lattice), 24)

    def test___iter__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertEqual(list(lattice), [])
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(list(lattice), [PrimeFactors(3)])
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(
            list(lattice),
            [
                PrimeFactors(6),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(1),
            ],
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(4),
                PrimeFactors(9),
            ],
        )
        self.assertEqual(
            lattice,
            {
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(4),
                PrimeFactors(6),
                PrimeFactors(9),
                PrimeFactors(10),
                PrimeFactors(15),
                PrimeFactors(14),
                PrimeFactors(21),
                PrimeFactors(35),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(20),
                PrimeFactors(30),
                PrimeFactors(45),
                PrimeFactors(28),
                PrimeFactors(42),
                PrimeFactors(63),
                PrimeFactors(70),
                PrimeFactors(105),
                PrimeFactors(36),
                PrimeFactors(60),
                PrimeFactors(90),
                PrimeFactors(84),
                PrimeFactors(126),
                PrimeFactors(140),
                PrimeFactors(210),
                PrimeFactors(315),
                PrimeFactors(180),
                PrimeFactors(252),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(1260),
            },
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(
            set(lattice),
            {
                # Level 0
                PrimeFactors(1),
                # Level 1
                PrimeFactors(2),
                # Level 2
                PrimeFactors(6),
                # Level 3
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                # Level 4
                PrimeFactors(36),
                PrimeFactors(60),
                PrimeFactors(84),
                PrimeFactors(90),
                PrimeFactors(126),
                PrimeFactors(210),
                # Level 5
                PrimeFactors(180),
                PrimeFactors(252),
                PrimeFactors(420),
                PrimeFactors(630),
                # Level 6
                PrimeFactors(1260),
                # Level 7
                PrimeFactors(2520),
                # Level 8
                PrimeFactors(5040),
                PrimeFactors(7560),
                # Level 9
                PrimeFactors(15120),
                # Level 10
                PrimeFactors(30240),
                # Level 11
                PrimeFactors(60480),
            },
        )

        self.assertEqual(
            set(
                ExtensibleFiniteLattice[PrimeFactors](
                    elements=[
                        PrimeFactors(2),
                        PrimeFactors(3),
                        PrimeFactors(4),
                        PrimeFactors(5),
                        PrimeFactors(225),
                    ],
                ),
            ),
            {
                # Level 0
                PrimeFactors(1),
                # Level 1
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(5),
                # Level 2
                PrimeFactors(6),
                PrimeFactors(10),
                PrimeFactors(4),
                PrimeFactors(15),
                PrimeFactors(225),
                # Level 3
                PrimeFactors(30),
                PrimeFactors(12),
                # Level 4
                PrimeFactors(20),
                PrimeFactors(450),
                PrimeFactors(60),
                # Level 5
                PrimeFactors(900),
            },
        )

    # noinspection PyTypeChecker
    def test___reversed__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertEqual(list(reversed(lattice)), [])
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
            ],
        )
        self.assertEqual(list(reversed(lattice)), [PrimeFactors(3)])
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(
            list(reversed(lattice)),
            [PrimeFactors(1), PrimeFactors(2), PrimeFactors(3), PrimeFactors(6)],
        )

    def test___lt__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertLess(
            lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLess(
            lattice,
            ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLess(
            lattice,
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(3), PrimeFactors(3), PrimeFactors(4)],
            ),
        )

    def test___gt__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertGreater(
            MutableFinitePartiallyOrderedSet[PrimeFactors](elements=[PrimeFactors(3)]),
            lattice,
        )
        self.assertGreater(
            ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)]),
            lattice,
        )
        self.assertGreater(
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(3), PrimeFactors(3), PrimeFactors(4)],
            ),
            lattice,
        )

    def test___le__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertLessEqual(
            lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](),
        )
        self.assertLessEqual(
            lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](),
        )
        self.assertLessEqual(
            lattice,
            MutableFinitePartiallyOrderedSet[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLessEqual(
            lattice,
            ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)]),
        )
        self.assertLessEqual(
            lattice,
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(3), PrimeFactors(3), PrimeFactors(4)],
            ),
        )

    def test___ge__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertGreaterEqual(
            MutableFinitePartiallyOrderedSet[PrimeFactors](elements=[PrimeFactors(3)]),
            lattice,
        )
        self.assertGreaterEqual(
            MutableFinitePartiallyOrderedSet[PrimeFactors](),
            lattice,
        )
        self.assertGreaterEqual(
            ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)]),
            lattice,
        )
        self.assertGreaterEqual(
            ExtensibleFiniteLattice[PrimeFactors](),
            lattice,
        )
        self.assertGreaterEqual(
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(3), PrimeFactors(3), PrimeFactors(4)],
            ),
            lattice,
        )

    def test___or__(self):
        lattice1 = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)])
        lattice2 = lattice1 | [PrimeFactors(2)]
        self.assertLessEqual(lattice1, lattice2)
        self.assertEqual(
            list(lattice2),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )

    def test___ior__(self):
        lattice1 = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)])
        lattice1 |= [PrimeFactors(2)]
        self.assertEqual(
            list(lattice1),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )

    def test___and__(self):
        lattice1 = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(2), PrimeFactors(5)],
        )
        lattice2 = lattice1 & [PrimeFactors(2), PrimeFactors(5), PrimeFactors(7)]
        self.assertGreaterEqual(lattice1, lattice2)
        self.assertEqual(
            list(lattice2),
            [PrimeFactors(10), PrimeFactors(2), PrimeFactors(5), PrimeFactors(1)],
        )
        lattice1 &= [PrimeFactors(2), PrimeFactors(5), PrimeFactors(7)]
        self.assertEqual(
            list(lattice1),
            [PrimeFactors(10), PrimeFactors(2), PrimeFactors(5), PrimeFactors(1)],
        )

    def test_join(self):
        lattice1 = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(3)])
        lattice2 = lattice1.join(
            ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(2)]),
        )
        self.assertLessEqual(lattice1, lattice2)
        self.assertEqual(
            list(lattice2),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )

    def test_meet(self):
        lattice1 = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(2), PrimeFactors(5)],
        )
        lattice2 = lattice1.meet(
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(2), PrimeFactors(5), PrimeFactors(7)],
            ),
        )
        self.assertGreaterEqual(lattice1, lattice2)
        self.assertEqual(
            list(lattice2),
            [PrimeFactors(10), PrimeFactors(2), PrimeFactors(5), PrimeFactors(1)],
        )

    def test_isdisjoint(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertTrue(lattice.isdisjoint([]))
        self.assertTrue(lattice.isdisjoint([PrimeFactors(4)]))
        self.assertFalse(lattice.isdisjoint([PrimeFactors(2)]))
        self.assertFalse(lattice.isdisjoint([PrimeFactors(3)]))
        self.assertFalse(lattice.isdisjoint([PrimeFactors(6)]))

    def test_issubset(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertFalse(lattice.issubset([]))
        self.assertFalse(lattice.issubset([PrimeFactors(4)]))
        self.assertFalse(lattice.issubset([PrimeFactors(2), PrimeFactors(3)]))
        self.assertTrue(
            lattice.issubset(
                [PrimeFactors(1), PrimeFactors(2), PrimeFactors(3), PrimeFactors(6)],
            ),
        )

    def test_issuperset(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertTrue(lattice.issuperset([]))
        self.assertFalse(lattice.issuperset([PrimeFactors(4)]))
        self.assertTrue(lattice.issuperset([PrimeFactors(2), PrimeFactors(3)]))
        self.assertTrue(
            lattice.issuperset([PrimeFactors(2), PrimeFactors(3), PrimeFactors(6)]),
        )
        self.assertTrue(
            lattice.issuperset(
                [PrimeFactors(1), PrimeFactors(2), PrimeFactors(3), PrimeFactors(6)],
            ),
        )
        self.assertFalse(
            lattice.issuperset(
                [
                    PrimeFactors(2),
                    PrimeFactors(3),
                    PrimeFactors(6),
                    PrimeFactors(4),
                ],
            ),
        )

    def test_minimum(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertIsNone(lattice.minimum)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
            ],
        )
        self.assertEqual(lattice.minimum, PrimeFactors(2))
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))

    def test_maximum(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertIsNone(lattice.maximum)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
            ],
        )
        self.assertEqual(lattice.maximum, PrimeFactors(2))
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
            ],
        )
        self.assertEqual(lattice.maximum, PrimeFactors(6))

    def test_order(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
            ],
        )
        self.assertEqual(len(lattice.order), 18)
        self.assertEqual(
            set(lattice.order),
            {
                (PrimeFactors(42), PrimeFactors(42)),
                (PrimeFactors(14), PrimeFactors(42)),
                (PrimeFactors(14), PrimeFactors(14)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(42)),
                (PrimeFactors(2), PrimeFactors(14)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(2)),
                (PrimeFactors(3), PrimeFactors(42)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(42)),
                (PrimeFactors(1), PrimeFactors(14)),
                (PrimeFactors(1), PrimeFactors(6)),
                (PrimeFactors(1), PrimeFactors(2)),
                (PrimeFactors(1), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(1)),
            },
        )
        for couple in lattice.order:
            self.assertIn(couple, lattice.order)
        self.assertNotIn(2, lattice.order)
        self.assertNotIn((PrimeFactors(3), PrimeFactors(7)), lattice.order)

    def test_order_universes(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            lattice.order.universes,
            (lattice.order.domain, lattice.order.domain),
        )
        self.assertEqual(
            list(lattice.order.domain),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )
        self.assertEqual(
            list(lattice.order.co_domain),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )

    def test_order_successors(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = lattice.order.successors(3)
        self.assertEqual(
            list(lattice.order.successors(PrimeFactors(72))),
            [
                PrimeFactors(2520),
                PrimeFactors(504),
                PrimeFactors(360),
                PrimeFactors(72),
            ],
        )
        self.assertEqual(
            len(lattice.order.successors(PrimeFactors(72))),
            4,
        )

        self.assertIn(PrimeFactors(504), lattice.order.successors(PrimeFactors(72)))
        self.assertNotIn(6, lattice.order.successors(PrimeFactors(72)))

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(
            set(lattice.order.successors(PrimeFactors(6))),
            {
                PrimeFactors(60480),
                PrimeFactors(30240),
                PrimeFactors(15120),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(2520),
                PrimeFactors(1260),
                PrimeFactors(180),
                PrimeFactors(420),
                PrimeFactors(252),
                PrimeFactors(60),
                PrimeFactors(36),
                PrimeFactors(84),
                PrimeFactors(12),
                PrimeFactors(630),
                PrimeFactors(90),
                PrimeFactors(210),
                PrimeFactors(126),
                PrimeFactors(30),
                PrimeFactors(18),
                PrimeFactors(42),
                PrimeFactors(6),
            },
        )
        self.assertEqual(len(lattice.order.successors(PrimeFactors(6))), 22)
        self.assertIn(PrimeFactors(42), lattice.order.successors(PrimeFactors(6)))
        self.assertNotIn(PrimeFactors(2), lattice.order.successors(PrimeFactors(6)))
        self.assertNotIn(1, lattice.order.successors(PrimeFactors(6)))

    def test_order_predecessors(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = lattice.order.predecessors(3)
        self.assertEqual(
            list(lattice.order.predecessors(PrimeFactors(72))),
            [
                PrimeFactors(72),
                PrimeFactors(24),
                PrimeFactors(36),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(6),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(1),
            ],
        )

        self.assertIn(PrimeFactors(3), lattice.order.predecessors(PrimeFactors(72)))
        self.assertNotIn(1, lattice.order.predecessors(PrimeFactors(72)))

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(
            set(lattice.order.predecessors(PrimeFactors(6))),
            {PrimeFactors(6), PrimeFactors(2), PrimeFactors(1)},
        )
        self.assertEqual(len(lattice.order.predecessors(PrimeFactors(6))), 3)
        self.assertIn(PrimeFactors(2), lattice.order.predecessors(PrimeFactors(6)))
        self.assertNotIn(PrimeFactors(42), lattice.order.predecessors(PrimeFactors(6)))
        self.assertNotIn(1, lattice.order.predecessors(PrimeFactors(6)))

    def test_order_sinks_sources(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(lattice.order.sinks),
            {PrimeFactors(2520)},
        )
        self.assertEqual(
            set(lattice.order.sources),
            {PrimeFactors(1)},
        )

    def test_order_domain(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(lattice.order.domain),
            {
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(12),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(60),
                PrimeFactors(84),
                PrimeFactors(420),
                PrimeFactors(36),
                PrimeFactors(168),
                PrimeFactors(90),
                PrimeFactors(180),
                PrimeFactors(210),
                PrimeFactors(840),
                PrimeFactors(72),
                PrimeFactors(120),
                PrimeFactors(360),
                PrimeFactors(126),
                PrimeFactors(630),
                PrimeFactors(252),
                PrimeFactors(1260),
                PrimeFactors(504),
                PrimeFactors(2520),
            },
        )

        self.assertEqual(len(lattice.order.domain), 29)

        self.assertIn(PrimeFactors(3), lattice.order.domain)
        self.assertIn(PrimeFactors(6), lattice.order.domain)
        self.assertNotIn(PrimeFactors(11), lattice.order.domain)

    def test_cover(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(3)],
        )
        self.assertNotIn(2, lattice.cover)
        self.assertEqual(len(lattice.cover), 0)
        self.assertEqual(list(lattice.cover), [])

        lattice.extend([PrimeFactors(2)])
        self.assertNotIn(2, lattice.cover)
        self.assertNotIn((2, 3), lattice.cover)
        self.assertIn((PrimeFactors(1), PrimeFactors(2)), lattice.cover)
        self.assertIn((PrimeFactors(1), PrimeFactors(3)), lattice.cover)
        self.assertIn((PrimeFactors(2), PrimeFactors(6)), lattice.cover)
        self.assertIn((PrimeFactors(3), PrimeFactors(6)), lattice.cover)
        self.assertEqual(len(lattice.cover), 4)
        self.assertEqual(
            list(lattice.cover),
            [
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(1), PrimeFactors(3)),
                (PrimeFactors(1), PrimeFactors(2)),
            ],
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(lattice.cover),
            {
                (PrimeFactors(840), PrimeFactors(2520)),
                (PrimeFactors(504), PrimeFactors(2520)),
                (PrimeFactors(360), PrimeFactors(2520)),
                (PrimeFactors(1260), PrimeFactors(2520)),
                (PrimeFactors(168), PrimeFactors(840)),
                (PrimeFactors(168), PrimeFactors(504)),
                (PrimeFactors(120), PrimeFactors(840)),
                (PrimeFactors(120), PrimeFactors(360)),
                (PrimeFactors(420), PrimeFactors(840)),
                (PrimeFactors(420), PrimeFactors(1260)),
                (PrimeFactors(72), PrimeFactors(504)),
                (PrimeFactors(72), PrimeFactors(360)),
                (PrimeFactors(252), PrimeFactors(504)),
                (PrimeFactors(252), PrimeFactors(1260)),
                (PrimeFactors(180), PrimeFactors(360)),
                (PrimeFactors(180), PrimeFactors(1260)),
                (PrimeFactors(630), PrimeFactors(1260)),
                (PrimeFactors(24), PrimeFactors(168)),
                (PrimeFactors(24), PrimeFactors(120)),
                (PrimeFactors(24), PrimeFactors(72)),
                (PrimeFactors(84), PrimeFactors(168)),
                (PrimeFactors(84), PrimeFactors(420)),
                (PrimeFactors(84), PrimeFactors(252)),
                (PrimeFactors(60), PrimeFactors(120)),
                (PrimeFactors(60), PrimeFactors(420)),
                (PrimeFactors(60), PrimeFactors(180)),
                (PrimeFactors(210), PrimeFactors(420)),
                (PrimeFactors(210), PrimeFactors(630)),
                (PrimeFactors(36), PrimeFactors(72)),
                (PrimeFactors(36), PrimeFactors(252)),
                (PrimeFactors(36), PrimeFactors(180)),
                (PrimeFactors(126), PrimeFactors(252)),
                (PrimeFactors(126), PrimeFactors(630)),
                (PrimeFactors(90), PrimeFactors(180)),
                (PrimeFactors(90), PrimeFactors(630)),
                (PrimeFactors(12), PrimeFactors(24)),
                (PrimeFactors(12), PrimeFactors(84)),
                (PrimeFactors(12), PrimeFactors(60)),
                (PrimeFactors(12), PrimeFactors(36)),
                (PrimeFactors(42), PrimeFactors(84)),
                (PrimeFactors(42), PrimeFactors(210)),
                (PrimeFactors(42), PrimeFactors(126)),
                (PrimeFactors(30), PrimeFactors(60)),
                (PrimeFactors(30), PrimeFactors(210)),
                (PrimeFactors(30), PrimeFactors(90)),
                (PrimeFactors(18), PrimeFactors(36)),
                (PrimeFactors(18), PrimeFactors(126)),
                (PrimeFactors(18), PrimeFactors(90)),
                (PrimeFactors(6), PrimeFactors(12)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(30)),
                (PrimeFactors(6), PrimeFactors(18)),
                (PrimeFactors(14), PrimeFactors(42)),
                (PrimeFactors(15), PrimeFactors(30)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(14)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(15)),
                (PrimeFactors(1), PrimeFactors(2)),
                (PrimeFactors(1), PrimeFactors(3)),
            },
        )
        for couple in lattice.cover:
            self.assertIn(couple, lattice.cover)
        relation = set(lattice.cover)
        for couple in lattice.order:
            if couple in relation:
                self.assertIn(couple, lattice.cover)
            else:
                self.assertNotIn(couple, lattice.cover)

    def test_cover_universes(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(2),
            ],
        )

        self.assertIs(lattice.cover.universes[0], lattice)
        self.assertIs(lattice.cover.universes[0], lattice.cover.universes[1])
        self.assertIs(lattice.cover.universes[0], lattice.cover.domain)
        self.assertIs(lattice.cover.universes[1], lattice.cover.co_domain)

        self.assertEqual(
            lattice.cover.universes,
            (lattice.cover.domain, lattice.cover.domain),
        )
        self.assertEqual(
            list(lattice.cover.domain),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )
        self.assertEqual(
            list(lattice.cover.co_domain),
            [PrimeFactors(6), PrimeFactors(3), PrimeFactors(2), PrimeFactors(1)],
        )

    def test_cover_successors(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            _ = lattice.cover.successors(PrimeFactors(25))
        self.assertTrue(lattice.cover.successors(PrimeFactors(18)))
        self.assertEqual(
            set(lattice.cover.successors(PrimeFactors(18))),
            {PrimeFactors(36), PrimeFactors(126), PrimeFactors(90)},
        )
        self.assertEqual(
            len(lattice.cover.successors(PrimeFactors(18))),
            3,
        )
        self.assertIn(PrimeFactors(36), lattice.cover.successors(PrimeFactors(18)))
        self.assertNotIn(PrimeFactors(3), lattice.cover.successors(PrimeFactors(18)))
        self.assertNotIn(3, lattice.cover.successors(PrimeFactors(18)))

        for neighbourhood in lattice.cover.neighbourhoods():
            self.assertNotIn(2, lattice.cover.successors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.successors),
                len(lattice.cover.successors(neighbourhood.element)),
            )
            self.assertEqual(
                set(neighbourhood.successors),
                set(lattice.cover.successors(neighbourhood.element)),
            )
            for element in lattice:
                if element in neighbourhood.successors:
                    self.assertIn(
                        element,
                        lattice.cover.successors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        lattice.cover.successors(neighbourhood.element),
                    )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(4),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = lattice.cover.successors(1)
        self.assertEqual(
            set(lattice.cover.successors(PrimeFactors(2))),
            {PrimeFactors(4), PrimeFactors(6)},
        )
        self.assertEqual(
            set(lattice.cover.successors(PrimeFactors(1))),
            {PrimeFactors(2), PrimeFactors(3)},
        )
        self.assertEqual(
            set(lattice.cover.successors(PrimeFactors(6))),
            {PrimeFactors(12)},
        )
        self.assertEqual(
            set(lattice.cover.successors(PrimeFactors(12))),
            set(),
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(4),
                PrimeFactors(5),
                PrimeFactors(225),
            ],
        )
        successors = {
            # Level 0
            PrimeFactors(1): {PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)},
            # Level 1
            PrimeFactors(2): {PrimeFactors(4), PrimeFactors(6), PrimeFactors(10)},
            PrimeFactors(3): {PrimeFactors(6), PrimeFactors(15)},
            PrimeFactors(5): {PrimeFactors(10), PrimeFactors(15)},
            # Level 2
            PrimeFactors(6): {PrimeFactors(12), PrimeFactors(30)},
            PrimeFactors(10): {PrimeFactors(20), PrimeFactors(30)},
            PrimeFactors(4): {PrimeFactors(12), PrimeFactors(20)},
            PrimeFactors(15): {PrimeFactors(225), PrimeFactors(30)},
            PrimeFactors(225): {PrimeFactors(450)},
            # Level 3
            PrimeFactors(30): {PrimeFactors(60), PrimeFactors(450)},
            PrimeFactors(12): {PrimeFactors(60)},
            # Level 4
            PrimeFactors(20): {PrimeFactors(60)},
            PrimeFactors(450): {PrimeFactors(900)},
            PrimeFactors(60): {PrimeFactors(900)},
            # Level 5
            PrimeFactors(900): set(),
        }
        for item in lattice:
            self.assertEqual(set(lattice.cover.successors(item)), successors[item])
        items = list(lattice)
        for item in items:
            self.assertEqual(set(lattice.cover.successors(item)), successors[item])

    def test_cover_predecessors(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        with self.assertRaises(ValueError):
            _ = lattice.cover.predecessors(PrimeFactors(25))
        self.assertTrue(lattice.cover.predecessors(PrimeFactors(12)))
        self.assertEqual(
            set(lattice.cover.predecessors(PrimeFactors(12))),
            {PrimeFactors(6)},
        )

        for neighbourhood in lattice.cover.neighbourhoods():
            self.assertNotIn(2, lattice.cover.predecessors(neighbourhood.element))
            self.assertEqual(
                len(neighbourhood.predecessors),
                len(lattice.cover.predecessors(neighbourhood.element)),
            )
            self.assertEqual(
                set(neighbourhood.predecessors),
                set(lattice.cover.predecessors(neighbourhood.element)),
            )
            for element in lattice:
                if element in neighbourhood.predecessors:
                    self.assertIn(
                        element,
                        lattice.cover.predecessors(neighbourhood.element),
                    )
                else:
                    self.assertNotIn(
                        element,
                        lattice.cover.predecessors(neighbourhood.element),
                    )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(4),
            ],
        )
        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = lattice.cover.predecessors(1)
        self.assertEqual(
            set(lattice.cover.predecessors(PrimeFactors(2))),
            {PrimeFactors(1)},
        )
        self.assertEqual(
            set(lattice.cover.predecessors(PrimeFactors(6))),
            {PrimeFactors(2), PrimeFactors(3)},
        )
        self.assertEqual(
            set(lattice.cover.predecessors(PrimeFactors(12))),
            {PrimeFactors(4), PrimeFactors(6)},
        )
        self.assertEqual(
            set(lattice.cover.predecessors(PrimeFactors(1))),
            set(),
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(4),
                PrimeFactors(5),
                PrimeFactors(225),
            ],
        )
        predecessors = {
            # Level 0
            PrimeFactors(1): set(),
            # Level 1
            PrimeFactors(2): {PrimeFactors(1)},
            PrimeFactors(3): {PrimeFactors(1)},
            PrimeFactors(5): {PrimeFactors(1)},
            # Level 2
            PrimeFactors(6): {PrimeFactors(2), PrimeFactors(3)},
            PrimeFactors(10): {PrimeFactors(2), PrimeFactors(5)},
            PrimeFactors(4): {PrimeFactors(2)},
            PrimeFactors(15): {PrimeFactors(3), PrimeFactors(5)},
            PrimeFactors(225): {PrimeFactors(15)},
            # Level 3
            PrimeFactors(30): {PrimeFactors(10), PrimeFactors(6), PrimeFactors(15)},
            PrimeFactors(12): {PrimeFactors(4), PrimeFactors(6)},
            # Level 4
            PrimeFactors(20): {PrimeFactors(4), PrimeFactors(10)},
            PrimeFactors(450): {PrimeFactors(225), PrimeFactors(30)},
            PrimeFactors(60): {PrimeFactors(12), PrimeFactors(20), PrimeFactors(30)},
            # Level 5
            PrimeFactors(900): {PrimeFactors(450), PrimeFactors(60)},
        }
        for item in lattice:
            self.assertEqual(set(lattice.cover.predecessors(item)), predecessors[item])
        items = list(lattice)
        for item in items:
            self.assertEqual(set(lattice.cover.predecessors(item)), predecessors[item])

    def test_cover_sinks_sources(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3)],
        )
        self.assertIs(lattice.cover.sinks, lattice.top)
        self.assertIs(lattice.cover.sources, lattice.bottom)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )

        self.assertEqual(
            set(lattice.cover.sinks),
            {PrimeFactors(2520)},
        )
        self.assertEqual(
            set(lattice.cover.sources),
            {
                PrimeFactors(1),
            },
        )

    def test_cover_neighbourhoods(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3)],
        )
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(6),
                    frozenset(),
                    frozenset([PrimeFactors(2), PrimeFactors(3)]),
                ),
                (
                    PrimeFactors(2),
                    frozenset([PrimeFactors(6)]),
                    frozenset([PrimeFactors(1)]),
                ),
                (
                    PrimeFactors(3),
                    frozenset([PrimeFactors(6)]),
                    frozenset([PrimeFactors(1)]),
                ),
                (
                    PrimeFactors(1),
                    frozenset([PrimeFactors(2), PrimeFactors(3)]),
                    frozenset(),
                ),
            },
        )
        for neighbourhood in lattice.cover.neighbourhoods():
            for successor in neighbourhood.successors:
                self.assertIn(
                    neighbourhood.element,
                    lattice.cover.predecessors(successor),
                )
            for predecessor in neighbourhood.predecessors:
                self.assertIn(
                    neighbourhood.element,
                    lattice.cover.successors(predecessor),
                )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(18), PrimeFactors(46), PrimeFactors(6)],
        )
        self.assertEqual(
            set(lattice.cover.domain),
            {
                PrimeFactors(414),
                PrimeFactors(138),
                PrimeFactors(18),
                PrimeFactors(46),
                PrimeFactors(6),
                PrimeFactors(2),
            },
        )
        self.assertEqual(
            set(lattice.cover.domain),
            {neighbourhood.element for neighbourhood in lattice.cover.neighbourhoods()},
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            {
                (
                    neighbourhood.element,
                    frozenset(neighbourhood.successors),
                    frozenset(neighbourhood.predecessors),
                )
                for neighbourhood in lattice.cover.neighbourhoods()
            },
            {
                (
                    PrimeFactors(2520),
                    frozenset(),
                    frozenset(
                        {
                            PrimeFactors(840),
                            PrimeFactors(504),
                            PrimeFactors(360),
                            PrimeFactors(1260),
                        },
                    ),
                ),
                (
                    PrimeFactors(840),
                    frozenset({PrimeFactors(2520)}),
                    frozenset(
                        {PrimeFactors(168), PrimeFactors(420), PrimeFactors(120)},
                    ),
                ),
                (
                    PrimeFactors(504),
                    frozenset({PrimeFactors(2520)}),
                    frozenset({PrimeFactors(168), PrimeFactors(72), PrimeFactors(252)}),
                ),
                (
                    PrimeFactors(360),
                    frozenset({PrimeFactors(2520)}),
                    frozenset({PrimeFactors(120), PrimeFactors(72), PrimeFactors(180)}),
                ),
                (
                    PrimeFactors(1260),
                    frozenset({PrimeFactors(2520)}),
                    frozenset(
                        {
                            PrimeFactors(180),
                            PrimeFactors(420),
                            PrimeFactors(252),
                            PrimeFactors(630),
                        },
                    ),
                ),
                (
                    PrimeFactors(168),
                    frozenset({PrimeFactors(840), PrimeFactors(504)}),
                    frozenset({PrimeFactors(24), PrimeFactors(84)}),
                ),
                (
                    PrimeFactors(120),
                    frozenset({PrimeFactors(840), PrimeFactors(360)}),
                    frozenset({PrimeFactors(24), PrimeFactors(60)}),
                ),
                (
                    PrimeFactors(420),
                    frozenset({PrimeFactors(840), PrimeFactors(1260)}),
                    frozenset({PrimeFactors(210), PrimeFactors(84), PrimeFactors(60)}),
                ),
                (
                    PrimeFactors(72),
                    frozenset({PrimeFactors(504), PrimeFactors(360)}),
                    frozenset({PrimeFactors(24), PrimeFactors(36)}),
                ),
                (
                    PrimeFactors(252),
                    frozenset({PrimeFactors(504), PrimeFactors(1260)}),
                    frozenset({PrimeFactors(84), PrimeFactors(126), PrimeFactors(36)}),
                ),
                (
                    PrimeFactors(180),
                    frozenset({PrimeFactors(360), PrimeFactors(1260)}),
                    frozenset({PrimeFactors(90), PrimeFactors(60), PrimeFactors(36)}),
                ),
                (
                    PrimeFactors(630),
                    frozenset({PrimeFactors(1260)}),
                    frozenset({PrimeFactors(210), PrimeFactors(90), PrimeFactors(126)}),
                ),
                (
                    PrimeFactors(24),
                    frozenset({PrimeFactors(168), PrimeFactors(72), PrimeFactors(120)}),
                    frozenset({PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(84),
                    frozenset(
                        {PrimeFactors(168), PrimeFactors(420), PrimeFactors(252)},
                    ),
                    frozenset({PrimeFactors(42), PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(60),
                    frozenset(
                        {PrimeFactors(120), PrimeFactors(180), PrimeFactors(420)},
                    ),
                    frozenset({PrimeFactors(12), PrimeFactors(30)}),
                ),
                (
                    PrimeFactors(210),
                    frozenset({PrimeFactors(420), PrimeFactors(630)}),
                    frozenset({PrimeFactors(42), PrimeFactors(30)}),
                ),
                (
                    PrimeFactors(36),
                    frozenset({PrimeFactors(72), PrimeFactors(180), PrimeFactors(252)}),
                    frozenset({PrimeFactors(18), PrimeFactors(12)}),
                ),
                (
                    PrimeFactors(126),
                    frozenset({PrimeFactors(252), PrimeFactors(630)}),
                    frozenset({PrimeFactors(42), PrimeFactors(18)}),
                ),
                (
                    PrimeFactors(90),
                    frozenset({PrimeFactors(180), PrimeFactors(630)}),
                    frozenset({PrimeFactors(18), PrimeFactors(30)}),
                ),
                (
                    PrimeFactors(12),
                    frozenset(
                        {
                            PrimeFactors(24),
                            PrimeFactors(84),
                            PrimeFactors(60),
                            PrimeFactors(36),
                        },
                    ),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(42),
                    frozenset({PrimeFactors(210), PrimeFactors(84), PrimeFactors(126)}),
                    frozenset({PrimeFactors(14), PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(30),
                    frozenset({PrimeFactors(210), PrimeFactors(60), PrimeFactors(90)}),
                    frozenset({PrimeFactors(6), PrimeFactors(15)}),
                ),
                (
                    PrimeFactors(18),
                    frozenset({PrimeFactors(90), PrimeFactors(36), PrimeFactors(126)}),
                    frozenset({PrimeFactors(6)}),
                ),
                (
                    PrimeFactors(6),
                    frozenset(
                        {
                            PrimeFactors(42),
                            PrimeFactors(18),
                            PrimeFactors(12),
                            PrimeFactors(30),
                        },
                    ),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(14),
                    frozenset({PrimeFactors(42)}),
                    frozenset({PrimeFactors(2)}),
                ),
                (
                    PrimeFactors(15),
                    frozenset({PrimeFactors(30)}),
                    frozenset({PrimeFactors(3)}),
                ),
                (
                    PrimeFactors(2),
                    frozenset({PrimeFactors(6), PrimeFactors(14)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(3),
                    frozenset({PrimeFactors(6), PrimeFactors(15)}),
                    frozenset({PrimeFactors(1)}),
                ),
                (
                    PrimeFactors(1),
                    frozenset({PrimeFactors(2), PrimeFactors(3)}),
                    frozenset(),
                ),
            },
        )
        for neighbourhood in lattice.cover.neighbourhoods():
            for successor in neighbourhood.successors:
                self.assertIn(successor, neighbourhood.successors)
            for predecessor in neighbourhood.predecessors:
                self.assertIn(predecessor, neighbourhood.predecessors)

    def test_top(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertEqual(list(lattice.top), [])
        self.assertEqual(len(lattice.top), 0)
        self.assertNotIn(1, lattice.top)
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(lattice.top),
            [PrimeFactors(2520)],
        )

        self.assertIn(PrimeFactors(2520), lattice.top)
        self.assertNotIn(PrimeFactors(6), lattice.top)
        self.assertNotIn(6, lattice.top)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(set(lattice.top), {PrimeFactors(60480)})
        self.assertEqual(len(lattice.top), 1)
        self.assertIn(PrimeFactors(60480), lattice.top)
        self.assertNotIn(PrimeFactors(42), lattice.top)
        self.assertNotIn(1, lattice.top)

    def test_bottom(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(lattice.bottom),
            [PrimeFactors(1)],
        )

        self.assertIn(PrimeFactors(1), lattice.bottom)
        self.assertNotIn(PrimeFactors(6), lattice.bottom)
        self.assertNotIn(6, lattice.bottom)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(set(lattice.bottom), {PrimeFactors(1)})
        self.assertEqual(len(lattice.bottom), 1)
        self.assertIn(PrimeFactors(1), lattice.bottom)
        self.assertNotIn(PrimeFactors(42), lattice.bottom)
        self.assertNotIn(1, lattice.bottom)

    # noinspection PyTypeChecker
    def test_filter(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(42))
        for element in bounded:
            self.assertIn(element, bounded)
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(2520),
                PrimeFactors(840),
                PrimeFactors(504),
                PrimeFactors(1260),
                PrimeFactors(168),
                PrimeFactors(420),
                PrimeFactors(252),
                PrimeFactors(630),
                PrimeFactors(84),
                PrimeFactors(210),
                PrimeFactors(126),
                PrimeFactors(42),
            },
        )
        with self.assertRaises(ValueError):
            _ = lattice.filter(PrimeFactors(5))

    # noinspection PyTypeChecker
    def test_ideal(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.ideal(PrimeFactors(840))
        for element in bounded:
            self.assertIn(element, bounded)
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(840),
                PrimeFactors(168),
                PrimeFactors(120),
                PrimeFactors(420),
                PrimeFactors(24),
                PrimeFactors(84),
                PrimeFactors(60),
                PrimeFactors(210),
                PrimeFactors(12),
                PrimeFactors(42),
                PrimeFactors(30),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(1),
            },
        )
        with self.assertRaises(ValueError):
            _ = lattice.ideal(PrimeFactors(5))

    # noinspection PyTypeChecker
    def test_filter_ideal(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.filter(PrimeFactors(6)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(840),
                PrimeFactors(168),
                PrimeFactors(120),
                PrimeFactors(420),
                PrimeFactors(24),
                PrimeFactors(84),
                PrimeFactors(60),
                PrimeFactors(210),
                PrimeFactors(12),
                PrimeFactors(42),
                PrimeFactors(30),
                PrimeFactors(6),
            },
        )
        bounded = lattice.filter(PrimeFactors(42)).ideal(PrimeFactors(840))
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(840),
                PrimeFactors(168),
                PrimeFactors(420),
                PrimeFactors(84),
                PrimeFactors(210),
                PrimeFactors(42),
            },
        )

    # noinspection PyTypeChecker
    def test_ideal_filter(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        bounded = lattice.ideal(PrimeFactors(840)).filter(PrimeFactors(6))
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(840),
                PrimeFactors(168),
                PrimeFactors(120),
                PrimeFactors(420),
                PrimeFactors(24),
                PrimeFactors(84),
                PrimeFactors(60),
                PrimeFactors(210),
                PrimeFactors(12),
                PrimeFactors(42),
                PrimeFactors(30),
                PrimeFactors(6),
            },
        )
        bounded = lattice.ideal(PrimeFactors(840)).filter(PrimeFactors(42))
        self.assertEqual(
            set(bounded),
            {
                PrimeFactors(840),
                PrimeFactors(168),
                PrimeFactors(420),
                PrimeFactors(84),
                PrimeFactors(210),
                PrimeFactors(42),
            },
        )

    def test_lower_limit(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for limit in lattice:
            for element in lattice:
                if element >= limit:
                    self.assertTrue(lattice.lower_limit(limit))
                    self.assertIn(element, lattice.lower_limit(limit))
                else:
                    self.assertNotIn(element, lattice.lower_limit(limit))

    def test_upper_limit(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        for limit in lattice:
            for element in lattice:
                if element <= limit:
                    self.assertTrue(lattice.upper_limit(limit))
                    self.assertIn(element, lattice.upper_limit(limit))
                else:
                    self.assertNotIn(element, lattice.upper_limit(limit))
            self.assertNotIn(2, lattice.upper_limit(limit))
            self.assertEqual(
                set(lattice.upper_limit(limit)),
                {element for element in lattice if element <= limit},
            )
            self.assertEqual(
                len(lattice.upper_limit(limit)),
                len({element for element in lattice if element <= limit}),
            )
            for element in lattice:
                if element < limit:
                    self.assertIn(
                        element,
                        lattice.upper_limit(limit, strict=True),
                    )
                else:
                    self.assertNotIn(
                        element,
                        lattice.upper_limit(limit, strict=True),
                    )
            self.assertNotIn(2, lattice.upper_limit(limit, strict=True))
            self.assertEqual(
                set(lattice.upper_limit(limit, strict=True)),
                {element for element in lattice if element < limit},
            )
            self.assertEqual(
                len(lattice.upper_limit(limit, strict=True)),
                len({element for element in lattice if element < limit}),
            )

    def test_join_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(12),
                PrimeFactors(3),
                PrimeFactors(2),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(24),
                PrimeFactors(18),
            },
        )

        self.assertIn(PrimeFactors(24), lattice.join_irreducible)
        self.assertNotIn(PrimeFactors(1), lattice.join_irreducible)
        self.assertNotIn(1, lattice.join_irreducible)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)],
        )
        self.assertIn(PrimeFactors(2), lattice.join_irreducible)
        self.assertIn(PrimeFactors(3), lattice.join_irreducible)
        self.assertIn(PrimeFactors(5), lattice.join_irreducible)
        self.assertNotIn(PrimeFactors(1), lattice.join_irreducible)

        lattice = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(1)])
        self.assertEqual(set(lattice.join_irreducible), set())

        lattice.extend([PrimeFactors(2)])
        self.assertEqual(set(lattice.join_irreducible), {PrimeFactors(2)})

        lattice.extend([PrimeFactors(12)])
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(2), PrimeFactors(12)},
        )

        lattice.extend([PrimeFactors(18)])
        self.assertEqual(
            lattice.join_irreducible,
            {PrimeFactors(2), PrimeFactors(12), PrimeFactors(6), PrimeFactors(18)},
        )

        lattice.extend([PrimeFactors(30)])
        self.assertEqual(
            lattice.join_irreducible,
            {
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(30),
            },
        )

        lattice.extend([PrimeFactors(42)])
        self.assertEqual(
            lattice.join_irreducible,
            {
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
            },
        )

        lattice.extend([PrimeFactors(5040)])
        self.assertEqual(
            lattice.join_irreducible,
            {
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
            },
        )

        lattice.extend([PrimeFactors(7560)])
        self.assertEqual(
            lattice.join_irreducible,
            {
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(2520),
                PrimeFactors(7560),
            },
        )

        lattice.extend([PrimeFactors(30240)])
        self.assertEqual(
            lattice.join_irreducible,
            {
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(2520),
                PrimeFactors(7560),
                PrimeFactors(30240),
            },
        )

        lattice.extend([PrimeFactors(60480)])
        self.assertEqual(
            lattice.join_irreducible,
            {
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(2520),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            },
        )

    def test_meet_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(630),
                PrimeFactors(840),
                PrimeFactors(504),
                PrimeFactors(360),
                PrimeFactors(1260),
            },
        )

        self.assertIn(PrimeFactors(14), lattice.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), lattice.meet_irreducible)
        self.assertNotIn(1, lattice.meet_irreducible)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)],
        )
        self.assertIn(PrimeFactors(6), lattice.meet_irreducible)
        self.assertIn(PrimeFactors(10), lattice.meet_irreducible)
        self.assertIn(PrimeFactors(15), lattice.meet_irreducible)
        self.assertNotIn(PrimeFactors(1), lattice.meet_irreducible)

        lattice = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(1)])
        self.assertEqual(set(lattice.meet_irreducible), set())

        lattice.extend([PrimeFactors(2)])
        self.assertEqual(set(lattice.meet_irreducible), {PrimeFactors(1)})

        lattice.extend([PrimeFactors(12)])
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(2), PrimeFactors(1)},
        )

        lattice.extend([PrimeFactors(18)])
        self.assertEqual(
            lattice.meet_irreducible,
            {PrimeFactors(12), PrimeFactors(18), PrimeFactors(2), PrimeFactors(1)},
        )

        lattice.extend([PrimeFactors(30)])
        self.assertEqual(
            lattice.meet_irreducible,
            {
                PrimeFactors(36),
                PrimeFactors(90),
                PrimeFactors(60),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

        lattice.extend([PrimeFactors(42)])
        self.assertEqual(
            lattice.meet_irreducible,
            {
                PrimeFactors(180),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(252),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

        lattice.extend([PrimeFactors(5040)])
        self.assertEqual(
            lattice.meet_irreducible,
            {
                PrimeFactors(1260),
                PrimeFactors(180),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(252),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

        lattice.extend([PrimeFactors(7560)])
        self.assertEqual(
            lattice.meet_irreducible,
            {
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(1260),
                PrimeFactors(180),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(252),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

        lattice.extend([PrimeFactors(30240)])
        self.assertEqual(
            lattice.meet_irreducible,
            {
                PrimeFactors(15120),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(1260),
                PrimeFactors(180),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(252),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

        lattice.extend([PrimeFactors(60480)])
        self.assertEqual(
            lattice.meet_irreducible,
            {
                PrimeFactors(30240),
                PrimeFactors(15120),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(1260),
                PrimeFactors(180),
                PrimeFactors(420),
                PrimeFactors(630),
                PrimeFactors(252),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

    def test_co_atoms(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertEqual(list(lattice.co_atoms), [])
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(lattice.co_atoms),
            {
                PrimeFactors(360),
                PrimeFactors(504),
                PrimeFactors(1260),
                PrimeFactors(840),
            },
        )

        self.assertIn(PrimeFactors(840), lattice.co_atoms)
        self.assertNotIn(PrimeFactors(1), lattice.co_atoms)
        self.assertNotIn(1, lattice.co_atoms)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(set(lattice.co_atoms), {PrimeFactors(30240)})
        self.assertEqual(len(lattice.co_atoms), 1)
        self.assertIn(PrimeFactors(30240), lattice.co_atoms)
        self.assertNotIn(PrimeFactors(42), lattice.co_atoms)
        self.assertNotIn(1, lattice.co_atoms)

    def test_atoms(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        self.assertEqual(list(lattice.atoms), [])
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(lattice.atoms),
            [PrimeFactors(3), PrimeFactors(2)],
        )

        self.assertIn(PrimeFactors(3), lattice.atoms)
        self.assertNotIn(PrimeFactors(1), lattice.atoms)
        self.assertNotIn(1, lattice.atoms)

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(set(lattice.atoms), {PrimeFactors(2)})
        self.assertEqual(len(lattice.atoms), 1)
        self.assertIn(PrimeFactors(2), lattice.atoms)
        self.assertNotIn(PrimeFactors(42), lattice.atoms)
        self.assertNotIn(1, lattice.atoms)

    def test_greatest_join_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            list(
                lattice.greatest_join_irreducible(
                    PrimeFactors(21),
                    strict=True,
                ),
            ),
            [PrimeFactors(3)],
        )
        self.assertEqual(
            list(
                lattice.greatest_join_irreducible(
                    PrimeFactors(6),
                    strict=True,
                ),
            ),
            [PrimeFactors(3), PrimeFactors(2)],
        )
        self.assertEqual(
            list(lattice.greatest_join_irreducible(PrimeFactors(72))),
            [PrimeFactors(24), PrimeFactors(18)],
        )
        self.assertIn(
            PrimeFactors(24),
            lattice.greatest_join_irreducible(PrimeFactors(72)),
        )
        self.assertNotIn(
            PrimeFactors(6),
            lattice.greatest_join_irreducible(PrimeFactors(72)),
        )
        self.assertNotIn(14, lattice.greatest_join_irreducible(PrimeFactors(72)))

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(
            set(lattice.greatest_join_irreducible(PrimeFactors(11))),
            set(),
        )
        self.assertEqual(
            set(lattice.greatest_join_irreducible(PrimeFactors(36))),
            {PrimeFactors(12), PrimeFactors(18)},
        )
        self.assertIn(
            PrimeFactors(12),
            lattice.greatest_join_irreducible(PrimeFactors(36)),
        )
        self.assertNotIn(
            PrimeFactors(1),
            lattice.greatest_join_irreducible(PrimeFactors(36)),
        )

    def test_smallest_meet_irreducible(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
                PrimeFactors(36),
            ],
        )
        self.assertEqual(
            set(
                lattice.smallest_meet_irreducible(
                    PrimeFactors(6),
                ),
            ),
            {
                PrimeFactors(630),
                PrimeFactors(840),
                PrimeFactors(504),
                PrimeFactors(360),
            },
        )
        self.assertEqual(
            set(
                lattice.smallest_meet_irreducible(
                    PrimeFactors(6),
                    strict=True,
                ),
            ),
            {
                PrimeFactors(630),
                PrimeFactors(840),
                PrimeFactors(504),
                PrimeFactors(360),
            },
        )
        self.assertEqual(
            list(lattice.smallest_meet_irreducible(PrimeFactors(360))),
            [PrimeFactors(360)],
        )
        self.assertIn(
            PrimeFactors(630),
            lattice.smallest_meet_irreducible(PrimeFactors(6)),
        )
        self.assertNotIn(
            PrimeFactors(6),
            lattice.smallest_meet_irreducible(PrimeFactors(6)),
        )
        self.assertNotIn(14, lattice.smallest_meet_irreducible(PrimeFactors(6)))

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(
            set(lattice.smallest_meet_irreducible(PrimeFactors(11))),
            set(),
        )
        self.assertEqual(
            set(lattice.smallest_meet_irreducible(PrimeFactors(36))),
            {PrimeFactors(180), PrimeFactors(252)},
        )
        self.assertIn(
            PrimeFactors(180),
            lattice.smallest_meet_irreducible(PrimeFactors(36)),
        )
        self.assertNotIn(
            PrimeFactors(1000),
            lattice.smallest_meet_irreducible(PrimeFactors(36)),
        )

    def test_interval(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(42),
                PrimeFactors(5040),
                PrimeFactors(7560),
                PrimeFactors(30240),
                PrimeFactors(60480),
            ],
        )
        self.assertEqual(
            lattice.interval(PrimeFactors(24)),
            (PrimeFactors(12), PrimeFactors(2520)),
        )

    def test_extend(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(6), PrimeFactors(14)],
        )
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(3), PrimeFactors(6), PrimeFactors(14)},
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(2), PrimeFactors(3), PrimeFactors(14)},
        )
        lattice.extend(
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[
                    PrimeFactors(15),
                    PrimeFactors(18),
                    PrimeFactors(24),
                    PrimeFactors(36),
                ],
            ),
        )
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(840),
                PrimeFactors(360),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(630),
                PrimeFactors(1260),
                PrimeFactors(504),
            },
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(12),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(18),
                PrimeFactors(24),
            },
        )

        # Import old tests
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(1), PrimeFactors(2)],
        )
        self.assertEqual(set(lattice), {PrimeFactors(1), PrimeFactors(2)})
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(2))
        self.assertEqual(set(lattice.meet_irreducible), {PrimeFactors(1)})
        self.assertEqual(set(lattice.join_irreducible), {PrimeFactors(2)})

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(1),
                PrimeFactors(2),
                PrimeFactors(6),
                PrimeFactors(30),
            ],
        )
        self.assertEqual(
            set(lattice),
            {PrimeFactors(30), PrimeFactors(6), PrimeFactors(2), PrimeFactors(1)},
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(30))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(1), PrimeFactors(2), PrimeFactors(6)},
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(2), PrimeFactors(6), PrimeFactors(30)},
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(3)],
        )
        self.assertEqual(
            set(lattice),
            {PrimeFactors(6), PrimeFactors(2), PrimeFactors(3), PrimeFactors(1)},
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(6))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(2), PrimeFactors(3)},
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(2), PrimeFactors(3)},
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(14), PrimeFactors(15)],
        )
        self.assertEqual(
            set(lattice),
            {PrimeFactors(210), PrimeFactors(14), PrimeFactors(15), PrimeFactors(1)},
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(210))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(14), PrimeFactors(15)},
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(14), PrimeFactors(15)},
        )

        lattice.extend([PrimeFactors(42)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(210),
                PrimeFactors(15),
                PrimeFactors(42),
                PrimeFactors(3),
                PrimeFactors(14),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(210))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(15), PrimeFactors(42), PrimeFactors(14)},
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(14), PrimeFactors(3), PrimeFactors(15)},
        )

        lattice.extend([PrimeFactors(105)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(210),
                PrimeFactors(42),
                PrimeFactors(105),
                PrimeFactors(14),
                PrimeFactors(21),
                PrimeFactors(15),
                PrimeFactors(7),
                PrimeFactors(3),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(210))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {PrimeFactors(42), PrimeFactors(105), PrimeFactors(15), PrimeFactors(14)},
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {PrimeFactors(3), PrimeFactors(7), PrimeFactors(14), PrimeFactors(15)},
        )

        lattice.extend([PrimeFactors(22)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(2310),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(30),
                PrimeFactors(66),
                PrimeFactors(105),
                PrimeFactors(42),
                PrimeFactors(154),
                PrimeFactors(6),
                PrimeFactors(15),
                PrimeFactors(22),
                PrimeFactors(21),
                PrimeFactors(14),
                PrimeFactors(3),
                PrimeFactors(2),
                PrimeFactors(7),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(2310))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(154),
                PrimeFactors(105),
            },
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(7),
                PrimeFactors(22),
                PrimeFactors(15),
            },
        )

        lattice.extend([PrimeFactors(11)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(2310),
                PrimeFactors(1155),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(165),
                PrimeFactors(105),
                PrimeFactors(231),
                PrimeFactors(30),
                PrimeFactors(66),
                PrimeFactors(42),
                PrimeFactors(154),
                PrimeFactors(15),
                PrimeFactors(33),
                PrimeFactors(21),
                PrimeFactors(77),
                PrimeFactors(6),
                PrimeFactors(22),
                PrimeFactors(14),
                PrimeFactors(3),
                PrimeFactors(11),
                PrimeFactors(7),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(2310))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(1155),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(154),
            },
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(11),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(7),
                PrimeFactors(15),
            },
        )

        lattice.extend([PrimeFactors(4620)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(4620),
                PrimeFactors(2310),
                PrimeFactors(1155),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(165),
                PrimeFactors(105),
                PrimeFactors(231),
                PrimeFactors(30),
                PrimeFactors(66),
                PrimeFactors(42),
                PrimeFactors(154),
                PrimeFactors(15),
                PrimeFactors(33),
                PrimeFactors(21),
                PrimeFactors(77),
                PrimeFactors(6),
                PrimeFactors(22),
                PrimeFactors(14),
                PrimeFactors(3),
                PrimeFactors(11),
                PrimeFactors(7),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(4620))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(2310),
                PrimeFactors(1155),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(154),
            },
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(11),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(7),
                PrimeFactors(15),
                PrimeFactors(4620),
            },
        )

        lattice.extend([PrimeFactors(5)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(4620),
                PrimeFactors(2310),
                PrimeFactors(770),
                PrimeFactors(1155),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
                PrimeFactors(385),
                PrimeFactors(110),
                PrimeFactors(70),
                PrimeFactors(154),
                PrimeFactors(165),
                PrimeFactors(105),
                PrimeFactors(231),
                PrimeFactors(30),
                PrimeFactors(66),
                PrimeFactors(42),
                PrimeFactors(55),
                PrimeFactors(35),
                PrimeFactors(77),
                PrimeFactors(10),
                PrimeFactors(22),
                PrimeFactors(14),
                PrimeFactors(15),
                PrimeFactors(33),
                PrimeFactors(21),
                PrimeFactors(6),
                PrimeFactors(5),
                PrimeFactors(11),
                PrimeFactors(7),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(4620))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(2310),
                PrimeFactors(770),
                PrimeFactors(1155),
                PrimeFactors(330),
                PrimeFactors(210),
                PrimeFactors(462),
            },
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(3),
                PrimeFactors(2),
                PrimeFactors(11),
                PrimeFactors(4620),
            },
        )

        lattice.extend([PrimeFactors(195)])  # , PrimeFactors(6930)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(60060),
                PrimeFactors(4620),
                PrimeFactors(30030),
                PrimeFactors(2310),
                PrimeFactors(2730),
                PrimeFactors(4290),
                PrimeFactors(15015),
                PrimeFactors(770),
                PrimeFactors(462),
                PrimeFactors(210),
                PrimeFactors(330),
                PrimeFactors(1155),
                PrimeFactors(390),
                PrimeFactors(1365),
                PrimeFactors(2145),
                PrimeFactors(154),
                PrimeFactors(70),
                PrimeFactors(110),
                PrimeFactors(385),
                PrimeFactors(42),
                PrimeFactors(66),
                PrimeFactors(231),
                PrimeFactors(30),
                PrimeFactors(105),
                PrimeFactors(165),
                PrimeFactors(195),
                PrimeFactors(14),
                PrimeFactors(22),
                PrimeFactors(77),
                PrimeFactors(10),
                PrimeFactors(35),
                PrimeFactors(55),
                PrimeFactors(6),
                PrimeFactors(21),
                PrimeFactors(33),
                PrimeFactors(15),
                PrimeFactors(2),
                PrimeFactors(7),
                PrimeFactors(11),
                PrimeFactors(5),
                PrimeFactors(3),
                PrimeFactors(1),
            },
        )
        self.assertEqual(lattice.minimum, PrimeFactors(1))
        self.assertEqual(lattice.maximum, PrimeFactors(60060))
        self.assertEqual(
            set(lattice.meet_irreducible),
            {
                PrimeFactors(30030),
                PrimeFactors(4620),
                PrimeFactors(2730),
                PrimeFactors(4290),
                PrimeFactors(15015),
                PrimeFactors(770),
                PrimeFactors(462),
            },
        )
        self.assertEqual(
            set(lattice.join_irreducible),
            {
                PrimeFactors(11),
                PrimeFactors(2),
                PrimeFactors(3),
                PrimeFactors(7),
                PrimeFactors(5),
                PrimeFactors(195),
                PrimeFactors(4620),
            },
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(12)])
        lattice.extend(
            ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(18)]),
        )
        self.assertEqual(
            set(lattice),
            {PrimeFactors(36), PrimeFactors(18), PrimeFactors(12), PrimeFactors(6)},
        )
        lattice.extend([PrimeFactors(3)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(36),
                PrimeFactors(18),
                PrimeFactors(12),
                PrimeFactors(6),
                PrimeFactors(3),
            },
        )
        lattice.extend([PrimeFactors(5)])
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(180),
                PrimeFactors(36),
                PrimeFactors(60),
                PrimeFactors(90),
                PrimeFactors(12),
                PrimeFactors(18),
                PrimeFactors(30),
                PrimeFactors(6),
                PrimeFactors(15),
                PrimeFactors(3),
                PrimeFactors(5),
                PrimeFactors(1),
            },
        )

        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(3), PrimeFactors(6)],
        )
        lattice.extend(
            ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(8), PrimeFactors(10), PrimeFactors(20)],
            ),
        )
        lattice2 = ExtensibleFiniteLattice[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(8),
                PrimeFactors(10),
                PrimeFactors(20),
            ],
        )
        self.assertEqual(lattice, lattice2)
        self.assertEqual(
            set(lattice.join_irreducible),
            set(lattice2.join_irreducible),
        )
        self.assertEqual(
            set(lattice.meet_irreducible),
            set(lattice2.meet_irreducible),
        )

    def test_extend_lattice(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(12)])
        other = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(4), PrimeFactors(5)],
        )
        lattice.extend(other)
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(60),
                PrimeFactors(20),
                PrimeFactors(12),
                PrimeFactors(10),
                PrimeFactors(4),
                PrimeFactors(5),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

    def test_extend_join_semi_lattice(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(12)])
        other = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(4), PrimeFactors(5)],
        )
        lattice.extend(other)
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(60),
                PrimeFactors(20),
                PrimeFactors(12),
                PrimeFactors(10),
                PrimeFactors(4),
                PrimeFactors(5),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )

    def test_extend_meet_semi_lattice(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](elements=[PrimeFactors(12)])
        other = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
            elements=[PrimeFactors(2), PrimeFactors(4), PrimeFactors(5)],
        )
        lattice.extend(other)
        self.assertEqual(
            set(lattice),
            {
                PrimeFactors(60),
                PrimeFactors(20),
                PrimeFactors(12),
                PrimeFactors(10),
                PrimeFactors(4),
                PrimeFactors(5),
                PrimeFactors(2),
                PrimeFactors(1),
            },
        )


class ReducedContextTestCase(TestCase):
    def test___init__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        context = ReducedContext[PrimeFactors](lattice)
        self.assertFalse(context)
        self.assertEqual(set(context), set())
        lattice.extend(
            [PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)],
        )
        self.assertEqual(
            set(context),
            {
                (PrimeFactors(5), PrimeFactors(15)),
                (PrimeFactors(5), PrimeFactors(10)),
                (PrimeFactors(2), PrimeFactors(6)),
                (PrimeFactors(2), PrimeFactors(10)),
                (PrimeFactors(3), PrimeFactors(6)),
                (PrimeFactors(3), PrimeFactors(15)),
            },
        )

    def test___contains__(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        context = ReducedContext[PrimeFactors](lattice)
        self.assertNotIn((PrimeFactors(2), PrimeFactors(15)), context)
        self.assertNotIn(1, context)
        lattice.extend(
            [PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)],
        )
        self.assertIn((PrimeFactors(5), PrimeFactors(15)), context)
        self.assertNotIn((PrimeFactors(2), PrimeFactors(15)), context)
        self.assertNotIn(1, context)

    def test_universes(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors]()
        context = ReducedContext[PrimeFactors](lattice)
        self.assertFalse(context.domain)
        self.assertEqual(list(context.domain), [])
        self.assertFalse(context.co_domain)
        self.assertEqual(list(context.co_domain), [])
        self.assertEqual(len(context.domain), 0)
        self.assertEqual(len(context.co_domain), 0)
        lattice.extend([PrimeFactors(2)])
        self.assertTrue(context.domain)
        self.assertTrue(context.co_domain)
        self.assertEqual(list(context.domain), [PrimeFactors(2)])
        self.assertEqual(list(context.co_domain), [PrimeFactors(2)])
        self.assertEqual(len(context.domain), 1)
        self.assertEqual(len(context.co_domain), 1)
        lattice.extend([PrimeFactors(3), PrimeFactors(5)])
        self.assertEqual(list(context.domain), list(lattice.join_irreducible))
        self.assertEqual(list(context.co_domain), list(lattice.meet_irreducible))
        self.assertEqual(len(context.domain), 3)
        self.assertEqual(len(context.co_domain), 3)

    def test_successors(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(6), PrimeFactors(5), PrimeFactors(7)],
        )
        context = ReducedContext[PrimeFactors](lattice)
        self.assertTrue(context.successors(PrimeFactors(7)))
        self.assertTrue(context.predecessors(PrimeFactors(35)))
        self.assertEqual(
            {
                (element, successor)
                for element in context.domain
                for successor in context.successors(element)
            },
            {
                (PrimeFactors(7), PrimeFactors(35)),
                (PrimeFactors(7), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(30)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(5), PrimeFactors(30)),
                (PrimeFactors(5), PrimeFactors(35)),
            },
        )
        with self.assertRaises(ValueError):
            _ = context.successors(PrimeFactors(1))
        successors_6 = context.successors(PrimeFactors(6))
        self.assertEqual(set(successors_6), {PrimeFactors(30), PrimeFactors(42)})
        self.assertEqual(len(successors_6), 2)
        self.assertIn(PrimeFactors(30), successors_6)
        self.assertNotIn(PrimeFactors(6), successors_6)
        self.assertNotIn(1, successors_6)

        lattice.extend([PrimeFactors(2), PrimeFactors(3)])
        self.assertEqual(set(successors_6), {PrimeFactors(30), PrimeFactors(42)})

    def test_predecessors(self):
        lattice = ExtensibleFiniteLattice[PrimeFactors](
            elements=[PrimeFactors(6), PrimeFactors(5), PrimeFactors(7)],
        )
        context = ReducedContext[PrimeFactors](lattice)
        self.assertEqual(
            {
                (element, successor)
                for element in context.domain
                for successor in context.successors(element)
            },
            {
                (PrimeFactors(7), PrimeFactors(35)),
                (PrimeFactors(7), PrimeFactors(42)),
                (PrimeFactors(6), PrimeFactors(30)),
                (PrimeFactors(6), PrimeFactors(42)),
                (PrimeFactors(5), PrimeFactors(30)),
                (PrimeFactors(5), PrimeFactors(35)),
            },
        )
        with self.assertRaises(ValueError):
            _ = context.predecessors(PrimeFactors(1))
        predecessors_30 = context.predecessors(PrimeFactors(30))
        self.assertEqual(set(predecessors_30), {PrimeFactors(6), PrimeFactors(5)})
        self.assertEqual(len(predecessors_30), 2)
        self.assertIn(PrimeFactors(5), predecessors_30)
        self.assertNotIn(PrimeFactors(2), predecessors_30)
        self.assertNotIn(1, predecessors_30)

        lattice.extend([PrimeFactors(60), PrimeFactors(90)])


class ReducedContextDiagramTestCase(TestCase):
    def test_source(self):
        with suppress(ModuleNotFoundError):
            lattice = ExtensibleFiniteLattice[PrimeFactors](
                elements=[PrimeFactors(2), PrimeFactors(3), PrimeFactors(5)],
            )
            context = ReducedContext[PrimeFactors](lattice)
            diagram = ReducedContextDiagram(
                context,
                domain_renderer=PrimeFactorsRenderer(),
                co_domain_renderer=PrimeFactorsRenderer(),
            )
            self.assertEqual(
                diagram.source.strip(),
                """
digraph {
    graph [rankdir=BT]
    {
        graph [rank=same]
        D0_0 [label=2 shape=circle]
        D0_1 [label=3 shape=circle]
        D0_2 [label=5 shape=circle]
    }
    {
        graph [rank=same]
        D1_0 [label=6 shape=circle]
        D1_1 [label=10 shape=circle]
        D1_2 [label=15 shape=circle]
    }
    D0_0 -> D1_0
    D0_0 -> D1_1
    D0_1 -> D1_0
    D0_1 -> D1_2
    D0_2 -> D1_1
    D0_2 -> D1_2
}
                """.strip().replace(
                    "    ",
                    "\t",
                ),
            )
