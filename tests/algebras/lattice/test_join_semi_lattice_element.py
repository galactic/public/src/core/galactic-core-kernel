"""The :mod:`test_join_semi_lattice_element` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.lattice import supremum, supremum_generators


class JoinableTest(TestCase):
    def test__or(self):
        a = PrimeFactors(32)
        b = PrimeFactors(56)
        self.assertEqual(
            a | b,
            PrimeFactors(224),
            "The least common multiple of 32 and 56 is 224",
        )
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            _ = a | 5

    def test_join(self):
        a = PrimeFactors(32)
        b = PrimeFactors(56)
        c = PrimeFactors(88)
        self.assertEqual(
            a.join(b, c),
            PrimeFactors(2464),
            "The least common multiple of 32, 56 and 88 is 2464",
        )

        self.assertEqual(
            supremum([a, b, c]),
            PrimeFactors(2464),
            "The least common multiple of 32, 56 and 88 is 2464",
        )

    def test_supremum_generators(self):
        self.assertEqual(
            set(
                supremum_generators(
                    [PrimeFactors(32), PrimeFactors(56), PrimeFactors(8)],
                ),
            ),
            {PrimeFactors(32), PrimeFactors(56), PrimeFactors(8)},
        )
