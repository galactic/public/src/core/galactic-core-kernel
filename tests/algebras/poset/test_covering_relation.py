"""The :mod:`test_covering_relation` module."""

import copy
from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.poset import MutableFinitePartiallyOrderedSet


class FiniteCoveringRelationTestCase(TestCase):
    def test___init__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]]()
        self.assertEqual(len(poset.cover), 0)
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        relation = poset.cover
        self.assertTrue(relation)
        self.assertEqual(
            set(relation.domain),
            {
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            },
        )

    def test___eq__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(poset.cover == other.cover, False)
        self.assertEqual(poset.cover, poset.cover)

    def test___ne__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = copy.copy(poset)
        other.add(frozenset([1, 2, 3, 5]))
        self.assertNotEqual(poset.cover, other.cover)
        self.assertNotEqual(poset.cover, 1)
        self.assertEqual(poset.cover != poset.cover, False)

    def test___contains__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertIn((frozenset([1, 2]), frozenset([1, 2, 3])), poset.cover)
        self.assertNotIn((frozenset([1, 2, 3]), frozenset([1, 2, 3])), poset.cover)
        self.assertNotIn(1, poset.cover)

    def test___len__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(len(poset.cover), 3)

    def test___iter__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            list(poset.cover),
            [
                (frozenset([1, 2]), frozenset([1, 2, 3])),
                (frozenset([2, 3]), frozenset([1, 2, 3])),
                (frozenset([1, 2, 3]), frozenset([1, 2, 3, 4])),
            ],
        )
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(210),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            list(poset.cover),
            [
                (PrimeFactors(5), PrimeFactors(25)),
                (PrimeFactors(5), PrimeFactors(35)),
                (PrimeFactors(5), PrimeFactors(30)),
                (PrimeFactors(7), PrimeFactors(35)),
                (PrimeFactors(2), PrimeFactors(30)),
                (PrimeFactors(35), PrimeFactors(210)),
                (PrimeFactors(30), PrimeFactors(210)),
            ],
        )

    def test_successors(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        successors = poset.cover.successors(frozenset([1, 2]))
        self.assertTrue(successors)
        self.assertIn(frozenset([1, 2, 3]), successors)
        self.assertNotIn(frozenset([1, 2]), successors)
        self.assertNotIn(1, successors)
        self.assertEqual(set(successors), {frozenset([1, 2, 3])})
        self.assertEqual(len(successors), 1)
        poset.discard(frozenset([1, 2]))
        with self.assertRaises(ValueError):
            _ = poset.cover.successors(frozenset([1, 2, 4]))

        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(210),
                PrimeFactors(2),
            ],
        )
        with self.assertRaises(ValueError):
            _ = integers.cover.successors(PrimeFactors(1))
        self.assertEqual(
            set(integers.cover.successors(PrimeFactors(5))),
            {PrimeFactors(25), PrimeFactors(30), PrimeFactors(35)},
            "The immediate successors of 5 are 25, 30, 35",
        )

        self.assertIn(PrimeFactors(30), integers.cover.successors(PrimeFactors(5)))
        self.assertNotIn(PrimeFactors(5), integers.cover.successors(PrimeFactors(5)))
        self.assertNotIn(1, integers.cover.successors(PrimeFactors(5)))
        self.assertNotIn({1}, integers.cover.successors(PrimeFactors(5)))

    def test_predecessors(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        predecessors = poset.cover.predecessors(frozenset([1, 2, 3]))
        self.assertTrue(predecessors)
        self.assertIn(frozenset([1, 2]), predecessors)
        self.assertNotIn(frozenset([1, 2, 3, 4]), predecessors)
        self.assertNotIn(1, predecessors)
        self.assertEqual(set(predecessors), {frozenset([1, 2]), frozenset([2, 3])})
        self.assertEqual(len(predecessors), 2)
        poset.discard(frozenset([1, 2, 3]))
        with self.assertRaises(ValueError):
            _ = poset.cover.predecessors(frozenset([1, 2, 4]))

        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
                PrimeFactors(10),
                PrimeFactors(15),
            ],
        )
        with self.assertRaises(ValueError):
            _ = integers.cover.predecessors(PrimeFactors(1))
        self.assertEqual(
            set(integers.cover.predecessors(PrimeFactors(70))),
            {PrimeFactors(10), PrimeFactors(35)},
            "The immediate predecessors of 70 are 10, 35",
        )

        self.assertIn(PrimeFactors(5), integers.cover.predecessors(PrimeFactors(10)))
        self.assertNotIn(PrimeFactors(2), integers.cover.predecessors(PrimeFactors(10)))
        self.assertNotIn(1, integers.cover.predecessors(PrimeFactors(10)))
        self.assertNotIn({1}, integers.cover.predecessors(PrimeFactors(10)))

    def test_universes(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            poset.cover.universes,
            (poset.cover.domain, poset.cover.co_domain),
        )

        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            poset.cover.universes,
            (poset.cover.domain, poset.cover.co_domain),
        )

        relation = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
                PrimeFactors(10),
                PrimeFactors(15),
            ],
        )
        self.assertEqual(
            set(relation.cover.universes[0]),
            {
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
                PrimeFactors(10),
                PrimeFactors(15),
            },
        )

    def test_sinks(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset.cover.sinks),
            [
                frozenset({1, 2, 3, 5}),
                frozenset({1, 2, 3, 4}),
            ],
        )
        self.assertIn(
            frozenset({1, 2, 3, 5}),
            poset.cover.sinks,
        )

    def test_sources(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            set(poset.cover.sources),
            {
                frozenset([1, 2]),
                frozenset([2, 3]),
            },
        )
        self.assertIn(
            frozenset([1, 2]),
            poset.cover.sources,
        )

    def test_neighbourhoods(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({1, 2}), {frozenset({1, 2, 3})}, set()),
                (frozenset({2, 3}), {frozenset({1, 2, 3})}, set()),
                (
                    frozenset({1, 2, 3}),
                    {frozenset({1, 2, 3, 4})},
                    {frozenset({2, 3}), frozenset({1, 2})},
                ),
                (frozenset({1, 2, 3, 4}), set(), {frozenset({1, 2, 3})}),
            ],
        )

        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
                PrimeFactors(10),
                PrimeFactors(15),
            ],
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    list(neighbourhood.successors),
                    list(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (
                    PrimeFactors(5),
                    [
                        PrimeFactors(25),
                        PrimeFactors(35),
                        PrimeFactors(10),
                        PrimeFactors(15),
                    ],
                    [],
                ),
                (
                    PrimeFactors(7),
                    [PrimeFactors(35)],
                    [],
                ),
                (
                    PrimeFactors(25),
                    [],
                    [PrimeFactors(5)],
                ),
                (
                    PrimeFactors(35),
                    [PrimeFactors(70)],
                    [PrimeFactors(5), PrimeFactors(7)],
                ),
                (
                    PrimeFactors(30),
                    [],
                    [PrimeFactors(10), PrimeFactors(15)],
                ),
                (
                    PrimeFactors(70),
                    [],
                    [PrimeFactors(35), PrimeFactors(10)],
                ),
                (
                    PrimeFactors(10),
                    [PrimeFactors(30), PrimeFactors(70)],
                    [PrimeFactors(5)],
                ),
                (
                    PrimeFactors(15),
                    [PrimeFactors(30)],
                    [PrimeFactors(5)],
                ),
            ],
        )

    def test_domain(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(210),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            set(poset.cover.domain),
            {
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(210),
                PrimeFactors(2),
            },
        )

    def test_domain_add(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        poset.add(frozenset([1, 2, 3, 5]))
        self.assertEqual(
            list(poset.cover.domain),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

    def test_domain_discard(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        poset.discard(frozenset([1, 2, 3, 5]))
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({1, 2}), {frozenset({1, 2, 3})}, set()),
                (frozenset({2, 3}), {frozenset({1, 2, 3})}, set()),
                (
                    frozenset({1, 2, 3}),
                    {frozenset({1, 2, 3, 4})},
                    {frozenset({2, 3}), frozenset({1, 2})},
                ),
                (frozenset({1, 2, 3, 4}), set(), {frozenset({1, 2, 3})}),
            ],
        )
        poset.discard(frozenset([1, 2, 3]))
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({1, 2}), {frozenset({1, 2, 3, 4})}, set()),
                (frozenset({2, 3}), {frozenset({1, 2, 3, 4})}, set()),
                (
                    frozenset({1, 2, 3, 4}),
                    set(),
                    {frozenset({1, 2}), frozenset({2, 3})},
                ),
            ],
        )
        poset.discard(frozenset([1, 2]))
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({2, 3}), {frozenset({1, 2, 3, 4})}, set()),
                (frozenset({1, 2, 3, 4}), set(), {frozenset({2, 3})}),
            ],
        )
        poset.discard(frozenset([2, 3]))
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({1, 2, 3, 4}), set(), set()),
            ],
        )
        poset.discard(frozenset([1, 2, 3, 4]))
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [],
        )

    def test_domain_pop(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

        self.assertEqual(
            poset.pop(),
            frozenset([1, 2]),
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({2, 3}), {frozenset({1, 2, 3})}, set()),
                (
                    frozenset({1, 2, 3}),
                    {frozenset({1, 2, 3, 5}), frozenset({1, 2, 3, 4})},
                    {frozenset({2, 3})},
                ),
                (frozenset({1, 2, 3, 4}), set(), {frozenset({1, 2, 3})}),
                (frozenset({1, 2, 3, 5}), set(), {frozenset({1, 2, 3})}),
            ],
        )

        self.assertEqual(
            poset.pop(),
            frozenset([2, 3]),
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (
                    frozenset({1, 2, 3}),
                    {frozenset({1, 2, 3, 5}), frozenset({1, 2, 3, 4})},
                    set(),
                ),
                (frozenset({1, 2, 3, 4}), set(), {frozenset({1, 2, 3})}),
                (frozenset({1, 2, 3, 5}), set(), {frozenset({1, 2, 3})}),
            ],
        )

        self.assertEqual(
            poset.pop(),
            frozenset({1, 2, 3}),
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({1, 2, 3, 4}), set(), set()),
                (frozenset({1, 2, 3, 5}), set(), set()),
            ],
        )

        self.assertEqual(
            poset.pop(),
            frozenset({1, 2, 3, 4}),
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [
                (frozenset({1, 2, 3, 5}), set(), set()),
            ],
        )

        self.assertEqual(
            poset.pop(),
            frozenset({1, 2, 3, 5}),
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    set(neighbourhood.successors),
                    set(neighbourhood.predecessors),
                )
                for neighbourhood in poset.cover.neighbourhoods()
            ],
            [],
        )

    def test_domain_clear(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        poset.clear()
        self.assertEqual(len(poset.cover), 0)

    def test_domain_extend(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset[int]](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        poset.extend([frozenset([1, 2, 4])])
        self.assertEqual(
            list(poset.cover.domain),
            [
                frozenset({1, 2}),
                frozenset({2, 3}),
                frozenset({1, 2, 3}),
                frozenset({1, 2, 4}),
                frozenset({1, 2, 3, 4}),
                frozenset({1, 2, 3, 5}),
            ],
        )

    def test_co_domain(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(210),
                PrimeFactors(2),
            ],
        )
        self.assertEqual(
            list(poset.cover.co_domain),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(2),
                PrimeFactors(25),
                PrimeFactors(210),
            ],
        )
        poset.extend({PrimeFactors(14)})
        self.assertEqual(
            list(poset.cover.co_domain),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(2),
                PrimeFactors(14),
                PrimeFactors(25),
                PrimeFactors(210),
            ],
        )
