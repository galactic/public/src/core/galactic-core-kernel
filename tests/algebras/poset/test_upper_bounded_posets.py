"""The :mod:`test_upper_bounded_posets` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.poset import (
    MutableFinitePartiallyOrderedSet,
)


class UpperBoundedSetTestCase(TestCase):
    def test___init__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        with self.assertRaises(ValueError):
            _ = integers.ideal(PrimeFactors(1))

    def test___len__(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertEqual(
            len(poset),
            3,
        )

    def test___contains__(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertIn(
            PrimeFactors(35),
            poset,
        )
        self.assertNotIn(
            PrimeFactors(25),
            poset,
        )
        self.assertNotIn(
            1,
            poset,
        )

    def test___iter__(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertEqual(
            list(poset),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )

    def test___eq__(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertEqual(
            poset,
            poset,
        )
        self.assertNotEqual(
            poset,
            1,
        )

    def test___ne__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        poset1 = integers.ideal(PrimeFactors(35))
        poset2 = integers.ideal(PrimeFactors(25))
        self.assertNotEqual(poset1, poset2)

    def test___le__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        poset1 = integers.ideal(PrimeFactors(35))
        poset2 = integers.ideal(PrimeFactors(70))
        self.assertLessEqual(poset1, poset1)
        self.assertLessEqual(poset1, poset2)

    def test___lt__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        poset1 = integers.ideal(PrimeFactors(35))
        poset2 = integers.ideal(PrimeFactors(70))
        self.assertFalse(poset1 < poset1)
        self.assertLess(poset1, poset2)

    def test___ge__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        poset1 = integers.ideal(PrimeFactors(35))
        poset2 = integers.ideal(PrimeFactors(70))
        self.assertGreaterEqual(poset1, poset1)
        self.assertGreaterEqual(poset2, poset1)

    def test___gt__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        poset1 = integers.ideal(PrimeFactors(35))
        poset2 = integers.ideal(PrimeFactors(70))
        self.assertFalse(poset1 > poset1)
        self.assertGreater(poset2, poset1)

    def test_upper_limit(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertEqual(
            list(poset.upper_limit(PrimeFactors(35))),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )
        self.assertEqual(
            list(poset.upper_limit(PrimeFactors(5))),
            [
                PrimeFactors(5),
            ],
        )

    def test_lower_limit(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertEqual(
            list(poset.lower_limit(PrimeFactors(5))),
            [
                PrimeFactors(5),
                PrimeFactors(35),
            ],
        )

    def test_cover(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        cover = poset.cover
        # Test covering relation
        self.assertIn((PrimeFactors(5), PrimeFactors(35)), cover)
        self.assertNotIn(1, cover)
        self.assertEqual(
            len(cover),
            2,
        )
        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    list(neighbourhood.successors),
                    list(neighbourhood.predecessors),
                )
                for neighbourhood in cover.neighbourhoods()
            ],
            [
                (PrimeFactors(5), [PrimeFactors(35)], []),
                (PrimeFactors(7), [PrimeFactors(35)], []),
                (PrimeFactors(35), [], [PrimeFactors(5), PrimeFactors(7)]),
            ],
        )

    def test_cover_sinks(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        cover = integers.ideal(PrimeFactors(35)).cover
        self.assertEqual(
            list(cover.sinks),
            [
                PrimeFactors(35),
            ],
        )

    def test_cover_sources(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        cover = integers.ideal(PrimeFactors(35)).cover
        self.assertEqual(
            list(cover.sources),
            [
                PrimeFactors(5),
                PrimeFactors(7),
            ],
        )

    def test_cover_domain(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        cover = poset.cover
        self.assertIn(
            PrimeFactors(35),
            cover.domain,
        )
        self.assertNotIn(
            PrimeFactors(25),
            cover.domain,
        )
        self.assertEqual(
            len(cover.domain),
            3,
        )
        self.assertEqual(
            list(cover.domain),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )
        self.assertEqual(
            list(cover.co_domain),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )
        self.assertIs(cover.universes[0], cover.domain)
        self.assertIs(cover.universes[0], cover.co_domain)

    def test_cover_predecessors(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        cover = poset.cover
        self.assertIn(
            PrimeFactors(5),
            cover.predecessors(PrimeFactors(35)),
        )
        self.assertEqual(
            len(cover.predecessors(PrimeFactors(35))),
            2,
        )
        self.assertEqual(
            list(cover.predecessors(PrimeFactors(35))),
            [PrimeFactors(5), PrimeFactors(7)],
        )
        self.assertIn(
            PrimeFactors(5),
            cover.predecessors(PrimeFactors(35)),
        )
        self.assertNotIn(
            PrimeFactors(25),
            cover.predecessors(PrimeFactors(35)),
        )
        self.assertNotIn(
            1,
            cover.predecessors(PrimeFactors(35)),
        )
        self.assertEqual(
            len(cover.predecessors(PrimeFactors(35))),
            2,
        )
        with self.assertRaises(ValueError):
            _ = cover.predecessors(PrimeFactors(25))

    def test_cover_successors(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        cover = poset.cover
        self.assertIn(
            PrimeFactors(35),
            cover.successors(PrimeFactors(5)),
        )
        self.assertNotIn(
            PrimeFactors(25),
            cover.successors(PrimeFactors(5)),
        )
        self.assertNotIn(
            1,
            cover.successors(PrimeFactors(5)),
        )
        self.assertEqual(
            len(cover.successors(PrimeFactors(5))),
            1,
        )

        with self.assertRaises(ValueError):
            _ = cover.successors(PrimeFactors(25))

    def test_order(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        order = poset.order
        # Test partial order
        self.assertIn((PrimeFactors(5), PrimeFactors(35)), order)
        self.assertNotIn((PrimeFactors(7), PrimeFactors(30)), order)
        self.assertNotIn(1, order)
        self.assertEqual(
            len(order),
            5,
        )

    def test_order_sinks(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        order = poset.order
        self.assertEqual(
            list(order.sinks),
            [
                PrimeFactors(35),
            ],
        )

    def test_order_sources(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        order = poset.order
        self.assertEqual(
            list(order.sources),
            [
                PrimeFactors(5),
                PrimeFactors(7),
            ],
        )

    def test_order_domain(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        order = poset.order
        self.assertIn(
            PrimeFactors(5),
            order.domain,
        )
        self.assertNotIn(
            PrimeFactors(25),
            order.domain,
        )
        self.assertEqual(
            len(order.domain),
            3,
        )
        self.assertEqual(
            list(order.domain),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )
        self.assertEqual(
            list(order.co_domain),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )
        self.assertIs(order.universes[0], order.domain)
        self.assertIs(order.universes[0], order.co_domain)

    def test_order_predecessors(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        order = poset.order
        self.assertIn(
            PrimeFactors(5),
            order.predecessors(PrimeFactors(35)),
        )
        self.assertIn(
            PrimeFactors(35),
            order.predecessors(PrimeFactors(35)),
        )
        self.assertEqual(
            len(order.predecessors(PrimeFactors(35))),
            3,
        )
        self.assertEqual(
            list(order.predecessors(PrimeFactors(35))),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
            ],
        )
        self.assertIn(
            PrimeFactors(5),
            order.predecessors(PrimeFactors(35)),
        )
        self.assertNotIn(
            PrimeFactors(25),
            order.predecessors(PrimeFactors(35)),
        )
        self.assertNotIn(
            1,
            order.predecessors(PrimeFactors(35)),
        )
        with self.assertRaises(ValueError):
            _ = order.predecessors(PrimeFactors(25))

    def test_order_successors(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        order = poset.order
        self.assertIn(
            PrimeFactors(35),
            order.successors(PrimeFactors(35)),
        )
        self.assertEqual(
            len(order.successors(PrimeFactors(5))),
            2,
        )
        self.assertEqual(
            list(order.successors(PrimeFactors(5))),
            [
                PrimeFactors(5),
                PrimeFactors(35),
            ],
        )
        with self.assertRaises(ValueError):
            _ = order.successors(PrimeFactors(25))

    def test_top(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        top = integers.ideal(PrimeFactors(35)).top
        self.assertIn(
            PrimeFactors(35),
            top,
        )
        self.assertNotIn(
            PrimeFactors(25),
            top,
        )
        self.assertNotIn(
            1,
            top,
        )
        self.assertEqual(
            len(top),
            1,
        )
        self.assertEqual(
            list(top),
            [
                PrimeFactors(35),
            ],
        )

    def test_maximum(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertEqual(poset.maximum, PrimeFactors(35))

    def test_bottom(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        bottom = integers.ideal(PrimeFactors(35)).bottom
        self.assertIn(
            PrimeFactors(5),
            bottom,
        )
        self.assertNotIn(
            PrimeFactors(25),
            bottom,
        )
        self.assertNotIn(
            1,
            bottom,
        )
        self.assertEqual(
            len(bottom),
            2,
        )
        self.assertEqual(
            list(bottom),
            [
                PrimeFactors(5),
                PrimeFactors(7),
            ],
        )

    def test_minimum(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        ).ideal(PrimeFactors(35))
        self.assertIsNone(poset.minimum)

    def test_ideal(self):
        poset = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .ideal(PrimeFactors(35))
            .ideal(PrimeFactors(5))
        )
        self.assertEqual(
            list(poset),
            [
                PrimeFactors(5),
            ],
        )
