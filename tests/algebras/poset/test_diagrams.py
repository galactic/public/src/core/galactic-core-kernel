"""The :mod:`test_diagrams` module."""

from contextlib import suppress
from unittest import TestCase

from galactic.algebras.examples.arithmetic import (
    PrimeFactors,
)
from galactic.algebras.poset import (
    MutableFinitePartiallyOrderedSet,
)
from galactic.algebras.poset.renderer import (
    HasseDiagram,
    IterativeDiagram,
)


class HasseDiagramTestCase(TestCase):
    def test_graphviz(self):
        with suppress(ModuleNotFoundError):
            integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            result = """
digraph {
    graph [rankdir=BT]
    N0 [label=5 shape=circle]
    N1 [label=25 shape=circle]
    N0 -> N1
    N2 [label=35 shape=circle]
    N0 -> N2
    N3 [label=30 shape=circle]
    N0 -> N3
    N4 [label=7 shape=circle]
    N4 -> N2
    N5 [label=70 shape=circle]
    N2 -> N5
}               """
            self.assertEqual(
                HasseDiagram(integers).source.strip().replace("\t", "    "),
                result.strip(),
            )

            integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(24),
                    PrimeFactors(18),
                    PrimeFactors(9),
                    PrimeFactors(6),
                    PrimeFactors(3),
                    PrimeFactors(2),
                ],
            )
            result = """
digraph {
    graph [rankdir=BT]
    N0 [label=3 shape=circle]
    N1 [label=9 shape=circle]
    N0 -> N1
    N2 [label=6 shape=circle]
    N0 -> N2
    N3 [label=2 shape=circle]
    N3 -> N2
    N4 [label=24 shape=circle]
    N2 -> N4
    N5 [label=18 shape=circle]
    N1 -> N5
    N2 -> N5
}
            """
            self.assertEqual(
                HasseDiagram(integers).source.strip().replace("\t", "    "),
                result.strip(),
            )


class IterativeDiagramTestCase(TestCase):
    def test_graphviz(self):
        with suppress(ModuleNotFoundError):
            integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(24),
                    PrimeFactors(18),
                    PrimeFactors(9),
                    PrimeFactors(6),
                    PrimeFactors(3),
                    PrimeFactors(2),
                ],
            )
            diagrams = IterativeDiagram(integers)
            iterator = iter(diagrams)
            diagram = next(iterator)
            self.assertEqual(
                diagram.source.strip().replace("\t", "    "),
                """
digraph {
    graph [rankdir=BT]
}
                """.strip(),
            )

            diagram = next(iterator)
            self.assertEqual(
                diagram.source.strip().replace("\t", "    "),
                """
digraph {
    graph [rankdir=BT]
    N0 [label=3 shape=circle]
    N1 [label=9 shape=circle]
    N0 -> N1
    N2 [label=6 shape=circle]
    N0 -> N2
}
                """.strip(),
            )

            diagram = next(iterator)
            self.assertEqual(
                diagram.source.strip().replace("\t", "    "),
                """
digraph {
    graph [rankdir=BT]
    N0 [label=3 shape=circle]
    N1 [label=9 shape=circle]
    N0 -> N1
    N2 [label=6 shape=circle]
    N0 -> N2
    N3 [label=2 shape=circle]
    N3 -> N2
}
                """.strip(),
            )

            diagram = next(iterator)
            self.assertEqual(
                diagram.source.strip().replace("\t", "    "),
                """
digraph {
    graph [rankdir=BT]
    N0 [label=3 shape=circle]
    N1 [label=9 shape=circle]
    N0 -> N1
    N2 [label=6 shape=circle]
    N0 -> N2
    N3 [label=2 shape=circle]
    N3 -> N2
    N4 [label=24 shape=circle]
    N2 -> N4
}
                """.strip(),
            )

            diagram = next(iterator)
            self.assertEqual(
                diagram.source.strip().replace("\t", "    "),
                """
digraph {
    graph [rankdir=BT]
    N0 [label=3 shape=circle]
    N1 [label=9 shape=circle]
    N0 -> N1
    N2 [label=6 shape=circle]
    N0 -> N2
    N3 [label=2 shape=circle]
    N3 -> N2
    N4 [label=24 shape=circle]
    N2 -> N4
    N5 [label=18 shape=circle]
    N1 -> N5
    N2 -> N5
}
                """.strip(),
            )
