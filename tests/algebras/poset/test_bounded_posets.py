"""The :mod:`test_bounded_posets` module."""

from unittest import TestCase

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.poset import (
    FinitePartiallyOrderedSetView,
    MutableFinitePartiallyOrderedSet,
)


class BoundedSetTestCase(TestCase):
    def test___init__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        view = FinitePartiallyOrderedSetView[PrimeFactors](integers)
        self.assertEqual(set(integers), set(view))

        bounded = integers.filter(PrimeFactors(5)).ideal(PrimeFactors(70))
        self.assertEqual(
            bounded,
            integers.ideal(PrimeFactors(70)).filter(PrimeFactors(5)),
        )
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        view = FinitePartiallyOrderedSetView[PrimeFactors](integers)
        self.assertEqual(view, integers)

    def test___eq__(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertEqual(
            list(bounded.order),
            [
                (PrimeFactors(5), PrimeFactors(5)),
                (PrimeFactors(5), PrimeFactors(35)),
                (PrimeFactors(5), PrimeFactors(70)),
                (PrimeFactors(35), PrimeFactors(35)),
                (PrimeFactors(35), PrimeFactors(70)),
                (PrimeFactors(70), PrimeFactors(70)),
            ],
        )

    def test___len__(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertEqual(len(bounded.order), 6)

    def test___contains__(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertIn((PrimeFactors(5), PrimeFactors(5)), bounded.order)
        self.assertNotIn(1, bounded.order)

    def test_isdisjoint(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertTrue(bounded.isdisjoint([PrimeFactors(4)]))

    def test_issubset(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertTrue(
            bounded.issubset(
                [
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            ),
        )

    def test_issuperset(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertFalse(
            bounded.issuperset(
                [
                    PrimeFactors(5),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(70),
                ],
            ),
        )
        self.assertTrue(
            bounded.issuperset(
                [
                    PrimeFactors(5),
                    PrimeFactors(35),
                    PrimeFactors(70),
                ],
            ),
        )

    def test_successors(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        with self.assertRaises(ValueError):
            _ = bounded.order.successors(PrimeFactors(1))
        self.assertTrue(bounded.order.successors(PrimeFactors(5)))
        self.assertEqual(
            list(bounded.order.successors(PrimeFactors(5))),
            [PrimeFactors(5), PrimeFactors(35), PrimeFactors(70)],
        )
        self.assertIn(PrimeFactors(35), bounded.order.successors(PrimeFactors(5)))
        self.assertNotIn(PrimeFactors(25), bounded.order.successors(PrimeFactors(5)))
        self.assertNotIn({1}, bounded.order.successors(PrimeFactors(5)))

    def test_predecessors(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        with self.assertRaises(ValueError):
            _ = bounded.order.predecessors(PrimeFactors(1))
        self.assertTrue(bounded.order.predecessors(PrimeFactors(35)))
        self.assertEqual(
            list(bounded.order.predecessors(PrimeFactors(35))),
            [PrimeFactors(5), PrimeFactors(35)],
        )
        self.assertIn(PrimeFactors(5), bounded.order.predecessors(PrimeFactors(35)))
        self.assertNotIn(PrimeFactors(25), bounded.order.predecessors(PrimeFactors(35)))
        self.assertNotIn({1}, bounded.order.predecessors(PrimeFactors(35)))

    def test_domain(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertEqual(
            list(bounded.order.domain),
            [PrimeFactors(5), PrimeFactors(35), PrimeFactors(70)],
        )
        self.assertIn(PrimeFactors(5), bounded.order.domain)
        self.assertNotIn(PrimeFactors(7), bounded.order.domain)
        self.assertNotIn({1}, bounded.order.domain)

    def test_cover(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertEqual(
            list(bounded.cover),
            [
                (PrimeFactors(5), PrimeFactors(35)),
                (PrimeFactors(35), PrimeFactors(70)),
            ],
        )
        self.assertEqual(
            list(bounded.cover.domain),
            [PrimeFactors(5), PrimeFactors(35), PrimeFactors(70)],
        )
        self.assertEqual(list(bounded.cover.sinks), [PrimeFactors(70)])
        self.assertEqual(list(bounded.cover.sources), [PrimeFactors(5)])

        self.assertTrue(bounded.cover.successors(PrimeFactors(35)))
        self.assertEqual(
            list(bounded.cover.successors(PrimeFactors(35))),
            [PrimeFactors(70)],
        )
        with self.assertRaises(ValueError):
            _ = bounded.cover.successors(PrimeFactors(1))
        self.assertTrue(bounded.cover.predecessors(PrimeFactors(35)))
        self.assertEqual(
            list(bounded.cover.predecessors(PrimeFactors(35))),
            [PrimeFactors(5)],
        )
        with self.assertRaises(ValueError):
            _ = bounded.cover.predecessors(PrimeFactors(1))

        self.assertEqual(
            [
                (
                    neighbourhood.element,
                    list(neighbourhood.successors),
                    list(neighbourhood.predecessors),
                )
                for neighbourhood in bounded.cover.neighbourhoods()
            ],
            [
                (PrimeFactors(5), [PrimeFactors(35)], []),
                (PrimeFactors(35), [PrimeFactors(70)], [PrimeFactors(5)]),
                (PrimeFactors(70), [], [PrimeFactors(35)]),
            ],
        )

    def test_top(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertTrue(bounded.top)
        self.assertEqual(list(bounded.top), [PrimeFactors(70)])
        self.assertIn(PrimeFactors(70), bounded.top)
        self.assertNotIn(PrimeFactors(35), bounded.top)

    def test_bottom(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertTrue(bounded.bottom)
        self.assertEqual(list(bounded.bottom), [PrimeFactors(5)])

    def test_ideal(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertEqual(
            list(bounded.ideal(PrimeFactors(35))),
            [PrimeFactors(5), PrimeFactors(35)],
        )

        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = bounded.ideal(1)

    def test_filter(self):
        bounded = (
            MutableFinitePartiallyOrderedSet[PrimeFactors](
                elements=[
                    PrimeFactors(5),
                    PrimeFactors(25),
                    PrimeFactors(7),
                    PrimeFactors(35),
                    PrimeFactors(30),
                    PrimeFactors(70),
                ],
            )
            .filter(PrimeFactors(5))
            .ideal(PrimeFactors(70))
        )
        self.assertEqual(
            list(bounded.filter(PrimeFactors(35))),
            [PrimeFactors(35), PrimeFactors(70)],
        )

        with self.assertRaises(ValueError):
            # noinspection PyTypeChecker
            _ = bounded.filter(1)


class DomainTestCase(TestCase):
    def test___iter__(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(4),
                PrimeFactors(12),
            ],
        )
        self.assertEqual(
            list(poset),
            [PrimeFactors(3), PrimeFactors(6), PrimeFactors(4), PrimeFactors(12)],
        )

    def test_add(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(4),
                PrimeFactors(12),
            ],
        )
        poset.add(PrimeFactors(2))
        self.assertEqual(
            list(poset),
            [
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(4),
                PrimeFactors(2),
                PrimeFactors(12),
            ],
        )

    def test_discard(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(4),
                PrimeFactors(12),
            ],
        )
        poset.discard(PrimeFactors(6))
        self.assertEqual(
            list(poset),
            [PrimeFactors(3), PrimeFactors(4), PrimeFactors(12)],
        )
        self.assertEqual(
            list(poset.order),
            [
                (PrimeFactors(3), PrimeFactors(3)),
                (PrimeFactors(3), PrimeFactors(12)),
                (PrimeFactors(4), PrimeFactors(4)),
                (PrimeFactors(4), PrimeFactors(12)),
                (PrimeFactors(12), PrimeFactors(12)),
            ],
        )

    def test_clear(self):
        poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(3),
                PrimeFactors(6),
                PrimeFactors(4),
                PrimeFactors(12),
            ],
        )
        poset.clear()
        self.assertEqual(list(poset), [])
        self.assertEqual(list(poset.order), [])

    def test_update(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        integers.update([PrimeFactors(5)])
        self.assertEqual(
            list(integers.order.successors(PrimeFactors(7))),
            [PrimeFactors(7), PrimeFactors(35), PrimeFactors(70)],
        )
        self.assertEqual(
            list(integers.order.predecessors(PrimeFactors(7))),
            [PrimeFactors(7)],
        )
        self.assertEqual(
            list(integers),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(25),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        integers.update([PrimeFactors(14)])
        self.assertEqual(
            list(integers.order.successors(PrimeFactors(7))),
            [PrimeFactors(7), PrimeFactors(35), PrimeFactors(14), PrimeFactors(70)],
        )
        self.assertEqual(
            list(integers.order.predecessors(PrimeFactors(14))),
            [PrimeFactors(7), PrimeFactors(14)],
        )
        self.assertEqual(
            list(integers),
            [
                PrimeFactors(5),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(14),
                PrimeFactors(25),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )

    def test___iand__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        integers &= MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(90),
            ],
        )
        self.assertEqual(list(integers), [PrimeFactors(5)])

    def test___ixor__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        integers ^= MutableFinitePartiallyOrderedSet[PrimeFactors](
            [
                PrimeFactors(5),
                PrimeFactors(90),
            ],
        )
        self.assertEqual(
            set(integers),
            {
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(25),
                PrimeFactors(70),
                PrimeFactors(90),
            },
        )

        integers ^= integers
        self.assertEqual(list(integers), [])

    def test___isub__(self):
        integers = MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(25),
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        integers -= MutableFinitePartiallyOrderedSet[PrimeFactors](
            elements=[
                PrimeFactors(5),
                PrimeFactors(90),
            ],
        )
        self.assertEqual(
            list(integers),
            [
                PrimeFactors(7),
                PrimeFactors(35),
                PrimeFactors(25),
                PrimeFactors(30),
                PrimeFactors(70),
            ],
        )
        integers -= integers
        self.assertEqual(list(integers), [])
