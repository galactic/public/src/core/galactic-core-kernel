"""The :mod:`test_partially_ordered` module."""

import copy
from unittest import TestCase

from galactic.algebras.poset import (
    FrozenFinitePartiallyOrderedSet,
    MutableFinitePartiallyOrderedSet,
    bottom,
    lower_limit,
    top,
    upper_limit,
)


class PosetTestCase(TestCase):
    def test_top(self):
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            _ = top(1)
        self.assertEqual(
            list(
                top(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 4]),
                        frozenset([1, 2, 3, 5]),
                    ],
                ),
            ),
            [
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

    def test_bottom(self):
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            _ = bottom(1)
        self.assertEqual(
            list(
                bottom(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 4]),
                        frozenset([1, 2, 3, 5]),
                    ],
                ),
            ),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
            ],
        )

    def test_upper_limit(self):
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            _ = next(upper_limit(1, 1))
        self.assertEqual(
            list(
                upper_limit(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 4]),
                        frozenset([1, 2, 3, 5]),
                    ],
                    frozenset([1, 2, 3]),
                ),
            ),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertEqual(
            list(
                upper_limit(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 4]),
                        frozenset([1, 2, 3, 5]),
                    ],
                    frozenset([1, 2, 3]),
                    strict=True,
                ),
            ),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
            ],
        )

    def test_lower_limit(self):
        with self.assertRaises(TypeError):
            # noinspection PyTypeChecker
            _ = next(lower_limit(1, 1))
        self.assertEqual(
            list(
                lower_limit(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 4]),
                        frozenset([1, 2, 3, 5]),
                    ],
                    frozenset([1, 2, 3]),
                ),
            ),
            [
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(
                lower_limit(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 4]),
                        frozenset([1, 2, 3, 5]),
                    ],
                    frozenset([1, 2, 3]),
                    strict=True,
                ),
            ),
            [
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )


class MutableFinitePartiallyOrderedSetTestCase(TestCase):
    def test___init__(self):
        with self.assertRaises(TypeError):
            _ = MutableFinitePartiallyOrderedSet[int](elements=1)
        self.assertFalse(MutableFinitePartiallyOrderedSet())
        self.assertEqual(len(MutableFinitePartiallyOrderedSet()), 0)
        self.assertEqual(
            list(MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])),
            [1, 2, 3],
        )
        self.assertTrue(MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3]))

    def test_hash(self):
        poset = FrozenFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        self.assertEqual(hash(poset), hash(copy.copy(poset)))

    def test___copy__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset),
            list(copy.copy(poset)),
        )

    def test___contains__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertIn(frozenset([1, 2]), poset)
        self.assertIn(frozenset([1, 2, 3, 5]), poset)
        self.assertNotIn(frozenset([1, 2, 3, 5, 6]), poset)
        self.assertNotIn(1, poset)

    def test___len__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            len(poset),
            5,
        )

    def test___iter__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

    def test___lt__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertLess(poset, other)
        self.assertFalse(other < poset)
        self.assertFalse(poset < poset)

    def test___gt__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertGreater(poset, other)
        self.assertFalse(other > poset)
        self.assertFalse(poset > poset)

    def test_isdisjoint(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertTrue(poset.isdisjoint([frozenset([1, 2, 3, 4])]))
        self.assertFalse(poset.isdisjoint([frozenset([1, 2, 3])]))

    def test___le__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertLessEqual(poset, other)
        self.assertLessEqual(poset, poset)
        self.assertFalse(other <= poset)

    def test_issubset(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertTrue(
            poset.issubset(
                [
                    frozenset([1, 2]),
                    frozenset([2, 3]),
                    frozenset([1, 2, 3]),
                    frozenset([1, 2, 3, 4]),
                ],
            ),
        )
        self.assertFalse(
            poset.issubset(
                [
                    frozenset([2, 3]),
                    frozenset([1, 2, 3]),
                    frozenset([1, 2, 3, 4]),
                ],
            ),
        )

    def test___ge__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertGreaterEqual(poset, other)
        self.assertGreaterEqual(poset, poset)
        self.assertFalse(other >= poset)

    def test_issuperset(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertFalse(
            poset.issuperset(
                [
                    frozenset([1, 2]),
                    frozenset([2, 3]),
                    frozenset([1, 2, 3]),
                    frozenset([1, 2, 3, 4]),
                ],
            ),
        )
        self.assertTrue(
            poset.issuperset(
                [
                    frozenset([2, 3]),
                    frozenset([1, 2, 3]),
                ],
            ),
        )

    def test_add(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        poset.add(frozenset([1, 2, 3, 5]))
        self.assertEqual(
            set(poset),
            {
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            },
        )

    def test_pop(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(poset.pop(), frozenset([1, 2]))
        self.assertEqual(poset.pop(), frozenset([2, 3]))
        self.assertEqual(poset.pop(), frozenset([1, 2, 3, 4]))
        self.assertEqual(poset.pop(), frozenset([1, 2, 3, 5]))
        with self.assertRaises(KeyError):
            poset.pop()

    def test_popitem(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(poset.popitem(last=True), frozenset([1, 2, 3, 5]))
        self.assertEqual(poset.popitem(last=True), frozenset([1, 2, 3, 4]))
        self.assertEqual(poset.popitem(last=True), frozenset([2, 3]))
        self.assertEqual(poset.popitem(last=True), frozenset([1, 2]))
        with self.assertRaises(KeyError):
            poset.popitem(last=True)

    def test___or__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            set(poset | other),
            {
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            },
        )

    def test___ior__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        poset |= other
        self.assertEqual(
            set(poset),
            {
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            },
        )

    def test_union(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            list(
                poset.union(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 5]),
                    ],
                ),
            ),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

    def test_update(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        poset.update(
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

    def test___and__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            set(poset & other),
            {
                frozenset([1, 2]),
                frozenset([2, 3]),
            },
        )

    def test_intersection(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            list(
                poset.intersection(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 5]),
                    ],
                ),
            ),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
            ],
        )

    def test_intersection_update(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        poset.intersection_update(
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
            ],
        )

    def test___sub__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            set(poset - other),
            {
                frozenset([1, 2, 3, 4]),
            },
        )

    def test_difference(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            set(
                poset.difference(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 5]),
                    ],
                ),
            ),
            {
                frozenset([1, 2, 3, 4]),
            },
        )

    def test_difference_update(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        poset.difference_update(
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset),
            [
                frozenset([1, 2, 3, 4]),
            ],
        )

    def test___xor__(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        other = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            set(poset ^ other),
            {
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            },
        )

    def test_symmetric_difference(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertEqual(
            set(
                poset.symmetric_difference(
                    [
                        frozenset([1, 2]),
                        frozenset([2, 3]),
                        frozenset([1, 2, 3]),
                        frozenset([1, 2, 3, 5]),
                    ],
                ),
            ),
            {
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            },
        )

    def test_symmetric_difference_update(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        poset.symmetric_difference_update(
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset),
            [
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )

    def test_order(self):
        poset = MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        self.assertTrue(poset.order)
        self.assertIn((1, 1), poset.order)
        self.assertIn((1, 3), poset.order)
        self.assertNotIn((3, 1), poset.order)
        self.assertNotIn(1, poset.order)
        self.assertEqual(len(poset.order), 6)
        self.assertEqual(
            list(poset.order),
            [(1, 1), (1, 2), (1, 3), (2, 2), (2, 3), (3, 3)],
        )

    def test_order_sinks(self):
        poset = MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        self.assertEqual(list(poset.order.sinks), [3])

    def test_order_sources(self):
        poset = MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        self.assertEqual(list(poset.order.sources), [1])

    def test_order_universes(self):
        poset = MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        self.assertEqual(list(poset.order.domain), [1, 2, 3])
        self.assertEqual(list(poset.order.co_domain), [1, 2, 3])
        self.assertEqual(
            tuple(list(universe) for universe in poset.order.universes),
            ([1, 2, 3], [1, 2, 3]),
        )

    def test_order_successors(self):
        poset = MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        successors = poset.order.successors(2)
        self.assertEqual(list(successors), [2, 3])
        self.assertIn(2, successors)
        self.assertNotIn(1, successors)
        self.assertNotIn({1}, successors)
        poset.remove(2)
        with self.assertRaises(ValueError):
            _ = poset.order.successors(2)

    def test_order_predecessors(self):
        poset = MutableFinitePartiallyOrderedSet[int](elements=[1, 2, 3])
        predecessors = poset.order.predecessors(2)
        self.assertEqual(list(predecessors), [1, 2])
        self.assertIn(2, predecessors)
        self.assertNotIn(3, predecessors)
        self.assertNotIn({1}, predecessors)
        poset.remove(2)
        with self.assertRaises(ValueError):
            _ = poset.order.predecessors(2)

    def test_top(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset.top),
            [
                frozenset([1, 2, 3, 5]),
                frozenset([1, 2, 3, 4]),
            ],
        )
        self.assertNotIn({1}, poset.top)

    def test_maximum(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertIsNone(poset.maximum)
        poset.discard(frozenset([1, 2, 3, 5]))
        self.assertEqual(poset.maximum, frozenset([1, 2, 3, 4]))

    def test_tail(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(poset.tail, frozenset([1, 2, 3, 5]))
        poset.clear()
        with self.assertRaises(KeyError):
            _ = poset.tail

    def test_bottom(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset.bottom),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
            ],
        )
        self.assertNotIn({1}, poset.bottom)

    def test_minimum(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertIsNone(poset.minimum)
        poset.discard(frozenset([1, 2]))
        self.assertEqual(poset.minimum, frozenset([2, 3]))

    def test_head(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(poset.head, frozenset([1, 2]))
        poset.clear()
        with self.assertRaises(KeyError):
            _ = poset.head

    def test_upper_limit(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        collection = poset.upper_limit(frozenset([1, 2, 3]))
        self.assertTrue(collection)
        self.assertIn(frozenset([1, 2]), collection)
        self.assertIn(frozenset([2, 3]), collection)
        self.assertIn(frozenset([1, 2, 3]), collection)
        self.assertNotIn(frozenset([1, 2, 3, 4]), collection)
        self.assertNotIn(1, collection)
        self.assertEqual(
            list(collection),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
            ],
        )
        self.assertEqual(
            list(poset.upper_limit(frozenset([1, 2, 3]), strict=True)),
            [
                frozenset([1, 2]),
                frozenset([2, 3]),
            ],
        )
        self.assertIn(
            frozenset([1, 2]),
            poset.upper_limit(frozenset([1, 2, 3]), strict=True),
        )
        self.assertNotIn(
            {1},
            poset.upper_limit(frozenset([1, 2, 3]), strict=True),
        )

    def test_lower_limit(self):
        poset = MutableFinitePartiallyOrderedSet[frozenset](
            elements=[
                frozenset([1, 2]),
                frozenset([2, 3]),
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        collection = poset.lower_limit(frozenset([1, 2, 3]))
        self.assertTrue(collection)
        self.assertIn(frozenset([1, 2, 3]), collection)
        self.assertIn(frozenset([1, 2, 3, 4]), collection)
        self.assertIn(frozenset([1, 2, 3, 5]), collection)
        self.assertNotIn(2, collection)
        self.assertNotIn(frozenset([1, 2]), collection)
        self.assertEqual(len(collection), 3)
        self.assertEqual(
            list(collection),
            [
                frozenset([1, 2, 3]),
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
        self.assertEqual(
            list(poset.lower_limit(frozenset([1, 2, 3]), strict=True)),
            [
                frozenset([1, 2, 3, 4]),
                frozenset([1, 2, 3, 5]),
            ],
        )
