Give the possibility to create a finite subset using a sorted array of
intervals of :class:`int`.

Implement :class:`~galactic.algebras.set.FiniteUniverse` methods:

* :meth:`~galactic.algebras.set.FiniteUniverse.parts`
* :meth:`~galactic.algebras.set.FiniteUniverse.singletons`

Implement :class:`~galactic.algebras.set.FiniteUniverse` attributes:

* :attr:`~galactic.algebras.set.FiniteUniverse.empty`
* :attr:`~galactic.algebras.set.FiniteUniverse.whole`.

Implement :class:`~galactic.algebras.set.FiniteSubSet` methods:

* :meth:`~galactic.algebras.set.FiniteSubSet.from_intervals`
* :meth:`~galactic.algebras.set.FiniteSubSet.intervals`
* :meth:`~galactic.algebras.set.FiniteSubSet.subsets`
* :meth:`~galactic.algebras.set.FiniteSubSet.supersets`
