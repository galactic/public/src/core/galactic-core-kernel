"""The :mod:`_mixins` module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Collection, Iterable, Iterator
from typing import Generic, TypeVar, cast

from galactic.algebras.relational import FiniteRelationMixin
from galactic.helpers.core import default_repr

from ._abstract import AbstractFinitePartiallyOrderedSet, Neighbourhood
from ._element import PartiallyComparable, lower_limit, upper_limit

_P = TypeVar("_P", bound=PartiallyComparable)


class FinitePartiallyOrderedSetMixin(Generic[_P]):
    """
    It represents finite poset mixin.
    """

    __slots__ = ()

    def __bool__(self: AbstractFinitePartiallyOrderedSet[_P]) -> bool:
        for _ in self:
            return True
        return False

    # Set comparison properties and methods

    def __eq__(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: object,
    ) -> bool:
        if isinstance(other, Collection):
            return len(self) == len(other) and all(item in other for item in self)
        return super().__eq__(other)

    def __lt__(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Collection[_P],
    ) -> bool:
        if isinstance(other, Collection):
            return (
                len(self) < len(other)
                and all(item in other for item in self)
                and not all(item in self for item in other)
            )
        return NotImplemented

    def __gt__(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Collection[_P],
    ) -> bool:
        if isinstance(other, Collection):
            return len(self) > len(other) and all(item in self for item in other)
        return NotImplemented

    def __le__(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Collection[_P],
    ) -> bool:
        if isinstance(other, Collection):
            return len(self) <= len(other) and all(item in other for item in self)
        return NotImplemented

    def __ge__(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Collection[_P],
    ) -> bool:
        if isinstance(other, Collection):
            return all(item in self for item in other)
        return NotImplemented

    def isdisjoint(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Iterable[_P],
    ) -> bool:
        """
        Test if the relation is disjoint from the other.

        Parameters
        ----------
        other
            An iterable of couples.

        Returns
        -------
        bool
            True if the relation is disjoint from the other.

        """
        return all(element not in self for element in other)

    def issubset(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Iterable[_P],
    ) -> bool:
        """
        Test whether every element in the relation is in other.

        Parameters
        ----------
        other
            An iterable of couples.

        Returns
        -------
        bool
            True if the relation is a subset of the other.

        """
        data = set(self)
        data.difference_update(other)
        return not data

    def issuperset(
        self: AbstractFinitePartiallyOrderedSet[_P],
        other: Iterable[_P],
    ) -> bool:
        """
        Test whether every element in the other relation is in the relation.

        Parameters
        ----------
        other
            An iterable of couples.

        Returns
        -------
        bool
            True if the relation is a superset of the other.

        """
        return all(element in self for element in other)

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def maximum(self: AbstractFinitePartiallyOrderedSet[_P]) -> _P | None:
        """
        Get the maximum element if any.

        Returns
        -------
        _P | None
            The maximum element.

        """
        if len(self.top) == 1:
            return next(iter(self.top))
        return None

    @property
    def minimum(self: AbstractFinitePartiallyOrderedSet[_P]) -> _P | None:
        """
        Get the minimum element if any.

        Returns
        -------
        _P | None
            The minimum element.

        """
        if len(self.bottom) == 1:
            return next(iter(self.bottom))
        return None

    def upper_limit(
        self: AbstractFinitePartiallyOrderedSet[_P],
        *limits: _P,
        strict: bool = False,
    ) -> Collection[_P]:
        """
        Get the values less than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_P]
            An collection of values.

        """

        class UpperLimit:
            """
            It represents the collection of values less than a limit.
            """

            __slots__ = (
                "_version",
                "_length",
            )

            _length: int | None
            _version: int

            def __init__(self) -> None:
                self._version = poset.version
                self._length = None

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in poset and all(item < limit for limit in limits)  # type: ignore[operator]
                return item in poset and all(item <= limit for limit in limits)  # type: ignore[operator]

            def __len__(self) -> int:
                if self._length is None or self._version != poset.version:
                    self._length = sum(1 for _ in self)
                    self._version = poset.version
                return self._length

            def __iter__(self) -> Iterator[_P]:
                return upper_limit(poset, *limits, strict=strict)

        poset = self
        return UpperLimit()

    def lower_limit(
        self: AbstractFinitePartiallyOrderedSet[_P],
        *limits: _P,
        strict: bool = False,
    ) -> Collection[_P]:
        """
        Get the values greater than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_P]
            A collection of values.

        """

        class LowerLimit:
            """
            It represents the collection of values greater than a limit.
            """

            __slots__ = (
                "_version",
                "_length",
            )

            _length: int | None
            _version: int

            def __init__(self) -> None:
                self._version = poset.version
                self._length = None

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in poset and all(item > limit for limit in limits)  # type: ignore[operator]
                return item in poset and all(item >= limit for limit in limits)  # type: ignore[operator]

            def __len__(self) -> int:
                if self._length is None or self._version != poset.version:
                    self._length = sum(1 for _ in self)
                    self._version = poset.version
                return self._length

            def __iter__(self) -> Iterator[_P]:
                return lower_limit(poset, *limits, strict=strict)

        poset = self
        return LowerLimit()


class FiniteCoveringRelationMixin(FiniteRelationMixin, Generic[_P]):
    """
    It represents finite covering relation.

    Parameters
    ----------
    poset
        A poset.

    """

    __slots__ = ("_version", "_length", "_poset", "_universes")

    _version: int
    _length: int | None
    _poset: AbstractFinitePartiallyOrderedSet[_P]
    _universes: tuple[Collection[_P], Collection[_P]]

    def __init__(self, poset: AbstractFinitePartiallyOrderedSet[_P]) -> None:
        self._poset = poset
        self._version = poset.version
        self._universes = (poset, poset)
        self._length = None

    def __bool__(self) -> bool:
        for _ in self:
            return True
        return False

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if isinstance(item, tuple) and len(item) == 2:
            source, dest = item
            return source in self.domain and dest in self.successors(source)
        return False

    def __len__(self) -> int:
        if self._length is None or self._version != self._poset.version:
            self._length = sum(
                len(neighbourhood.successors) for neighbourhood in self.neighbourhoods()
            )
            self._version = self._poset.version
        return self._length

    def __iter__(self) -> Iterator[tuple[_P, _P]]:
        for neighbourhood in self.neighbourhoods():
            for successor in neighbourhood.successors:
                yield neighbourhood.element, successor

    def __reversed__(self) -> Iterator[tuple[_P, _P]]:
        for neighbourhood in self.neighbourhoods(reverse=True):
            for predecessor in neighbourhood.predecessors:
                yield predecessor, neighbourhood.element

    # AbstractRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[_P], Collection[_P]]:
        """
        Get the universes of this covering relation.

        Returns
        -------
        tuple[Collection[_P], Collection[_P]]
            The universes of this covering relation.

        """
        return self._universes

    # AbstractBinaryRelation properties and methods

    @property
    def domain(self) -> Collection[_P]:
        """
        Get the domain of this covering relation.

        Returns
        -------
        Collection[_P]
            The domain of this covering relation.

        Notes
        -----
        The domain and the co-domain of a covering relation are identical.

        """
        return self._universes[0]

    @property
    def co_domain(self) -> Collection[_P]:
        """
        Get the co-domain of this covering relation.

        Returns
        -------
        Collection[_P]
            The co-domain of this covering relation.

        Notes
        -----
        The domain and the co-domain of a covering relation are identical.

        """
        return self._universes[1]

    @abstractmethod
    def successors(self, element: _P) -> Collection[_P]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
        Collection[_P]
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the element is no longer part of the
        original poset.

        """
        raise NotImplementedError

    @abstractmethod
    def predecessors(self, element: _P) -> Collection[_P]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
        Collection[_P]
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the element is no longer part of the
        original poset.

        """
        raise NotImplementedError

    # AbstractDirectedAcyclicRelation properties and methods

    @property
    def sinks(self) -> Collection[_P]:
        """
        Get the elements with no successors.

        Returns
        -------
        Collection[_P]
            The elements with no successors.

        """
        return self._poset.top

    @property
    def sources(self) -> Collection[_P]:
        """
        Get the elements with no predecessors.

        Returns
        -------
        Collection[_P]
            The elements with no predecessors.

        """
        return self._poset.bottom

    # AbstractCoveringRelation properties and methods

    @abstractmethod
    def neighbourhoods(self, reverse: bool = False) -> Iterator[Neighbourhood[_P]]:
        """
        Get an iterator over the neighbourhoods of an element.

        Parameters
        ----------
        reverse
            Is this a bottom-up generation ?

        Returns
        -------
        Iterator[Neighbourhood[_P]]
            An iterator giving a triple (element, successors, predecessors) for
            each element in the covering relation represented by a
            neighbourhood.

        Raises
        ------
        NotImplementedError
        """
        raise NotImplementedError
