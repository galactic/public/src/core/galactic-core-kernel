"""The :mod:`_abstract` module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Collection, Iterable, Iterator
from dataclasses import dataclass
from typing import Generic, Protocol, TypeVar, cast, runtime_checkable

from galactic.algebras.relational import AbstractFiniteDirectedAcyclicRelation
from galactic.helpers.core import default_repr

from ._element import PartiallyComparable

_P = TypeVar("_P", bound=PartiallyComparable)


@dataclass(frozen=True)
class Neighbourhood(Generic[_P]):
    """
    It represents the neighbourhood in a covering relation.

    It associates an element with its direct successors and predecessors.
    It is a dataclass containing three fields:

    * the element whose neighbourhood is considered;
    * the immediate successors of the element;
    * the immediate predecessors of the element.
    """

    element: _P
    successors: Collection[_P]
    predecessors: Collection[_P]

    def __repr__(self) -> str:
        return cast(str, default_repr(self))


# pylint: disable=too-few-public-methods,duplicate-bases
@runtime_checkable
class AbstractFiniteCoveringRelation(
    AbstractFiniteDirectedAcyclicRelation[_P],
    Protocol[_P],
):
    """
    It represents a covering relation.
    """

    # AbstractFiniteCoveringRelation properties and methods

    @abstractmethod
    def neighbourhoods(self, reverse: bool = False) -> Iterator[Neighbourhood[_P]]:
        """
        Get an iterator over the neighbourhoods of an element.

        Parameters
        ----------
        reverse
            Is this a bottom-up generation ?

        Returns
        -------
        Iterator[Neighbourhood[_P]]
            An iterable giving a triple (element, successors, predecessors) for
            each element in the covering relation represented by a neighbourhood.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


# pylint: disable=duplicate-bases
@runtime_checkable
class AbstractFinitePartialOrder(
    AbstractFiniteDirectedAcyclicRelation[_P],
    Protocol[_P],
):
    """
    It represents finite partial orders.
    """


@runtime_checkable
class AbstractFinitePartiallyOrderedSet(
    Collection[_P],
    Protocol[_P],
):
    """
    It represents finite partially ordered set (poset).
    """

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def isdisjoint(self, other: Iterable[_P]) -> bool:
        """
        Test if the poset is disjoint from the other.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the poset is disjoint from the other.

        """

    @abstractmethod
    def issubset(self, other: Iterable[_P]) -> bool:
        """
        Test whether every element in the poset is in other.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the poset is a subset of the other.

        """

    @abstractmethod
    def issuperset(self, other: Iterable[_P]) -> bool:
        """
        Test whether every element in the other is in the poset.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the poset is a superset of the other.

        """

    # AbstractFinitePartiallyOrderedSet properties and methods

    @abstractmethod
    def upper_limit(self, *limits: _P, strict: bool = False) -> Collection[_P]:
        """
        Get the values less than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_P]
            A collection of values.

        """

    @abstractmethod
    def lower_limit(self, *limits: _P, strict: bool = False) -> Collection[_P]:
        """
        Get the values greater than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_P]
            A collection of values.

        """

    @property
    @abstractmethod
    def order(self) -> AbstractFinitePartialOrder[_P]:
        """
        Get the partial order of this poset.

        Returns
        -------
        AbstractFinitePartialOrder[_P]
            The partial order.

        """

    @property
    @abstractmethod
    def cover(self) -> AbstractFiniteCoveringRelation[_P]:
        """
        Get the covering relation of this poset.

        Returns
        -------
        AbstractFiniteCoveringRelation[_P]
            The covering relation.

        """

    @property
    @abstractmethod
    def top(self) -> Collection[_P]:
        """
        Get the top elements.

        Returns
        -------
        Collection[_P]
            The top elements.

        """

    @property
    @abstractmethod
    def maximum(self) -> _P | None:
        """
        Get the maximum element if any.

        Returns
        -------
        _P | None
            The maximum element.

        """

    @property
    @abstractmethod
    def bottom(self) -> Collection[_P]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_P]
            The bottom elements.

        """

    @property
    @abstractmethod
    def minimum(self) -> _P | None:
        """
        Get the minimum element if any.

        Returns
        -------
        _P | None
            The minimum element.

        """

    @abstractmethod
    def filter(
        self,
        element: _P,
    ) -> AbstractFinitePartiallyOrderedSet[_P]:
        """
        Get a filter of a poset.

        Parameters
        ----------
        element
            The lower limit

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_P]
            A view on the lower bounded poset.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the lower bound is no longer part of the
        original poset.

        """

    @abstractmethod
    def ideal(
        self,
        element: _P,
    ) -> AbstractFinitePartiallyOrderedSet[_P]:
        """
        Get an ideal of the poset.

        Parameters
        ----------
        element
            The upper limit

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_P]
            A view on the upper bounded set.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the upper bound is no longer part of the
        original poset.

        """

    @property
    @abstractmethod
    def version(self) -> int:
        """
        Get the poset version.

        Returns
        -------
        int
            The poset version.

        Notes
        -----
        This can be used to detect a change in the poset.

        """
