"""The :mod:`_element` module."""

from abc import abstractmethod
from collections.abc import Iterable, Iterator
from typing import Protocol, TypeVar

from typing_extensions import Self

from galactic.algebras.set import FIFOSet


class PartiallyComparable(Protocol):
    r"""
    Partially comparable elements.

    A partially ordered type sets for each pair of elements :math:`a`, :math:`b` either:

    * :math:`a \leq b`
    * :math:`b \leq a`
    * a and b are incomparable (:math:`a \| b`)

    The relation :math:`\leq` must be:

    * reflexive (:math:`a \leq a`)
    * transitive (:math:`a \leq b` and :math:`b \leq c` implies :math:`a \leq c`)
    * anti-symmetric (:math:`a \leq b` and :math:`b \leq a` implies :math:`a = b`)

    A class implementing the
    :class:`PartiallyComparable` protocol must be declared by inheriting from
    :class:`PartiallyComparable` and must implement the methods:

    * :meth:`__lt__`
    * :meth:`__gt__`
    * :meth:`__le__`
    * :meth:`__ge__`

    Notes
    -----
    The builtin classes

    * :class:`~python:bool`
    * :class:`~python:int`
    * :class:`~python:float`
    * :class:`~python:frozenset`

    are considered as subclasses of :class:`PartiallyComparable`.

    Example
    -------
    Let the integers ordered by the relation :math:`a \leq b`: is :math:`a` a
    divisor of :math:`b`?

    implemented by the :class:`~galactic.algebras.examples.arithmetic.PrimeFactors`
    class:

    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> PrimeFactors(5) <= PrimeFactors(10)  # 5 is a divisor of 10
    True
    >>> PrimeFactors(5) <= PrimeFactors(11)  # 5 is not a divisor of 11
    False
    >>> PrimeFactors(5) >= PrimeFactors(11)  # 5 is not a multiple of 11
    False

    """

    __slots__ = ()

    @abstractmethod
    def __lt__(self, other: Self) -> bool:
        """
        Test if this element is lesser than the other.

        Parameters
        ----------
        other
            the other element

        Returns
        -------
        bool
            True if this element is lesser than the other.

        """
        return NotImplemented

    @abstractmethod
    def __gt__(self, other: Self) -> bool:
        """
        Test if this element is greater than the other.

        Parameters
        ----------
        other
            the other element

        Returns
        -------
        bool
            True if this element is greater than the other.

        """
        return NotImplemented

    @abstractmethod
    def __le__(self, other: Self) -> bool:
        """
        Test if this element is lesser than or equal to the other.

        Parameters
        ----------
        other
            the other element

        Returns
        -------
        bool
            True if this element is lesser than or equal to the other.

        """
        equal = self.__eq__(other)
        if equal and equal is not NotImplemented:
            return True
        less_than = self.__lt__(other)
        if less_than and less_than is not NotImplemented:
            return True
        if equal is less_than is NotImplemented:
            return NotImplemented
        return False

    @abstractmethod
    def __ge__(self, other: Self) -> bool:
        """
        Test if this element is greater than or equal to the other.

        Parameters
        ----------
        other
            the other element

        Returns
        -------
        bool
            True if this element is greater than or equal to the other.

        """
        equal = self.__eq__(other)
        if equal and equal is not NotImplemented:
            return True
        less_than = self.__gt__(other)
        if less_than and less_than is not NotImplemented:
            return True
        if equal is less_than is NotImplemented:
            return NotImplemented
        return False


_P = TypeVar("_P", bound=PartiallyComparable)


def top(iterable: Iterable[_P]) -> Iterator[_P]:
    """
    Compute an iterator over the top elements of the iterable.

    This operation computes in :math:`O(n^2)` where :math:`n` is the number of
    elements in the iterable.

    Parameters
    ----------
    iterable
        An iterable of partially ordered elements.

    Returns
    -------
    Iterator[_P]
        An iterator over the top elements.

    Example
    -------
    >>> from galactic.algebras.poset import top
    >>> values = [{1, 2, 3}, {1, 2, 3, 4}, {2, 3, 4, 5}]
    >>> list(top(list(map(frozenset, values))))
    [frozenset({1, 2, 3, 4}), frozenset({2, 3, 4, 5})]

    """
    elements: FIFOSet[_P] = FIFOSet[_P]()
    for value in iterable:
        add = True
        for other in list(elements):
            if value < other:
                add = False
                break
            if other < value:
                elements.discard(other)
        if add:
            elements.add(value)
    return iter(elements)


def bottom(iterable: Iterable[_P]) -> Iterator[_P]:
    # noinspection PyTypeChecker
    """
    Compute an iterator over the bottom elements of the iterable.

    This operation computes in :math:`O(n^2)` where :math:`n` is the number
    of elements in the iterable.

    Parameters
    ----------
    iterable
        An iterable of partially ordered elements.

    Returns
    -------
    Iterator[_P]
        An iterator over the bottom elements.

    Example
    -------
    >>> from galactic.algebras.poset import top
    >>> values = [{1, 2, 3}, {1, 2, 3, 4}, {2, 3, 4, 5}]
    >>> list(bottom(list(map(frozenset, values))))
    [frozenset({1, 2, 3}), frozenset({2, 3, 4, 5})]

    """
    elements: FIFOSet[_P] = FIFOSet[_P]()
    for value in iterable:
        add = True
        for other in list(elements):
            if value > other:
                add = False
                break
            if other > value:
                elements.discard(other)
        if add:
            elements.add(value)
    return iter(elements)


def upper_limit(
    iterable: Iterable[_P],
    *limits: _P,
    strict: bool = False,
) -> Iterator[_P]:
    """
    Compute an iterator over the elements lesser than the limits.

    This operation computes in :math:`O(n)` where :math:`n` is the number of elements
    in the iterable.

    Parameters
    ----------
    iterable
        An iterable of partially ordered elements.
    *limits
        The upper limits
    strict
        Is the comparison strict?

    Yields
    ------
    _P
        A selected element.

    Example
    -------
    >>> from galactic.algebras.poset import upper_limit
    >>> values = [{1, 2}, {1, 2, 3}, {2, 3}]
    >>> list(upper_limit(list(map(frozenset, values)), frozenset({2, 3})))
    [frozenset({2, 3})]

    # noqa: DAR301

    """
    for value in iterable:
        if strict:
            if all(value < limit for limit in limits):
                yield value
        elif all(value <= limit for limit in limits):
            yield value


def lower_limit(
    iterable: Iterable[_P],
    *limits: _P,
    strict: bool = False,
) -> Iterator[_P]:
    """
    Compute an iterator over the elements greater than the limits.

    This operation computes in :math:`O(n)` where :math:`n` is the number of elements
    in the iterable.

    Parameters
    ----------
    iterable
        An iterable of partially ordered elements.
    *limits
        The lower limits
    strict
        Is the comparison strict?

    Yields
    ------
    _P
        A selected element

    Example
    -------
    >>> from galactic.algebras.poset import lower_limit
    >>> values = [{1, 2}, {1, 2, 3}, {2, 3}]
    >>> list(lower_limit(list(map(frozenset, values)), frozenset({2, 3})))
    [frozenset({1, 2, 3}), frozenset({2, 3})]

    # noqa: DAR301

    """
    for value in iterable:
        if strict:
            if all(value > limit for limit in limits):
                yield value
        elif all(value >= limit for limit in limits):
            yield value
