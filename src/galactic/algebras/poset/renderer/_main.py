"""The :mod:`_main` module."""

# pylint: disable=unused-argument

from __future__ import annotations

import subprocess
from collections.abc import Collection, Iterable, Iterator, Sequence
from pathlib import Path
from typing import Generic, TypeVar

try:
    from graphviz import Digraph
    from graphviz.quoting import attr_list
except ImportError as error:
    raise ImportError("Install galactic-core-kernel with extra 'docs'") from error

from galactic.algebras.poset import (
    AbstractFinitePartiallyOrderedSet,
    PartiallyComparable,
)
from galactic.algebras.relational.renderer import (
    EdgeRenderer,
    GraphRenderer,
    NodeRenderer,
)

_P = TypeVar("_P", bound=PartiallyComparable)


class HasseDiagramRenderer(GraphRenderer[_P, _P]):
    """
    It renders graph attributes.

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = ()

    def attributes(self) -> dict[str, str]:
        """
        Return a dictionary of graphviz attributes for a Hasse diagram.

        Returns
        -------
            A dictionary of graphviz attributes

        """
        return {"rankdir": "BT"}


# pylint: disable=too-few-public-methods
class HasseDiagram(Generic[_P]):
    """
    It is used for drawing Hasse diagrams.

    Parameters
    ----------
    poset
        A poset.
    graph_renderer
        A Hasse diagram renderer.
    domain_renderer
        A node renderer.
    edge_renderer
        An edge renderer.
    reverse
        Is it a reverse traversal ?

    """

    __slots__ = (
        "_poset",
        "_graph_renderer",
        "_domain_renderer",
        "_edge_renderer",
        "_reverse",
    )

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        graph_renderer: HasseDiagramRenderer[_P] | None = None,
        domain_renderer: NodeRenderer[_P, _P] | None = None,
        edge_renderer: EdgeRenderer[_P, _P] | None = None,
        reverse: bool = False,
    ) -> None:
        if graph_renderer is None:
            graph_renderer = HasseDiagramRenderer[_P]()
        if domain_renderer is None:
            domain_renderer = NodeRenderer[_P, _P]()
        if edge_renderer is None:
            edge_renderer = EdgeRenderer[_P, _P]()
        self._poset = poset
        self._graph_renderer = graph_renderer
        self._domain_renderer = domain_renderer
        self._edge_renderer = edge_renderer
        self._reverse = reverse

    @property
    def poset(self) -> AbstractFinitePartiallyOrderedSet[_P]:
        """
        Get the poset.

        Returns
        -------
            The underlying poset.

        """
        return self._poset

    @property
    def graph_renderer(self) -> HasseDiagramRenderer[_P]:
        """
        Get the Hasse diagram renderer.

        Returns
        -------
            The Hasse diagram renderer.

        """
        return self._graph_renderer

    @property
    def domain_renderer(self) -> NodeRenderer[_P, _P]:
        """
        Get the domain renderer.

        Returns
        -------
            The domain renderer.

        """
        return self._domain_renderer

    @property
    def edge_renderer(self) -> EdgeRenderer[_P, _P]:
        """
        Get the edge renderer.

        Returns
        -------
            The edge renderer.

        """
        return self._edge_renderer

    @property
    def _digraph(self) -> Digraph:
        diagram = DynamicDiagram(
            graph_renderer=self._graph_renderer,
            domain_renderer=self._domain_renderer,
            edge_renderer=self._edge_renderer,
        )
        diagram.start()
        for neighbourhood in self._poset.cover.neighbourhoods(self._reverse):
            diagram.step(
                neighbourhood.element,
                neighbourhood.successors,
                neighbourhood.predecessors,
            )
        diagram.finish()
        return Digraph(
            graph_attr=self._graph_renderer.attributes(),
            body=diagram.body,
        )

    def render(self, filename: str) -> None:
        """
        Render the diagram in a file.

        Parameters
        ----------
        filename
            A file path.

        """
        path = Path(filename)
        stem = path.stem
        suffix = path.suffix
        if suffix == ".dot":
            with path.open("w", encoding="utf-8") as file:
                file.write(self.source)
        else:
            try:
                self._digraph.render(
                    stem,
                    format=suffix[1:],
                    renderer="cairo",
                    formatter="cairo",
                    cleanup=True,
                    quiet=True,
                )
                Path(stem + ".cairo.cairo" + suffix).rename(path.name)
            except subprocess.CalledProcessError:
                self._digraph.render(
                    stem,
                    format=suffix[1:],
                    cleanup=True,
                    quiet=True,
                )

    def pipe(self, output: str = "svg") -> bytes:
        """
        Pipe the diagram.

        Parameters
        ----------
        output
            The format used

        Returns
        -------
        bytes
            The image of the diagram.

        """
        try:
            return self._digraph.pipe(  # type: ignore[no-any-return]
                output,
                renderer="cairo",
                formatter="cairo",
                quiet=True,
            )
        except subprocess.CalledProcessError:
            return self._digraph.pipe(output)  # type: ignore[no-any-return]

    @property
    def source(self) -> str:
        """
        Get the graphviz source for this partially ordered set.

        Returns
        -------
            The graphviz output for this partially ordered set.

        """
        return self._digraph.source  # type: ignore[no-any-return]

    def _repr_png_(self) -> bytes:
        return self.pipe("png")


# pylint: disable=too-many-instance-attributes
class DynamicDiagram(Generic[_P]):
    """
    It is designed to allow dynamic graph rendering.

    Parameters
    ----------
    graph_renderer
        A Hasse diagram renderer.
    domain_renderer
        A node renderer.
    edge_renderer
        An edge renderer.

    """

    __slots__ = (
        "_graph_renderer",
        "_domain_renderer",
        "_edge_renderer",
        "_count",
        "_index",
        "_identifiers",
        "_node_position",
        "_edge_position",
        "_references",
        "_successors",
        "_predecessors",
        "_body",
        "_element",
    )

    def __init__(
        self,
        graph_renderer: HasseDiagramRenderer[_P] | None = None,
        domain_renderer: NodeRenderer[_P, _P] | None = None,
        edge_renderer: EdgeRenderer[_P, _P] | None = None,
    ) -> None:
        if graph_renderer is None:
            graph_renderer = HasseDiagramRenderer[_P]()
        if domain_renderer is None:
            domain_renderer = NodeRenderer[_P, _P]()
        if edge_renderer is None:
            edge_renderer = EdgeRenderer[_P, _P]()
        self._graph_renderer = graph_renderer
        self._domain_renderer = domain_renderer
        self._edge_renderer = edge_renderer
        self._count = 0
        self._index = 0
        self._identifiers: dict[_P, int] = {}
        self._node_position: dict[_P, int] = {}
        self._edge_position: dict[tuple[_P, _P], int] = {}
        self._references: dict[_P, int] = {}
        self._successors: dict[_P, Collection[_P]] = {}
        self._predecessors: dict[_P, Collection[_P]] = {}
        self._body: list[str] = []
        self._element: _P | None = None

    def start(self) -> None:
        """
        Start a new iteration.
        """
        self._index = 0
        self._count = 0
        self._identifiers = {}
        self._node_position = {}
        self._edge_position = {}
        self._references = {}
        self._successors = {}
        self._predecessors = {}
        self._body = []
        self._element = None

    def _check(self, element: _P) -> None:
        if self._references[element] == 0:
            del self._references[element]
            del self._identifiers[element]
            for predecessor in self._predecessors[element]:
                if predecessor not in self._references:
                    del self._edge_position[(predecessor, element)]
            for successor in self._successors[element]:
                if successor not in self._references:
                    del self._edge_position[(element, successor)]
            del self._node_position[element]
            del self._predecessors[element]
            del self._successors[element]

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def _node_line(
        self,
        element: _P,
        index: int | None,
        current: bool,
        successors: Collection[_P] | None,
        predecessors: Collection[_P] | None,
    ) -> str:
        self._graph_renderer.add_source(
            element,
            index=index,
            current=current,
            successors=successors,
            predecessors=predecessors,
        )
        attributes = self._domain_renderer.attributes(
            element=element,
            index=index,
            current=current,
            successors=successors,
            predecessors=predecessors,
        )
        return f"\tN{self._identifiers[element]}{attr_list(attributes=attributes)}\n"

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def _update_node(
        self,
        element: _P,
        index: int | None,
        current: bool,
        successors: Collection[_P] | None,
        predecessors: Collection[_P] | None,
    ) -> None:
        line = self._node_line(element, index, current, successors, predecessors)
        self._body[self._node_position[element]] = line

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def _create_node(
        self,
        element: _P,
        index: int | None,
        current: bool,
        successors: Collection[_P] | None,
        predecessors: Collection[_P] | None,
    ) -> None:
        line = self._node_line(element, index, current, successors, predecessors)
        self._node_position[element] = len(self._body)
        self._body.append(line)

    def _edge_line(
        self,
        source: _P,
        destination: _P,
        successors: Collection[_P] | None,
        predecessors: Collection[_P] | None,
    ) -> str:
        self._graph_renderer.add_edge(
            source=source,
            destination=destination,
            successors=successors,
            predecessors=predecessors,
        )
        attributes = self._edge_renderer.attributes(
            source=source,
            destination=destination,
            successors=successors,
            predecessors=predecessors,
        )
        src = self._identifiers[source]
        dst = self._identifiers[destination]
        return f"\tN{src} -> N{dst}{attr_list(attributes=attributes)}\n"

    def _create_edge(
        self,
        source: _P,
        destination: _P,
        successors: Collection[_P] | None,
        predecessors: Collection[_P] | None,
    ) -> None:
        line = self._edge_line(source, destination, successors, predecessors)
        self._edge_position[(source, destination)] = len(self._body)
        self._body.append(line)

    def _update_edge(
        self,
        source: _P,
        destination: _P,
        successors: Collection[_P] | None,
        predecessors: Collection[_P] | None,
    ) -> None:
        line = self._edge_line(source, destination, successors, predecessors)
        self._body[self._edge_position[(source, destination)]] = line

    # pylint: disable=too-many-branches
    def step(
        self,
        element: _P,
        successors: Collection[_P],
        predecessors: Collection[_P],
    ) -> None:
        """
        Insert a new element in the diagram.

        Parameters
        ----------
        element
            The new element
        successors
            The immediate successors of the element.
        predecessors
            The immediate predecessors of the element.

        """
        if self._element is not None:
            self._update_node(
                self._element,
                self._index - 1,
                False,
                self._successors[self._element],
                self._predecessors[self._element],
            )
            self._check(self._element)

        self._successors[element] = successors
        self._predecessors[element] = predecessors
        if element in self._node_position:
            self._update_node(element, self._index, True, successors, predecessors)
        else:
            self._identifiers[element] = self._count
            self._count += 1
            self._create_node(element, self._index, True, successors, predecessors)
        self._references[element] = len(successors) + len(predecessors)
        for successor in successors:
            if successor in self._references:
                self._update_edge(
                    element,
                    successor,
                    successors,
                    self._predecessors[successor],
                )
                self._references[element] -= 1
                self._references[successor] -= 1
                self._check(successor)
            elif successor in self._node_position:
                if (element, successor) in self._edge_position:
                    self._update_edge(element, successor, successors, None)
                else:
                    self._create_edge(element, successor, successors, None)
            else:
                self._identifiers[successor] = self._count
                self._count += 1
                self._create_node(successor, None, False, None, None)
                self._create_edge(element, successor, successors, None)

        for predecessor in predecessors:
            if predecessor in self._references:
                self._update_edge(
                    predecessor,
                    element,
                    self._successors[predecessor],
                    predecessors,
                )
                self._references[element] -= 1
                self._references[predecessor] -= 1
                self._check(predecessor)
            elif predecessor in self._node_position:
                if (predecessor, element) in self._edge_position:
                    self._update_edge(predecessor, element, None, predecessors)
                else:
                    self._create_edge(predecessor, element, None, predecessors)
            else:
                self._identifiers[predecessor] = self._count
                self._count += 1
                self._create_node(predecessor, None, False, None, None)
                self._create_edge(predecessor, element, None, predecessors)

        self._element = element
        self._index += 1

    def finish(self) -> None:
        """
        Finish the iteration.
        """
        if self._element is not None:
            self._update_node(
                self._element,
                self._index - 1,
                False,
                self._successors[self._element],
                self._predecessors[self._element],
            )
            self._check(self._element)

    @property
    def body(self) -> Sequence[str]:
        """
        Get the current graphviz body.

        Returns
        -------
            The current body.

        """
        return self._body

    @property
    def _digraph(self) -> Digraph:
        return Digraph(
            graph_attr=self._graph_renderer.attributes(),
            body=self._body,
        )

    def render(self, filename: str) -> None:
        """
        Render the diagram in a file.

        Parameters
        ----------
        filename
            A file path.

        """
        path = Path(filename)
        stem = path.stem
        suffix = path.suffix
        if suffix == ".dot":
            with path.open("w", encoding="utf-8") as file:
                file.write(self.source)
        else:
            try:
                self._digraph.render(
                    stem,
                    format=suffix[1:],
                    renderer="cairo",
                    formatter="cairo",
                    cleanup=True,
                    quiet=True,
                )
                Path(stem + ".cairo.cairo" + suffix).rename(path.name)
            except subprocess.CalledProcessError:
                self._digraph.render(
                    stem,
                    format=suffix[1:],
                    cleanup=True,
                    quiet=True,
                )

    def pipe(self, output: str = "svg") -> bytes:
        """
        Pipe the diagram.

        Parameters
        ----------
        output
            The format used

        Returns
        -------
        bytes
            The image of the diagram.

        """
        try:
            return self._digraph.pipe(  # type: ignore[no-any-return]
                output,
                renderer="cairo",
                formatter="cairo",
                quiet=True,
            )
        except subprocess.CalledProcessError:
            return self._digraph.pipe(output)  # type: ignore[no-any-return]

    @property
    def source(self) -> str:
        """
        Get the graphviz source for this diagram.

        Returns
        -------
            The graphviz output for this diagram.

        """
        return self._digraph.source  # type: ignore[no-any-return]

    def _repr_png_(self) -> bytes:
        return self.pipe("png")


class IterativeDiagram(Iterable[DynamicDiagram[_P]]):
    """
    It allows iterative rendering.

    Parameters
    ----------
    poset
        A poset.
    graph_renderer
        A Hasse diagram renderer renderer.
    domain_renderer
        A node renderer.
    edge_renderer
        An edge renderer.

    """

    __slots__ = ("_poset", "_graph_renderer", "_domain_renderer", "_edge_renderer")

    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        graph_renderer: HasseDiagramRenderer[_P] | None = None,
        domain_renderer: NodeRenderer[_P, _P] | None = None,
        edge_renderer: EdgeRenderer[_P, _P] | None = None,
    ) -> None:
        self._poset = poset
        self._graph_renderer = graph_renderer
        self._domain_renderer = domain_renderer
        self._edge_renderer = edge_renderer

    def __iter__(self) -> Iterator[DynamicDiagram[_P]]:
        diagram = DynamicDiagram(
            graph_renderer=self._graph_renderer,
            domain_renderer=self._domain_renderer,
            edge_renderer=self._edge_renderer,
        )
        diagram.start()
        yield diagram
        step = False
        for neighbourhood in self._poset.cover.neighbourhoods():
            step = True
            diagram.step(
                neighbourhood.element,
                neighbourhood.successors,
                neighbourhood.predecessors,
            )
            yield diagram
        diagram.finish()
        if step:
            yield diagram
