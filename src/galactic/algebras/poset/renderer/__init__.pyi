from collections.abc import Collection, Iterable, Iterator, Sequence
from typing import Generic, TypeVar

from galactic.algebras.poset import (
    AbstractFinitePartiallyOrderedSet,
    PartiallyComparable,
)
from galactic.algebras.relational.renderer import (
    EdgeRenderer,
    GraphRenderer,
    NodeRenderer,
)

_P = TypeVar("_P", bound=PartiallyComparable)

class HasseDiagramRenderer(GraphRenderer[_P, _P]): ...

class HasseDiagram(Generic[_P]):
    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        graph_renderer: HasseDiagramRenderer[_P] | None = None,
        domain_renderer: NodeRenderer[_P, _P] | None = None,
        edge_renderer: EdgeRenderer[_P, _P] | None = None,
        reverse: bool = False,
    ) -> None: ...
    @property
    def source(self) -> str: ...
    @property
    def poset(self) -> AbstractFinitePartiallyOrderedSet[_P]: ...
    @property
    def graph_renderer(self) -> HasseDiagramRenderer[_P]: ...
    @property
    def domain_renderer(self) -> NodeRenderer[_P, _P]: ...
    @property
    def edge_renderer(self) -> EdgeRenderer[_P, _P]: ...

class DynamicDiagram(Generic[_P]):
    def __init__(
        self,
        graph_renderer: HasseDiagramRenderer[_P] | None = None,
        domain_renderer: NodeRenderer[_P, _P] | None = None,
        edge_renderer: EdgeRenderer[_P, _P] | None = None,
    ) -> None: ...
    def start(self) -> None: ...
    def step(
        self,
        element: _P,
        successors: Collection[_P],
        predecessors: Collection[_P],
    ) -> None: ...
    def finish(self) -> None: ...
    @property
    def body(self) -> Sequence[str]: ...
    def render(self, filename: str) -> None: ...
    def pipe(self, output: str = "svg") -> bytes: ...
    @property
    def source(self) -> str: ...

class IterativeDiagram(Iterable[DynamicDiagram[_P]]):
    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        graph_renderer: HasseDiagramRenderer[_P] | None = None,
        domain_renderer: NodeRenderer[_P, _P] | None = None,
        edge_renderer: EdgeRenderer[_P, _P] | None = None,
    ) -> None: ...
    def __iter__(self) -> Iterator[DynamicDiagram[_P]]: ...
