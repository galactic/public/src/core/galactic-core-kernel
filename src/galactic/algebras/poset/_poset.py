"""The :mod:`_poset` module."""

# pylint: disable=too-many-lines

from __future__ import annotations

import copy
import itertools
from collections import OrderedDict
from collections.abc import (
    Collection,
    Iterable,
    Iterator,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import Any, TypeVar, cast

from typing_extensions import Self

from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._abstract import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    Neighbourhood,
)
from ._element import (
    PartiallyComparable,
)
from ._mixins import (
    FiniteCoveringRelationMixin,
    FinitePartiallyOrderedSetMixin,
)
from ._order import (
    FinitePartialOrder,
)
from ._view import (
    FinitePartiallyOrderedSetView,
)

_P = TypeVar("_P", bound=PartiallyComparable)


# pylint: disable=too-many-instance-attributes
class FrozenFinitePartiallyOrderedSet(FinitePartiallyOrderedSetMixin[_P]):
    r"""
    It implements posets.

    The initializer can take 0 or 1 argument that should be iterable.

    An instance stores its values in two dictionaries for the immediate successors
    (:math:`\succ`) and the immediate predecessors (:math:`\prec`).

    Its memory complexity is in :math:`O(n)`.

    Operation complexities:

    *  :meth:`__contains__`: :math:`O(1)`
    *  :meth:`__len__`: :math:`O(1)`
    *  :meth:`__iter__`: :math:`O(1)`
    *  :meth:`top`: :math:`O(1)`
    *  :meth:`bottom`: :math:`O(1)`
    *  :meth:`successors`: :math:`O(1)`
    *  :meth:`predecessors`: :math:`O(1)`

    Parameters
    ----------
    *others
        A sequence of poset
    elements
        An iterable of values

    Example
    -------
    >>> from galactic.algebras.poset import MutableFinitePartiallyOrderedSet
    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> poset = MutableFinitePartiallyOrderedSet[PrimeFactors](
    ...     elements=[
    ...         PrimeFactors(2*3*5*7),
    ...         PrimeFactors(3*5*7*11),
    ...         PrimeFactors(3*5*7),
    ...         PrimeFactors(3*5),
    ...         PrimeFactors(5),
    ...         PrimeFactors(7),
    ...     ]
    ... )
    >>> poset
    <galactic.algebras.poset.MutableFinitePartiallyOrderedSet object at 0x...>

    It's possible to iterate over the poset elements. The elements are iterated level by
    level starting from the top ones.

    Example
    -------
    >>> sorted(list(map(int, poset)))
    [5, 7, 15, 105, 210, 1155]

    It's possible to know the poset length.

    Example
    -------
    >>> len(poset)
    6

    It's possible to know if an element is in the poset.

    Example
    -------
    >>> PrimeFactors(210) in poset
    True
    >>> PrimeFactors(211) in poset
    False

    It's possible to iterate over the top and bottom elements.

    Example
    -------
    >>> sorted(list(map(int, poset.top)))
    [210, 1155]
    >>> sorted(list(map(int, poset.bottom)))
    [5, 7]

    It's possible to iterate over the descendants and the ascendants of an element.

    Example
    -------
    >>> sorted(list(map(int, poset.filter(PrimeFactors(105)))))
    [105, 210, 1155]
    >>> sorted(list(map(int, poset.ideal(PrimeFactors(105)))))
    [5, 7, 15, 105]

    It's possible to iterate over the (immediate) successors and the (immediate)
    predecessors of an element.

    Example
    -------
    >>> sorted(list(map(int, poset.cover.successors(PrimeFactors(105)))))
    [210, 1155]
    >>> sorted(list(map(int, poset.order.successors(PrimeFactors(105)))))
    [105, 210, 1155]
    >>> sorted(list(map(int, poset.cover.predecessors(PrimeFactors(105)))))
    [7, 15]
    >>> sorted(list(map(int, poset.order.predecessors(PrimeFactors(105)))))
    [5, 7, 15, 105]

    It's possible to enlarge a partially ordered set.

    Example
    -------
    >>> poset.update([PrimeFactors(13)])
    >>> sorted(list(map(int, poset)))
    [5, 7, 13, 15, 105, 210, 1155]

    """

    __slots__ = (
        "_version",
        "_successors",
        "_predecessors",
        "_cover",
        "_order",
        "_top",
        "_bottom",
        "_hash_value",
    )

    _version: int
    _successors: OrderedDict[_P, FIFOSet[_P]]
    _predecessors: OrderedDict[_P, FIFOSet[_P]]
    _cover: AbstractFiniteCoveringRelation[_P]
    _order: AbstractFinitePartialOrder[_P]
    _top: Collection[_P]
    _bottom: Collection[_P]
    _hash_value: int | None

    # pylint: disable=too-many-statements
    def __init__(
        self,
        *others: AbstractFinitePartiallyOrderedSet[_P],
        elements: Iterable[_P] | None = None,
    ) -> None:

        class CoveringRelation(FiniteCoveringRelationMixin[_P]):
            """
            It represents finite covering relation.
            """

            # AbstractBinaryRelation properties and methods

            def successors(self, element: _P) -> Collection[_P]:
                """
                Get the successors of an element.

                Parameters
                ----------
                element
                    The element whose successors are requested

                Returns
                -------
                Collection[_P]
                    The successors.

                Raises
                ------
                ValueError
                    If the element does not belong to the poset.

                Notes
                -----
                Unpredictable behavior can occur if the element is no longer part of the
                original poset.

                """

                class Successors:
                    """
                    It represents the successors af an element.
                    """

                    def __repr__(self) -> str:
                        return cast(str, default_repr(self, long=False))

                    # Collection properties and methods

                    def __contains__(self, item: object) -> bool:
                        return item in poset._successors[element]

                    def __len__(self) -> int:
                        return len(poset._successors[element])

                    def __iter__(self) -> Iterator[_P]:
                        return iter(poset._successors[element])

                if element not in poset:
                    raise ValueError(f"{element} does not belong to the poset")
                return Successors()

            def predecessors(self, element: _P) -> Collection[_P]:
                """
                Get the predecessors of an element.

                Parameters
                ----------
                element
                    The element whose predecessors are requested

                Returns
                -------
                Collection[_P]
                    The predecessors.

                Raises
                ------
                ValueError
                    If the element does not belong to the poset.

                Notes
                -----
                Unpredictable behavior can occur if the element is no longer part of the
                original poset.

                """

                class Predecessors:
                    """
                    It represents the predecessors af an element.
                    """

                    def __repr__(self) -> str:
                        return cast(str, default_repr(self, long=False))

                    # Collection properties and methods

                    def __contains__(self, item: object) -> bool:
                        return item in poset._predecessors[element]

                    def __len__(self) -> int:
                        return len(poset._predecessors[element])

                    def __iter__(self) -> Iterator[_P]:
                        return iter(poset._predecessors[element])

                if element not in poset:
                    raise ValueError(f"{element} does not belong to the poset")
                return Predecessors()

            # AbstractCoveringRelation properties and methods

            def neighbourhoods(
                self,
                reverse: bool = False,
            ) -> Iterator[Neighbourhood[_P]]:
                """
                Get an iterator over the neighbourhoods of an element.

                Parameters
                ----------
                reverse
                    Is this a bottom-up generation ?

                Returns
                -------
                Iterator[Neighbourhood[_P]]
                    An iterator giving a triple (element, successors, predecessors) for
                    each element in the covering relation represented by a
                    neighbourhood.

                """
                # pylint: disable=protected-access
                if reverse:
                    return (
                        Neighbourhood[_P](
                            element=element,
                            successors=poset._successors[element],
                            predecessors=poset._predecessors[element],
                        )
                        for element in poset._successors
                    )

                return (
                    Neighbourhood[_P](
                        element=element,
                        successors=poset._successors[element],
                        predecessors=poset._predecessors[element],
                    )
                    for element in poset._predecessors
                )

        class Top:
            """
            It represents the elements without successors.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                try:
                    return (
                        item in poset._successors
                        and not poset._successors[item]  # type: ignore[index]
                    )
                except TypeError:
                    return False

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_P]:
                for element, successors in reversed(poset._successors.items()):
                    if not successors:
                        yield element
                    else:
                        break

        class Bottom:
            """
            It represents the elements without predecessors.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                try:
                    return (
                        item in poset._predecessors
                        and not poset._predecessors[item]  # type: ignore[index]
                    )
                except TypeError:
                    return False

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_P]:
                for element, predecessors in poset._predecessors.items():
                    if not predecessors:
                        yield element
                    else:
                        break

        poset = self
        self._version = 1
        self._successors = OrderedDict()
        self._predecessors = OrderedDict()
        self._cover = CoveringRelation[_P](self)  # type: ignore[misc]
        self._order = FinitePartialOrder[_P](self)  # type: ignore[assignment]
        self._top = Top()
        self._bottom = Bottom()
        self._extend(
            itertools.chain(
                itertools.chain.from_iterable(others),
                elements or [],
            ),
        )
        self._reorder()
        self._hash_value = None

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number.

        Notes
        -----
        This can be used to detect a change in the poset.

        """
        return self._version

    def _inc_version(self) -> None:
        self._version += 1

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __hash__(self) -> int:
        if self._hash_value is None:
            # noinspection PyProtectedMember
            self._hash_value = AbstractSet._hash(self)  # type: ignore[arg-type]
        return self._hash_value

    def __copy__(self) -> Self:
        return self.__class__(elements=self._successors)

    def copy(self) -> Self:
        """
        Get a copy of the partially ordered set.

        Returns
        -------
        Self
            The copy of this poset.

        """
        return copy.copy(self)

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        try:
            return item in self._successors
        except TypeError:
            return False

    def __len__(self) -> int:
        return len(self._successors)

    def __iter__(self) -> Iterator[_P]:
        return iter(self._successors)

    # Set properties and methods

    def __or__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            return self.__class__(elements=itertools.chain(self, other))
        return NotImplemented

    def __and__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            return self.__class__(elements=(item for item in self if item in other))
        return NotImplemented

    def __sub__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            return self.__class__(elements=(item for item in self if item not in other))
        return NotImplemented

    def __xor__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            return self.__class__(
                elements=itertools.chain(
                    (item for item in self if item not in other),
                    (item for item in other if item not in self),
                ),
            )
        return NotImplemented

    def union(self, *others: Iterable[_P]) -> Self:
        """
        Compute the union of the poset and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The union of the poset and the others.

        """
        return self.__class__(
            elements=itertools.chain(self, itertools.chain.from_iterable(others)),
        )

    def intersection(self, *others: Iterable[Any]) -> Self:
        """
        Compute the intersection between the poset and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The intersection between the poset and the others

        """
        elements = FIFOSet[_P](self)
        for other in others:
            data = FIFOSet[_P]()
            for element in other:
                if element in elements:
                    data.add(element)
            elements = data
        return self.__class__(elements=elements)

    def difference(self, *others: Iterable[Any]) -> Self:
        """
        Compute the difference between the poset and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The difference between the poset and the others

        """
        result: Self = self.copy()
        for other in others:
            for element in other:
                # pylint: disable=protected-access
                result._discard(element)
        return result

    def symmetric_difference(self, other: Iterable[_P]) -> Self:
        """
        Compute the symmetric difference between the poset and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        Self
            The symmetric difference between the poset and the other.

        """
        result: Self = self.copy()
        for element in other:
            # pylint: disable=protected-access
            if element in result:
                result._remove(element)
            else:
                result._add(element)
                result._reorder()
        return result

    # AbstractPartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_P]:
        """
        Get the partial order of this poset.

        Returns
        -------
        AbstractFinitePartialOrder[_P]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_P]:
        """
        Get the covering relation of this poset.

        Returns
        -------
        AbstractFiniteCoveringRelation[_P]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_P]:
        """
        Get the top elements.

        Returns
        -------
        Collection[_P]
            The top elements.

        """
        return self._top

    @property
    def tail(self) -> _P | None:
        """
        Get the tail element if any.

        Returns
        -------
        _P | None
            The tail element.

        Raises
        ------
        KeyError
            If the semi-lattice is empty.

        """
        try:
            return next(iter(self.top))
        except StopIteration as error:
            raise KeyError from error

    @property
    def bottom(self) -> Collection[_P]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_P]
            The bottom elements.

        """
        return self._bottom

    @property
    def head(self) -> _P | None:
        """
        Get the head element if any.

        Returns
        -------
        _P | None
            The head element.

        Raises
        ------
        KeyError
            If the semi-lattice is empty.

        """
        try:
            return next(iter(self.bottom))
        except StopIteration as error:
            raise KeyError from error

    def filter(
        self,
        element: _P,
    ) -> AbstractFinitePartiallyOrderedSet[_P]:
        """
        Get a filter of a poset.

        This is a view of this poset restricted the lower limit.

        Parameters
        ----------
        element
            The lower limit

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_P]
            A view on the lower bounded poset.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the lower bound is no longer part of the
        original poset.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the poset")
        return FinitePartiallyOrderedSetView[_P](self, lower=element)

    def ideal(self, element: _P) -> AbstractFinitePartiallyOrderedSet[_P]:
        """
        Get an ideal of the poset.

        This is a view of this poset restricted the upper limit.

        Parameters
        ----------
        element
            The upper limit

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_P]
            A view on the upper bounded set.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the upper bound is no longer part of the
        original poset.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the poset")
        return FinitePartiallyOrderedSetView[_P](self, upper=element)

    def _add(self, value: _P) -> None:
        if value not in self._successors:
            final_successors: FIFOSet[_P] = FIFOSet[_P]()
            final_predecessors: FIFOSet[_P] = FIFOSet[_P]()
            for element, successors in self._successors.items():
                if value > element:
                    remove = {
                        successor for successor in successors if value < successor
                    }
                    final_successors |= remove
                    successors -= remove
                    if not any(successor < value for successor in successors):
                        successors.add(value)
                        final_predecessors.add(element)
            for element, predecessors in self._predecessors.items():
                if value < element:
                    remove = {
                        predecessor
                        for predecessor in predecessors
                        if value > predecessor
                    }
                    final_predecessors |= remove
                    predecessors -= remove
                    if not any(predecessor > value for predecessor in predecessors):
                        predecessors.add(value)
                        final_successors.add(element)
            self._successors[value] = final_successors
            self._predecessors[value] = final_predecessors

    def _extend(self, iterable: Iterable[_P]) -> None:
        for element in iterable:
            self._add(element)

    def _remove(self, value: _P) -> None:
        successors = self._successors[value]
        predecessors = self._predecessors[value]
        for successor in successors:
            self._predecessors[successor].discard(value)
        for predecessor in predecessors:
            self._successors[predecessor].discard(value)
        for successor in successors:
            for predecessor in predecessors:
                self._predecessors[successor].add(predecessor)
                self._successors[predecessor].add(successor)
        del self._successors[value]
        del self._predecessors[value]

    def _discard(self, value: _P) -> None:
        if value in self._successors:
            self._remove(value)

    def _pop(self, last: bool = False) -> _P:
        if last:
            value, predecessors = self._predecessors.popitem(last=last)
            for predecessor in predecessors:
                self._successors[predecessor].discard(value)
            del self._successors[value]
            return value
        value, successors = self._successors.popitem(last=last)
        for successor in successors:
            self._predecessors[successor].discard(value)
        del self._predecessors[value]
        return value

    def _clear(self) -> None:
        self._successors.clear()
        self._predecessors.clear()

    def _reorder(self) -> None:
        # Reorder predecessors
        for element, predecessors in reversed(copy.copy(self._predecessors).items()):
            if not predecessors:
                self._predecessors.move_to_end(element, False)
        # Reorder successors
        for element, successors in copy.copy(self._successors).items():
            if not successors:
                self._successors.move_to_end(element)


class MutableFinitePartiallyOrderedSet(FrozenFinitePartiallyOrderedSet[_P]):
    """
    It represents mutable finite poset.
    """

    __slots__ = ()

    # FinitePartiallyOrderedSet properties and methods

    __hash__ = None  # type: ignore[assignment]

    # Mutable set properties and methods

    def add(self, value: _P) -> None:
        """
        Add a value to the poset.

        Parameters
        ----------
        value
            The value to add.

        """
        super()._add(value)
        super()._reorder()
        super()._inc_version()

    def remove(self, value: _P) -> None:
        """
        Remove a value from the poset.

        Parameters
        ----------
        value
            The value to remove.

        """
        super()._remove(value)
        super()._reorder()
        super()._inc_version()

    def discard(self, value: _P) -> None:
        """
        Discard a value from the poset.

        Parameters
        ----------
        value
            The value to discard.

        """
        super()._discard(value)
        super()._reorder()
        super()._inc_version()

    def pop(self) -> _P:
        """
        Remove and return the first value of the set.

        Returns
        -------
        _P
            The value removed.

        """
        value = super()._pop()
        super()._reorder()
        super()._inc_version()
        return value

    def popitem(self, last: bool = False) -> _P:
        """
        Remove and return the first value of the set.

        Parameters
        ----------
        last
            Does the remove take place at the end or the beginning?

        Returns
        -------
        _P
            The value removed.

        """
        value = super()._pop(last=last)
        super()._reorder()
        super()._inc_version()
        return value

    def clear(self) -> None:
        """
        Clear the poset.
        """
        super()._clear()
        super()._inc_version()

    def __ior__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            super()._extend(other)
            super()._inc_version()
            return self
        return NotImplemented

    def __iand__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            result = FIFOSet[_P](self)
            for element in result:
                if element not in other:
                    super()._remove(element)
            super()._inc_version()
            return self
        return NotImplemented

    def __isub__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            if self is other:
                super()._clear()
            else:
                for element in other:
                    super()._discard(element)
            super()._inc_version()
            return self
        return NotImplemented

    def __ixor__(self, other: AbstractFinitePartiallyOrderedSet[_P]) -> Self:
        if isinstance(other, AbstractFinitePartiallyOrderedSet):
            if self is other:
                super()._clear()
            else:
                for element in other:
                    if element in self:
                        super()._remove(element)
                    else:
                        super()._add(element)
            super()._inc_version()
            return self
        return NotImplemented

    def extend(self, iterable: Iterable[_P]) -> None:
        """
        Extend a poset with the iterable.

        Parameters
        ----------
        iterable
            An iterable of values

        """
        super()._extend(iterable)
        super()._reorder()
        super()._inc_version()

    def update(self, *others: Iterable[_P]) -> None:
        """
        Update a poset with the union of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for iterable in others:
            super()._extend(iterable)
        super()._reorder()
        super()._inc_version()

    def intersection_update(self, *others: Iterable[Any]) -> None:
        """
        Update a poset with the intersection of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            data = FIFOSet[Any]()
            for element in other:
                if element in self:
                    data.add(element)
            super()._clear()
            super()._extend(data)
        super()._inc_version()

    def difference_update(self, *others: Iterable[Any]) -> None:
        """
        Update a poset with the difference of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            for element in other:
                super()._discard(element)
        super()._reorder()
        super()._inc_version()

    def symmetric_difference_update(self, other: Iterable[_P]) -> None:
        """
        Update a poset with the symmetric difference of itself and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        """
        for element in other:
            if element in self:
                super()._remove(element)
            else:
                super()._add(element)
        super()._reorder()
        super()._inc_version()
