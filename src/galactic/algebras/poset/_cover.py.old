import collections
import collections.abc
import copy
import itertools
from typing import (
    Any,
    Collection,
    Generic,
    Iterable,
    Iterator,
    Tuple,
    TypeVar,
)

from typing_extensions import OrderedDict

from galactic.algebras.relational import FiniteRelationMixin
from galactic.algebras.set import FIFOSet

from ._abstract import Neighbourhood
from ._element import PartiallyComparable

_P = TypeVar("_P", bound=PartiallyComparable)


def check(
    element: _P,
    collection: OrderedDict[Any, FIFOSet[_P]],
) -> None:
    """
    Check if the element belong to the poset.

    Parameters
    ----------
    element
        The element.
    collection
        A poset.

    Raises
    ------
    RuntimeError
        If the element does not belong to the poset anymore.
    """
    if element not in collection:
        raise RuntimeError(f"{element} does not belong to the universe anymore")


class FiniteCoveringRelation(FiniteRelationMixin, Generic[_P]):
    """
    It represents finite covering relation.
    """

    __slots__ = (
        "_successors",
        "_predecessors",
        "_universes",
        "_sinks",
        "_sources",
    )

    _successors: OrderedDict[Any, FIFOSet[_P]]
    _predecessors: OrderedDict[Any, FIFOSet[_P]]
    _universes: Tuple[Collection[_P], Collection[_P]]
    _sinks: Collection[_P]
    _sources: Collection[_P]

    def __init__(self, domain: Iterable[_P]) -> None:
        """
        Initialise an instance.

        Parameters
        ----------
        domain
            The domain of the covering relation.
        """

        class Sinks:
            """
            It represents the elements without successors.
            """

            def __repr__(self) -> str:
                return f"<Collection object at 0x{id(self):x}>"  # cov: ignore

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in relation._successors and not relation._successors[item]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_P]:
                for element, successors in reversed(relation._successors.items()):
                    if not successors:
                        yield element
                    else:
                        break

        class Sources:
            """
            It represents the elements without predecessors.
            """

            def __repr__(self) -> str:
                return f"<Collection object at 0x{id(self):x}>"  # cov: ignore

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return (
                    item in relation._predecessors and not relation._predecessors[item]
                )

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_P]:
                for element, predecessors in relation._predecessors.items():
                    if not predecessors:
                        yield element
                    else:
                        break

        class Universe:
            """
            It represents the domain af the covering relation.
            """

            def __repr__(self) -> str:
                return f"<Universe object at 0x{id(self):x}>"  # cov: ignore

            # Container properties and methods

            def __contains__(self, item: object) -> bool:
                return item in relation._successors

            # AbstractSet properties and methods

            def __len__(self) -> int:
                return len(relation._successors)

            def __iter__(self) -> Iterator[_P]:
                return iter(relation._successors)

        relation = self
        universe = Universe()
        self._successors = collections.OrderedDict()
        self._predecessors = collections.OrderedDict()
        self._universes = (universe, universe)
        self._sinks = Sinks()
        self._sources = Sources()
        self._extend(domain)

    def __repr__(self) -> str:
        return f"<CoveringRelation object at 0x{id(self):x}>"  # cov: ignore

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            if len(self.domain) != len(other.domain):
                return False
            return all(item in other.domain for item in self.domain)
        return super().__eq__(other)

    def __ne__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            if len(self.domain) != len(other.domain):
                return True
            return any(item not in other.domain for item in self.domain)
        return super().__ne__(other)

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if isinstance(item, tuple) and len(item) == 2:
            source, dest = item
            return source in self._successors and dest in self._successors[source]
        return False

    def __len__(self) -> int:
        return sum(len(successors) for successors in self._successors.values())

    def __iter__(self) -> Iterator[Tuple[_P, _P]]:
        return itertools.chain.from_iterable(
            ((source, dest) for source in self._predecessors[dest])
            for dest in self._predecessors
        )

    # AbstractRelation properties and methods

    @property
    def universes(self) -> Tuple[Collection[_P], Collection[_P]]:
        """
        Get the universes of this covering relation.

        Returns
        -------
            The universes of this covering relation.
        """
        return self._universes

    # AbstractBinaryRelation properties and methods

    @property
    def domain(self) -> Collection[_P]:
        """
        Get the domain of this covering relation.

        Returns
        -------
            The domain of this covering relation.

        Notes
        -----
        The domain and the co-domain of a covering relation are identical.
        """
        return self.universes[0]

    @property
    def co_domain(self) -> Collection[_P]:
        """
        Get the co-domain of this covering relation.

        Returns
        -------
            The co-domain of this covering relation.

        Notes
        -----
        The domain and the co-domain of a covering relation are identical.
        """
        return self.universes[1]

    def successors(self, element: _P) -> Collection[_P]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the covering relation.

        Notes
        -----
        Unpredictable behavior can occur if the element is no longer part of the
        original poset.
        """

        class Successors:
            """
            It represents the successors af an element.
            """

            def __repr__(self) -> str:
                check(element, relation._successors)  # cov: ignore
                return f"<Successors object at 0x{id(self):x}>"  # cov: ignore

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                check(element, relation._successors)
                return item in relation._successors[element]

            def __len__(self) -> int:
                check(element, relation._successors)
                return len(relation._successors[element])

            def __iter__(self) -> Iterator[_P]:
                check(element, relation._successors)
                return iter(relation._successors[element])

        if element not in self._successors:
            raise ValueError(f"{element} does not belong to this covering relation")
        relation = self
        return Successors()

    def predecessors(self, element: _P) -> Collection[_P]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the covering relation.

        Notes
        -----
        Unpredictable behavior can occur if the element is no longer part of the
        original poset.
        """

        class Predecessors:
            """
            It represents the predecessors af an element.
            """

            def __repr__(self) -> str:
                check(element, relation._predecessors)  # cov: ignore
                return f"<Predecessors object at 0x{id(self):x}>"  # cov: ignore

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                check(element, relation._predecessors)
                return item in relation._predecessors[element]

            def __len__(self) -> int:
                check(element, relation._predecessors)
                return len(relation._predecessors[element])

            def __iter__(self) -> Iterator[_P]:
                check(element, relation._predecessors)
                return iter(relation._predecessors[element])

        if element not in self._predecessors:
            raise ValueError(f"{element} does not belong to this covering relation")
        relation = self
        return Predecessors()

    # AbstractDirectedAcyclicGraph properties and methods

    @property
    def sinks(self) -> Collection[_P]:
        """
        Get the elements with no successors.

        Returns
        -------
            The elements with no successors.
        """
        return self._sinks

    @property
    def sources(self) -> Collection[_P]:
        """
        Get the elements with no predecessors.

        Returns
        -------
            The elements with no predecessors.
        """
        return self._sources

    # AbstractCoveringRelation properties and methods

    def neighbourhoods(self) -> Iterator[Neighbourhood[_P]]:
        """
        Get an iterator over the neighbourhoods of an element.

        Returns
        -------
            An iterator giving a triple (element, successors, predecessors) for
            each element in the covering relation represented by a neighbourhood.
        """
        return (
            Neighbourhood[_P](
                element=element,
                successors=self._successors[element],
                predecessors=self._predecessors[element],
            )
            for element in self._predecessors
        )

    def _extend(self, iterable: Iterable[_P]) -> None:
        """
        Extend this covering relation.

        Parameters
        ----------
        iterable
            An iterable of values.
        """
        for element in iterable:
            self._add(element)
        self._reorder()

    # protected properties and methods

    def _add(self, value: _P) -> None:
        if value not in self._successors:
            final_successors: FIFOSet[_P] = FIFOSet[_P]()
            final_predecessors: FIFOSet[_P] = FIFOSet[_P]()
            for element, successors in self._successors.items():
                if value > element:
                    remove = {
                        successor for successor in successors if value < successor
                    }
                    final_successors |= remove
                    successors -= remove
                    if not any(successor < value for successor in successors):
                        successors.add(value)
                        final_predecessors.add(element)
            for element, predecessors in self._predecessors.items():
                if value < element:
                    remove = {
                        predecessor
                        for predecessor in predecessors
                        if value > predecessor
                    }
                    final_predecessors |= remove
                    predecessors -= remove
                    if not any(predecessor > value for predecessor in predecessors):
                        predecessors.add(value)
                        final_successors.add(element)
            self._successors[value] = final_successors
            self._predecessors[value] = final_predecessors

    def _remove(self, value: _P) -> None:
        successors = self._successors[value]
        predecessors = self._predecessors[value]
        for successor in successors:
            self._predecessors[successor].discard(value)
        for predecessor in predecessors:
            self._successors[predecessor].discard(value)
        for successor in successors:
            for predecessor in predecessors:
                self._predecessors[successor].add(predecessor)
                self._successors[predecessor].add(successor)
        del self._successors[value]
        del self._predecessors[value]
        self._reorder()

    def _discard(self, value: _P) -> None:
        if value in self._successors:
            self._remove(value)

    def _pop(self) -> _P:
        value, successors = self._successors.popitem(last=False)
        for successor in successors:
            self._predecessors[successor].discard(value)
        del self._predecessors[value]
        self._reorder()
        return value  # type: ignore

    def _clear(self) -> None:
        self._successors.clear()
        self._predecessors.clear()

    def _reorder(self) -> None:
        # Reorder predecessors
        for element, predecessors in reversed(copy.copy(self._predecessors).items()):
            if not predecessors:
                self._predecessors.move_to_end(element, False)
        # Reorder successors
        for element, successors in copy.copy(self._successors).items():
            if not successors:
                self._successors.move_to_end(element)
