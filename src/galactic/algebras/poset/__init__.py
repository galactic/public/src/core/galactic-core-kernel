"""
The :mod:`galactic.algebras.poset` module.

..  rubric:: Abstract classes for posets

* :class:`Neighbourhood`
* :class:`AbstractFinitePartiallyOrderedSet`;

..  rubric:: Concrete classes for posets

* :class:`FrozenFinitePartiallyOrderedSet`;
* :class:`MutableFinitePartiallyOrderedSet`;
* :class:`FinitePartiallyOrderedSetView`;
* :class:`FinitePartiallyOrderedSetMixin`.

..  rubric:: Elements which are partially comparable:

* :class:`PartiallyComparable`

..  rubric:: Function on elements which are partially comparable

* :func:`bottom`
* :func:`top`
* :func:`lower_limit`
* :func:`upper_limit`

..  rubric:: Classes for representing underlying order relation

* :class:`AbstractFinitePartialOrder`;
* :class:`FinitePartialOrder`;
* :class:`AbstractFiniteCoveringRelation`;
* :class:`FiniteCoveringRelationMixin`.

..  rubric:: Mixins for the top and bottom collections

* :class:`Bottom`;
* :class:`Top`.
"""

__all__ = (
    "Neighbourhood",
    "AbstractFinitePartiallyOrderedSet",
    "FrozenFinitePartiallyOrderedSet",
    "MutableFinitePartiallyOrderedSet",
    "FinitePartiallyOrderedSetView",
    "FinitePartiallyOrderedSetMixin",
    "PartiallyComparable",
    "bottom",
    "top",
    "lower_limit",
    "upper_limit",
    "AbstractFinitePartialOrder",
    "FinitePartialOrder",
    "AbstractFiniteCoveringRelation",
    "FiniteCoveringRelationMixin",
    "Bottom",
    "Top",
)

from ._abstract import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    Neighbourhood,
)
from ._element import (
    PartiallyComparable,
    bottom,
    lower_limit,
    top,
    upper_limit,
)
from ._mixins import (
    FiniteCoveringRelationMixin,
    FinitePartiallyOrderedSetMixin,
)
from ._order import (
    FinitePartialOrder,
)
from ._poset import (
    FrozenFinitePartiallyOrderedSet,
    MutableFinitePartiallyOrderedSet,
)
from ._view import (
    Bottom,
    FinitePartiallyOrderedSetView,
    Top,
)
