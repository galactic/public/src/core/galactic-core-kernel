"""The :mod:`_order` module."""

from __future__ import annotations

from typing import TYPE_CHECKING, Generic, TypeVar, cast

from galactic.algebras.relational import FiniteRelationMixin
from galactic.helpers.core import default_repr

from ._abstract import AbstractFinitePartiallyOrderedSet
from ._element import PartiallyComparable, lower_limit, upper_limit

if TYPE_CHECKING:
    from collections.abc import Collection, Iterator

_P = TypeVar("_P", bound=PartiallyComparable)


class FinitePartialOrder(FiniteRelationMixin, Generic[_P]):
    """
    It represents a partial order.

    Parameters
    ----------
    poset
        A poset.

    """

    __slots__ = (
        "_poset",
        "_universes",
    )

    _poset: AbstractFinitePartiallyOrderedSet[_P]
    _universes: tuple[Collection[_P], Collection[_P]]

    def __init__(self, poset: AbstractFinitePartiallyOrderedSet[_P]) -> None:
        self._poset = poset
        self._universes = (poset, poset)

    def __bool__(self) -> bool:
        for _ in self:
            return True
        return False

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if isinstance(item, tuple) and len(item) == 2:
            source, dest = item
            return source in self._poset and dest in self._poset and source <= dest
        return False

    def __len__(self) -> int:
        return sum(1 for _ in self)

    def __iter__(self) -> Iterator[tuple[_P, _P]]:
        for source in self._poset:
            for dest in self._poset.filter(source):
                yield source, dest

    # AbstractRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[_P], Collection[_P]]:
        """
        Get the universes.

        Returns
        -------
        tuple[Collection[_P], Collection[_P]]
            The universes.

        """
        return self._universes

    @property
    def domain(self) -> Collection[_P]:
        """
        Get the domain.

        Returns
        -------
        Collection[_P]
            The domain.

        """
        return self._universes[0]

    @property
    def co_domain(self) -> Collection[_P]:
        """
        Get the co-domain of this partial order.

        Returns
        -------
        Collection[_P]
            The co-domain of this partial order.

        Notes
        -----
        The domain and the co-domain of a partial order are identical.

        """
        return self._universes[1]

    # AbstractDirectedAcyclicRelation properties and methods

    @property
    def sinks(self) -> Collection[_P]:
        """
        Get the sink elements.

        Returns
        -------
        Collection[_P]
            The sink elements.

        """
        return self._poset.top

    @property
    def sources(self) -> Collection[_P]:
        """
        Get the source elements.

        Returns
        -------
        Collection[_P]
            The source elements.

        """
        return self._poset.bottom

    def successors(self, element: _P) -> Collection[_P]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested.

        Returns
        -------
        Collection[_P]
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        """

        class Successors:
            """
            It represents the successors af an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in order._poset and item >= element  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_P]:
                return lower_limit(order._poset, element)

        if element not in self._poset:
            raise ValueError(f"{element} does not belong to the semi_lattice")
        order = self
        return Successors()

    def predecessors(self, element: _P) -> Collection[_P]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested.

        Returns
        -------
        Collection[_P]
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        """

        class Predecessors:
            """
            It represents the predecessors af an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in order._poset and item <= element  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_P]:
                return upper_limit(order._poset, element)

        if element not in self._poset:
            raise ValueError(f"{element} does not belong to the semi_lattice")
        order = self
        return Predecessors()
