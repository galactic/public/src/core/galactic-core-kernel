"""The :mod:`_view` module."""

from __future__ import annotations

from typing import TYPE_CHECKING, Generic, TypeVar, cast

from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._abstract import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    Neighbourhood,
)
from ._element import PartiallyComparable
from ._mixins import FiniteCoveringRelationMixin, FinitePartiallyOrderedSetMixin
from ._order import FinitePartialOrder

if TYPE_CHECKING:
    from collections.abc import Collection, Iterator

_P = TypeVar("_P", bound=PartiallyComparable)


class Bottom(Generic[_P]):
    """
    It represents the bottom elements in a bounded set.

    Parameters
    ----------
    poset
        The inner poset.
    lower
        The lower bound.
    upper
        The upper bound.

    """

    __slots__ = ("_poset", "_version", "_lower", "_upper", "_elements")

    _poset: AbstractFinitePartiallyOrderedSet[_P]
    _version: int
    _lower: _P | None
    _upper: _P | None
    _elements: FIFOSet[_P]

    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        lower: _P | None,
        upper: _P | None,
    ) -> None:
        self._poset = poset
        self._version = 0
        self._lower = lower
        self._upper = upper
        self._elements = FIFOSet[_P]()

    def __repr__(self) -> str:
        return cast(str, default_repr(self, "Collection", long=False))

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if self._lower is None:
            self._update()
            return item in self._elements
        return item == self._lower

    def __len__(self) -> int:
        if self._lower is None:
            self._update()
            return len(self._elements)
        return 1

    def __iter__(self) -> Iterator[_P]:
        if self._lower is None:
            self._update()
            return iter(self._elements)
        return iter([self._lower])

    def _update(self) -> None:
        if self._version != self._poset.version:
            self._elements.clear()
            self._elements.update(
                element
                for element in self._poset.bottom
                if (
                    (self._upper is None or self._upper >= element)
                    and (self._lower is None or self._lower <= element)
                )
            )
            self._version = self._poset.version


class Top(Generic[_P]):
    """
    It represents the top elements in a bounded set.

    Parameters
    ----------
    poset
        The inner poset.
    lower
        The lower bound.
    upper
        The upper bound.

    """

    __slots__ = ("_poset", "_version", "_lower", "_upper", "_elements")

    _poset: AbstractFinitePartiallyOrderedSet[_P]
    _version: int
    _lower: _P | None
    _upper: _P | None
    _elements: FIFOSet[_P]

    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        lower: _P | None,
        upper: _P | None,
    ) -> None:
        self._poset = poset
        self._version = 0
        self._lower = lower
        self._upper = upper
        self._elements = FIFOSet[_P]()

    def __repr__(self) -> str:
        return cast(str, default_repr(self, "Collection", long=False))

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if self._upper is None:
            self._update()
            return item in self._elements
        return item == self._upper

    def __len__(self) -> int:
        if self._upper is None:
            self._update()
            return len(self._elements)
        return 1

    def __iter__(self) -> Iterator[_P]:
        if self._upper is None:
            self._update()
            return iter(self._elements)
        return iter([self._upper])

    def _update(self) -> None:
        if self._version != self._poset.version:
            self._elements.clear()
            self._elements.update(
                element
                for element in self._poset.top
                if (
                    (self._upper is None or self._upper >= element)
                    and (self._lower is None or self._lower <= element)
                )
            )
            self._version = self._poset.version


# pylint: disable=too-many-instance-attributes
class FinitePartiallyOrderedSetView(FinitePartiallyOrderedSetMixin[_P]):
    """
    It represents view on partially ordered sets.

    Parameters
    ----------
    poset
        A poset.
    lower
        The lower limit.
    upper
        The upper limit.

    Notes
    -----
    Unpredictable behavior can occur if the lower or the upper bound is no longer
    part of the original poset.

    """

    __slots__ = (
        "_poset",
        "_version",
        "_cover",
        "_order",
        "_length",
        "_lower",
        "_upper",
        "_top",
        "_bottom",
    )

    _poset: AbstractFinitePartiallyOrderedSet[_P]
    _version: int
    _cover: AbstractFiniteCoveringRelation[_P]
    _order: AbstractFinitePartialOrder[_P]
    _length: int | None
    _lower: _P | None
    _upper: _P | None
    _top: Collection[_P]
    _bottom: Collection[_P]

    # pylint: disable=too-many-statements
    def __init__(
        self,
        poset: AbstractFinitePartiallyOrderedSet[_P],
        lower: _P | None = None,
        upper: _P | None = None,
    ) -> None:

        class CoveringRelation(FiniteCoveringRelationMixin[_P]):
            """
            It represents the covering relation of a bounded set.
            """

            __slots__ = ()

            # AbstractBinaryRelation properties and methods

            def successors(self, element: _P) -> Collection[_P]:
                """
                Get the successors of an element.

                Parameters
                ----------
                element
                    The element whose successors are requested

                Returns
                -------
                Collection[_P]
                    The successors.

                Raises
                ------
                ValueError
                    If the element does not belong to the partial order.

                """

                class Successors:
                    """
                    It represents the successors af an element.
                    """

                    __slots__ = (
                        "_version",
                        "_length",
                    )

                    _length: int | None
                    _version: int

                    def __init__(self) -> None:
                        self._version = poset.version
                        self._length = None

                    def __repr__(self) -> str:
                        return cast(str, default_repr(self, long=False))

                    def __bool__(self) -> bool:
                        for _ in self:
                            return True
                        return False

                    # Collection properties and methods

                    def __contains__(self, item: object) -> bool:
                        try:
                            return (
                                (upper is None or item <= upper)  # type: ignore[operator]
                                and (lower is None or item >= lower)  # type: ignore[operator]
                                and item in poset.cover.successors(element)
                            )
                        except TypeError:
                            return False

                    def __len__(self) -> int:
                        if self._length is None or self._version != poset.version:
                            self._length = sum(1 for _ in self)
                            self._version = poset.version
                        return self._length

                    def __iter__(self) -> Iterator[_P]:
                        return (
                            item
                            for item in poset.cover.successors(element)
                            if (upper is None or item <= upper)
                            and (lower is None or item >= lower)
                        )

                if element not in bounded:
                    raise ValueError(f"{element} does not belong to the poset")
                return Successors()

            def predecessors(self, element: _P) -> Collection[_P]:
                """
                Get the predecessors of an element.

                Parameters
                ----------
                element
                    The element whose predecessors are requested

                Returns
                -------
                Collection[_P]
                    The predecessors.

                Raises
                ------
                ValueError
                    If the element does not belong to the poset.

                """

                class Predecessors:
                    """
                    It represents the predecessors af an element.
                    """

                    __slots__ = (
                        "_version",
                        "_length",
                    )

                    _length: int | None

                    def __init__(self) -> None:
                        self._version = poset.version
                        self._length = None

                    def __repr__(self) -> str:
                        return cast(str, default_repr(self, long=False))

                    def __bool__(self) -> bool:
                        for _ in self:
                            return True
                        return False

                    # Collection properties and methods

                    def __contains__(self, item: object) -> bool:
                        try:
                            return (
                                (upper is None or item <= upper)  # type: ignore[operator]
                                and (lower is None or item >= lower)  # type: ignore[operator]
                                and item in poset.cover.predecessors(element)
                            )
                        except TypeError:
                            return False

                    def __len__(self) -> int:
                        if self._length is None or self._version != poset.version:
                            self._length = sum(1 for _ in self)
                            self._version = poset.version
                        return self._length

                    def __iter__(self) -> Iterator[_P]:
                        return (
                            item
                            for item in poset.cover.predecessors(element)
                            if (upper is None or item <= upper)
                            and (lower is None or item >= lower)
                        )

                if element not in bounded:
                    raise ValueError(f"{element} does not belong to the poset")
                return Predecessors()

            # AbstractFiniteCoveringRelation properties and methods

            def neighbourhoods(
                self,
                reverse: bool = False,
            ) -> Iterator[Neighbourhood[_P]]:
                """
                Get an iterator over the neighbourhoods of an element.

                It's an iterator giving a triple (element, successors, predecessors) for
                each element in the covering relation represented by a
                neighbourhood.

                Parameters
                ----------
                reverse
                    Is this a bottom-up generation ?

                Yields
                ------
                Neighbourhood[_P]
                    A neighbourhood.

                """
                for item in bounded:
                    yield Neighbourhood[_P](
                        element=item,
                        successors=self.successors(item),
                        predecessors=self.predecessors(item),
                    )

        bounded = self
        self._poset = poset
        self._version = poset.version
        self._cover = CoveringRelation[_P](self)  # type: ignore[misc]
        self._order = FinitePartialOrder[_P](self)  # type: ignore[assignment]
        self._length = None
        self._lower = lower
        self._upper = upper
        if lower is upper is None:
            self._top = poset.top
            self._bottom = poset.bottom
        else:
            self._top = Top(poset, lower, upper)
            self._bottom = Bottom(poset, lower, upper)

    @property
    def version(self) -> int:
        """
        Get the poset version.

        Returns
        -------
        int
            The poset version.

        Notes
        -----
        This can be used to detect a change in the poset.

        """
        return self._poset.version

    def __repr__(self) -> str:
        if self._lower is None:
            class_name = "BoundedSet" if self._upper is None else "UpperBoundedSet"
        elif self._upper is None:
            class_name = "LowerBoundedSet"
        else:
            class_name = "PartiallyOrderedSet"
        return cast(str, default_repr(self, class_name))

    def __bool__(self) -> bool:
        for _ in self:
            return True
        return False

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        return (
            item in self._poset
            and (self._upper is None or item <= self._upper)  # type: ignore[operator]
            and (self._lower is None or item >= self._lower)  # type: ignore[operator]
        )

    def __len__(self) -> int:
        if self._length is None or self._version != self._poset.version:
            self._length = sum(1 for _ in self)
            self._version = self._poset.version
        return self._length

    def __iter__(self) -> Iterator[_P]:
        return (
            item
            for item in self._poset
            if (self._upper is None or item <= self._upper)
            and (self._lower is None or item >= self._lower)
        )

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_P]:
        """
        Get the partial order of this bounded set.

        Returns
        -------
        AbstractFinitePartialOrder[_P]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_P]:
        """
        Get the covering relation of this bounded set.

        Returns
        -------
        AbstractFiniteCoveringRelation[_P]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_P]:
        """
        Get the top elements.

        Returns
        -------
        Collection[_P]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[_P]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_P]
            The bottom elements.

        """
        return self._bottom

    def filter(self, element: _P) -> FinitePartiallyOrderedSetView[_P]:
        """
        Get a filter of a bounded set.

        This is a view of this poset restricted by the lower limit.

        Parameters
        ----------
        element
            The lower limit

        Returns
        -------
        FinitePartiallyOrderedSetView[_P]
            A view on the lower bounded poset.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the lower bound is no longer part of the
        original poset.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the poset")
        return FinitePartiallyOrderedSetView[_P](self, lower=element, upper=self._upper)

    def ideal(self, element: _P) -> FinitePartiallyOrderedSetView[_P]:
        """
        Get an ideal of the bounded set.

        This is a view of this poset restricted by the upper limit.

        Parameters
        ----------
        element
            The upper limit

        Returns
        -------
        FinitePartiallyOrderedSetView[_P]
            A view on the upper bounded set.

        Raises
        ------
        ValueError
            If the element does not belong to the poset.

        Notes
        -----
        Unpredictable behavior can occur if the upper bound is no longer part of the
        original poset.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the poset")
        return FinitePartiallyOrderedSetView[_P](self, lower=self._lower, upper=element)
