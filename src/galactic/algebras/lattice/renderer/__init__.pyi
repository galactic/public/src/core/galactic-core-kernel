from typing import TypeVar

from galactic.algebras.lattice import Element, ReducedContext
from galactic.algebras.relational.renderer import (
    EdgeRenderer,
    NodeRenderer,
    SagittalDiagram,
    SagittalDiagramRenderer,
)

_E = TypeVar("_E", bound=Element)

class ReducedContextDiagramRenderer(SagittalDiagramRenderer[_E, _E]): ...

class ReducedContextDiagram(SagittalDiagram[_E, _E]):
    def __init__(
        self,
        context: ReducedContext[_E],
        graph_renderer: ReducedContextDiagramRenderer[_E] | None = None,
        domain_renderer: NodeRenderer[_E, _E] | None = None,
        co_domain_renderer: NodeRenderer[_E, _E] | None = None,
        edge_renderer: EdgeRenderer[_E, _E] | None = None,
    ) -> None: ...
