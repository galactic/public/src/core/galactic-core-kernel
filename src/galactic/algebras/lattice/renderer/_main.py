"""The :mod:`_main` module."""

# pylint: disable=unused-argument

from __future__ import annotations

from typing import TypeVar

from galactic.algebras.lattice import Element, ReducedContext
from galactic.algebras.relational.renderer import (
    EdgeRenderer,
    NodeRenderer,
    SagittalDiagram,
    SagittalDiagramRenderer,
)

_E = TypeVar("_E", bound=Element)


class ReducedContextDiagramRenderer(SagittalDiagramRenderer[_E, _E]):
    """
    It renders graphviz attributes.

    it is used for a reduced context diagram.

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = ()

    def attributes(self) -> dict[str, str]:
        """
        Get the graphviz attributes for the graph.

        Returns
        -------
            A dictionary of graphviz attributes

        """
        return {"rankdir": "BT"}


class ReducedContextDiagram(SagittalDiagram[_E, _E]):
    """
    It is used for drawing sagittal diagrams.

    It is useful for drawing sagittal diagrams of lattice reduced context in jupyter
    notebooks.

    Parameters
    ----------
    context
        The reduced context.
    graph_renderer
        The graph renderer.
    domain_renderer
        The domain renderer.
    co_domain_renderer
        The domain renderer.
    edge_renderer
        The edge renderer.

    """

    __slots__ = ()

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def __init__(
        self,
        context: ReducedContext[_E],
        graph_renderer: ReducedContextDiagramRenderer[_E] | None = None,
        domain_renderer: NodeRenderer[_E, _E] | None = None,
        co_domain_renderer: NodeRenderer[_E, _E] | None = None,
        edge_renderer: EdgeRenderer[_E, _E] | None = None,
    ) -> None:
        if graph_renderer is None:
            graph_renderer = ReducedContextDiagramRenderer[_E]()
        super().__init__(
            context,
            graph_renderer=graph_renderer,
            domain_renderer=domain_renderer,
            co_domain_renderer=co_domain_renderer,
            edge_renderer=edge_renderer,
        )
