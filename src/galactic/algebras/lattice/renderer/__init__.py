"""
The :mod:`galactic.algebras.lattice.renderer` module.

It defines classes for rendering diagrams:

* :class:`ReducedContextDiagram`
* :class:`ReducedContextDiagramRenderer`
"""

__all__ = (
    "ReducedContextDiagram",
    "ReducedContextDiagramRenderer",
)

from ._main import (
    ReducedContextDiagram,
    ReducedContextDiagramRenderer,
)
