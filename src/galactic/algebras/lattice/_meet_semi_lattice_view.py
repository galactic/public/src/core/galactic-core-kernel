"""The :mod:`_meet_semi_lattice_view` module."""

from __future__ import annotations

from typing import TYPE_CHECKING, TypeVar, cast

from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    Bottom,
    FinitePartiallyOrderedSetView,
    FinitePartialOrder,
    MutableFinitePartiallyOrderedSet,
    Top,
)
from galactic.helpers.core import default_repr

from ._meet_semi_lattice_abstract import AbstractFiniteMeetSemiLattice
from ._meet_semi_lattice_element import Meetable
from ._meet_semi_lattice_mixins import (
    Atoms,
    FiniteMeetSemiLatticeMixin,
    MeetSemiLatticeCoveringRelation,
)

if TYPE_CHECKING:
    from collections.abc import Collection

_M = TypeVar("_M", bound=Meetable)


# pylint: disable=too-many-instance-attributes
class FiniteMeetSemiLatticeView(FiniteMeetSemiLatticeMixin[_M]):
    """
    It represents bounded finite meet semi-lattice.

    Parameters
    ----------
    semi_lattice
        The inner semi-lattice.
    lower
        The lower bound.
    upper
        The upper bound.

    """

    __slots__ = (
        "_semi_lattice",
        "_version",
        "_elements",
        "_irreducible",
        "_length",
        "_cover",
        "_order",
        "_lower",
        "_upper",
        "_atoms",
        "_top",
        "_bottom",
    )

    _semi_lattice: AbstractFiniteMeetSemiLattice[_M]
    _version: int
    _elements: MutableFinitePartiallyOrderedSet[_M]
    _irreducible: AbstractFinitePartiallyOrderedSet[_M]
    _length: int | None
    _cover: AbstractFiniteCoveringRelation[_M]
    _order: AbstractFinitePartialOrder[_M]
    _lower: _M | None
    _upper: _M | None
    _atoms: Collection[_M]
    _top: Collection[_M]
    _bottom: Collection[_M]

    def __init__(
        self,
        semi_lattice: AbstractFiniteMeetSemiLattice[_M],
        lower: _M | None = None,
        upper: _M | None = None,
    ) -> None:
        self._semi_lattice = semi_lattice
        if upper is lower is None:
            self._version = semi_lattice.version
            self._elements = semi_lattice.meet_irreducible  # type: ignore[assignment]
            self._bottom = semi_lattice.bottom
            self._top = semi_lattice.top
            self._atoms = semi_lattice.atoms
        else:
            self._version = 0
            self._elements = MutableFinitePartiallyOrderedSet[_M]()
            self._bottom = Bottom(semi_lattice, lower, upper)
            self._top = Top(semi_lattice, lower, upper)
            self._atoms = Atoms[_M](self)
        # noinspection PyTypeChecker
        self._irreducible = FinitePartiallyOrderedSetView[_M](self._elements)
        self._length = None
        self._cover = MeetSemiLatticeCoveringRelation[_M](self)  # type: ignore[assignment]
        self._order = FinitePartialOrder[_M](self)
        self._lower = lower
        self._upper = upper

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number.

        Notes
        -----
        This can be used to detect a change in the semi-lattice.

        """
        return self._semi_lattice.version

    def __repr__(self) -> str:
        if self._lower is None:
            if self._upper is None:
                class_name = "MeetSemiLattice"
            else:
                class_name = "UpperBoundedMeetSemiLattice"
        elif self._upper is None:
            class_name = "LowerBoundedMeetSemiLattice"
        else:
            class_name = "BoundedMeetSemiLattice"
        return cast(str, default_repr(self, class_name))

    def __len__(self) -> int:
        if self._length is None or self._version != self._semi_lattice.version:
            self._length = super().__len__()
        return self._length

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_M]:
        """
        Get the partial order of this meet semi-lattice.

        Returns
        -------
        AbstractFinitePartialOrder[_M]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_M]:
        """
        Get the covering relation of this meet semi-lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[_M]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_M]:
        """
        Get the top elements.

        Returns
        -------
        Collection[_M]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[_M]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_M]
            The bottom elements.

        """
        return self._bottom

    def filter(
        self,
        element: _M,
    ) -> AbstractFiniteMeetSemiLattice[_M]:
        """
        Get a filter of a meet semi-lattice.

        Parameters
        ----------
        element
            The lower limit.

        Returns
        -------
        AbstractFiniteMeetSemiLattice[_M]
            A view on the bounded semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the meet semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        # noinspection PyTypeChecker
        return FiniteMeetSemiLatticeView[_M](
            self._semi_lattice,
            lower=element,
            upper=self._upper,
        )

    def ideal(
        self,
        element: _M,
    ) -> AbstractFiniteMeetSemiLattice[_M]:
        """
        Get an ideal of a meet semi-lattice.

        Parameters
        ----------
        element
            The upper limit.

        Returns
        -------
        AbstractFiniteMeetSemiLattice[_M]
            A view on the bounded semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the meet semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to this semi-lattice")
        # noinspection PyTypeChecker
        return FiniteMeetSemiLatticeView[_M](
            self._semi_lattice,
            lower=self._lower,
            upper=element,
        )

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def atoms(self) -> Collection[_M]:
        """
        Get the atoms of this meet semi-lattice.

        Returns
        -------
        Collection[_M]
            The atoms.

        """
        return self._atoms

    @property
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_M]:
        """
        Get the meet-irreducible elements of this meet semi-lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_M]
            The meet-irreducible elements.

        """
        if self._version != self._semi_lattice.version and (
            self._lower is not None or self._upper is not None
        ):
            self._elements.clear()
            self._elements.extend(
                irreducible if self._upper is None else irreducible & self._upper
                for irreducible in self._semi_lattice.meet_irreducible
                if self._lower is None or irreducible >= self._lower
            )
            self._version = self._semi_lattice.version
        return self._irreducible
