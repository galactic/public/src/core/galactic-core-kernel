"""The :mod:`_meet_semi_lattice` module."""

from __future__ import annotations

import copy
import itertools
from collections.abc import (
    Collection,
    Iterable,
    Iterator,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import TypeVar, cast

from typing_extensions import Self

from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    FinitePartiallyOrderedSetView,
    FinitePartialOrder,
    MutableFinitePartiallyOrderedSet,
)
from galactic.helpers.core import default_repr

from ._meet_semi_lattice_abstract import AbstractFiniteMeetSemiLattice
from ._meet_semi_lattice_element import Meetable, infimum
from ._meet_semi_lattice_mixins import (
    Atoms,
    FiniteMeetSemiLatticeMixin,
    MeetSemiLatticeCoveringRelation,
)
from ._meet_semi_lattice_view import FiniteMeetSemiLatticeView

_M = TypeVar("_M", bound=Meetable)


# pylint: disable=too-many-instance-attributes
class FrozenFiniteMeetSemiLattice(FiniteMeetSemiLatticeMixin[_M]):
    """
    It represents concrete finite meet semi-lattice.

    An instance stores its irreducible in a poset.

    Its memory complexity is in :math:`O(j)`

    Parameters
    ----------
    *others
        A sequence of meet semi-lattices.
    elements
        The initial elements.

    Example
    -------
    >>> from galactic.algebras.lattice import ExtensibleFiniteMeetSemiLattice
    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> semi_lattice = ExtensibleFiniteMeetSemiLattice[PrimeFactors](
    ...     elements = [
    ...         PrimeFactors(2*3*5),
    ...         PrimeFactors(3*5*7),
    ...         PrimeFactors(5*7*11)
    ...     ]
    ... )
    >>> semi_lattice
    <galactic.algebras.lattice.ExtensibleFiniteMeetSemiLattice object at 0x...>

    It's possible to get the maximum element.

    Example
    -------
    >>> int(semi_lattice.minimum)
    5

    It's possible to iterate over the elements.

    Example
    -------
    >>> sorted(list(map(int, semi_lattice)))
    [5, 15, 30, 35, 105, 385]

    It's possible to iterate over the atoms.

    Example
    -------
    >>> sorted(list(map(int, semi_lattice.atoms)))
    [15, 35]

    It's possible to iterate over the meet irreducible.

    Example
    -------
    >>> sorted(list(map(int, semi_lattice.meet_irreducible)))
    [30, 105, 385]

    It's possible to iterate over the smallest meet irreducible greater than a limit.

    Example
    -------
    >>> sorted(
    ...     list(
    ...         map(
    ...             int,
    ...             semi_lattice.smallest_meet_irreducible(PrimeFactors(7))
    ...         )
    ...     )
    ... )
    [105, 385]

    It's possible to enlarge a meet semi-lattice.

    Example
    -------
    >>> semi_lattice.extend([PrimeFactors(13)])
    >>> sorted(list(map(int, semi_lattice)))
    [1, 5, 13, 15, 30, 35, 105, 385]

    """

    __slots__ = (
        "_version",
        "_elements",
        "_irreducible",
        "_successors",
        "_length",
        "_cover",
        "_order",
        "_minimum",
        "_atoms",
        "_bottom",
        "_hash_value",
    )

    _version: int
    _elements: MutableFinitePartiallyOrderedSet[_M]
    _irreducible: FinitePartiallyOrderedSetView[_M]
    _successors: dict[_M, _M]
    _length: int | None
    _cover: AbstractFiniteCoveringRelation[_M]
    _order: AbstractFinitePartialOrder[_M]
    _minimum: _M | None
    _atoms: Collection[_M]
    _bottom: Collection[_M]
    _hash_value: int | None

    def __init__(
        self,
        *others: AbstractFiniteMeetSemiLattice[_M],
        elements: Iterable[_M] | None = None,
    ) -> None:

        class Bottom:
            """
            It represents the bottom elements in a meet semi-lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if semi_lattice._minimum is None:
                    return False
                return item == semi_lattice._minimum

            def __len__(self) -> int:
                if semi_lattice._minimum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[_M]:
                if semi_lattice._minimum is not None:
                    yield semi_lattice._minimum

        semi_lattice = self
        self._version = 1
        self._elements = MutableFinitePartiallyOrderedSet[_M]()
        self._irreducible = FinitePartiallyOrderedSetView[_M](self._elements)
        self._successors: dict[_M, _M] = {}
        self._length = None
        self._cover = MeetSemiLatticeCoveringRelation[_M](self)  # type: ignore[assignment]
        self._order = FinitePartialOrder[_M](self)
        self._minimum = None
        self._atoms = Atoms[_M](self)
        self._bottom = Bottom()
        self._hash_value = None
        self._extend(
            itertools.chain(
                itertools.chain.from_iterable(others),
                elements or [],
            ),
        )

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number.

        Notes
        -----
        This can be used to detect a change in the semi-lattice.

        """
        return self._version

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __hash__(self) -> int:
        if self._hash_value is None:
            # noinspection PyProtectedMember
            self._hash_value = AbstractSet._hash(self._elements)  # type: ignore[arg-type]
        return self._hash_value

    def __copy__(self) -> Self:
        # noinspection PyTypeChecker
        return self.__class__(self)

    def copy(self) -> Self:
        """
        Get a copy of the meet semi-lattice.

        Returns
        -------
        Self
            The copy of this meet semi-lattice.

        """
        return copy.copy(self)

    def __len__(self) -> int:
        if self._length is None:
            self._length = super().__len__()
        return self._length

    # Lattice properties and method

    def __or__(self: Self, iterable: Iterable[_M]) -> Self:
        return self.__class__(self, elements=iterable)

    def __ior__(self: Self, iterable: Iterable[_M]) -> Self:
        self._extend(iterable)
        return self

    def __and__(self: Self, iterable: Iterable[_M]) -> Self:
        return self.__class__(elements=[item for item in iterable if item in self])

    def join(self: Self, *others: Self) -> Self:
        """
        Compute the join of this meet-semilattice and others.

        Parameters
        ----------
        *others
            A sequence of meet-semilattice.

        Returns
        -------
        Self
            The join of this meet-semilattice and others
        """
        return self.__class__(self, *others)

    def meet(self: Self, *others: Self) -> Self:
        """
        Compute the meet of this meet-semilattice and others.

        Parameters
        ----------
        *others
            A sequence of meet-semilattice.

        Returns
        -------
        Self
            The meet of this meet-semilattice and others
        """
        best = self
        best_value = len(self.meet_irreducible)
        for other in others:
            value = len(other.meet_irreducible)
            if value < best_value:
                best = other
                best_value = value
        if best is self:
            return self.__class__(
                elements=[
                    item for item in self if all(item in other for other in others)
                ],
            )
        return self.__class__(
            elements=[
                item
                for item in best
                if all(item in other for other in others if other is not best)
                and item in self
            ],
        )

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_M]:
        """
        Get the partial order of this meet semi-lattice.

        Returns
        -------
        AbstractFinitePartialOrder[_M]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_M]:
        """
        Get the covering relation of this meet semi-lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[_M]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_M]:
        """
        Get the top elements.

        The collection contains only one element, the maximum element.

        Returns
        -------
        Collection[_M]
            The top elements.

        """
        return self.meet_irreducible.top

    @property
    def bottom(self) -> Collection[_M]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_M]
            The bottom elements.

        """
        return self._bottom

    def filter(
        self,
        element: _M,
    ) -> AbstractFiniteMeetSemiLattice[_M]:
        """
        Get a filter of a meet semi-lattice.

        Parameters
        ----------
        element
            The lower limit.

        Returns
        -------
        AbstractFiniteMeetSemiLattice[_M]
            A view on the lower bounded meet semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the meet semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        # noinspection PyTypeChecker
        return FiniteMeetSemiLatticeView[_M](self, lower=element)

    def ideal(self, element: _M) -> AbstractFiniteMeetSemiLattice[_M]:
        """
        Get an ideal of a meet semi-lattice.

        Parameters
        ----------
        element
            The upper limit.

        Returns
        -------
        AbstractFiniteMeetSemiLattice[_M]
            A view on the upper bounded meet semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the meet semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to this semi-lattice")
        # noinspection PyTypeChecker
        return FiniteMeetSemiLatticeView[_M](self, upper=element)

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def atoms(self) -> Collection[_M]:
        """
        Get the atoms of this meet semi-lattice.

        Returns
        -------
        Collection[_M]
            The atoms.

        """
        return self._atoms

    @property
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_M]:
        """
        Get the meet-irreducible elements of this meet semi-lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_M]
            The meet-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._irreducible

    # FiniteMeetSemiLattice properties and methods

    def _extend(self, iterable: Iterable[_M]) -> None:
        if not isinstance(iterable, self.__class__):
            values = iterable
        else:
            # Extend only by irreducible
            values = iterable.meet_irreducible
        for value in values:
            if self._minimum is None:
                self._minimum = value
                self._elements.add(value)
            elif value not in self:
                self._minimum &= value
                self._elements.add(value)
                if self._elements.cover.successors(value):
                    self._successors[value] = infimum(
                        self._elements.cover.successors(value),
                    )
                remove = set()
                for predecessor in self._elements.cover.predecessors(value):
                    if predecessor in self._successors:
                        self._successors[predecessor] &= value
                        if self._successors[predecessor] == predecessor:
                            remove.add(predecessor)
                            del self._successors[predecessor]
                    else:
                        self._successors[predecessor] = value
                self._elements.difference_update(remove)
        self._version += 1
        self._length = None


class ExtensibleFiniteMeetSemiLattice(FrozenFiniteMeetSemiLattice[_M]):
    """
    It represents extensible meet finite semi-lattices.
    """

    __slots__ = ()
    __hash__ = None  # type: ignore[assignment]

    def extend(self, iterable: Iterable[_M]) -> None:
        """
        Extend this meet semi-lattice.

        Parameters
        ----------
        iterable
            An iterable of values.

        """
        self._extend(iterable)
