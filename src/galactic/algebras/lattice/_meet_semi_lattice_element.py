"""The :mod:`_meet_semi_lattice_element` module."""

from abc import abstractmethod
from collections.abc import Iterable, Iterator
from typing import Protocol, TypeVar, runtime_checkable

from typing_extensions import Self

from galactic.algebras.poset import PartiallyComparable, lower_limit


@runtime_checkable
class Meetable(PartiallyComparable, Protocol):
    r"""
    It represents *meetable* elements.

    This type sets for each pair of elements a, b a unique infimum
    :math:`c = a \wedge b` (also called one of the greatest lower bound or meet).

    Example
    -------
    Let the integers ordered by the relation :math:`a \leq b`: is :math:`a` a
    divisor of :math:`b`?

    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> PrimeFactors(24) & PrimeFactors(36)
    PrimeFactors(12)
    >>> PrimeFactors(24).meet(PrimeFactors(36), PrimeFactors(10))
    PrimeFactors(2)

    """

    __slots__ = ()

    @abstractmethod
    def __and__(self, other: Self) -> Self:
        """
        Return the meet of this element and the other.

        Parameters
        ----------
        other
            the other element

        Returns
        -------
        Self
            The meet of this element and the other

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    def meet(self, *others: Self) -> Self:
        """
        Return the infimum of this element and the others.

        Parameters
        ----------
        *others
            The others elements

        Returns
        -------
        Self
            The meet of this element and the others

        """
        result = self
        for value in others:
            result = result & value
        return result

    def meet_level(self, generators: Iterable[Self]) -> int:
        """
        Compute the level of an element considering an iterable of generators.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The level of the element.

        """
        return sum(1 for generator in generators if generator >= self)

    def meet_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return self.meet_level(generators)


_M = TypeVar("_M", bound=Meetable)


def infimum(iterable: Iterable[_M]) -> _M:
    """
    Compute the infimum of meetable elements.

    Parameters
    ----------
    iterable
        An iterable collection of meetable elements.

    Returns
    -------
    _M
        The meet of all elements from the iterable collection.

    Raises
    ------
    ValueError
        If the iterable is empty.

    """
    iterator = iter(iterable)
    try:
        element = next(iterator)
    except StopIteration as error:
        raise ValueError("infimum expected a non-empty argument") from error
    return element.meet(*list(iterator))


def infimum_generators(iterable: Iterable[_M]) -> Iterator[_M]:
    """
    Produce the infimum generators from the iterable of meetable elements.

    Parameters
    ----------
    iterable
        The iterable collection of elements.

    Yields
    ------
    _M
        An infimum generator.

    """
    for element in iterable:
        try:
            meet = infimum(lower_limit(iterable, element, strict=True))
            if meet > element:
                yield element
        except ValueError:  # noqa: PERF203
            yield element
