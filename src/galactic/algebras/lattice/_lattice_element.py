"""The :mod:`_lattice_element` module."""

from typing import Protocol

from ._join_semi_lattice_element import Joinable
from ._meet_semi_lattice_element import Meetable


# pylint: disable=too-few-public-methods,too-many-ancestors
class Element(Meetable, Joinable, Protocol):
    """
    It represents elements that can be member of a lattice.
    """

    __slots__ = ()
