"""The :mod:`_lattice_mixins` module."""

from __future__ import annotations

import itertools
from typing import TYPE_CHECKING, TypeVar, cast

from galactic.algebras.poset import (
    FiniteCoveringRelationMixin,
    FinitePartiallyOrderedSetMixin,
    FrozenFinitePartiallyOrderedSet,
    Neighbourhood,
    bottom,
    lower_limit,
    top,
    upper_limit,
)
from galactic.helpers.core import default_repr

from ._join_semi_lattice_element import supremum
from ._join_semi_lattice_mixins import (
    FiniteJoinSemiLatticeMixin,
    generate_neighbourhoods_from_join,
)
from ._lattice_abstract import AbstractFiniteLattice
from ._lattice_element import Element
from ._meet_semi_lattice_element import infimum
from ._meet_semi_lattice_mixins import (
    FiniteMeetSemiLatticeMixin,
    generate_neighbourhoods_from_meet,
)

if TYPE_CHECKING:
    from collections.abc import Collection, Iterator


_E = TypeVar("_E", bound=Element)


class FiniteLatticeMixin(FinitePartiallyOrderedSetMixin[_E]):
    """
    It represents finite lattice mixin.
    """

    __slots__ = ()

    def __eq__(self: AbstractFiniteLattice[_E], other: object) -> bool:
        if not isinstance(other, AbstractFiniteLattice):
            return super().__eq__(other)
        if len(self.meet_irreducible) < len(self.join_irreducible):
            return self.meet_irreducible == other.meet_irreducible
        return self.join_irreducible == other.join_irreducible

    # Collection properties and methods

    def __contains__(self: AbstractFiniteLattice[_E], item: object) -> bool:
        try:
            # Compute in O(|J|+|M|)
            return self.minimum is not None and (
                # Compute in constant time
                item in (self.minimum, self.maximum)
                # Compute in constant time
                or item in self.join_irreducible
                # Compute in constant time
                or item in self.meet_irreducible
                # Compute in O(|J|+|M|)
                or item
                == supremum(
                    upper_limit(self.join_irreducible, item),  # type: ignore[type-var]
                )
                == infimum(
                    lower_limit(self.meet_irreducible, item),  # type: ignore[type-var]
                )
            )
        except (ValueError, TypeError):
            return False

    def __len__(self: AbstractFiniteLattice[_E]) -> int:
        return sum(1 for _ in self)

    def __iter__(self: AbstractFiniteLattice[_E]) -> Iterator[_E]:
        for neighbourhood in self.cover.neighbourhoods():
            yield neighbourhood.element

    def __reversed__(self: AbstractFiniteLattice[_E]) -> Iterator[_E]:
        for neighbourhood in self.cover.neighbourhoods(True):
            yield neighbourhood.element

    # Set comparison properties and methods
    def __lt__(
        self: AbstractFiniteLattice[_E],
        other: Collection[_E],
    ) -> bool:
        if not isinstance(other, AbstractFiniteLattice):
            return super().__lt__(other)
        if self.maximum is None and other.maximum is not None:
            return True
        if len(self.meet_irreducible) + len(other.meet_irreducible) < len(
            self.join_irreducible,
        ) + len(other.join_irreducible):
            return all(element in other for element in self.meet_irreducible) and any(
                element not in self for element in other.meet_irreducible
            )
        return all(element in other for element in self.join_irreducible) and any(
            element not in self for element in other.join_irreducible
        )

    def __gt__(
        self: AbstractFiniteLattice[_E],
        other: Collection[_E],
    ) -> bool:
        if not isinstance(other, AbstractFiniteLattice):
            return super().__gt__(other)
        if self.maximum is not None and other.maximum is None:
            return True
        if len(self.meet_irreducible) + len(other.meet_irreducible) < len(
            self.join_irreducible,
        ) + len(other.join_irreducible):
            return all(element in self for element in other.meet_irreducible) and any(
                element not in other for element in self.meet_irreducible
            )
        return all(element in self for element in other.join_irreducible) and any(
            element not in other for element in self.join_irreducible
        )

    def __le__(
        self: AbstractFiniteLattice[_E],
        other: Collection[_E],
    ) -> bool:
        if not isinstance(other, AbstractFiniteLattice):
            return super().__le__(other)
        if len(self.meet_irreducible) < len(self.join_irreducible):
            return all(element in other for element in self.meet_irreducible)
        return all(element in other for element in self.join_irreducible)

    def __ge__(
        self: AbstractFiniteLattice[_E],
        other: Collection[_E],
    ) -> bool:
        if not isinstance(other, AbstractFiniteLattice):
            return super().__ge__(other)
        if len(self.meet_irreducible) < len(self.join_irreducible):
            return all(element in self for element in other.meet_irreducible)
        return all(element in self for element in other.join_irreducible)

    # AbstractFinitePartiallyOrderedSet properties and methods

    def upper_limit(
        self: AbstractFiniteLattice[_E],
        *limits: _E,
        strict: bool = False,
    ) -> Collection[_E]:
        """
        Get the values less than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_E]
            Values which are less than the limits.

        """

        class UpperLimit:
            """
            It represents the collection of values less than a limit.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in lattice and all(item < limit for limit in limits)  # type: ignore[operator]
                return item in lattice and all(item <= limit for limit in limits)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_E]:
                if lattice.minimum is not None:
                    # noinspection PyTypeChecker
                    for neighbourhood in generate_neighbourhoods_from_join(
                        FrozenFinitePartiallyOrderedSet[_E](
                            elements=itertools.chain(
                                [lattice.minimum],
                                (
                                    irreducible
                                    for irreducible in lattice.join_irreducible
                                    if (
                                        (
                                            strict
                                            and all(
                                                irreducible < limit for limit in limits
                                            )
                                        )
                                        or (
                                            not strict
                                            and all(
                                                irreducible <= limit for limit in limits
                                            )
                                        )
                                    )
                                ),
                            ),
                        ),
                    ):
                        if strict:
                            if all(neighbourhood.element < limit for limit in limits):
                                yield neighbourhood.element
                        elif all(neighbourhood.element <= limit for limit in limits):
                            yield neighbourhood.element

        lattice = self
        return UpperLimit()

    def lower_limit(
        self: AbstractFiniteLattice[_E],
        *limits: _E,
        strict: bool = False,
    ) -> Collection[_E]:
        """
        Get the values greater than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_E]
            A collection of values.

        """

        class LowerLimit:
            """
            It represents the collection of values greater than a limit.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in lattice and all(item > limit for limit in limits)  # type: ignore[operator]
                return item in lattice and all(item >= limit for limit in limits)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_E]:
                if lattice.maximum is not None:
                    # noinspection PyTypeChecker
                    for neighbourhood in generate_neighbourhoods_from_meet(
                        FrozenFinitePartiallyOrderedSet[_E](
                            elements=itertools.chain(
                                [lattice.maximum],
                                (
                                    irreducible
                                    for irreducible in lattice.meet_irreducible
                                    if (
                                        (
                                            strict
                                            and all(
                                                irreducible > limit for limit in limits
                                            )
                                        )
                                        or (
                                            not strict
                                            and all(
                                                irreducible >= limit for limit in limits
                                            )
                                        )
                                    )
                                ),
                            ),
                        ),
                    ):
                        if strict:
                            if all(neighbourhood.element > limit for limit in limits):
                                yield neighbourhood.element
                        elif all(neighbourhood.element >= limit for limit in limits):
                            yield neighbourhood.element

        lattice = self
        return LowerLimit()

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def co_atoms(self: AbstractFiniteLattice[_E]) -> Collection[_E]:
        """
        Get the co-atoms of this lattice.

        Returns
        -------
        Collection[_E]
            The co-atoms.

        """
        return self.meet_irreducible.top

    greatest_join_irreducible = FiniteJoinSemiLatticeMixin.greatest_join_irreducible
    floor = FiniteJoinSemiLatticeMixin.floor

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def atoms(self: AbstractFiniteLattice[_E]) -> Collection[_E]:
        """
        Get the atoms of this lattice.

        Returns
        -------
        Collection[_E]
            The atoms.

        """
        return self.join_irreducible.bottom

    smallest_meet_irreducible = FiniteMeetSemiLatticeMixin.smallest_meet_irreducible
    ceil = FiniteMeetSemiLatticeMixin.ceil

    # AbstractFiniteLattice propertites and methods

    def interval(
        self: AbstractFiniteLattice[_E],
        *limits: _E,
    ) -> tuple[_E | None, _E | None]:
        """
        Get the interval of limits.

        Parameters
        ----------
        *limits
            The limits whose interval is requested

        Returns
        -------
        tuple[_E | None, _E | None]
            The interval of the limits.

        """
        return self.floor(*limits), self.ceil(*limits)


class LatticeCoveringRelation(FiniteCoveringRelationMixin[_E]):
    """
    It represents the covering relation.

    Parameters
    ----------
    lattice
        The lattice.

    """

    __slots__ = ("_lattice",)

    _lattice: AbstractFiniteLattice[_E]

    # noinspection PyProtocol
    def __init__(self, lattice: AbstractFiniteLattice[_E]) -> None:
        super().__init__(lattice)
        self._lattice = lattice

    # AbstractBinaryRelation properties and methods

    # noinspection PyProtocol
    def successors(self, element: _E) -> Collection[_E]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested.

        Returns
        -------
        Collection[_E]
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the lattice.

        """

        class Successors:
            """
            It represents the successors of an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                return item in iter(self)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_E]:
                return bottom(
                    element | irreducible
                    for irreducible in lattice.join_irreducible
                    if not irreducible <= element
                )

        if element not in self._lattice:
            raise ValueError(f"{element} does not belong to the lattice")
        lattice = self._lattice
        return Successors()

    # noinspection PyProtocol
    def predecessors(self, element: _E) -> Collection[_E]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested.

        Returns
        -------
        Collection[_E]
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the lattice.

        """

        class Predecessors:
            """
            It represents the predecessors of an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                return item in iter(self)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_E]:
                return top(
                    element & irreducible
                    for irreducible in lattice.meet_irreducible
                    if not irreducible >= element
                )

        if element not in self._lattice:
            raise ValueError(f"{element} does not belong to the lattice")
        lattice = self._lattice
        return Predecessors()

    # AbstractFiniteCoveringRelation properties and methods

    def neighbourhoods(self, reverse: bool = False) -> Iterator[Neighbourhood[_E]]:
        """
        Get an iterator over the neighbourhoods of an element.

        Parameters
        ----------
        reverse
            Is this a bottom-up generation ?

        Returns
        -------
        Iterator[Neighbourhood[_E]]
            An iterator giving a triple (element, successors, predecessors) for
            each element in the covering relation.

        """
        if self._lattice.minimum is self._lattice.maximum is None:
            return iter([])
        if reverse:
            return generate_neighbourhoods_from_join(
                FrozenFinitePartiallyOrderedSet(
                    self._lattice.join_irreducible,
                    elements=[self._lattice.minimum],  # type: ignore[list-item]
                ),
            )
        return generate_neighbourhoods_from_meet(
            FrozenFinitePartiallyOrderedSet(
                self._lattice.meet_irreducible,
                elements=[self._lattice.maximum],  # type: ignore[list-item]
            ),
        )
