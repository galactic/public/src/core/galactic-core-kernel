"""The :mod:`_meet_semi_lattice_abstract` module."""

from abc import abstractmethod
from collections.abc import Collection
from typing import Protocol, TypeVar, runtime_checkable

from galactic.algebras.poset import AbstractFinitePartiallyOrderedSet

from ._meet_semi_lattice_element import Meetable

_M = TypeVar("_M", bound=Meetable)


# pylint: disable=too-many-ancestors
@runtime_checkable
class AbstractFiniteMeetSemiLattice(
    AbstractFinitePartiallyOrderedSet[_M],
    Collection[_M],
    Protocol,
):
    """
    It describes finite meet semi-lattices.
    """

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    @abstractmethod
    def atoms(self) -> Collection[_M]:
        """
        Get the atoms of this meet semi-lattice.

        Returns
        -------
        Collection[_M]
            The atoms.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_M]:
        """
        Get the meet irreducible elements.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_M]
            The meet irreducible elements.

        """
        raise NotImplementedError

    @abstractmethod
    def ceil(self, *limits: _M) -> _M | None:
        """
        Get the ceil of item.

        Parameters
        ----------
        *limits
            Elements whose ceil is requested.

        Returns
        -------
        _M | None
            The ceil of others

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def smallest_meet_irreducible(
        self,
        *limits: _M,
        strict: bool = False,
    ) -> Collection[_M]:
        """
        Get the smallest meet irreducible greater than the limits.

        Parameters
        ----------
        *limits
            The limits whose smallest meet irreducible are requested.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_M]
            The smallest meet irreducible greater than the limits.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
