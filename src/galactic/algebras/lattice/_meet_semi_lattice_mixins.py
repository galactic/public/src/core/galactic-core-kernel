"""The :mod:`_meet_semi_lattice_mixins` module."""

from __future__ import annotations

import contextlib
from collections import defaultdict
from collections.abc import Collection, Iterator
from heapq import heappop, heappush
from queue import SimpleQueue
from typing import Generic, TypeVar, cast

from galactic.algebras.poset import (
    AbstractFinitePartiallyOrderedSet,
    FiniteCoveringRelationMixin,
    FinitePartiallyOrderedSetMixin,
    FrozenFinitePartiallyOrderedSet,
    Neighbourhood,
    bottom,
    lower_limit,
    top,
)
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._meet_semi_lattice_abstract import AbstractFiniteMeetSemiLattice
from ._meet_semi_lattice_element import Meetable, infimum

_M = TypeVar("_M", bound=Meetable)


def generate_neighbourhoods_from_meet(
    poset: AbstractFinitePartiallyOrderedSet[_M],
) -> Iterator[Neighbourhood[_M]]:
    """
    Generate neighbourhoods from meet elements.

    Parameters
    ----------
    poset
        The poset of meet elements.

    Returns
    -------
    Iterator[Neighbourhood[_M]]
        An iterator over the neighbourhood.

    """

    class Neighbourhoods(Iterator[Neighbourhood[_M]]):
        # pylint: disable=too-few-public-methods
        """
        It class represents the neighbourhoods af meet semi-lattice.
        """

        __slots__ = ("_heap", "_count", "_instances", "_successors")

        _heap: list[tuple[int, int, _M]]
        _count: int
        _instances: dict[_M, _M]
        _successors: dict[_M, FIFOSet[_M]]

        def __init__(self) -> None:
            # Initialise with the bottom elements
            self._heap = []
            self._count = 0
            self._successors = defaultdict(FIFOSet[_M])
            self._instances = {}
            for element in poset.top:
                self._instances[element] = element
                self._successors[element] = FIFOSet[_M]()
                priority = element.meet_priority([element])
                heappush(self._heap, (priority, self._count, element))
                self._count += 1

        def __next__(self) -> Neighbourhood[_M]:
            class Successors:
                """
                It represents the successors af an element.
                """

                def __repr__(self) -> str:
                    return cast(str, default_repr(self, long=False))

                def __bool__(self) -> bool:
                    for _ in self:
                        return True
                    return False

                # Collection properties and methods

                def __contains__(self, item: object) -> bool:
                    return item in successors

                def __len__(self) -> int:
                    return len(successors)

                def __iter__(self) -> Iterator[_M]:
                    return iter(successors)

            class Predecessors:
                """
                It represents the predecessors af an element.
                """

                def __repr__(self) -> str:
                    return cast(str, default_repr(self, long=False))

                def __bool__(self) -> bool:
                    for _ in self:
                        return True
                    return False

                # Collection properties and methods

                def __contains__(self, item: object) -> bool:
                    return item in predecessors

                def __len__(self) -> int:
                    return len(predecessors)

                def __iter__(self) -> Iterator[_M]:
                    return iter(predecessors)

            try:
                _, _, current = heappop(self._heap)
                predecessors = FIFOSet[_M](
                    top(
                        current & irreducible
                        for irreducible in poset
                        if not irreducible >= current
                    ),
                )
                for predecessor in predecessors:
                    self._successors[predecessor].add(current)
                    if predecessor not in self._instances:
                        self._instances[predecessor] = predecessor
                        priority = predecessor.meet_priority(poset)
                        heappush(self._heap, (priority, self._count, predecessor))
                        self._count += 1
                successors = self._successors[current]
                del self._successors[current]
                del self._instances[current]
            except IndexError as error:
                raise StopIteration from error
            return Neighbourhood[_M](
                element=current,
                successors=Successors(),
                predecessors=Predecessors(),
            )

    return Neighbourhoods()


class Atoms(Generic[_M]):
    """
    It represents the atoms of a meet semi-lattice.

    Parameters
    ----------
    semi_lattice
        The inner meet semi-lattice.

    """

    __slots__ = ("_semi_lattice", "_version", "_elements")

    _semi_lattice: AbstractFiniteMeetSemiLattice[_M]
    _version: int
    _elements: FIFOSet[_M]

    def __init__(self, semi_lattice: AbstractFiniteMeetSemiLattice[_M]) -> None:
        self._semi_lattice = semi_lattice
        self._version = 0
        self._elements = FIFOSet[_M]()

    def __repr__(self) -> str:
        return cast(str, default_repr(self, "Collection", long=False))

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        self._update()
        return item in self._elements

    def __len__(self) -> int:
        self._update()
        return len(self._elements)

    def __iter__(self) -> Iterator[_M]:
        self._update()
        return iter(self._elements)

    def _update(self) -> None:
        if self._version != self._semi_lattice.version:
            if self._semi_lattice.minimum is None:
                self._elements = FIFOSet[_M]()
            else:
                self._elements = FIFOSet[_M](
                    self._semi_lattice.cover.successors(self._semi_lattice.minimum),
                )
            self._version = self._semi_lattice.version


class MeetSemiLatticeCoveringRelation(FiniteCoveringRelationMixin[_M]):
    """
    It represents the covering relation.

    Parameters
    ----------
    semi_lattice
        The semi-lattice.

    """

    __slots__ = ("_semi_lattice",)

    _semi_lattice: AbstractFiniteMeetSemiLattice[_M]

    # noinspection PyProtocol
    def __init__(self, semi_lattice: AbstractFiniteMeetSemiLattice[_M]) -> None:
        super().__init__(semi_lattice)
        self._semi_lattice = semi_lattice

    # AbstractBinaryRelation properties and methods

    # noinspection PyProtocol
    def successors(self, element: _M) -> Collection[_M]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested.

        Returns
        -------
        Collection[_M]
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the meet semi-lattice.

        """

        class Successors:
            """
            It represents the successors of an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                try:
                    return item > element and all(  # type: ignore[operator]
                        not meet & item > element  # type: ignore[operator]
                        for meet in semi_lattice.meet_irreducible
                        if not meet >= item  # type: ignore[operator]
                    )
                except TypeError:
                    return False

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_M]:
                queue = SimpleQueue[tuple[int, list[_M]]]()
                irreducible = [
                    meet for meet in semi_lattice.meet_irreducible if meet > element
                ]
                if irreducible:
                    queue.put((0, irreducible))
                collection: set[_M] = set()
                while not queue.empty():
                    start, irreducible = queue.get()
                    value = infimum(irreducible)
                    if all(not value >= item for item in collection):
                        if value > element:
                            collection.add(value)
                            yield value
                        else:
                            for index in range(start, len(irreducible)):
                                clone = irreducible.copy()
                                del clone[index]
                                if clone:
                                    queue.put((index, clone))

        if element not in self._semi_lattice:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        semi_lattice = self._semi_lattice
        return Successors()

    # noinspection PyProtocol
    def predecessors(self, element: _M) -> Collection[_M]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested.

        Returns
        -------
        Collection[_M]
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the meet semi-lattice.

        """

        class Predecessors:
            """
            It represents the predecessors of an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                return item in iter(self)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_M]:
                return top(
                    element & irreducible
                    for irreducible in semi_lattice.meet_irreducible
                    if not irreducible >= element
                )

        if element not in self._semi_lattice:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        semi_lattice = self._semi_lattice
        return Predecessors()

    # AbstractFiniteCoveringRelation properties and methods

    def neighbourhoods(self, reverse: bool = False) -> Iterator[Neighbourhood[_M]]:
        """
        Get an iterator over the neighbourhoods of an element.

        Parameters
        ----------
        reverse
            Is this a bottom-up generation ?

        Returns
        -------
        Iterator[Neighbourhood[_M]]
            An iterator giving a triple (element, successors, predecessors) for
            each element in the covering relation.

        Raises
        ------
        NotImplementedError
            if reverse is set.

        """
        if reverse:
            raise NotImplementedError
        return generate_neighbourhoods_from_meet(self._semi_lattice.meet_irreducible)


class FiniteMeetSemiLatticeMixin(FinitePartiallyOrderedSetMixin[_M]):
    """
    It represents finite meet semi-lattice mixin.
    """

    __slots__ = ()

    def __bool__(self) -> bool:
        return self.minimum is not None

    def __eq__(self: AbstractFiniteMeetSemiLattice[_M], other: object) -> bool:
        if not isinstance(other, AbstractFiniteMeetSemiLattice):
            return super().__eq__(other)
        return self.meet_irreducible == other.meet_irreducible

    # Collection properties and methods

    def __contains__(self: AbstractFiniteMeetSemiLattice[_M], item: object) -> bool:
        if self.minimum is None:
            return False
        if item == self.minimum:
            return True
        if item in self.meet_irreducible:
            return True
        with contextlib.suppress(TypeError, ValueError):
            minimum = infimum(
                lower_limit(self.meet_irreducible, item, strict=True),  # type: ignore[type-var]
            )
            if minimum == item:
                return True
        return False

    def __len__(self: AbstractFiniteMeetSemiLattice[_M]) -> int:
        return sum(1 for _ in self)

    def __iter__(self: AbstractFiniteMeetSemiLattice[_M]) -> Iterator[_M]:
        return (neighbourhood.element for neighbourhood in self.cover.neighbourhoods())

    # Set comparison properties and methods
    def __lt__(
        self: AbstractFiniteMeetSemiLattice[_M],
        other: Collection[_M],
    ) -> bool:
        if not isinstance(other, AbstractFiniteMeetSemiLattice):
            return super().__lt__(other)
        return (self.minimum is None and other.minimum is not None) or (
            all(element in other for element in self.meet_irreducible)
            and any(element not in self for element in other.meet_irreducible)
        )

    def __gt__(
        self: AbstractFiniteMeetSemiLattice[_M],
        other: Collection[_M],
    ) -> bool:
        if not isinstance(other, AbstractFiniteMeetSemiLattice):
            return super().__gt__(other)
        return (other.minimum is None and self.minimum is not None) or (
            all(element in self for element in other.meet_irreducible)
            and any(element not in other for element in self.meet_irreducible)
        )

    def __le__(
        self: AbstractFiniteMeetSemiLattice[_M],
        other: Collection[_M],
    ) -> bool:
        if not isinstance(other, AbstractFiniteMeetSemiLattice):
            return super().__le__(other)
        return all(element in other for element in self.meet_irreducible)

    def __ge__(
        self: AbstractFiniteMeetSemiLattice[_M],
        other: Collection[_M],
    ) -> bool:
        if not isinstance(other, AbstractFiniteMeetSemiLattice):
            return super().__ge__(other)
        return all(element in self for element in other.meet_irreducible)

    # AbstractFinitePartiallyOrderedSet properties and methods

    def upper_limit(
        self: AbstractFiniteMeetSemiLattice[_M],
        *limits: _M,
        strict: bool = False,
    ) -> Collection[_M]:
        """
        Get the values less than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_M]
            Values which are less than the limits.

        """

        class UpperLimit:
            """
            It represents the collection of values less than a limit.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in semi_lattice and all(
                        item < limit for limit in limits  # type: ignore[operator]
                    )
                return item in semi_lattice and all(
                    item <= limit for limit in limits  # type: ignore[operator]
                )

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_M]:
                # noinspection PyTypeChecker
                for neighbourhood in generate_neighbourhoods_from_meet(
                    FrozenFinitePartiallyOrderedSet[_M](
                        elements=semi_lattice.meet_irreducible,
                    ),
                ):
                    if strict:
                        if all(neighbourhood.element < limit for limit in limits):
                            yield neighbourhood.element
                    elif all(neighbourhood.element <= limit for limit in limits):
                        yield neighbourhood.element

        semi_lattice = self
        return UpperLimit()

    def lower_limit(
        self: AbstractFiniteMeetSemiLattice[_M],
        *limits: _M,
        strict: bool = False,
    ) -> Collection[_M]:
        """
        Get the values greater than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_M]
            A collection of values.

        """

        class LowerLimit:
            """
            It represents the collection of values greater than a limit.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in semi_lattice and all(
                        item > limit for limit in limits  # type: ignore[operator]
                    )
                return item in semi_lattice and all(
                    item >= limit for limit in limits  # type: ignore[operator]
                )

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_M]:
                # noinspection PyTypeChecker
                for neighbourhood in generate_neighbourhoods_from_meet(
                    FrozenFinitePartiallyOrderedSet[_M](
                        elements=(
                            irreducible
                            for irreducible in semi_lattice.meet_irreducible
                            if (
                                (
                                    strict
                                    and all(irreducible > limit for limit in limits)
                                )
                                or (
                                    not strict
                                    and all(irreducible >= limit for limit in limits)
                                )
                            )
                        ),
                    ),
                ):
                    if strict:
                        if all(neighbourhood.element > limit for limit in limits):
                            yield neighbourhood.element
                    elif all(neighbourhood.element >= limit for limit in limits):
                        yield neighbourhood.element

        semi_lattice = self
        return LowerLimit()

    # AbstractFiniteMeetSemiLattice properties and methods

    def ceil(self: AbstractFiniteMeetSemiLattice[_M], *limits: _M) -> _M | None:
        """
        Get the ceil of item.

        Parameters
        ----------
        *limits
            Elements whose ceil is requested.

        Returns
        -------
        _M | None
            The ceil of others

        """
        irreducible = self.smallest_meet_irreducible(*limits)
        if not irreducible:
            return None
        iterator = iter(irreducible)
        element: _M = next(iterator)
        return element.meet(*iterator)

    def smallest_meet_irreducible(
        self: AbstractFiniteMeetSemiLattice[_M],
        *limits: _M,
        strict: bool = False,
    ) -> Collection[_M]:
        """
        Get the smallest meet irreducible greater than a limits.

        Parameters
        ----------
        *limits
            The limits whose smallest meet irreducible are requested.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_M]
            The smallest meet irreducible greater than the limits.

        """

        class SmallestMeetIrreducible(Collection[_M]):
            """
            It represents a subset of the meet irreducible.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in iter(self)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_M]:
                return bottom(
                    lower_limit(semi_lattice.meet_irreducible, *limits, strict=strict),
                )

        semi_lattice = self
        return SmallestMeetIrreducible()
