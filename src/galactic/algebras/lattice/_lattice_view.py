"""The :mod:`_lattice_view` module."""

from __future__ import annotations

from typing import TYPE_CHECKING, TypeVar, cast

from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    FinitePartiallyOrderedSetView,
    FinitePartialOrder,
    MutableFinitePartiallyOrderedSet,
)
from galactic.helpers.core import default_repr

from ._join_semi_lattice_mixins import CoAtoms
from ._lattice_abstract import AbstractFiniteLattice
from ._lattice_element import Element
from ._lattice_mixins import FiniteLatticeMixin, LatticeCoveringRelation
from ._meet_semi_lattice_mixins import Atoms

if TYPE_CHECKING:
    from collections.abc import Collection, Iterator

_E = TypeVar("_E", bound=Element)


# pylint: disable=too-many-instance-attributes
class FiniteLatticeView(FiniteLatticeMixin[_E]):
    """
    It represents a view of a lattice using ideal or filter construction.

    Parameters
    ----------
    lattice
        The inner lattice.
    lower
        The lower bound.
    upper
        The upper bound.

    """

    __slots__ = (
        "_lattice",
        "_version",
        "_join_elements",
        "_join_irreducible",
        "_meet_elements",
        "_meet_irreducible",
        "_length",
        "_cover",
        "_order",
        "_lower",
        "_upper",
        "_atoms",
        "_co_atoms",
        "_top",
        "_bottom",
    )

    _lattice: AbstractFiniteLattice[_E]
    _version: int
    _join_elements: MutableFinitePartiallyOrderedSet[_E]
    _join_irreducible: AbstractFinitePartiallyOrderedSet[_E]
    _meet_elements: MutableFinitePartiallyOrderedSet[_E]
    _meet_irreducible: AbstractFinitePartiallyOrderedSet[_E]
    _length: int | None
    _cover: AbstractFiniteCoveringRelation[_E]
    _order: AbstractFinitePartialOrder[_E]
    _lower: _E | None
    _upper: _E | None
    _atoms: Collection[_E]
    _co_atoms: Collection[_E]
    _top: Collection[_E]
    _bottom: Collection[_E]

    def __init__(
        self,
        lattice: AbstractFiniteLattice[_E],
        lower: _E | None = None,
        upper: _E | None = None,
    ) -> None:

        class Top:
            """
            It represents the top elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if upper is None:
                    if bounded._lattice.maximum is None:
                        return False
                    return item == bounded._lattice.maximum
                return item == upper

            def __len__(self) -> int:
                if upper is bounded._lattice.maximum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[_E]:
                if upper is None:
                    if bounded._lattice.maximum is not None:
                        yield bounded._lattice.maximum
                else:
                    yield upper

        class Bottom:
            """
            It represents the bottom elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lower is None:
                    if bounded._lattice.minimum is None:
                        return False
                    return item == bounded._lattice.minimum
                return item == lower

            def __len__(self) -> int:
                if lower is bounded._lattice.minimum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[_E]:
                if lower is None:
                    if bounded._lattice.minimum is not None:
                        yield bounded._lattice.minimum
                else:
                    yield lower

        bounded = self
        self._lattice = lattice
        if upper is lower is None:
            self._version = lattice.version
            self._join_elements = lattice.join_irreducible  # type: ignore[assignment]
            self._meet_elements = lattice.meet_irreducible  # type: ignore[assignment]
            self._bottom = lattice.bottom
            self._top = lattice.top
            self._atoms = lattice.atoms
            self._co_atoms = lattice.co_atoms
        else:
            self._version = 0
            self._join_elements = MutableFinitePartiallyOrderedSet[_E]()
            self._meet_elements = MutableFinitePartiallyOrderedSet[_E]()
            self._bottom = Bottom()
            self._top = Top()
            self._atoms = Atoms[_E](self)
            self._co_atoms = CoAtoms[_E](self)

        # noinspection PyTypeChecker
        self._join_irreducible = FinitePartiallyOrderedSetView[_E](self._join_elements)
        # noinspection PyTypeChecker
        self._meet_irreducible = FinitePartiallyOrderedSetView[_E](self._meet_elements)
        self._length = None
        self._cover = LatticeCoveringRelation[_E](self)  # type: ignore[assignment]
        self._order = FinitePartialOrder[_E](self)
        self._lower = lower
        self._upper = upper

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number.

        Notes
        -----
        This can be used to detect a change in the lattice.

        """
        return self._lattice.version

    def __repr__(self) -> str:
        if self._lower is None:
            class_name = "Lattice" if self._upper is None else "UpperBoundedLattice"
        elif self._upper is None:
            class_name = "LowerBoundedLattice"
        else:
            class_name = "BoundedLattice"
        return cast(str, default_repr(self, class_name))

    def __len__(self) -> int:
        if self._length is None or self._version != self._lattice.version:
            self._length = super().__len__()
        return self._length

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_E]:
        """
        Get the partial order of this lattice.

        Returns
        -------
        AbstractFinitePartialOrder[_E]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_E]:
        """
        Get the covering relation of this lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[_E]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_E]:
        """
        Get the top elements.

        Returns
        -------
        Collection[_E]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[_E]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_E]
            The bottom elements.

        """
        return self._bottom

    def filter(
        self,
        element: _E,
    ) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get a filter of a lattice.

        Parameters
        ----------
        element
            The lower limit.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            A view on the bounded lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the lattice")
        # noinspection PyTypeChecker
        return FiniteLatticeView[_E](
            self._lattice,
            lower=element,
            upper=self._upper,
        )

    def ideal(self, element: _E) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get an ideal of a lattice.

        Parameters
        ----------
        element
            The upper limit.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            A view on the bounded lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to this lattice")
        # noinspection PyTypeChecker
        return FiniteLatticeView[_E](
            self._lattice,
            lower=self._lower,
            upper=element,
        )

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def atoms(self) -> Collection[_E]:
        """
        Get the atoms of this lattice.

        Returns
        -------
        Collection[_E]
            The atoms.

        """
        return self._atoms

    @property
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get the meet-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            The meet-irreducible elements.

        """
        if self._version != self._lattice.version and (
            self._lower is not None or self._upper is not None
        ):
            self._update()
        return self._meet_irreducible

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    def co_atoms(self) -> Collection[_E]:
        """
        Get the co-atoms of this lattice.

        Returns
        -------
        Collection[_E]
            The co-atoms.

        """
        return self._co_atoms

    @property
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get the join-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            The join-irreducible elements.

        """
        if self._version != self._lattice.version and (
            self._lower is not None or self._upper is not None
        ):
            self._update()
        return self._join_irreducible

    # FiniteLattice properties and methods

    def _update(self) -> None:
        self._meet_elements.clear()
        self._meet_elements.extend(
            irreducible if self._upper is None else irreducible & self._upper
            for irreducible in self._lattice.meet_irreducible
            if self._lower is None or irreducible >= self._lower
        )
        self._join_elements.clear()
        self._join_elements.extend(
            irreducible if self._lower is None else irreducible | self._lower
            for irreducible in self._lattice.join_irreducible
            if self._upper is None or irreducible <= self._upper
        )
        self._version = self._lattice.version
