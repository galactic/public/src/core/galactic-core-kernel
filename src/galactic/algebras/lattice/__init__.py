"""
The :mod:`galactic.algebras.lattice` module.

It defines types for representing lattices.

..  rubric:: Elements of semi-lattices and lattices

* :class:`Joinable` for joinable elements;
* :class:`Meetable` for meetable elements;
* :class:`Element` for joinable and meetable elements.

..  rubric:: Function on elements of semi-lattices and lattices

* :func:`supremum`
* :func:`supremum_generators`
* :func:`infimum`
* :func:`infimum_generators`

..  rubric:: Abstract classes of semi-lattices and lattices

* :class:`AbstractFiniteJoinSemiLattice`
* :class:`AbstractFiniteMeetSemiLattice`
* :class:`AbstractFiniteLattice`

..  rubric:: Mixin classes of semi-lattices and lattices

* :class:`FiniteJoinSemiLatticeMixin`
* :class:`FiniteMeetSemiLatticeMixin`
* :class:`FiniteLatticeMixin`

..  rubric:: Concrete classes of semi-lattices and lattices

* :class:`FrozenFiniteJoinSemiLattice`
* :class:`FrozenFiniteMeetSemiLattice`
* :class:`FrozenFiniteLattice`
* :class:`ExtensibleFiniteJoinSemiLattice`
* :class:`ExtensibleFiniteMeetSemiLattice`
* :class:`ExtensibleFiniteLattice`
* :class:`FiniteJoinSemiLatticeView`
* :class:`FiniteMeetSemiLatticeView`
* :class:`FiniteLatticeView`
"""

__all__ = (
    "Joinable",
    "Meetable",
    "Element",
    "infimum",
    "infimum_generators",
    "supremum",
    "supremum_generators",
    "AbstractFiniteJoinSemiLattice",
    "AbstractFiniteMeetSemiLattice",
    "AbstractFiniteLattice",
    "FiniteJoinSemiLatticeMixin",
    "FiniteMeetSemiLatticeMixin",
    "FiniteLatticeMixin",
    "FrozenFiniteJoinSemiLattice",
    "FrozenFiniteMeetSemiLattice",
    "FrozenFiniteLattice",
    "ExtensibleFiniteJoinSemiLattice",
    "ExtensibleFiniteMeetSemiLattice",
    "ExtensibleFiniteLattice",
    "FiniteJoinSemiLatticeView",
    "FiniteMeetSemiLatticeView",
    "FiniteLatticeView",
    "ReducedContext",
    "LatticeCoveringRelation",
    "JoinSemiLatticeCoveringRelation",
    "MeetSemiLatticeCoveringRelation",
)

from ._join_semi_lattice import (
    ExtensibleFiniteJoinSemiLattice,
    FrozenFiniteJoinSemiLattice,
)
from ._join_semi_lattice_abstract import (
    AbstractFiniteJoinSemiLattice,
)
from ._join_semi_lattice_element import (
    Joinable,
    supremum,
    supremum_generators,
)
from ._join_semi_lattice_mixins import (
    FiniteJoinSemiLatticeMixin,
    JoinSemiLatticeCoveringRelation,
)
from ._join_semi_lattice_view import (
    FiniteJoinSemiLatticeView,
)
from ._lattice import (
    ExtensibleFiniteLattice,
    FrozenFiniteLattice,
    ReducedContext,
)
from ._lattice_abstract import (
    AbstractFiniteLattice,
)
from ._lattice_element import (
    Element,
)
from ._lattice_mixins import (
    FiniteLatticeMixin,
    LatticeCoveringRelation,
)
from ._lattice_view import (
    FiniteLatticeView,
)
from ._meet_semi_lattice import (
    ExtensibleFiniteMeetSemiLattice,
    FrozenFiniteMeetSemiLattice,
)
from ._meet_semi_lattice_abstract import (
    AbstractFiniteMeetSemiLattice,
)
from ._meet_semi_lattice_element import (
    Meetable,
    infimum,
    infimum_generators,
)
from ._meet_semi_lattice_mixins import (
    FiniteMeetSemiLatticeMixin,
    MeetSemiLatticeCoveringRelation,
)
from ._meet_semi_lattice_view import (
    FiniteMeetSemiLatticeView,
)
