"""The :mod:`_lattice` module."""

# pylint: disable=too-many-lines

from __future__ import annotations

import copy
import itertools
from collections.abc import (
    Collection,
    Iterable,
    Iterator,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import Generic, TypeVar, cast

from typing_extensions import Self

from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    FinitePartiallyOrderedSetView,
    FinitePartialOrder,
    MutableFinitePartiallyOrderedSet,
    lower_limit,
    upper_limit,
)
from galactic.algebras.relational import (
    FiniteRelationMixin,
)
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._join_semi_lattice_abstract import AbstractFiniteJoinSemiLattice
from ._join_semi_lattice_element import supremum
from ._lattice_abstract import AbstractFiniteLattice
from ._lattice_element import Element
from ._lattice_mixins import FiniteLatticeMixin, LatticeCoveringRelation
from ._lattice_view import FiniteLatticeView
from ._meet_semi_lattice_abstract import AbstractFiniteMeetSemiLattice
from ._meet_semi_lattice_element import infimum

_E = TypeVar("_E", bound=Element)


# pylint: disable=too-many-instance-attributes
class FrozenFiniteLattice(FiniteLatticeMixin[_E]):
    """
    It represents frozen finite lattices.

    Parameters
    ----------
    *others
        A sequence of lattices.
    elements
        The initial elements.

    """

    __slots__ = (
        "_version",
        "_join_elements",
        "_join_irreducible",
        "_join_predecessors",
        "_meet_elements",
        "_meet_irreducible",
        "_meet_successors",
        "_length",
        "_cover",
        "_order",
        "_maximum",
        "_minimum",
        "_atoms",
        "_co_atoms",
        "_top",
        "_bottom",
        "_hash_value",
    )

    _version: int
    _join_elements: MutableFinitePartiallyOrderedSet[_E]
    _join_irreducible: FinitePartiallyOrderedSetView[_E]
    _join_predecessors: dict[_E, _E]
    _meet_elements: MutableFinitePartiallyOrderedSet[_E]
    _meet_irreducible: FinitePartiallyOrderedSetView[_E]
    _meet_successors: dict[_E, _E]
    _length: int | None
    _cover: AbstractFiniteCoveringRelation[_E]
    _order: AbstractFinitePartialOrder[_E]
    _maximum: _E | None
    _minimum: _E | None
    _top: Collection[_E]
    _bottom: Collection[_E]
    _hash_value: int | None

    def __init__(
        self,
        *others: AbstractFiniteLattice[_E],
        elements: Iterable[_E] | None = None,
    ) -> None:

        class Top:
            """
            It represents the top elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice._maximum is None:
                    return False
                return item == lattice._maximum

            def __len__(self) -> int:
                if lattice._maximum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[_E]:
                if lattice._maximum is not None:
                    yield lattice._maximum

        class Bottom:
            """
            It represents the bottom elements in a lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if lattice._minimum is None:
                    return False
                return item == lattice._minimum

            def __len__(self) -> int:
                if lattice._minimum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[_E]:
                if lattice._minimum is not None:
                    yield lattice._minimum

        lattice = self
        self._version = 1
        self._join_elements = MutableFinitePartiallyOrderedSet[_E]()
        self._join_irreducible = FinitePartiallyOrderedSetView[_E](self._join_elements)
        self._join_predecessors: dict[_E, _E] = {}
        self._meet_elements = MutableFinitePartiallyOrderedSet[_E]()
        self._meet_irreducible = FinitePartiallyOrderedSetView[_E](self._meet_elements)
        self._meet_successors: dict[_E, _E] = {}
        self._length = None
        self._cover = LatticeCoveringRelation[_E](self)  # type: ignore[assignment]
        self._order = FinitePartialOrder[_E](self)
        self._maximum = None
        self._minimum = None
        self._top = Top()
        self._bottom = Bottom()
        self._hash_value = None
        self._extend(
            itertools.chain(
                itertools.chain.from_iterable(others),
                elements or [],
            ),
        )

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number

        """
        return self._version

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    # noinspection PyProtectedMember
    def __hash__(self) -> int:
        if self._hash_value is None:
            if len(self._join_elements) < len(self._meet_elements):
                self._hash_value = AbstractSet._hash(
                    self._join_elements,  # type: ignore[arg-type]
                )
            else:
                self._hash_value = AbstractSet._hash(
                    self._meet_elements,  # type: ignore[arg-type]
                )
        return self._hash_value

    def __copy__(self) -> Self:
        # noinspection PyTypeChecker
        return self.__class__(self)

    def copy(self) -> Self:
        """
        Get a copy of the lattice.

        Returns
        -------
        Self
            The copy of this lattice.

        """
        return copy.copy(self)

    def __len__(self) -> int:
        if self._length is None:
            self._length = super().__len__()
        return self._length

    # Lattice properties and method

    def __or__(self: Self, iterable: Iterable[_E]) -> Self:
        return self.__class__(self, elements=iterable)

    def __ior__(self: Self, iterable: Iterable[_E]) -> Self:
        self._extend(iterable)
        return self

    def __and__(self: Self, iterable: Iterable[_E]) -> Self:
        return self.__class__(elements=[item for item in iterable if item in self])

    def join(self: Self, *others: Self) -> Self:
        """
        Compute the join of this object and others.

        Parameters
        ----------
        *others
            A sequence of lattice.

        Returns
        -------
        Self
            The join of this object and others
        """
        return self.__class__(self, *others)

    def meet(self: Self, *others: Self) -> Self:
        """
        Compute the meet of this object and others.

        Parameters
        ----------
        *others
            A sequence of lattice.

        Returns
        -------
        Self
            The meet of this object and others
        """
        best = self
        best_value = len(self.meet_irreducible) * len(self.join_irreducible)
        for other in others:
            value = len(other.meet_irreducible) * len(other.join_irreducible)
            if value < best_value:
                best = other
                best_value = value
        if best is self:
            return self.__class__(
                elements=[
                    item for item in self if all(item in other for other in others)
                ],
            )
        return self.__class__(
            elements=[
                item
                for item in best
                if all(item in other for other in others if other is not best)
                and item in self
            ],
        )

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_E]:
        """
        Get the partial order of this lattice.

        Returns
        -------
        AbstractFinitePartialOrder[_E]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_E]:
        """
        Get the covering relation of this lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[_E]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_E]:
        """
        Get the top elements.

        Returns
        -------
        Collection[_E]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[_E]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_E]
            The bottom elements.

        """
        return self._bottom

    def filter(
        self,
        element: _E,
    ) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get a filter of a lattice.

        Parameters
        ----------
        element
            The lower limit.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            A view on the bounded lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the lattice")
        # noinspection PyTypeChecker
        return FiniteLatticeView[_E](self, lower=element)

    def ideal(
        self,
        element: _E,
    ) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get an ideal of a lattice.

        Parameters
        ----------
        element
            The upper limit.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            A view on the bounded lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to this lattice")
        # noinspection PyTypeChecker
        return FiniteLatticeView[_E](self, upper=element)

    # AbstractFiniteMeetSemiLattice properties and methods

    @property
    def meet_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get the meet-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            The meet-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._meet_irreducible

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_E]:
        """
        Get the join-irreducible elements of this lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_E]
            The join-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._join_irreducible

    # FiniteLattice properties and methods

    def _extend(self, iterable: Iterable[_E]) -> None:
        """
        In-place enlarge the lattice with an iterable of elements.

        Parameters
        ----------
        iterable
            An iterable of elements

        """
        # If iterable is a lattice or a semi-lattice, loop on remarkable values
        if isinstance(iterable, AbstractFiniteLattice):
            if iterable.maximum is not None and iterable.minimum is not None:
                if len(iterable.join_irreducible) < len(iterable.meet_irreducible):
                    self._reduced_context_completion(
                        itertools.chain(
                            iterable.join_irreducible,
                            [iterable.minimum],
                        ),
                    )
                else:
                    self._reduced_context_completion(
                        itertools.chain(
                            iterable.meet_irreducible,
                            [iterable.maximum],
                        ),
                    )
        elif isinstance(iterable, AbstractFiniteJoinSemiLattice):
            self._reduced_context_completion(iterable.join_irreducible)
        elif isinstance(iterable, AbstractFiniteMeetSemiLattice):
            self._reduced_context_completion(iterable.meet_irreducible)
        # Finally, loop on iterable
        else:
            self._reduced_context_completion(iterable)
        self._version += 1
        self._length = None

    def _reduced_context_completion(self, iterable: Iterable[_E]) -> None:
        """
        Naive version of the reduced context completion algorithm.

        Parameters
        ----------
        iterable
            An iterable of elements

        """
        irreducible_candidates = FIFOSet[_E]()
        for value in iterable:
            # The lattice is empty
            if self._maximum is None or self._minimum is None:
                self._set_maximum(value)
                self._set_minimum(value)
                continue

            # The value is a remarkable case:
            # - minimum
            # - maximum
            # - join irreducible
            # - meet irreducible
            if value in (self._minimum, self._maximum):
                continue
            if value in self._join_elements:
                continue
            if value in self._meet_elements:
                continue

            # The value is greater than the maximum
            if value > self._maximum:
                self._meet_elements.add(self._maximum)
                self._join_elements.add(value)
                self._meet_successors[self._maximum] = value
                self._join_predecessors[value] = self._maximum
                self._set_maximum(value)
                continue

            # The value is lesser than the minimum
            if value < self._minimum:
                self._join_elements.add(self._minimum)
                self._meet_elements.add(value)
                self._join_predecessors[self._minimum] = value
                self._meet_successors[value] = self._minimum
                self._set_minimum(value)
                continue

            # Case if value not comparable to minimum
            # Add new minimum based on inserted value
            if not value > self._minimum:
                self._join_elements.add(self._minimum)
                minimum = self._minimum & value
                self._join_predecessors[self._minimum] = minimum
                self._set_minimum(minimum)

            # Case if value not comparable to maximum
            # Add new maximum based on inserted value
            if not value < self._maximum:
                self._meet_elements.add(self._maximum)
                maximum = self._maximum | value
                self._meet_successors[self._maximum] = maximum
                self._set_maximum(maximum)

            # Initialization : Add value as join irreducible, meet irreducible
            # and irreducible candidate
            irreducible_candidates.add(value)
            self._join_elements.add(value)
            self._meet_elements.add(value)

            # Continue the processing until no new irreducible elements are found
            while irreducible_candidates:
                # Compute the lower and upper bound of the irreducible_candidate
                # between each irreducible of the lattice
                candidate = irreducible_candidates.pop()
                join_list, meet_list = self._insert_irreducible_naive(candidate)
                # Update the table based on irreducible candidates
                # found in insert_irreducible_naive
                self._update_irreducible(join_list, meet_list, irreducible_candidates)

    def _set_maximum(self, maximum: _E) -> None:
        """
        Set the maximum of the lattice.

        Parameters
        ----------
        maximum
            The new maximum.

        """
        self._maximum = maximum

    def _set_minimum(self, minimum: _E) -> None:
        """
        Set the minimum of the lattice.

        Parameters
        ----------
        minimum
            The new minimum.

        """
        self._minimum = minimum

    def _set_join_irreducible(self, irreducible: Iterable[_E]) -> None:
        """
        Set the join irreducible of the lattice.

        Note
        ----
        The minimum must be set before setting the join irreducible.

        Parameters
        ----------
        irreducible
            The new join irreducible.

        """
        self._join_elements.clear()
        for element in irreducible:
            self._join_elements.add(element)

        self._join_predecessors.clear()
        remove = []
        for element in self._join_elements:
            predecessor = self._minimum.join(  # type: ignore[union-attr]
                *self._join_elements.cover.predecessors(element),
            )
            if predecessor == element:
                remove.append(element)
            else:
                self._join_predecessors[element] = predecessor
        for element in remove:
            self._join_elements.remove(element)

    def _set_meet_irreducible(self, irreducible: Iterable[_E]) -> None:
        """
        Set the meet irreducible of the lattice.

        Note
        ----
        The maximum must be set before setting the meet irreducible.

        Parameters
        ----------
        irreducible
            The new meet irreducible.

        """
        self._meet_elements.clear()
        for element in irreducible:
            self._meet_elements.add(element)

        self._meet_successors.clear()
        remove = []
        for element in self._meet_elements:
            successor = self._maximum.meet(  # type: ignore[union-attr]
                *self._meet_elements.cover.successors(element),
            )
            if successor == element:
                remove.append(element)
            else:
                self._meet_successors[element] = successor
        for element in remove:
            self._meet_elements.remove(element)

    def _insert_irreducible_naive(self, value: _E) -> tuple[FIFOSet[_E], FIFOSet[_E]]:
        join_list = FIFOSet[_E]()
        meet_list = FIFOSet[_E]()

        for element in itertools.chain(
            self._join_elements,
            self._meet_elements,
        ):
            # Compute meet between element and irreducible
            meet = element & value
            # Compute join between element and irreducible
            join = element | value

            # Add result as irreducible candidate if not equal to minimum
            if meet != self._minimum:
                join_list.add(meet)

            # Add result as irreducible candidate if not equal to maximum
            if join != self._maximum:
                meet_list.add(join)

        return join_list, meet_list

    def _update_irreducible(
        self,
        join_list: FIFOSet[_E],
        meet_list: FIFOSet[_E],
        irreducible_candidates: FIFOSet[_E],
    ) -> None:
        # set of new meet irreducible elements
        new_meet_elements = FIFOSet[_E]()
        # set of new join irreducible elements
        new_join_elements = FIFOSet[_E]()

        # Loop over irreducible candidate :
        #   - Previous meet and join elements to update them, they
        #       can lose the property of being irreducible
        #   - New elements found in insert_irreducible_naive function
        for irreducible_candidate in itertools.chain(
            self._meet_elements,
            meet_list,
            self._join_elements,
            join_list,
        ):
            # Compute the greatest lower bound of all
            # elements above the irreducible candidate
            meet_successor = infimum(  # type: ignore[type-var]
                itertools.chain(
                    lower_limit(
                        itertools.chain(
                            self._meet_elements,
                            self._join_elements,
                        ),
                        irreducible_candidate,
                        strict=True,
                    ),
                    [self._maximum],
                ),
            )

            # If the successor of the irreducible candidate == meet_successor
            # Then the irreducible candidate is not meet irreducible
            if irreducible_candidate not in (
                meet_successor,
                self._maximum,
            ):
                # If irreducible candidate is meet irreducible,
                # add in new_meet_elements set
                self._meet_successors[
                    irreducible_candidate
                ] = meet_successor  # type: ignore[assignment]
                new_meet_elements.add(irreducible_candidate)
                if irreducible_candidate not in self._meet_elements:
                    # If irreducible candidate is a new meet irreducible,
                    # add in irreducible_candidates
                    irreducible_candidates.add(irreducible_candidate)

            # Compute the least upper bound of all elements
            # below the irreducible candidate
            join_predecessor = supremum(  # type: ignore[type-var]
                itertools.chain(
                    upper_limit(
                        itertools.chain(
                            self._join_elements,
                            self._meet_elements,
                        ),
                        irreducible_candidate,
                        strict=True,
                    ),
                    [self._minimum],
                ),
            )

            # If the successor of the irreducible candidate == join_predecessor
            # Then the irreducible candidate is not join irreducible
            if irreducible_candidate not in (
                join_predecessor,
                self._minimum,
            ):
                # If irreducible candidate is join irreducible,
                # add in new_join_elements set
                self._join_predecessors[
                    irreducible_candidate
                ] = join_predecessor  # type: ignore[assignment]
                new_join_elements.add(irreducible_candidate)
                if irreducible_candidate not in self._join_elements:
                    # If irreducible candidate is a new join irreducible,
                    # add in irreducible_candidates
                    irreducible_candidates.add(irreducible_candidate)

        # Update irreducible elements
        self._set_join_irreducible(new_join_elements)
        self._set_meet_irreducible(new_meet_elements)


class ExtensibleFiniteLattice(FrozenFiniteLattice[_E]):
    r"""
    It represents extensible finite lattices.

    It uses its join irreducible and meet irreducible elements.

    The elements of this can be iterated from the maximum
    element to the minimum element or from the minimum element to the maximum element
    using the :func:`reversed` built-in function.
    It is not guaranteed that the order will be exactly the reverse order, but it is
    guaranteed that an element will always be iterated before its successors.

    Example
    -------
    Let the integers ordered by the relation :math:`a \leq b`: is :math:`a` a
    divisor of :math:`b`?

    implemented by the :class:`~galactic.algebras.examples.arithmetic.PrimeFactors`
    class:

    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> from galactic.algebras.lattice import ExtensibleFiniteLattice
    >>> lattice = ExtensibleFiniteLattice[PrimeFactors](
    ...     elements=[PrimeFactors(2), PrimeFactors(3)]
    ... )
    >>> lattice
    <galactic.algebras.lattice.ExtensibleFiniteLattice object at 0x...>
    >>> list(map(int, lattice))
    [6, 2, 3, 1]

    """

    __slots__ = ()
    __hash__ = None  # type: ignore[assignment]

    def extend(self, iterable: Iterable[_E]) -> None:
        """
        Extend this lattice.

        Parameters
        ----------
        iterable
            An iterable of values.

        """
        self._extend(iterable)


class ReducedContext(FiniteRelationMixin, Generic[_E]):
    """
    It represents the reduced context of a lattice.

    It is a binary relation between the join irreducible and the meet irreducible
    elements.

    Parameters
    ----------
    lattice
        The lattice.

    """

    __slots__ = ("_universes",)

    _universes: tuple[Collection[_E], Collection[_E]]

    def __init__(self, lattice: AbstractFiniteLattice[_E]) -> None:
        class Universe:
            """
            Meet and join irreducible universe.
            """

            __slots__ = ("_irreducible",)

            def __init__(self, irreducible: Collection[_E]) -> None:
                self._irreducible = irreducible

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __contains__(self, item: object) -> bool:
                return item is not None and (
                    item in self._irreducible or item is lattice.maximum
                )

            def __len__(self) -> int:
                return len(self._irreducible) or int(lattice.maximum is not None)

            def __iter__(self) -> Iterator[_E]:
                if self._irreducible:
                    return iter(self._irreducible)
                if lattice.maximum is not None:
                    return iter([lattice.maximum])
                return iter([])

        self._universes = (
            Universe(lattice.join_irreducible),
            Universe(lattice.meet_irreducible),
        )

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if isinstance(item, tuple) and len(item) == 2:
            try:
                (join, meet) = item
            except TypeError:
                return False
            return (
                join in self._universes[0]
                and meet in self._universes[1]
                and join <= meet
            )
        return False

    def __len__(self) -> int:
        return sum(1 for _ in self)

    def __iter__(self) -> Iterator[tuple[_E, _E]]:
        for join in self._universes[0]:
            for meet in self._universes[1]:
                if join <= meet:
                    yield join, meet

    # AbstractRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[_E], Collection[_E]]:
        """
        Get the universes of this reduced context.

        Returns
        -------
        tuple[Collection[_E], Collection[_E]]
            The universes of this reduced context.

        """
        return self._universes

    # AbstractBinaryRelation properties and methods

    @property
    def domain(self) -> Collection[_E]:
        """
        Get the domain of this reduced context.

        The domain is composed of the join irreducible of the underlying lattice.

        Returns
        -------
        Collection[_E]
            The join irreducible of the underlying lattice.

        """
        return self._universes[0]

    @property
    def co_domain(self) -> Collection[_E]:
        """
        Get the co-domain of this reduced context.

        The co-domain is composed of the meet irreducible of the underlying lattice.

        Returns
        -------
        Collection[_E]
            The meet irreducible of the underlying lattice.

        """
        return self._universes[1]

    def successors(self, element: _E) -> Collection[_E]:
        """
        Get the successors of a join-irreducible, i.e. its attributes.

        Parameters
        ----------
        element
            A join-irreducible of the underlying lattice.

        Returns
        -------
        Collection[_E]
            The successors of the element.

        Raises
        ------
        ValueError
            If the element does not belong to the domain of this reduced context.

        """

        class Successors:
            """
            It represents the successors in a context.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in context.co_domain and element <= item  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_E]:
                for meet in context.co_domain:
                    if element <= meet:
                        yield meet

        if element not in self._universes[0]:
            raise ValueError(
                f"{element} does not belong to the domain of this reduced context.",
            )
        context = self
        return Successors()

    def predecessors(self, element: _E) -> Collection[_E]:
        """
        Get the predecessors of a meet-irreducible, i.e. its objects.

        Parameters
        ----------
        element
            A meet-irreducible of the underlying lattice.

        Returns
        -------
        Collection[_E]
            The predecessors of the element.

        Raises
        ------
        ValueError
            If the element does not belong to the domain of this reduced context.

        """

        class Predecessors:
            """
            It represents the predecessors in a context.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in context.domain and element >= item  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_E]:
                for join in context.domain:
                    if element >= join:
                        yield join

        if element not in self._universes[1]:
            raise ValueError(
                f"{element} does not belong to the co-domain of this reduced context.",
            )
        context = self
        return Predecessors()
