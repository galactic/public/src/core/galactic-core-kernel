"""The :mod:`_join_semi_lattice` module."""

from __future__ import annotations

import copy
import itertools
from collections.abc import (
    Collection,
    Iterable,
    Iterator,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import TypeVar, cast

from typing_extensions import Self

from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    FinitePartiallyOrderedSetView,
    FinitePartialOrder,
    MutableFinitePartiallyOrderedSet,
)
from galactic.helpers.core import default_repr

from ._join_semi_lattice_abstract import AbstractFiniteJoinSemiLattice
from ._join_semi_lattice_element import Joinable, supremum
from ._join_semi_lattice_mixins import (
    CoAtoms,
    FiniteJoinSemiLatticeMixin,
    JoinSemiLatticeCoveringRelation,
)
from ._join_semi_lattice_view import FiniteJoinSemiLatticeView

_J = TypeVar("_J", bound=Joinable)


# pylint: disable=too-many-instance-attributes
class FrozenFiniteJoinSemiLattice(FiniteJoinSemiLatticeMixin[_J]):
    """
    It represents concrete finite join semi-lattice.

    An instance stores its irreducible in a poset.

    Its memory complexity is in :math:`O(j)`

    Parameters
    ----------
    *others
        A sequence of join semi-lattices.
    elements
        The initial elements.

    Example
    -------
    >>> from galactic.algebras.lattice import ExtensibleFiniteJoinSemiLattice
    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> semi_lattice = ExtensibleFiniteJoinSemiLattice[PrimeFactors](
    ...     elements = [
    ...         PrimeFactors(2*3*5),
    ...         PrimeFactors(3*5*7),
    ...         PrimeFactors(5*7*11)
    ...     ]
    ... )
    >>> semi_lattice
    <galactic.algebras.lattice.ExtensibleFiniteJoinSemiLattice object at 0x...>

    It's possible to get the maximum element.

    Example
    -------
    >>> int(semi_lattice.maximum)
    2310

    It's possible to iterate over the elements.

    Example
    -------
    >>> sorted(list(map(int, semi_lattice)))
    [30, 105, 210, 385, 1155, 2310]

    It's possible to iterate over the co-atoms.

    Example
    -------
    >>> sorted(list(map(int, semi_lattice.co_atoms)))
    [210, 1155]

    It's possible to iterate over the join irreducible.

    Example
    -------
    >>> sorted(list(map(int, semi_lattice.join_irreducible)))
    [30, 105, 385]

    It's possible to iterate over the greatest join irreducible smaller than a limit.

    Example
    -------
    >>> sorted(
    ...     list(
    ...         map(
    ...             int,
    ...             semi_lattice.greatest_join_irreducible(PrimeFactors(2*3*5*7))
    ...         )
    ...     )
    ... )
    [30, 105]

    It's possible to enlarge a join semi-lattice.

    Example
    -------
    >>> semi_lattice.extend([PrimeFactors(13)])
    >>> sorted(list(map(int, semi_lattice)))
    [13, 30, 105, 210, 385, 390, 1155, 1365, 2310, 2730, 5005, 15015, 30030]

    """

    __slots__ = (
        "_version",
        "_elements",
        "_irreducible",
        "_predecessors",
        "_length",
        "_cover",
        "_order",
        "_maximum",
        "_co_atoms",
        "_top",
        "_hash_value",
    )

    _version: int
    _elements: MutableFinitePartiallyOrderedSet[_J]
    _irreducible: FinitePartiallyOrderedSetView[_J]
    _predecessors: dict[_J, _J]
    _length: int | None
    _cover: AbstractFiniteCoveringRelation[_J]
    _order: AbstractFinitePartialOrder[_J]
    _maximum: _J | None
    _co_atoms: Collection[_J]
    _top: Collection[_J]
    _hash_value: int | None

    def __init__(
        self,
        *others: AbstractFiniteJoinSemiLattice[_J],
        elements: Iterable[_J] | None = None,
    ) -> None:

        class Top:
            """
            It represents the top elements in a join semi-lattice.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                if semi_lattice._maximum is None:
                    return False
                return item == semi_lattice._maximum

            def __len__(self) -> int:
                if semi_lattice._maximum is None:
                    return 0
                return 1

            def __iter__(self) -> Iterator[_J]:
                if semi_lattice._maximum is not None:
                    yield semi_lattice._maximum

        semi_lattice = self
        self._version = 1
        self._elements = MutableFinitePartiallyOrderedSet[_J]()
        self._irreducible = FinitePartiallyOrderedSetView[_J](self._elements)
        self._predecessors: dict[_J, _J] = {}
        self._length = None
        self._cover = JoinSemiLatticeCoveringRelation[_J](self)  # type: ignore[assignment]
        self._order = FinitePartialOrder[_J](self)
        self._maximum = None
        self._co_atoms = CoAtoms[_J](self)
        self._top = Top()
        self._hash_value = None
        self._extend(
            itertools.chain(
                itertools.chain.from_iterable(others),
                elements or [],
            ),
        )

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number.

        Notes
        -----
        This can be used to detect a change in the semi-lattice.

        """
        return self._version

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __hash__(self) -> int:
        if self._hash_value is None:
            # noinspection PyProtectedMember
            self._hash_value = AbstractSet._hash(self._elements)  # type: ignore[arg-type]
        return self._hash_value

    def __copy__(self) -> Self:
        # noinspection PyTypeChecker
        return self.__class__(self)

    def copy(self) -> Self:
        """
        Get a copy of the join semi-lattice.

        Returns
        -------
        Self
            The copy of this join semi-lattice.

        """
        return copy.copy(self)

    def __len__(self) -> int:
        if self._length is None:
            self._length = super().__len__()
        return self._length

    # Lattice properties and method

    def __or__(self: Self, iterable: Iterable[_J]) -> Self:
        return self.__class__(self, elements=iterable)

    def __ior__(self: Self, iterable: Iterable[_J]) -> Self:
        self._extend(iterable)
        return self

    def __and__(self: Self, iterable: Iterable[_J]) -> Self:
        return self.__class__(elements=[item for item in iterable if item in self])

    def join(self: Self, *others: Self) -> Self:
        """
        Compute the join of this join-semilattice and others.

        Parameters
        ----------
        *others
            A sequence of join-semilattice.

        Returns
        -------
        Self
            The join of this join-semilattice and others
        """
        return self.__class__(self, *others)

    def meet(self: Self, *others: Self) -> Self:
        """
        Compute the meet of this join-semilattice and others.

        Parameters
        ----------
        *others
            A sequence of join-semilattice.

        Returns
        -------
        Self
            The meet of this join-semilattice and others
        """
        best = self
        best_value = len(self.join_irreducible)
        for other in others:
            value = len(other.join_irreducible)
            if value < best_value:
                best = other
                best_value = value
        if best is self:
            return self.__class__(
                elements=[
                    item for item in self if all(item in other for other in others)
                ],
            )
        return self.__class__(
            elements=[
                item
                for item in best
                if all(item in other for other in others if other is not best)
                and item in self
            ],
        )

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_J]:
        """
        Get the partial order of this join semi-lattice.

        Returns
        -------
        AbstractFinitePartialOrder[_J]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_J]:
        """
        Get the covering relation of this join semi-lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[_J]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_J]:
        """
        Get the top elements.

        The collection contains only one element, the maximum element.

        Returns
        -------
        Collection[_J]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[_J]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_J]
            The bottom elements.

        """
        return self.join_irreducible.bottom

    def filter(
        self,
        element: _J,
    ) -> AbstractFiniteJoinSemiLattice[_J]:
        """
        Get a filter of a join semi-lattice.

        Parameters
        ----------
        element
            The lower limit.

        Returns
        -------
        AbstractFiniteJoinSemiLattice[_J]
            A view on the lower bounded join semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the join semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        # noinspection PyTypeChecker
        return FiniteJoinSemiLatticeView[_J](self, lower=element)

    def ideal(self, element: _J) -> AbstractFiniteJoinSemiLattice[_J]:
        """
        Get an ideal of a join semi-lattice.

        Parameters
        ----------
        element
            The upper limit.

        Returns
        -------
        AbstractFiniteJoinSemiLattice[_J]
            A view on the upper bounded join semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the join semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to this semi-lattice")
        # noinspection PyTypeChecker
        return FiniteJoinSemiLatticeView[_J](self, upper=element)

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    def co_atoms(self) -> Collection[_J]:
        """
        Get the co-atoms of this join semi-lattice.

        Returns
        -------
        Collection[_J]
            The co-atoms.

        """
        return self._co_atoms

    @property
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_J]:
        """
        Get the join-irreducible elements of this join semi-lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_J]
            The join-irreducible elements.

        """
        # noinspection PyTypeChecker
        return self._irreducible

    # FiniteJoinSemiLattice properties and methods

    def _extend(self, iterable: Iterable[_J]) -> None:
        if not isinstance(iterable, self.__class__):
            values = iterable
        else:
            # Extend only by irreducible
            values = iterable.join_irreducible
        for value in values:
            if self._maximum is None:
                self._maximum = value
                self._elements.add(value)
            elif value not in self:
                self._maximum |= value
                self._elements.add(value)
                if self._elements.cover.predecessors(value):
                    self._predecessors[value] = supremum(
                        self._elements.cover.predecessors(value),
                    )
                remove = set()
                for successor in self._elements.cover.successors(value):
                    if successor in self._predecessors:
                        self._predecessors[successor] |= value
                        if self._predecessors[successor] == successor:
                            remove.add(successor)
                            del self._predecessors[successor]
                    else:
                        self._predecessors[successor] = value
                self._elements.difference_update(remove)
        self._version += 1
        self._length = None


class ExtensibleFiniteJoinSemiLattice(FrozenFiniteJoinSemiLattice[_J]):
    """
    It represents extensible join finite semi-lattices.
    """

    __slots__ = ()
    __hash__ = None  # type: ignore[assignment]

    def extend(self, iterable: Iterable[_J]) -> None:
        """
        Extend this join semi-lattice.

        Parameters
        ----------
        iterable
            An iterable of values.

        """
        self._extend(iterable)
