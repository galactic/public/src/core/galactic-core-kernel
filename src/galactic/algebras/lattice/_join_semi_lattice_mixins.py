"""The :mod:`_join_semi_lattice_mixins` module."""

from __future__ import annotations

import contextlib
from collections import defaultdict
from collections.abc import Collection, Iterator
from heapq import heappop, heappush
from queue import SimpleQueue
from typing import Generic, TypeVar, cast

from galactic.algebras.poset import (
    AbstractFinitePartiallyOrderedSet,
    FiniteCoveringRelationMixin,
    FinitePartiallyOrderedSetMixin,
    FrozenFinitePartiallyOrderedSet,
    Neighbourhood,
    bottom,
    top,
    upper_limit,
)
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._join_semi_lattice_abstract import AbstractFiniteJoinSemiLattice
from ._join_semi_lattice_element import Joinable, supremum

_J = TypeVar("_J", bound=Joinable)


def generate_neighbourhoods_from_join(
    poset: AbstractFinitePartiallyOrderedSet[_J],
) -> Iterator[Neighbourhood[_J]]:
    """
    Generate neighbourhoods from join elements.

    Parameters
    ----------
    poset
        The poset of join elements.

    Returns
    -------
    Iterator[Neighbourhood[_J]]
        An iterator over the neighbourhood.

    """

    class Neighbourhoods(Iterator[Neighbourhood[_J]]):
        # pylint: disable=too-few-public-methods
        """
        It class represents the neighbourhoods af join semi-lattice.
        """

        __slots__ = ("_heap", "_count", "_instances", "_predecessors")

        _heap: list[tuple[int, int, _J]]
        _count: int
        _instances: dict[_J, _J]
        _predecessors: dict[_J, FIFOSet[_J]]

        def __init__(self) -> None:
            self._heap = []
            self._count = 0
            self._predecessors = defaultdict(FIFOSet[_J])
            self._instances = {}
            for element in poset.bottom:
                self._instances[element] = element
                self._predecessors[element] = FIFOSet[_J]()
                priority = element.join_priority([element])
                heappush(self._heap, (priority, self._count, element))
                self._count += 1

        def __next__(self) -> Neighbourhood[_J]:
            class Successors:
                """
                It represents the successors af an element.
                """

                def __repr__(self) -> str:
                    return cast(str, default_repr(self, long=False))

                # Collection properties and methods

                def __contains__(self, item: object) -> bool:
                    return item in successors

                def __len__(self) -> int:
                    return len(successors)

                def __iter__(self) -> Iterator[_J]:
                    return iter(successors)

            class Predecessors:
                """
                It represents the predecessors af an element.
                """

                def __repr__(self) -> str:
                    return cast(str, default_repr(self, long=False))

                # Collection properties and methods

                def __contains__(self, item: object) -> bool:
                    return item in predecessors

                def __len__(self) -> int:
                    return len(predecessors)

                def __iter__(self) -> Iterator[_J]:
                    return iter(predecessors)

            try:
                _, _, current = heappop(self._heap)
                successors = FIFOSet[_J](
                    bottom(
                        current | irreducible
                        for irreducible in poset
                        if not irreducible <= current
                    ),
                )
                for successor in successors:
                    self._predecessors[successor].add(current)
                    if successor not in self._instances:
                        self._instances[successor] = successor
                        priority = successor.join_priority(poset)
                        heappush(self._heap, (priority, self._count, successor))
                        self._count += 1
                predecessors = self._predecessors[current]
                del self._predecessors[current]
                del self._instances[current]
            except IndexError as error:
                raise StopIteration from error
            return Neighbourhood[_J](
                element=current,
                successors=Successors(),
                predecessors=Predecessors(),
            )

    return Neighbourhoods()


class CoAtoms(Generic[_J]):
    """
    It represents the co-atoms of a join semi-lattice.

    Parameters
    ----------
    semi_lattice
        The inner join semi-lattice.

    """

    __slots__ = ("_semi_lattice", "_version", "_elements")

    _semi_lattice: AbstractFiniteJoinSemiLattice[_J]
    _version: int
    _elements: FIFOSet[_J]

    def __init__(self, semi_lattice: AbstractFiniteJoinSemiLattice[_J]) -> None:
        self._semi_lattice = semi_lattice
        self._version = 0
        self._elements = FIFOSet[_J]()

    def __repr__(self) -> str:
        return cast(str, default_repr(self, "Collection", long=False))

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        self._update()
        return item in self._elements

    def __len__(self) -> int:
        self._update()
        return len(self._elements)

    def __iter__(self) -> Iterator[_J]:
        self._update()
        return iter(self._elements)

    def _update(self) -> None:
        if self._version != self._semi_lattice.version:
            if self._semi_lattice.maximum is None:
                self._elements = FIFOSet[_J]()
            else:
                self._elements = FIFOSet[_J](
                    self._semi_lattice.cover.predecessors(self._semi_lattice.maximum),
                )
            self._version = self._semi_lattice.version


class JoinSemiLatticeCoveringRelation(FiniteCoveringRelationMixin[_J]):
    """
    It represents the covering relation.

    Parameters
    ----------
    semi_lattice
        The semi-lattice.

    """

    __slots__ = ("_semi_lattice",)

    _semi_lattice: AbstractFiniteJoinSemiLattice[_J]

    # noinspection PyProtocol
    def __init__(self, semi_lattice: AbstractFiniteJoinSemiLattice[_J]) -> None:
        super().__init__(semi_lattice)
        self._semi_lattice = semi_lattice

    # AbstractBinaryRelation properties and methods

    # noinspection PyProtocol
    def successors(self, element: _J) -> Collection[_J]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested.

        Returns
        -------
        Collection[_J]
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the join semi-lattice.

        """

        class Successors:
            """
            It represents the successors of an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                return item in iter(self)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_J]:
                return bottom(
                    element | irreducible
                    for irreducible in semi_lattice.join_irreducible
                    if not irreducible <= element
                )

        if element not in self._semi_lattice:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        semi_lattice = self._semi_lattice
        return Successors()

    # noinspection PyProtocol
    def predecessors(self, element: _J) -> Collection[_J]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested.

        Returns
        -------
        Collection[_J]
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the join semi-lattice.

        """

        class Predecessors:
            """
            It represents the predecessors of an element.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                try:
                    return item < element and all(  # type: ignore[operator]
                        not join | item < element  # type: ignore[operator]
                        for join in semi_lattice.join_irreducible
                        if not join <= item  # type: ignore[operator]
                    )
                except TypeError:
                    return False

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_J]:
                queue = SimpleQueue[tuple[int, list[_J]]]()
                irreducible = [
                    join for join in semi_lattice.join_irreducible if join < element
                ]
                if irreducible:
                    queue.put((0, irreducible))
                collection: set[_J] = set()
                while not queue.empty():
                    start, irreducible = queue.get()
                    value = supremum(irreducible)
                    if all(not value <= item for item in collection):
                        if value < element:
                            collection.add(value)
                            yield value
                        else:
                            for index in range(start, len(irreducible)):
                                clone = irreducible.copy()
                                del clone[index]
                                if clone:
                                    queue.put((index, clone))

        if element not in self._semi_lattice:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        semi_lattice = self._semi_lattice
        return Predecessors()

    # AbstractFiniteCoveringRelation properties and methods

    def neighbourhoods(self, reverse: bool = False) -> Iterator[Neighbourhood[_J]]:
        """
        Get an iterator over the neighbourhoods of an element.

        Parameters
        ----------
        reverse
            Is this a bottom-up generation ?

        Returns
        -------
        Iterator[Neighbourhood[_J]]
            An iterator giving a triple (element, successors, predecessors) for
            each element in the covering relation.

        Raises
        ------
        NotImplementedError
            if reverse is set.

        """
        if reverse:
            raise NotImplementedError
        return generate_neighbourhoods_from_join(self._semi_lattice.join_irreducible)


class FiniteJoinSemiLatticeMixin(FinitePartiallyOrderedSetMixin[_J]):
    """
    It represents finite join semi-lattice mixin.
    """

    __slots__ = ()

    def __bool__(self) -> bool:
        return self.maximum is not None

    def __eq__(self: AbstractFiniteJoinSemiLattice[_J], other: object) -> bool:
        if not isinstance(other, AbstractFiniteJoinSemiLattice):
            return super().__eq__(other)
        return self.join_irreducible == other.join_irreducible

    # Collection properties and methods

    def __contains__(self: AbstractFiniteJoinSemiLattice[_J], item: object) -> bool:
        if self.maximum is None:
            return False
        if item == self.maximum:
            return True
        if item in self.join_irreducible:
            return True
        with contextlib.suppress(TypeError, ValueError):
            maximum = supremum(  # type: ignore[type-var]
                upper_limit(self.join_irreducible, item, strict=True),  # type: ignore[type-var]
            )
            if maximum == item:
                return True
        return False

    def __len__(self: AbstractFiniteJoinSemiLattice[_J]) -> int:
        return sum(1 for _ in self)

    def __iter__(self: AbstractFiniteJoinSemiLattice[_J]) -> Iterator[_J]:
        return (neighbourhood.element for neighbourhood in self.cover.neighbourhoods())

    # Set comparison properties and methods
    def __lt__(
        self: AbstractFiniteJoinSemiLattice[_J],
        other: Collection[_J],
    ) -> bool:
        if isinstance(other, AbstractFiniteJoinSemiLattice):
            return (self.maximum is None and other.maximum is not None) or (
                all(element in other for element in self.join_irreducible)
                and any(element not in self for element in other.join_irreducible)
            )
        return super().__lt__(other)

    def __gt__(
        self: AbstractFiniteJoinSemiLattice[_J],
        other: Collection[_J],
    ) -> bool:
        if isinstance(other, AbstractFiniteJoinSemiLattice):
            return (other.maximum is None and self.maximum is not None) or (
                all(element in self for element in other.join_irreducible)
                and any(element not in other for element in self.join_irreducible)
            )
        return super().__gt__(other)

    def __le__(
        self: AbstractFiniteJoinSemiLattice[_J],
        other: Collection[_J],
    ) -> bool:
        if isinstance(other, AbstractFiniteJoinSemiLattice):
            return all(element in other for element in self.join_irreducible)
        return super().__le__(other)

    def __ge__(
        self: AbstractFiniteJoinSemiLattice[_J],
        other: Collection[_J],
    ) -> bool:
        if isinstance(other, AbstractFiniteJoinSemiLattice):
            return all(element in self for element in other.join_irreducible)
        return super().__ge__(other)

    # AbstractFinitePartiallyOrderedSet properties and methods

    def upper_limit(
        self: AbstractFiniteJoinSemiLattice[_J],
        *limits: _J,
        strict: bool = False,
    ) -> Collection[_J]:
        """
        Get the values less than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_J]
            Values which are less than the limits.

        """

        class UpperLimit:
            """
            It represents the collection of values less than a limit.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in semi_lattice and all(
                        item < limit for limit in limits  # type: ignore[operator]
                    )
                return item in semi_lattice and all(
                    item <= limit for limit in limits  # type: ignore[operator]
                )

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_J]:
                # noinspection PyTypeChecker
                for neighbourhood in generate_neighbourhoods_from_join(
                    FrozenFinitePartiallyOrderedSet[_J](
                        elements=(
                            irreducible
                            for irreducible in semi_lattice.join_irreducible
                            if (
                                (
                                    strict
                                    and all(irreducible < limit for limit in limits)
                                )
                                or (
                                    not strict
                                    and all(irreducible <= limit for limit in limits)
                                )
                            )
                        ),
                    ),
                ):
                    if strict:
                        if all(neighbourhood.element < limit for limit in limits):
                            yield neighbourhood.element
                    elif all(neighbourhood.element <= limit for limit in limits):
                        yield neighbourhood.element

        semi_lattice = self
        return UpperLimit()

    def lower_limit(
        self: AbstractFiniteJoinSemiLattice[_J],
        *limits: _J,
        strict: bool = False,
    ) -> Collection[_J]:
        """
        Get the values greater than the limits.

        Parameters
        ----------
        *limits
            The limits.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_J]
            A collection of values.

        """

        class LowerLimit:
            """
            It represents the collection of values greater than a limit.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            def __contains__(self, item: object) -> bool:
                if strict:
                    return item in semi_lattice and all(
                        item > limit for limit in limits  # type: ignore[operator]
                    )
                return item in semi_lattice and all(
                    item >= limit for limit in limits  # type: ignore[operator]
                )

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_J]:
                # noinspection PyTypeChecker
                for neighbourhood in generate_neighbourhoods_from_join(
                    FrozenFinitePartiallyOrderedSet[_J](
                        elements=semi_lattice.join_irreducible,
                    ),
                ):
                    if strict:
                        if all(neighbourhood.element > limit for limit in limits):
                            yield neighbourhood.element
                    elif all(neighbourhood.element >= limit for limit in limits):
                        yield neighbourhood.element

        semi_lattice = self
        return LowerLimit()

    # AbstractFiniteJoinSemiLattice properties and methods

    def floor(self: AbstractFiniteJoinSemiLattice[_J], *limits: _J) -> _J | None:
        """
        Get the floor of item.

        Parameters
        ----------
        *limits
            Elements whose floor is requested.

        Returns
        -------
        _J | None
            The floor of limits

        """
        irreducible = self.greatest_join_irreducible(*limits)
        if not irreducible:
            return None
        iterator = iter(irreducible)
        element: _J = next(iterator)
        return element.join(*iterator)

    def greatest_join_irreducible(
        self: AbstractFiniteJoinSemiLattice[_J],
        *limits: _J,
        strict: bool = False,
    ) -> Collection[_J]:
        """
        Get the greatest join irreducible smaller than a limit.

        Parameters
        ----------
        *limits
            The limits whose greatest join irreducible are requested.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_J]
            The greatest join irreducible smaller than the limits.

        """

        class GreatestJoinIrreducible(Collection[_J]):
            """
            It represents a subset of the join irreducible.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, "Collection", long=False))

            def __bool__(self) -> bool:
                for _ in self:
                    return True
                return False

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in iter(self)  # type: ignore[operator]

            def __len__(self) -> int:
                return sum(1 for _ in self)

            def __iter__(self) -> Iterator[_J]:
                return top(
                    upper_limit(semi_lattice.join_irreducible, *limits, strict=strict),
                )

        semi_lattice = self
        return GreatestJoinIrreducible()
