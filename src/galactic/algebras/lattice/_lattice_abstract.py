"""The :mod:`_lattice_abstract` module."""

from abc import abstractmethod
from collections.abc import Collection
from typing import Protocol, TypeVar, runtime_checkable

from ._join_semi_lattice_abstract import AbstractFiniteJoinSemiLattice
from ._lattice_element import Element
from ._meet_semi_lattice_abstract import AbstractFiniteMeetSemiLattice

_E = TypeVar("_E", bound=Element)


@runtime_checkable
class AbstractFiniteLattice(
    AbstractFiniteJoinSemiLattice[_E],
    AbstractFiniteMeetSemiLattice[_E],
    Collection[_E],
    Protocol,
):
    """
    It represents finite lattices.
    """

    @abstractmethod
    def interval(self, *limits: _E) -> tuple[_E | None, _E | None]:
        """
        Get the interval of limits.

        Parameters
        ----------
        *limits
            The limits whose interval is requested

        Returns
        -------
        tuple[_E | None, _E | None]
            The interval of the limits.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
