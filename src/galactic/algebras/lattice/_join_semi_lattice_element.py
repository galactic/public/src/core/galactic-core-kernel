"""The :mod:`_join_semi_lattice_element` module."""

from abc import abstractmethod
from collections.abc import Iterable, Iterator
from typing import Protocol, TypeVar, runtime_checkable

from typing_extensions import Self

from galactic.algebras.poset import PartiallyComparable, upper_limit


@runtime_checkable
class Joinable(PartiallyComparable, Protocol):
    r"""
    It represents *joinable* elements.

    This class sets for each pair of elements a, b a unique supremum
    :math:`c = a \vee b` (also called a least upper bound or join).

    Example
    -------
    Let the integers ordered by the relation :math:`a \leq b`: is :math:`a` a
    divisor of :math:`b`?

    >>> from galactic.algebras.examples.arithmetic import PrimeFactors
    >>> PrimeFactors(24) | PrimeFactors(36)
    PrimeFactors(72)
    >>> PrimeFactors(24).join(PrimeFactors(36), PrimeFactors(10))
    PrimeFactors(360)

    """

    __slots__ = ()

    @abstractmethod
    def __or__(self, other: Self) -> Self:
        """
        Return the join of this element and the other.

        Parameters
        ----------
        other
            the other element

        Returns
        -------
        Self
            The join of this element and the other

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    def join(self, *others: Self) -> Self:
        """
        Return the supremum of this element and the others.

        Parameters
        ----------
        *others
            The others elements

        Returns
        -------
        Self
            The join of this element and the others

        """
        result = self
        for value in others:
            result = result | value
        return result

    def join_level(self, generators: Iterable[Self]) -> int:
        """
        Compute the level of an element considering an iterable of generators.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The level of the element.

        """
        return sum(1 for generator in generators if generator <= self)

    def join_priority(self, generators: Iterable[Self]) -> int:
        """
        Compute the priority of an element considering an iterable of generators.

        Parameters
        ----------
        generators
            An iterable of generators.

        Returns
        -------
        int
            The priority.

        """
        return self.join_level(generators)


_J = TypeVar("_J", bound=Joinable)


def supremum(iterable: Iterable[_J]) -> _J:
    """
    Compute the supremum of joinable elements.

    Parameters
    ----------
    iterable
        An iterable collection of joinable elements.

    Returns
    -------
    _J
        The supremum of all elements from the iterable collection.

    Raises
    ------
    ValueError
        If the iterable is empty.

    """
    iterator = iter(iterable)
    try:
        element = next(iterator)
    except StopIteration as error:
        raise ValueError("supremum expected a non-empty argument") from error
    return element.join(*list(iterator))


def supremum_generators(iterable: Iterable[_J]) -> Iterator[_J]:
    """
    Produce the supremum generators from the iterable of joinable elements.

    Parameters
    ----------
    iterable
        The iterable collection of elements.

    Yields
    ------
    _J
        A supremum generator.

    """
    for element in iterable:
        try:
            join = supremum(upper_limit(iterable, element, strict=True))
            if join < element:
                yield element
        except ValueError:  # noqa: PERF203
            yield element
