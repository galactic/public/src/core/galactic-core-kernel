"""The :mod:`_join_semi_lattice_abstract` module."""

from abc import abstractmethod
from collections.abc import Collection
from typing import Protocol, TypeVar, runtime_checkable

from galactic.algebras.poset import AbstractFinitePartiallyOrderedSet

from ._join_semi_lattice_element import Joinable

_J = TypeVar("_J", bound=Joinable)


# pylint: disable=too-many-ancestors
@runtime_checkable
class AbstractFiniteJoinSemiLattice(
    AbstractFinitePartiallyOrderedSet[_J],
    Collection[_J],
    Protocol,
):
    """
    It describes finite join semi-lattices.
    """

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    @abstractmethod
    def co_atoms(self) -> Collection[_J]:
        """
        Get the co-atoms of this join semi-lattice.

        Returns
        -------
        Collection[_J]
            The co-atoms.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_J]:
        """
        Get the join irreducible elements.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_J]
            The join irreducible elements.

        """
        raise NotImplementedError

    @abstractmethod
    def floor(self, *limits: _J) -> _J | None:
        """
        Get the floor of item.

        Parameters
        ----------
        *limits
            Elements whose floor is requested.

        Returns
        -------
        _J | None
            The floor of limits

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def greatest_join_irreducible(
        self,
        *limits: _J,
        strict: bool = False,
    ) -> Collection[_J]:
        """
        Get the greatest join irreducible smaller than the limits.

        Parameters
        ----------
        *limits
            The limits whose greatest join irreducible are requested.
        strict
            Is the comparison strict?

        Returns
        -------
        Collection[_J]
            The greatest join irreducible smaller than the limits.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
