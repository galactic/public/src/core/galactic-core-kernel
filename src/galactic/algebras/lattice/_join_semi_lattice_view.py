"""The :mod:`_join_semi_lattice_view` module."""

from __future__ import annotations

from typing import TYPE_CHECKING, TypeVar, cast

from galactic.algebras.poset import (
    AbstractFiniteCoveringRelation,
    AbstractFinitePartiallyOrderedSet,
    AbstractFinitePartialOrder,
    Bottom,
    FinitePartiallyOrderedSetView,
    FinitePartialOrder,
    MutableFinitePartiallyOrderedSet,
    Top,
)
from galactic.helpers.core import default_repr

from ._join_semi_lattice_abstract import AbstractFiniteJoinSemiLattice
from ._join_semi_lattice_element import Joinable
from ._join_semi_lattice_mixins import (
    CoAtoms,
    FiniteJoinSemiLatticeMixin,
    JoinSemiLatticeCoveringRelation,
)

if TYPE_CHECKING:
    from collections.abc import Collection

_J = TypeVar("_J", bound=Joinable)


# pylint: disable=too-many-instance-attributes
class FiniteJoinSemiLatticeView(FiniteJoinSemiLatticeMixin[_J]):
    """
    It represents bounded finite join semi-lattice.

    Parameters
    ----------
    semi_lattice
        The inner semi-lattice.
    lower
        The lower bound.
    upper
        The upper bound.

    """

    __slots__ = (
        "_semi_lattice",
        "_version",
        "_elements",
        "_irreducible",
        "_length",
        "_cover",
        "_order",
        "_lower",
        "_upper",
        "_co_atoms",
        "_top",
        "_bottom",
    )

    _semi_lattice: AbstractFiniteJoinSemiLattice[_J]
    _version: int
    _elements: MutableFinitePartiallyOrderedSet[_J]
    _irreducible: AbstractFinitePartiallyOrderedSet[_J]
    _length: int | None
    _cover: AbstractFiniteCoveringRelation[_J]
    _order: AbstractFinitePartialOrder[_J]
    _lower: _J | None
    _upper: _J | None
    _co_atoms: Collection[_J]
    _top: Collection[_J]
    _bottom: Collection[_J]

    def __init__(
        self,
        semi_lattice: AbstractFiniteJoinSemiLattice[_J],
        lower: _J | None = None,
        upper: _J | None = None,
    ) -> None:
        self._semi_lattice = semi_lattice
        if upper is lower is None:
            self._version = semi_lattice.version
            self._elements = semi_lattice.join_irreducible  # type: ignore[assignment]
            self._bottom = semi_lattice.bottom
            self._top = semi_lattice.top
            self._co_atoms = semi_lattice.co_atoms
        else:
            self._version = 0
            self._elements = MutableFinitePartiallyOrderedSet[_J]()
            self._bottom = Bottom(semi_lattice, lower, upper)
            self._top = Top(semi_lattice, lower, upper)
            self._co_atoms = CoAtoms[_J](self)
        # noinspection PyTypeChecker
        self._irreducible = FinitePartiallyOrderedSetView[_J](self._elements)
        self._length = None
        self._cover = JoinSemiLatticeCoveringRelation[_J](self)  # type: ignore[assignment]
        self._order = FinitePartialOrder[_J](self)
        self._lower = lower
        self._upper = upper

    @property
    def version(self) -> int:
        """
        Get the version number.

        Returns
        -------
        int
            The version number.

        Notes
        -----
        This can be used to detect a change in the semi-lattice.

        """
        return self._semi_lattice.version

    def __repr__(self) -> str:
        if self._lower is None:
            if self._upper is None:
                class_name = "JoinSemiLattice"
            else:
                class_name = "UpperBoundedJoinSemiLattice"
        elif self._upper is None:
            class_name = "LowerBoundedJoinSemiLattice"
        else:
            class_name = "BoundedJoinSemiLattice"
        return cast(str, default_repr(self, class_name))

    def __len__(self) -> int:
        if self._length is None or self._version != self._semi_lattice.version:
            self._length = super().__len__()
        return self._length

    # AbstractFinitePartiallyOrderedSet properties and methods

    @property
    def order(self) -> AbstractFinitePartialOrder[_J]:
        """
        Get the partial order of this join semi-lattice.

        Returns
        -------
        AbstractFinitePartialOrder[_J]
            The partial order.

        """
        return self._order

    @property
    def cover(self) -> AbstractFiniteCoveringRelation[_J]:
        """
        Get the covering relation of this join semi-lattice.

        Returns
        -------
        AbstractFiniteCoveringRelation[_J]
            The covering relation.

        """
        return self._cover

    @property
    def top(self) -> Collection[_J]:
        """
        Get the top elements.

        The collection contains only one element, the maximum element.

        Returns
        -------
        Collection[_J]
            The top elements.

        """
        return self._top

    @property
    def bottom(self) -> Collection[_J]:
        """
        Get the bottom elements.

        Returns
        -------
        Collection[_J]
            The bottom elements.

        """
        return self._bottom

    def filter(
        self,
        element: _J,
    ) -> AbstractFiniteJoinSemiLattice[_J]:
        """
        Get a filter of a join semi-lattice.

        Parameters
        ----------
        element
            The lower limit.

        Returns
        -------
        AbstractFiniteJoinSemiLattice[_J]
            A view on the bounded semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the join semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to the semi-lattice")
        # noinspection PyTypeChecker
        return FiniteJoinSemiLatticeView[_J](
            self._semi_lattice,
            lower=element,
            upper=self._upper,
        )

    def ideal(
        self,
        element: _J,
    ) -> AbstractFiniteJoinSemiLattice[_J]:
        """
        Get an ideal of a join semi-lattice.

        Parameters
        ----------
        element
            The upper limit.

        Returns
        -------
        AbstractFiniteJoinSemiLattice[_J]
            A view on the bounded semi-lattice.

        Raises
        ------
        ValueError
            If the element does not belong to the join semi-lattice.

        """
        if element not in self:
            raise ValueError(f"{element} does not belong to this semi-lattice")
        # noinspection PyTypeChecker
        return FiniteJoinSemiLatticeView[_J](
            self._semi_lattice,
            lower=self._lower,
            upper=element,
        )

    # AbstractFiniteJoinSemiLattice properties and methods

    @property
    def co_atoms(self) -> Collection[_J]:
        """
        Get the co-atoms of this join semi-lattice.

        Returns
        -------
        Collection[_J]
            The co-atoms.

        """
        return self._co_atoms

    @property
    def join_irreducible(self) -> AbstractFinitePartiallyOrderedSet[_J]:
        """
        Get the join-irreducible elements of this join semi-lattice.

        Returns
        -------
        AbstractFinitePartiallyOrderedSet[_J]
            The join-irreducible elements.

        """
        if self._version != self._semi_lattice.version and (
            self._lower is not None or self._upper is not None
        ):
            self._elements.clear()
            self._elements.extend(
                irreducible if self._lower is None else irreducible | self._lower
                for irreducible in self._semi_lattice.join_irreducible
                if self._upper is None or irreducible <= self._upper
            )
            self._version = self._semi_lattice.version
        return self._irreducible
