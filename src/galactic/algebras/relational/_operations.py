"""The :mod:`_operations` module."""

from __future__ import annotations

from typing import TYPE_CHECKING, Any

from ._abstract import AbstractFiniteRelation
from ._mixins import FiniteRelationMixin

if TYPE_CHECKING:
    from collections.abc import Collection, Iterator, Mapping, Sequence


class FiniteSelection(FiniteRelationMixin):
    """
    Select some tuple from a relation using criteria.

    Parameters
    ----------
    relation
        An relation.
    criteria
        A set of criterion.

    Examples
    --------
    >>> from galactic.algebras.relational import MutableFiniteRelation, FiniteSelection
    >>> relation = MutableFiniteRelation(
    ...     [1, 2, 3],
    ...     [2, 3, 4],
    ...     [3, 4, 5],
    ...     elements=[(1, 2, 3), (2, 3, 3), (1, 4, 5)]
    ... )
    >>> relation
    <galactic.algebras.relational.MutableFiniteRelation object at 0x...>
    >>> selection = FiniteSelection(relation, {2: 3})
    >>> selection
    <galactic.algebras.relational.FiniteSelection object at 0x...>
    >>> list(selection)
    [(1, 2, 3), (2, 3, 3)]

    """

    __slots__ = (
        "_relation",
        "_criteria",
        "_hash_value",
    )

    _relation: AbstractFiniteRelation
    _criteria: Mapping[int, Any]

    def __init__(
        self,
        relation: AbstractFiniteRelation,
        criteria: Mapping[int, Any],
    ) -> None:
        self._relation = relation
        self._criteria = dict(criteria)

    def __bool__(self) -> bool:
        for _ in self:
            return True
        return False

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            return self is other or (
                self._relation == other._relation and self._criteria == other._criteria
            )
        return super().__eq__(other)

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        return (
            isinstance(item, tuple)
            and item in self._relation
            and all(
                item[index] == criterion for index, criterion in self._criteria.items()
            )
        )

    def __len__(self) -> int:
        return sum(1 for _ in self)

    def __iter__(self) -> Iterator[tuple[Any, ...]]:
        for element in self._relation:
            for index, criterion in self._criteria.items():
                if element[index] != criterion:
                    break
            else:
                yield element

    # AbstractRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[Any], ...]:
        """
        Get the universe of this relation.

        Returns
        -------
        tuple[Collection[Any], ...]
            The universes.

        """
        return self._relation.universes


class FiniteProjection(FiniteRelationMixin):
    """
    Project a relation.

    Parameters
    ----------
    relation
        A relation.
    indexes
        An iterable of indexes.

    Examples
    --------
    >>> from galactic.algebras.relational import MutableFiniteRelation, FiniteProjection
    >>> relation = MutableFiniteRelation(
    ...     [1, 2, 3],
    ...     [2, 3, 4],
    ...     [3, 4, 5],
    ...     elements=[(1, 2, 3), (2, 3, 3), (1, 4, 5)])
    >>> relation
    <galactic.algebras.relational.MutableFiniteRelation object at 0x...>
    >>> projection = FiniteProjection(relation, [1, 2])
    >>> projection
    <galactic.algebras.relational.FiniteProjection object at 0x...>
    >>> list(projection)
    [(2, 3), (3, 3), (4, 5)]

    """

    __slots__ = (
        "_relation",
        "_indexes",
        "_universes",
    )

    _relation: AbstractFiniteRelation
    _indexes: Sequence[int]
    _universes: tuple[Collection[Any], ...]

    def __init__(
        self,
        relation: AbstractFiniteRelation,
        indexes: tuple[int, ...],
    ) -> None:
        self._relation = relation
        self._indexes = indexes
        self._universes = tuple(relation.universes[index] for index in self._indexes)

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            return self is other or (
                self._relation == other._relation and self._indexes == other._indexes
            )
        return super().__eq__(other)

    def __bool__(self) -> bool:
        for _ in self:
            return True
        return False

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        return any(
            tuple(element[index] for index in self._indexes) == item
            for element in self._relation
        )

    def __len__(self) -> int:
        return sum(1 for _ in self)

    def __iter__(self) -> Iterator[tuple[Any, ...]]:
        yielded = set()
        for element in self._relation:
            item = tuple(element[index] for index in self._indexes)
            if item not in yielded:
                yielded.add(item)
                yield item

    # AbstractRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[Any], ...]:
        """
        Get the universe of this relation.

        Returns
        -------
        tuple[Collection[Any], ...]
            The universes.

        """
        return self._universes
