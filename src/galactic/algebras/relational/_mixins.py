"""The :mod:`_mixins` module."""

from __future__ import annotations

import itertools
from abc import abstractmethod
from typing import TYPE_CHECKING, Any, Protocol, cast

from typing_extensions import Self

from galactic.algebras.relational._abstract import AbstractFiniteRelation
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

if TYPE_CHECKING:
    from collections.abc import Iterable


# pylint: disable=abstract-method
class FiniteRelationMixin(AbstractFiniteRelation, Protocol):
    """
    It represents a mixin for set like comparison.
    """

    __slots__ = ()

    def _check(self, other: AbstractFiniteRelation) -> bool:
        if len(self.universes) != len(other.universes):
            return False
        for index, universe in enumerate(self.universes):
            if len(universe) != len(other.universes[index]):
                return False
            for element in universe:
                if element not in other.universes[index]:
                    return False
        return True

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __bool__(self) -> bool:
        for _ in self:
            return True
        return False

    def __eq__(self, other: object) -> bool:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            if len(self) != len(other):
                return False
            return all(element in other for element in self)
        return NotImplemented

    def __lt__(self, other: AbstractFiniteRelation) -> bool:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            if len(self) >= len(other):
                return False
            return all(element in other for element in self)
        return NotImplemented

    def __le__(self, other: AbstractFiniteRelation) -> bool:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            if len(self) > len(other):
                return False
            return all(element in other for element in self)
        return NotImplemented

    def __gt__(self, other: AbstractFiniteRelation) -> bool:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            if len(self) <= len(other):
                return False
            return all(element in self for element in other)
        return NotImplemented

    def __ge__(self, other: AbstractFiniteRelation) -> bool:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            if len(self) < len(other):
                return False
            return all(element in self for element in other)
        return NotImplemented

    def isdisjoint(self, other: Iterable[tuple[Any, ...]]) -> bool:
        """
        Test if the relation is disjoint from the other.

        Parameters
        ----------
        other
            An iterable of ntuple.

        Returns
        -------
        bool
            True if the relation is disjoint from the other.

        """
        return all(element not in self for element in other)

    def issubset(self, other: Iterable[tuple[Any, ...]]) -> bool:
        """
        Test whether every element in the relation is in other.

        Parameters
        ----------
        other
            An iterable of ntuple.

        Returns
        -------
        bool
            True if the relation is a subset of the other.

        """
        data = set(self)
        data.difference_update(other)
        return not data

    def issuperset(self, other: Iterable[tuple[Any, ...]]) -> bool:
        """
        Test whether every element in the other relation is in the relation.

        Parameters
        ----------
        other
            An iterable of ntuple.

        Returns
        -------
        bool
            True if the relation is a superset of the other.

        """
        return all(element in self for element in other)


class ConcreteFiniteRelationMixin(FiniteRelationMixin, Protocol):
    """
    It represents a mixin for set like operations.

    Parameters
    ----------
    *universes
        The universes.
    elements
        The initial tuples.

    Raises
    ------
    NotImplementedError

    """

    __slots__ = ()

    # noinspection PyUnusedLocal
    @abstractmethod
    def __init__(
        self,
        *universes: Iterable[Any],
        elements: Iterable[tuple[Any, ...]] | None = None,
    ) -> None:
        raise NotImplementedError

    @abstractmethod
    def _add(self, element: tuple[Any, ...]) -> None:
        raise NotImplementedError

    @abstractmethod
    def _remove(self, element: tuple[Any, ...]) -> None:
        raise NotImplementedError

    @abstractmethod
    def _discard(self, element: tuple[Any, ...]) -> None:
        raise NotImplementedError

    @abstractmethod
    def _pop(self) -> tuple[Any, ...]:
        raise NotImplementedError

    @abstractmethod
    def _clear(self) -> None:
        raise NotImplementedError

    def _extend(self, iterable: Iterable[tuple[Any, ...]]) -> None:
        for element in iterable:
            self._add(element)

    def __copy__(self) -> Self:
        return self.__class__(*self.universes, elements=iter(self))

    def copy(self) -> Self:
        """
        Compute a copy of this relation.

        Returns
        -------
        Self
            A copy of this relation.

        """
        return self.__class__(*self.universes, elements=iter(self))

    # Set properties and methods

    def __or__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            return self.__class__(
                *self.universes,
                elements=itertools.chain(self, other),
            )
        return NotImplemented

    def __and__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            return self.__class__(
                *self.universes,
                elements=(element for element in self if element in other),
            )
        return NotImplemented

    def __sub__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            return self.__class__(
                *self.universes,
                elements=(element for element in self if element not in other),
            )
        return NotImplemented

    def __xor__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            return self.__class__(
                *self.universes,
                elements=itertools.chain(
                    (element for element in self if element not in other),
                    (element for element in other if element not in self),
                ),
            )
        return NotImplemented

    def union(self, *others: Iterable[tuple[Any, ...]]) -> Self:
        """
        Compute the union of the relation and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The union of the relation and the others.

        """
        result: Self = self.copy()
        for other in others:
            for element in other:
                result._add(element)  # pylint: disable=protected-access
        return result

    def intersection(self, *others: Iterable[tuple[Any, ...]]) -> Self:
        """
        Compute the intersection between the relation and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The intersection between the relation and the others

        """
        elements = FIFOSet[tuple[Any, ...]](self)
        for other in others:
            data = FIFOSet[tuple[Any, ...]]()
            for element in other:
                if element in elements:
                    data.add(element)
            elements = data
        return self.__class__(*self.universes, elements=elements)

    def difference(self, *others: Iterable[tuple[Any, ...]]) -> Self:
        """
        Compute the difference between the relation and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The difference between the relation and the others

        """
        result: Self = self.copy()
        for other in others:
            for element in other:
                result._discard(element)  # pylint: disable=protected-access
        return result

    def symmetric_difference(self, other: Iterable[tuple[Any, ...]]) -> Self:
        """
        Compute the symmetric difference between the relation and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        Self
            The symmetric difference between the relation and the other.

        """
        result: Self = self.copy()
        for element in other:
            if element in result:
                result._remove(element)  # pylint: disable=protected-access
            else:
                result._add(element)  # pylint: disable=protected-access
        return result


class MutableFiniteRelationMixin(ConcreteFiniteRelationMixin, Protocol):
    """
    It represents a mixin for mutable sets.
    """

    __slots__ = ()

    # Mutable collection properties and methods

    def add(self, element: tuple[Any, ...]) -> None:
        """
        Add a element to the relation.

        Parameters
        ----------
        element
            The element to add.

        """
        self._add(element)

    def remove(self, element: tuple[Any, ...]) -> None:
        """
        Remove element from the relation if it is present.

        Parameters
        ----------
        element
            The value to remove.

        """
        self._remove(element)

    def discard(self, element: tuple[Any, ...]) -> None:
        """
        Discard a tuple from the relation.

        Parameters
        ----------
        element
            The element to discard.

        """
        self._discard(element)

    def pop(self) -> tuple[Any, ...]:
        """
        Remove and return an arbitrary element from the relation.

        Returns
        -------
        tuple[Any, ...]
            An arbitrary element from the relation.

        """
        return self._pop()

    def clear(self) -> None:
        """
        Clear the relation.
        """
        self._clear()

    def extend(self, iterable: Iterable[tuple[Any, ...]]) -> None:
        """
        Extend the relation.

        Parameters
        ----------
        iterable
            An iterable of values

        """
        self._extend(iterable)

    # Mutable set properties and methods

    def __ior__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            self.update(other)
            return self
        return NotImplemented

    def __iand__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            result = FIFOSet[tuple[Any, ...]](iter(self))
            for element in result:
                if element not in other:
                    self.remove(element)
            return self
        return NotImplemented

    def __isub__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            for element in other:
                self.discard(element)
            return self
        return NotImplemented

    def __ixor__(self, other: AbstractFiniteRelation) -> Self:
        if isinstance(other, AbstractFiniteRelation) and self._check(other):
            for element in other:
                if element in self:
                    self.remove(element)
                else:
                    self.add(element)
            return self
        return NotImplemented

    def update(self, *others: Iterable[tuple[Any, ...]]) -> None:
        """
        Update a relation with the union of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            for element in other:
                self.add(element)

    def intersection_update(self, *others: Iterable[Any]) -> None:
        """
        Update a relation with the intersection of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            data = FIFOSet[tuple[Any, ...]]()
            for element in other:
                if element in self:
                    data.add(element)
            self.clear()
            self.update(data)

    def difference_update(self, *others: Iterable[Any]) -> None:
        """
        Update a relation with the difference of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            for element in other:
                self.discard(element)

    def symmetric_difference_update(self, other: Iterable[tuple[Any, ...]]) -> None:
        """
        Update a relation with the symmetric difference of itself and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        """
        for element in other:
            if element in self:
                self.remove(element)
            else:
                self.add(element)
