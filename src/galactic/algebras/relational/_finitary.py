"""The :mod:`_finitary` module."""

# pylint: disable=protected-access

from __future__ import annotations

from collections.abc import (
    Collection,
    Iterable,
    Iterator,
)
from collections.abc import (
    Set as AbstractSet,
)
from itertools import starmap
from typing import Any, cast

from galactic.algebras.collection import MutableCollection
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._mixins import ConcreteFiniteRelationMixin, MutableFiniteRelationMixin


class FrozenFiniteRelation(ConcreteFiniteRelationMixin):
    """
    It represents concrete finite relation.

    Parameters
    ----------
    *universes
        The universes.
    elements
        An iterable of tuple.

    Raises
    ------
    ValueError
        If a tuple element is not of the correct size.
    ValueError
        If a value is not in the corresponding universe.

    """

    __slots__ = ("_universes", "_elements", "_hash_value")

    _universes: tuple[Collection[Any], ...]
    _elements: FIFOSet[tuple[Any, ...]]
    _hash_value: int | None

    # noinspection PyProtocol
    def __init__(
        self,
        *universes: Iterable[Any],
        elements: Iterable[tuple[Any, ...]] | None = None,
    ) -> None:
        self._universes = self._create_universes(*universes)
        self._elements = FIFOSet[tuple[Any, ...]]()

        if elements is not None:
            for element in elements:
                if len(element) != len(self._universes):
                    raise ValueError(f"{element} is not of the correct size")
                for index, value in enumerate(element):
                    if value not in self._universes[index]:
                        raise ValueError(f"{value} is not in universe {index}")
                self._elements.add(element)
        self._hash_value = None

    def _create_universes(
        self,
        *universes: Iterable[Any],
    ) -> tuple[Collection[Any], ...]:
        class FiniteUniverse:
            """
            It represents universes of a relation.

            Parameters
            ----------
            values
                An iterable of objects

            """

            __slots__ = ("_elements",)

            _elements: FIFOSet[Any]

            def __init__(self, values: Iterable[Any]) -> None:
                self._elements = FIFOSet[Any](values)

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in self._elements

            def __len__(self) -> int:
                return len(self._elements)

            def __iter__(self) -> Iterator[Any]:
                return iter(self._elements)

        return tuple(FiniteUniverse(universe) for universe in universes)

    # noinspection PyProtectedMember
    def __hash__(self) -> int:
        if self._hash_value is None:
            self._hash_value = AbstractSet._hash(self)  # type: ignore[arg-type]
            for universe in self._universes:
                self._hash_value ^= AbstractSet._hash(universe)  # type: ignore[arg-type]
        return self._hash_value

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        return item in self._elements

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[tuple[Any, ...]]:
        return iter(self._elements)

    def _add(self, element: tuple[Any, ...]) -> None:
        for index, value in enumerate(element):
            if value not in self._universes[index]:
                raise ValueError(f"{value} is not in universe {index}")
        self._elements.add(element)

    def _remove(self, element: tuple[Any, ...]) -> None:
        self._elements.remove(element)

    def _discard(self, element: tuple[Any, ...]) -> None:
        self._elements.discard(element)

    def _pop(self) -> tuple[Any, ...]:
        return self._elements.pop()

    def _clear(self) -> None:
        self._elements.clear()

    # AbstractRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[Any], ...]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Collection[Any], ...]
            The universes of this relation.

        """
        return self._universes


# pylint: disable=too-many-ancestors
class MutableFiniteRelation(FrozenFiniteRelation, MutableFiniteRelationMixin):
    """
    It represents concrete finite relation.

    A relation is a subset of the Cartesian product between domains.

    Parameters
    ----------
    *universes
        The universes.
    elements
        The initiaples tuples.

    Examples
    --------
    >>> from galactic.algebras.relational import MutableFiniteRelation
    >>> relation = MutableFiniteRelation(
    ...     [1, 2, 3],
    ...     [2, 3, 4],
    ...     [3, 4, 5],
    ...     elements=[(1, 2, 3), (2, 3, 4), (3, 4, 5)]
    ... )
    >>> relation
    <galactic.algebras.relational.MutableFiniteRelation object at 0x...>
    >>> list(relation)
    [(1, 2, 3), (2, 3, 4), (3, 4, 5)]
    >>> relation.discard((2, 3, 4))
    >>> list(relation)
    [(1, 2, 3), (3, 4, 5)]
    >>> list(relation.universes[0])
    [1, 2, 3]
    >>> list(relation.universes[1])
    [2, 3, 4]
    >>> list(relation.universes[2])
    [3, 4, 5]

    """

    __slots__ = ()

    # noinspection PyProtocol
    def __init__(
        self,
        *universes: Iterable[Any],
        elements: Iterable[tuple[Any, ...]] | None = None,
    ) -> None:
        if elements is None:
            super().__init__(*universes, elements=[])
        else:
            super().__init__(*universes, elements=elements)

    __hash__ = None  # type: ignore[assignment]

    def _create_universes(
        self,
        *universes: Iterable[Any],
    ) -> tuple[MutableCollection[Any], ...]:
        class MutableUniverse:
            """
            It represents universes of a relation.

            Parameters
            ----------
            index
                The universe index.
            values
                An iterable of objects

            """

            __slots__ = ("_elements", "_index")

            _elements: FIFOSet[Any]
            _index: int

            def __init__(self, index: int, values: Iterable[Any]) -> None:
                self._index = index
                self._elements = FIFOSet[Any](values)

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                return item in self._elements

            def __len__(self) -> int:
                return len(self._elements)

            def __iter__(self) -> Iterator[Any]:
                return iter(self._elements)

            def add(self, value: object) -> None:
                """
                Add a value to the collection.

                Parameters
                ----------
                value
                    The value to add.

                """
                self._elements.add(value)

            def remove(self, value: object) -> None:
                """
                Remove a value from the collection.

                This method can raise a KeyError if the value does not belong to the
                collection.

                Parameters
                ----------
                value
                    The value to remove.

                """
                self._elements.remove(value)
                relation._elements = FIFOSet[tuple[Any, ...]](
                    ntuple
                    for ntuple in relation._elements
                    if ntuple[self._index] != value
                )

            def discard(self, value: object) -> None:
                """
                Discard a value from the collection.

                Parameters
                ----------
                value
                    The value to discard.

                """
                self._elements.discard(value)
                relation._elements = FIFOSet[tuple[Any, ...]](
                    ntuple
                    for ntuple in relation._elements
                    if ntuple[self._index] != value
                )

            def pop(self) -> object:
                """
                Remove and return an arbitrary element from the collection.

                This method can raise a KeyError if the collection is empty.

                Returns
                -------
                object
                    The value removed.

                """
                value = self._elements.pop()
                relation._elements = FIFOSet[tuple[Any, ...]](
                    ntuple
                    for ntuple in relation._elements
                    if ntuple[self._index] != value
                )
                return value

            def clear(self) -> None:
                """
                Clear the collection.
                """
                self._elements.clear()
                relation._elements.clear()

            def extend(self, iterable: Iterable[Any]) -> None:
                """
                Extend the collection by adding the elements of the iterable.

                Parameters
                ----------
                iterable
                    An iterable of values

                """
                for value in iterable:
                    self.add(value)

        relation = self
        return tuple(starmap(MutableUniverse, enumerate(universes)))

    @property
    def universes(self) -> tuple[MutableCollection[Any], ...]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[MutableCollection[Any], ...]
            The universes of this relation.

        """
        return cast(tuple[MutableCollection[Any], ...], self._universes)
