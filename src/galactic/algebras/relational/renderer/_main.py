"""The :mod:`_main` module."""

# pylint: disable=unused-argument

from __future__ import annotations

from typing import TYPE_CHECKING, Generic, TypeVar

try:
    from graphviz import Digraph
except ImportError as error:
    raise ImportError("Install galactic-core-kernel with extra 'docs'") from error

from galactic.algebras.relational import (
    AbstractFiniteBinaryRelation,
    AbstractFiniteEndoRelation,
)

if TYPE_CHECKING:
    from collections.abc import Collection

_E = TypeVar("_E")
_F = TypeVar("_F")


# pylint: disable=too-few-public-methods
class NodeRenderer(Generic[_E, _F]):
    """
    It renders graphviz nodes.

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = ()

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def attributes(
        self,
        element: _E,
        index: int | None = None,
        current: bool = False,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_F] | None = None,
    ) -> dict[str, str]:
        """
        Get the dictionary of graphviz attributes for a node.

        Parameters
        ----------
        element
            The element to render.
        index
            The element index.
        current
            Is it the current element?
        successors
            The successors of the element.
        predecessors
            The predecessors of the element.

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes.

        """
        return {"shape": "circle", "label": str(element)}


class EdgeRenderer(Generic[_E, _F]):
    """
    It renders edge attributes.

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = ()

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def attributes(
        self,
        source: _E,
        destination: _F,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_E] | None = None,
    ) -> dict[str, str]:
        """
        Get the dictionary of graphviz attributes for an edge.

        Parameters
        ----------
        source
            The edge source
        destination
            The edge destination
        successors
            The successors of source (including current destination).
        predecessors
            The predecessors of destination (including current source).

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes

        """
        return {}


class GraphRenderer(Generic[_E, _F]):
    """
    It renders graph attributes.

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = ()

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def add_source(
        self,
        element: _E,
        index: int | None = None,
        current: bool = False,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_F] | None = None,
    ) -> None:
        """
        Add a source to the graph.

        Parameters
        ----------
        element
            The source to add.
        index
            The source index.
        current
            Is it the current element?
        successors
            The successors of the source.
        predecessors
            The predecessors of the source.

        """

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def add_destination(
        self,
        element: _F,
        index: int | None = None,
        current: bool = False,
        successors: Collection[_E] | None = None,
        predecessors: Collection[_E] | None = None,
    ) -> None:
        """
        Add a destination to the graph.

        Parameters
        ----------
        element
            The destination to add.
        index
            The destination index.
        current
            Is it the current element?
        successors
            The successors of the destination.
        predecessors
            The predecessors of the destination.

        """

    def add_edge(
        self,
        source: _E,
        destination: _F,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_E] | None = None,
    ) -> None:
        """
        Add an edge to the graph.

        Parameters
        ----------
        source
            The edge source
        destination
            The edge destination
        successors
            The successors of source (including current destination).
        predecessors
            The predecessors of destination (including current source).

        """

    def attributes(self) -> dict[str, str]:
        """
        Get the graphviz attributes for the graph.

        Returns
        -------
            A dictionary of graphviz attributes

        """
        return {}


class SagittalDiagramRenderer(GraphRenderer[_E, _F], Generic[_E, _F]):
    """
    It renders sagittal diagram attributes.

    Notes
    -----
    See http://www.graphviz.org/doc/info/attrs.html.

    """

    __slots__ = ()

    def attributes(self) -> dict[str, str]:
        """
        Get the graphviz attributes for the graph.

        Returns
        -------
            A dictionary of graphviz attributes for the graph

        """
        return {"rankdir": "LR"}


# pylint: disable=too-many-instance-attributes
class SagittalDiagram(Generic[_E, _F]):
    """
    It is used for drawing Sagittal diagrams.

    It is useful for visualizing binary relations in jupyter notebooks.

    Parameters
    ----------
    relation
        The binary relation.
    graph_renderer
        The graph renderer.
    domain_renderer
        The domain renderer.
    co_domain_renderer
        The co-domain renderer.
    edge_renderer
        The co-domain renderer.

    """

    __slots__ = (
        "_relation",
        "_graph_renderer",
        "_domain_renderer",
        "_co_domain_renderer",
        "_edge_renderer",
    )

    _relation: AbstractFiniteBinaryRelation[_E, _F]
    _graph_renderer: SagittalDiagramRenderer[_E, _F]
    _domain_renderer: NodeRenderer[_E, _F]
    _co_domain_renderer: NodeRenderer[_F, _E]
    _edge_renderer: EdgeRenderer[_E, _F]

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def __init__(
        self,
        relation: AbstractFiniteBinaryRelation[_E, _F],
        graph_renderer: SagittalDiagramRenderer[_E, _F] | None = None,
        domain_renderer: NodeRenderer[_E, _F] | None = None,
        co_domain_renderer: NodeRenderer[_F, _E] | None = None,
        edge_renderer: EdgeRenderer[_E, _F] | None = None,
    ) -> None:
        if graph_renderer is None:
            graph_renderer = SagittalDiagramRenderer[_E, _F]()
        if domain_renderer is None:
            domain_renderer = NodeRenderer[_E, _F]()
        if co_domain_renderer is None:
            co_domain_renderer = NodeRenderer[_F, _E]()
        if edge_renderer is None:
            edge_renderer = EdgeRenderer[_E, _F]()
        self._relation = relation
        self._graph_renderer = graph_renderer
        self._domain_renderer = domain_renderer
        self._co_domain_renderer = co_domain_renderer
        self._edge_renderer = edge_renderer

    @property
    def source(self) -> str:
        """
        Get the graphviz source for this partially ordered set.

        Returns
        -------
            The graphviz output for this partially ordered set.

        """
        return self._compute_digraph().source  # type: ignore[no-any-return,attr-defined]

    @property
    def relation(self) -> AbstractFiniteBinaryRelation[_E, _F]:
        """
        Get the relation.

        Returns
        -------
            The underlying relation.

        """
        return self._relation

    @property
    def graph_renderer(self) -> GraphRenderer[_E, _F]:
        """
        Get the graph renderer.

        Returns
        -------
            The graph renderer.

        """
        return self._graph_renderer

    @property
    def domain_renderer(self) -> NodeRenderer[_E, _F]:
        """
        Get the domain renderer.

        Returns
        -------
            The domain renderer.

        """
        return self._domain_renderer

    @property
    def co_domain_renderer(self) -> NodeRenderer[_F, _E]:
        """
        Get the co-domain renderer.

        Returns
        -------
            The co-domain renderer.

        """
        return self._co_domain_renderer

    @property
    def edge_renderer(self) -> EdgeRenderer[_E, _F]:
        """
        Get the edge renderer.

        Returns
        -------
            The edge renderer.

        """
        return self._edge_renderer

    def _repr_png_(self) -> bytes:
        return self._compute_digraph().pipe("png", quiet=True)  # type: ignore[no-any-return,attr-defined]

    # pylint: disable=protected-access,too-many-locals
    def _compute_digraph(self) -> object:
        # pylint: disable=import-outside-toplevel,import-error

        digraph = Digraph()
        sources = Digraph(graph_attr={"rank": "same"})
        sinks = Digraph(graph_attr={"rank": "same"})

        domain_labels: dict[_E, int] = {}
        co_domain_labels: dict[_F, int] = {}

        for index, element1 in enumerate(self._relation.domain):
            domain_labels[element1] = index
            element1_successors = self._relation.successors(element1)
            element1_predecessors: Collection[_F] = set()
            self._graph_renderer.add_source(
                element1,
                index=index,
                successors=element1_successors,
                predecessors=element1_predecessors,
            )
            sources.node(
                f"D0_{index}",
                **self._domain_renderer.attributes(
                    element=element1,
                    index=index,
                    successors=element1_successors,
                    predecessors=element1_predecessors,
                ),
            )
        digraph.subgraph(sources)

        for index, element2 in enumerate(self._relation.co_domain):
            co_domain_labels[element2] = index
            element2_predecessors = self._relation.predecessors(element2)
            element2_successors: Collection[_E] = set()
            self._graph_renderer.add_destination(
                element2,
                index=index,
                successors=element2_successors,
                predecessors=element2_predecessors,
            )
            sinks.node(
                f"D1_{index}",
                **self._co_domain_renderer.attributes(
                    element=element2,
                    index=index,
                    successors=element2_successors,
                    predecessors=element2_predecessors,
                ),
            )
        digraph.subgraph(sinks)

        for source, destination in self._relation:
            successors = self._relation.successors(source)
            predecessors = self._relation.predecessors(destination)
            self._graph_renderer.add_edge(
                source=source,
                destination=destination,
                successors=successors,
                predecessors=predecessors,
            )
            digraph.edge(
                f"D0_{domain_labels[source]}",
                f"D1_{co_domain_labels[destination]}",
                **self._edge_renderer.attributes(
                    source=source,
                    destination=destination,
                    successors=successors,
                    predecessors=predecessors,
                ),
            )
        digraph.graph_attr = self._graph_renderer.attributes()
        return digraph


class EndoDiagram(Generic[_E]):
    """
    It is used for drawing endo diagrams.

    It is useful for visualizing endo-relations in jupyter notebooks.

    Parameters
    ----------
    relation
        The binary relation.
    graph_renderer
        The graph renderer.
    domain_renderer
        The domain renderer.
    edge_renderer
        The co-domain renderer.

    """

    __slots__ = (
        "_relation",
        "_graph_renderer",
        "_domain_renderer",
        "_edge_renderer",
    )

    _relation: AbstractFiniteEndoRelation[_E]
    _graph_renderer: GraphRenderer[_E, _E]
    _domain_renderer: NodeRenderer[_E, _E]
    _edge_renderer: EdgeRenderer[_E, _E]

    # pylint: disable=too-many-arguments
    def __init__(
        self,
        relation: AbstractFiniteEndoRelation[_E],
        graph_renderer: GraphRenderer[_E, _E] | None = None,
        domain_renderer: NodeRenderer[_E, _E] | None = None,
        edge_renderer: EdgeRenderer[_E, _E] | None = None,
    ) -> None:
        if graph_renderer is None:
            graph_renderer = GraphRenderer[_E, _E]()
        if domain_renderer is None:
            domain_renderer = NodeRenderer[_E, _E]()
        if edge_renderer is None:
            edge_renderer = EdgeRenderer[_E, _E]()
        self._relation = relation
        self._graph_renderer = graph_renderer
        self._domain_renderer = domain_renderer
        self._edge_renderer = edge_renderer

    @property
    def source(self) -> str:
        """
        Get the graphviz source for this partially ordered set.

        Returns
        -------
            The graphviz output for this partially ordered set.

        """
        return self._compute_digraph().source  # type: ignore[no-any-return,attr-defined]

    @property
    def relation(self) -> AbstractFiniteEndoRelation[_E]:
        """
        Get the relation.

        Returns
        -------
            The underlying relation.

        """
        return self._relation

    @property
    def graph_renderer(self) -> GraphRenderer[_E, _E]:
        """
        Get the graph renderer.

        Returns
        -------
            The graph renderer.

        """
        return self._graph_renderer

    @property
    def domain_renderer(self) -> NodeRenderer[_E, _E]:
        """
        Get the domain renderer.

        Returns
        -------
            The domain renderer.

        """
        return self._domain_renderer

    @property
    def edge_renderer(self) -> EdgeRenderer[_E, _E]:
        """
        Get the edge renderer.

        Returns
        -------
            The edge renderer.

        """
        return self._edge_renderer

    def _repr_png_(self) -> bytes:
        return self._compute_digraph().pipe("png", quiet=True)  # type: ignore[no-any-return,attr-defined]

    # pylint: disable=protected-access,too-many-locals
    def _compute_digraph(self) -> object:
        # pylint: disable=import-outside-toplevel,import-error

        digraph = Digraph(engine="neato")

        domain_labels: dict[_E, int] = {}

        for index, element1 in enumerate(self._relation.domain):
            domain_labels[element1] = index
            element1_successors = self._relation.successors(element1)
            element1_predecessors: Collection[_E] = set()
            self._graph_renderer.add_source(
                element1,
                index=index,
                successors=element1_successors,
                predecessors=element1_predecessors,
            )
            digraph.node(
                f"D0_{index}",
                **self._domain_renderer.attributes(
                    element=element1,
                    index=index,
                    successors=element1_successors,
                    predecessors=element1_predecessors,
                ),
            )

        for source, destination in self._relation:
            successors = self._relation.successors(source)
            predecessors = self._relation.predecessors(destination)
            self._graph_renderer.add_edge(
                source=source,
                destination=destination,
                successors=successors,
                predecessors=predecessors,
            )
            digraph.edge(
                f"D0_{domain_labels[source]}",
                f"D0_{domain_labels[destination]}",
                **self._edge_renderer.attributes(
                    source=source,
                    destination=destination,
                    successors=successors,
                    predecessors=predecessors,
                ),
            )
        digraph.graph_attr = self._graph_renderer.attributes()
        return digraph


class CellRenderer(Generic[_E]):
    """
    It is used to render cell elements for a binary table.
    """

    __slots__ = ()

    # pylint: disable=unused-argument
    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def render(self, element: _E, index: int) -> str:
        """
        Render an element.

        Parameters
        ----------
        element
            The element to render
        index
            The element index.

        Returns
        -------
        str
            The string representation of the element into a table.

        """
        return str(element)


class BinaryTable(Generic[_E, _F]):
    """
    It renders binary relation in jupyter notebook.

    Parameters
    ----------
    relation
        The binary relation.
    domain_renderer
        The domain renderer.
    co_domain_renderer
        The co-domain renderer.

    """

    __slots__ = ("_relation", "_domain_renderer", "_co_domain_renderer")

    _relation: AbstractFiniteBinaryRelation[_E, _F]
    _domain_renderer: CellRenderer[_E]
    _co_domain_renderer: CellRenderer[_F]

    def __init__(
        self,
        relation: AbstractFiniteBinaryRelation[_E, _F],
        domain_renderer: CellRenderer[_E] | None = None,
        co_domain_renderer: CellRenderer[_F] | None = None,
    ) -> None:
        if domain_renderer is None:
            domain_renderer = CellRenderer[_E]()
        if co_domain_renderer is None:
            co_domain_renderer = CellRenderer[_F]()
        self._relation = relation
        self._domain_renderer = domain_renderer
        self._co_domain_renderer = co_domain_renderer

    @property
    def relation(self) -> AbstractFiniteBinaryRelation[_E, _F]:
        """
        Get the relation.

        Returns
        -------
            The underlying relation.

        """
        return self._relation

    @property
    def domain_renderer(self) -> CellRenderer[_E]:
        """
        Get the domain renderer.

        Returns
        -------
            The domain renderer.

        """
        return self._domain_renderer

    @property
    def co_domain_renderer(self) -> CellRenderer[_F]:
        """
        Get the co-domain renderer.

        Returns
        -------
            The co-domain renderer.

        """
        return self._co_domain_renderer

    def _repr_markdown_(self) -> str:
        header = [
            self._co_domain_renderer.render(element=element2, index=index)
            for index, element2 in enumerate(self._relation.co_domain)
        ]

        separator = "|-:|" + "|".join(["-:" for _ in header]) + "|" + "\n"

        rows = []
        for index, element1 in enumerate(self._relation.domain):
            row = [self._domain_renderer.render(element=element1, index=index)]
            for element2 in self._relation.co_domain:
                if (element1, element2) in self._relation:
                    row.append(" $\\checkmark$")
                else:
                    row.append("  ")
            rows.append("|" + "|".join(row) + "|")

        return "| |" + "|".join(header) + "|" + "\n" + separator + "\n".join(rows)

    def _repr_html_(self) -> str:
        header = [
            self._co_domain_renderer.render(element=element2, index=index)
            for index, element2 in enumerate(self._relation.co_domain)
        ]

        rows = []
        for index, element1 in enumerate(self._relation.domain):
            row = [self._domain_renderer.render(element=element1, index=index)]
            for element2 in self._relation.co_domain:
                if (element1, element2) in self._relation:
                    row.append("$\\checkmark$")
                else:
                    row.append("")
            rows.append(
                '    <tr>\n      <td align="right">'
                + '</td>\n      <td align="right">'.join(row)
                + "</td>\n    </tr>",
            )

        head = (
            "  <thead>\n    <tr>\n"
            '      <th align="right"></th>\n      <th align="right">'
            + '</th>\n      <th align="right">'.join(header)
            + "</th>\n    </tr>\n  </thead>"
        )
        body = "  <tbody>\n" + "\n".join(rows) + "\n  </tbody>"
        return f"<table>\n{head}\n{body}\n</table>"
