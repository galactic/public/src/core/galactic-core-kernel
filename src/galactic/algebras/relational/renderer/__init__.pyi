from collections.abc import Collection
from typing import Generic, TypeVar

from galactic.algebras.relational import (
    AbstractFiniteBinaryRelation,
    AbstractFiniteEndoRelation,
)

_E = TypeVar("_E")
_F = TypeVar("_F")

class GraphRenderer(Generic[_E, _F]):
    def add_source(
        self,
        element: _E,
        index: int | None = None,
        current: bool = False,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_F] | None = None,
    ) -> None: ...
    def add_destination(
        self,
        element: _F,
        index: int | None = None,
        current: bool = False,
        successors: Collection[_E] | None = None,
        predecessors: Collection[_E] | None = None,
    ) -> None: ...
    def add_edge(
        self,
        source: _E,
        destination: _F,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_E] | None = None,
    ) -> None: ...
    def attributes(self) -> dict[str, str]: ...

class NodeRenderer(Generic[_E, _F]):
    def attributes(
        self,
        element: _E,
        index: int | None = None,
        current: bool = False,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_F] | None = None,
    ) -> dict[str, str]: ...

class EdgeRenderer(Generic[_E, _F]):
    def attributes(
        self,
        source: _E,
        destination: _F,
        successors: Collection[_F] | None = None,
        predecessors: Collection[_E] | None = None,
    ) -> dict[str, str]: ...

class SagittalDiagramRenderer(GraphRenderer[_E, _F]): ...

class SagittalDiagram(Generic[_E, _F]):
    def __init__(
        self,
        relation: AbstractFiniteBinaryRelation[_E, _F],
        graph_renderer: SagittalDiagramRenderer[_E, _F] | None = None,
        domain_renderer: NodeRenderer[_E, _F] | None = None,
        co_domain_renderer: NodeRenderer[_F, _E] | None = None,
        edge_renderer: EdgeRenderer[_E, _F] | None = None,
    ) -> None: ...
    @property
    def source(self) -> str: ...
    @property
    def relation(self) -> AbstractFiniteBinaryRelation[_E, _F]: ...
    @property
    def graph_renderer(self) -> SagittalDiagramRenderer[_E, _F]: ...
    @property
    def domain_renderer(self) -> NodeRenderer[_E, _F]: ...
    @property
    def co_domain_renderer(self) -> NodeRenderer[_F, _E]: ...
    @property
    def edge_renderer(self) -> EdgeRenderer[_E, _F]: ...

class EndoDiagram(Generic[_E]):
    def __init__(
        self,
        relation: AbstractFiniteEndoRelation[_E],
        graph_renderer: GraphRenderer[_E, _E] | None = None,
        domain_renderer: NodeRenderer[_E, _E] | None = None,
        edge_renderer: EdgeRenderer[_E, _E] | None = None,
    ) -> None: ...
    @property
    def source(self) -> str: ...
    @property
    def relation(self) -> AbstractFiniteEndoRelation[_E]: ...
    @property
    def graph_renderer(self) -> GraphRenderer[_E, _E]: ...
    @property
    def domain_renderer(self) -> NodeRenderer[_E, _E]: ...
    @property
    def edge_renderer(self) -> EdgeRenderer[_E, _E]: ...

class CellRenderer(Generic[_E]):
    def render(self, element: _E, index: int) -> str: ...

class BinaryTable(Generic[_E, _F]):
    def __init__(
        self,
        relation: AbstractFiniteBinaryRelation[_E, _F],
        domain_renderer: CellRenderer[_E] | None = None,
        co_domain_renderer: CellRenderer[_F] | None = None,
    ) -> None: ...
    @property
    def relation(self) -> AbstractFiniteBinaryRelation[_E, _F]: ...
    @property
    def domain_renderer(self) -> CellRenderer[_E]: ...
    @property
    def co_domain_renderer(self) -> CellRenderer[_F]: ...
