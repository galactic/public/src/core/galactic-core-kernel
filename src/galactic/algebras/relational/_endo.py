"""The :mod:`_endo` module."""

# pylint: disable=protected-access

from __future__ import annotations

import contextlib
from typing import TYPE_CHECKING, Any, TypeVar, cast

from galactic.algebras.collection import MutableCollection
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._binary import FrozenFiniteBinaryRelation, MutableFiniteBinaryRelation

if TYPE_CHECKING:
    from collections.abc import Collection, Iterable, Iterator

_E = TypeVar("_E")


# noinspection PyProtocol
class FrozenFiniteEndoRelation(FrozenFiniteBinaryRelation[_E, _E]):
    """
    It represents concrete frozen endo relation.

    Parameters
    ----------
    domain
        An iterable of initial values for the domain.
    elements
        An iterable of couple.

    """

    __slots__ = ("_domain",)

    _domain: Collection[_E]

    def __init__(
        self,
        domain: Iterable[_E],
        *,
        elements: Iterable[tuple[_E, _E]] | None = None,
    ) -> None:
        super().__init__(domain, domain, elements=elements)

    def _create_domain(self) -> Collection[_E]:
        self._domain = super()._create_domain()
        return self._domain

    def _create_co_domain(self) -> Collection[_E]:
        return self._domain

    def _pop(self) -> tuple[Any, ...]:
        raise NotImplementedError


# pylint: disable=too-many-ancestors
class MutableFiniteEndoRelation(MutableFiniteBinaryRelation[_E, _E]):
    """
    It represents concrete mutable endo relation.

    Parameters
    ----------
    domain
        An iterable of initial values for the domain.
    elements
        An iterable of couple.

    Examples
    --------
    >>> from galactic.algebras.relational import MutableFiniteEndoRelation
    >>> endo = MutableFiniteEndoRelation[int, int](domain=[1, 2, 3])
    >>> endo
    <galactic.algebras.relational.MutableFiniteEndoRelation object at 0x...>
    >>> list(endo)
    []
    >>> list(endo.domain)
    [1, 2, 3]
    >>> list(endo.co_domain)
    [1, 2, 3]
    >>> list(endo.universes[0])
    [1, 2, 3]
    >>> endo.update([(1, 2), (2, 3)])
    >>> list(endo)
    [(1, 2), (2, 3)]
    >>> list(endo.successors(1))
    [2]
    >>> list(endo.predecessors(3))
    [2]
    >>> len(endo.predecessors(3))
    1

    """

    __slots__ = ("_domain",)

    _domain: MutableCollection[_E]

    def __init__(
        self,
        domain: Iterable[_E],
        *,
        elements: Iterable[tuple[_E, _E]] | None = None,
    ) -> None:
        super().__init__(domain, domain, elements=elements)

    def _create_domain(self) -> MutableCollection[_E]:
        class MutableUniverse:
            """
            It represents the domain af relation.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                try:
                    return item in relation._successors
                except TypeError:
                    return False

            def __len__(self) -> int:
                return len(relation._successors)

            def __iter__(self) -> Iterator[_E]:
                return iter(relation._successors)

            # noinspection PyMethodMayBeStatic
            def add(self, value: _E) -> None:
                """
                Add a value to the collection.

                Parameters
                ----------
                value
                    The value to add.

                """
                if value not in relation._successors:
                    relation._successors[value] = FIFOSet[_E]()
                    relation._predecessors[value] = FIFOSet[_E]()

            # noinspection PyMethodMayBeStatic
            def remove(self, value: _E) -> None:
                """
                Remove a value from the collection.

                This method can raise a KeyError if the value does not belong to the
                collection.

                Parameters
                ----------
                value
                    The value to remove.

                """
                for successor in relation._successors[value]:
                    relation._predecessors[successor].remove(value)
                del relation._successors[value]
                for predecessor in relation._predecessors[value]:
                    relation._successors[predecessor].remove(value)
                del relation._predecessors[value]

            def discard(self, value: _E) -> None:
                """
                Discard a value from the collection.

                Parameters
                ----------
                value
                    The value to discard.

                """
                with contextlib.suppress(KeyError):
                    self.remove(value)

            # noinspection PyMethodMayBeStatic
            def pop(self) -> _E:
                """
                Remove and return an arbitrary element from the collection.

                This method can raise a KeyError if the collection is empty.

                Returns
                -------
                _E
                    The value removed.

                """
                value, successors = relation._successors.popitem(last=False)
                for successor in successors:
                    relation._predecessors[successor].remove(value)
                predecessors = relation._predecessors[value]
                for predecessor in predecessors:
                    relation._successors[predecessor].remove(value)
                del relation._predecessors[value]
                return value

            # noinspection PyMethodMayBeStatic
            def clear(self) -> None:
                """
                Clear the collection.
                """
                relation._successors.clear()
                relation._predecessors.clear()

            def extend(self, iterable: Iterable[_E]) -> None:
                """
                Extend the collection by adding the elements of the iterable.

                Other elements can be added by the method in order to maintain the
                algebraic consistency of the structures involved.

                Parameters
                ----------
                iterable
                    An iterable of values

                """
                for value in iterable:
                    self.add(value)

        relation = self
        self._domain = MutableUniverse()
        return self._domain

    def _create_co_domain(self) -> MutableCollection[_E]:
        return self._domain
