"""The :mod:`_binary` module."""

# pylint: disable=protected-access

from __future__ import annotations

import contextlib
import itertools
from collections import OrderedDict
from collections.abc import (
    Collection,
    Iterable,
    Iterator,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import Any, Generic, TypeVar, cast

from galactic.algebras.collection import MutableCollection
from galactic.algebras.set import FIFOSet
from galactic.helpers.core import default_repr

from ._mixins import ConcreteFiniteRelationMixin, MutableFiniteRelationMixin

_E = TypeVar("_E")
_F = TypeVar("_F")


def check(
    element: object,
    universe: Collection[Any],
) -> None:
    """
    Check if the element belong to the universe.

    Parameters
    ----------
    element
        The element.
    universe
        A universe.

    Raises
    ------
    RuntimeError
        If the element does not belong to the universe anymore.

    """
    if element is not None and element not in universe:
        raise RuntimeError(f"{element} does not belong to the universe anymore")


# noinspection PyProtocol
class FrozenFiniteBinaryRelation(ConcreteFiniteRelationMixin, Generic[_E, _F]):
    """
    It represents concrete frozen binary relation.

    Parameters
    ----------
    domain
        An iterable of initial values for the domain.
    co_domain
        An iterable of initial values for the co-domain.
    elements
        An iterable of couple.

    """

    __slots__ = ("_universes", "_successors", "_predecessors", "_hash_value")

    _universes: tuple[Collection[_E], Collection[_F]]
    _successors: OrderedDict[_E, FIFOSet[_F]]
    _predecessors: OrderedDict[_F, FIFOSet[_E]]
    _hash_value: int | None

    def __init__(
        self,
        domain: Iterable[_E],
        co_domain: Iterable[_F],
        *,
        elements: Iterable[tuple[_E, _F]] | None = None,
    ) -> None:
        self._successors = OrderedDict()
        self._predecessors = OrderedDict()
        self._universes = (self._create_domain(), self._create_co_domain())
        for element1 in domain:
            self._successors[element1] = FIFOSet[_F]()
        for element2 in co_domain:
            self._predecessors[element2] = FIFOSet[_E]()
        if elements is not None:
            for element in elements:
                self._add(element)
        self._hash_value = None

    def _create_domain(self) -> Collection[_E]:
        class FiniteUniverse:
            """
            It represents the domain af relation.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                try:
                    return item in relation._successors
                except TypeError:
                    return False

            def __len__(self) -> int:
                return len(relation._successors)

            def __iter__(self) -> Iterator[_E]:
                return iter(relation._successors)

        relation = self
        return FiniteUniverse()

    def _create_co_domain(self) -> Collection[_F]:
        class FiniteUniverse:
            """
            It represents the co-domain af relation.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __contains__(self, item: object) -> bool:
                try:
                    return item in relation._predecessors
                except TypeError:
                    return False

            def __len__(self) -> int:
                return len(relation._predecessors)

            def __iter__(self) -> Iterator[_F]:
                return iter(relation._predecessors)

        relation = self
        return FiniteUniverse()

    def __hash__(self) -> int:
        if self._hash_value is None:
            # noinspection PyProtectedMember
            self._hash_value = (
                AbstractSet._hash(self._universes[0])  # type: ignore[arg-type]
                ^ AbstractSet._hash(self._universes[1])  # type: ignore[arg-type]
                ^ AbstractSet._hash(self)  # type: ignore[arg-type]
            )
        return self._hash_value

    def __bool__(self) -> bool:
        return any(successors for successors in self._successors.values())

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if isinstance(item, tuple) and len(item) == 2:
            element1, element2 = item
            try:
                return (
                    element1 in self._successors
                    and element2 in self._successors[element1]
                )
            except TypeError:
                return False
        return False

    def __len__(self) -> int:
        return sum(len(successors) for successors in self._successors.values())

    def __iter__(self) -> Iterator[tuple[_E, _F]]:
        return itertools.chain.from_iterable(
            ((element1, element2) for element2 in self._successors[element1])
            for element1 in self._successors
        )

    # FiniteBinaryRelation properties and methods

    def _add(self, element: tuple[Any, ...]) -> None:
        (source, destination) = element
        self._successors[source].add(destination)
        self._predecessors[destination].add(source)

    def _remove(self, element: tuple[Any, ...]) -> None:
        (source, destination) = element
        self._successors[source].remove(destination)
        self._predecessors[destination].remove(source)

    def _discard(self, element: tuple[Any, ...]) -> None:
        with contextlib.suppress(TypeError):
            (source, destination) = element
            if source in self._successors:
                self._successors[source].discard(destination)
            if destination in self._predecessors:
                self._predecessors[destination].discard(source)

    def _pop(self) -> tuple[Any, ...]:
        raise NotImplementedError

    def _clear(self) -> None:
        for successors in self._successors.values():
            successors.clear()
        for predecessors in self._predecessors.values():
            predecessors.clear()

    # AbstractFiniteBinaryRelation properties and methods

    @property
    def universes(self) -> tuple[Collection[_E], Collection[_F]]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Collection[_E], Collection[_F]]
            The universes of this relation.

        """
        return self._universes

    @property
    def domain(self) -> Collection[_E]:
        """
        Get the domain of this binary relation.

        It returns the first universe.

        Returns
        -------
        Collection[_E]
            The domain of this binary relation.

        """
        return self._universes[0]

    @property
    def co_domain(self) -> Collection[_F]:
        """
        Get the co-domain of this binary relation.

        It returns the second universe.

        Returns
        -------
        Collection[_F]
            The co-domain of this binary relation.

        """
        return self._universes[1]

    def successors(self, element: _E) -> Collection[_F]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
        Collection[_F]
            The successors.

        Raises
        ------
        ValueError
            If the element does not belong to the relation.

        """
        if element not in self.domain:
            raise ValueError(f"{element} does not belong to the domain")
        return self._create_successors(element)

    def _create_successors(self, element: _E) -> Collection[_F]:
        class Successors:
            """
            It represents the successors af an element.
            """

            def __repr__(self) -> str:
                check(element, relation._successors)
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                check(element, relation._successors)
                return item in relation._successors[element]

            def __len__(self) -> int:
                check(element, relation._successors)
                return len(relation._successors[element])

            def __iter__(self) -> Iterator[_F]:
                check(element, relation._successors)
                return iter(relation._successors[element])

        relation = self
        return Successors()

    def predecessors(self, element: _F) -> Collection[_E]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
        Collection[_E]
            The predecessors.

        Raises
        ------
        ValueError
            If the element does not belong to the relation.

        """
        if element not in self.co_domain:
            raise ValueError(f"{element} does not belong to the co-domain")
        return self._create_predecessors(element)

    def _create_predecessors(self, element: _F) -> Collection[_E]:
        class Predecessors:
            """
            It represents the predecessors af an element.
            """

            def __repr__(self) -> str:
                check(element, relation._predecessors)
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                check(element, relation._predecessors)
                return item in relation._predecessors[element]

            def __len__(self) -> int:
                check(element, relation._predecessors)
                return len(relation._predecessors[element])

            def __iter__(self) -> Iterator[_E]:
                check(element, relation._predecessors)
                return iter(relation._predecessors[element])

        relation = self
        return Predecessors()


# pylint: disable=too-many-ancestors
class MutableFiniteBinaryRelation(
    FrozenFiniteBinaryRelation[_E, _F],
    MutableFiniteRelationMixin,
    Generic[_E, _F],
):
    # noinspection PyUnresolvedReferences
    """
    It represents concrete mutable binary relation.

    Parameters
    ----------
    domain
        The elements of the domain.
    co_domain
        The elements of the co-domain.
    elements
        The initial couples.

    Examples
    --------
    >>> from galactic.algebras.relational import MutableFiniteBinaryRelation
    >>> b = MutableFiniteBinaryRelation[int, int](
    ...     domain=[1, 2, 3],
    ...     co_domain=[2, 3, 4]
    ... )
    >>> b
    <galactic.algebras.relational.MutableFiniteBinaryRelation object at 0x...>
    >>> list(b)
    []
    >>> list(b.domain)
    [1, 2, 3]
    >>> list(b.co_domain)
    [2, 3, 4]
    >>> list(b.universes[0])
    [1, 2, 3]
    >>> b.update([(1, 4), (3, 4)])
    >>> list(b)
    [(1, 4), (3, 4)]
    >>> list(b.successors(1))
    [4]
    >>> list(b.predecessors(4))
    [1, 3]
    >>> len(b.predecessors(4))
    2

    """

    __slots__ = ()

    # noinspection PyProtocol
    def __init__(
        self,
        domain: Iterable[_E],
        co_domain: Iterable[_F],
        *,
        elements: Iterable[tuple[_E, _F]] | None = None,
    ) -> None:
        if elements is None:
            super().__init__(domain, co_domain, elements=[])
        else:
            super().__init__(domain, co_domain, elements=elements)

    __hash__ = None  # type: ignore[assignment]

    def _create_domain(self) -> MutableCollection[_E]:
        class MutableUniverse:
            """
            It represents the domain af relation.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                try:
                    return item in relation._successors
                except TypeError:
                    return False

            def __len__(self) -> int:
                return len(relation._successors)

            def __iter__(self) -> Iterator[_E]:
                return iter(relation._successors)

            # noinspection PyMethodMayBeStatic
            def add(self, value: _E) -> None:
                """
                Add a value to the collection.

                Parameters
                ----------
                value
                    The value to add.

                """
                if value not in relation._successors:
                    relation._successors[value] = FIFOSet[_F]()

            # noinspection PyMethodMayBeStatic
            def remove(self, value: _E) -> None:
                """
                Remove a value from the collection.

                This method can raise a KeyError if the value does not belong to the
                collection.

                Parameters
                ----------
                value
                    The value to remove.

                """
                for successor in relation._successors[value]:
                    relation._predecessors[successor].remove(value)
                del relation._successors[value]

            def discard(self, value: _E) -> None:
                """
                Discard a value from the collection.

                Parameters
                ----------
                value
                    The value to discard.

                """
                with contextlib.suppress(KeyError):
                    self.remove(value)

            # noinspection PyMethodMayBeStatic
            def pop(self) -> _E:
                """
                Remove and return an arbitrary element from the collection.

                This method can raise a KeyError if the collection is empty.

                Returns
                -------
                _E
                    The value removed.

                """
                value, successors = relation._successors.popitem(last=False)
                for successor in successors:
                    relation._predecessors[successor].remove(value)
                return value

            # noinspection PyMethodMayBeStatic
            def clear(self) -> None:
                """
                Clear the collection.
                """
                relation._successors.clear()
                relation._predecessors.clear()

            def extend(self, iterable: Iterable[_E]) -> None:
                """
                Extend the collection by adding the elements of the iterable.

                Other elements can be added by the method in order to maintain the
                algebraic consistency of the structures involved.

                Parameters
                ----------
                iterable
                    An iterable of values

                """
                for value in iterable:
                    self.add(value)

        relation = self
        return MutableUniverse()

    def _create_co_domain(self) -> MutableCollection[_F]:
        class MutableUniverse:
            """
            It represents the co-domain af relation.
            """

            def __repr__(self) -> str:
                return cast(str, default_repr(self, long=False))

            def __contains__(self, item: object) -> bool:
                try:
                    return item in relation._predecessors
                except TypeError:
                    return False

            def __len__(self) -> int:
                return len(relation._predecessors)

            def __iter__(self) -> Iterator[_F]:
                return iter(relation._predecessors)

            # noinspection PyMethodMayBeStatic
            def add(self, value: _F) -> None:
                """
                Add a value to the collection.

                Parameters
                ----------
                value
                    The value to add.

                """
                if value not in relation._predecessors:
                    relation._predecessors[value] = FIFOSet[_E]()

            # noinspection PyMethodMayBeStatic
            def remove(self, value: _F) -> None:
                """
                Remove a value from the collection.

                This method can raise a KeyError if the value does not belong to the
                collection.

                Parameters
                ----------
                value
                    The value to remove.

                """
                for successor in relation._predecessors[value]:
                    relation._successors[successor].remove(value)
                del relation._predecessors[value]

            def discard(self, value: _F) -> None:
                """
                Discard a value from the collection.

                Parameters
                ----------
                value
                    The value to discard.

                """
                with contextlib.suppress(KeyError):
                    self.remove(value)

            # noinspection PyMethodMayBeStatic
            def pop(self) -> _F:
                """
                Remove and return an arbitrary element from the collection.

                This method can raise a KeyError if the collection is empty.

                Returns
                -------
                _F
                    The value removed.

                """
                value, predecessors = relation._predecessors.popitem(last=False)
                for predecessor in predecessors:
                    relation._successors[predecessor].remove(value)
                return value

            # noinspection PyMethodMayBeStatic
            def clear(self) -> None:
                """
                Clear the collection.
                """
                relation._successors.clear()
                relation._predecessors.clear()

            def extend(self, iterable: Iterable[_F]) -> None:
                """
                Extend the collection by adding the elements of the iterable.

                Other elements can be added by the method in order to maintain the
                algebraic consistency of the structures involved.

                Parameters
                ----------
                iterable
                    An iterable of values

                """
                for value in iterable:
                    self.add(value)

        relation = self
        return MutableUniverse()

    def _create_successors(self, element: _E) -> MutableCollection[_F]:
        class MutableSuccessors:
            """
            It represents the successors af an element.
            """

            def __repr__(self) -> str:
                check(element, relation._successors)
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                check(element, relation._successors)
                return item in relation._successors[element]

            def __len__(self) -> int:
                check(element, relation._successors)
                return len(relation._successors[element])

            def __iter__(self) -> Iterator[_F]:
                check(element, relation._successors)
                return iter(relation._successors[element])

            # noinspection PyMethodMayBeStatic
            def add(self, value: _F) -> None:
                """
                Add a value to the collection.

                Parameters
                ----------
                value
                    The value to add.

                """
                check(element, relation._successors)
                relation._predecessors[value].add(element)
                relation._successors[element].add(value)

            # noinspection PyMethodMayBeStatic
            def remove(self, value: _F) -> None:
                """
                Remove a value from the collection.

                This method can raise a KeyError if the value does not belong to the
                collection.

                Parameters
                ----------
                value
                    The value to remove.

                """
                check(element, relation._successors)
                relation._predecessors[value].remove(element)
                relation._successors[element].remove(value)

            def discard(self, value: _F) -> None:
                """
                Discard a value from the collection.

                Parameters
                ----------
                value
                    The value to discard.

                """
                check(element, relation._successors)
                with contextlib.suppress(KeyError):
                    self.remove(value)

            # noinspection PyMethodMayBeStatic
            def pop(self) -> _F:
                """
                Remove and return an arbitrary element from the collection.

                This method can raise a KeyError if the collection is empty.

                Returns
                -------
                _F
                    The value removed.

                """
                check(element, relation._successors)
                successor = relation._successors[element].pop()
                relation._predecessors[successor].remove(element)
                return successor

            # noinspection PyMethodMayBeStatic
            def clear(self) -> None:
                """
                Clear the collection.
                """
                check(element, relation._successors)
                for successor in relation._successors[element]:
                    relation._predecessors[successor].remove(element)
                relation._successors[element].clear()

            def extend(self, iterable: Iterable[_F]) -> None:
                """
                Extend the collection by adding the elements of the iterable.

                Other elements can be added by the method in order to maintain the
                algebraic consistency of the structures involved.

                Parameters
                ----------
                iterable
                    An iterable of values

                """
                check(element, relation._successors)
                for value in iterable:
                    self.add(value)

        relation = self
        return MutableSuccessors()

    def _create_predecessors(self, element: _F) -> MutableCollection[_E]:
        class MutablePredecessors:
            """
            It represents the predecessors af an element.
            """

            def __repr__(self) -> str:
                check(element, relation._predecessors)
                return cast(str, default_repr(self, long=False))

            # Collection properties and methods

            def __contains__(self, item: object) -> bool:
                check(element, relation._predecessors)
                return item in relation._predecessors[element]

            def __len__(self) -> int:
                check(element, relation._predecessors)
                return len(relation._predecessors[element])

            def __iter__(self) -> Iterator[_E]:
                check(element, relation._predecessors)
                return iter(relation._predecessors[element])

            # noinspection PyMethodMayBeStatic
            def add(self, value: _E) -> None:
                """
                Add a value to the collection.

                Parameters
                ----------
                value
                    The value to add.

                """
                check(element, relation._predecessors)
                relation._successors[value].add(element)
                relation._predecessors[element].add(value)

            # noinspection PyMethodMayBeStatic
            def remove(self, value: _E) -> None:
                """
                Remove a value from the collection.

                This method can raise a KeyError if the value does not belong to the
                collection.

                Parameters
                ----------
                value
                    The value to remove.

                """
                check(element, relation._predecessors)
                relation._successors[value].remove(element)
                relation._predecessors[element].remove(value)

            def discard(self, value: _E) -> None:
                """
                Discard a value from the collection.

                Parameters
                ----------
                value
                    The value to discard.

                """
                check(element, relation._predecessors)
                with contextlib.suppress(KeyError):
                    self.remove(value)

            # noinspection PyMethodMayBeStatic
            def pop(self) -> _E:
                """
                Remove and return an arbitrary element from the collection.

                This method can raise a KeyError if the collection is empty.

                Returns
                -------
                _E
                    The value removed.

                """
                check(element, relation._predecessors)
                predecessor = relation._predecessors[element].pop()
                relation._successors[predecessor].remove(element)
                return predecessor

            # noinspection PyMethodMayBeStatic
            def clear(self) -> None:
                """
                Clear the collection.
                """
                check(element, relation._predecessors)
                for predecessor in relation._predecessors[element]:
                    relation._successors[predecessor].remove(element)
                relation._predecessors[element].clear()

            def extend(self, iterable: Iterable[_E]) -> None:
                """
                Extend the collection by adding the elements of the iterable.

                Other elements can be added by the method in order to maintain the
                algebraic consistency of the structures involved.

                Parameters
                ----------
                iterable
                    An iterable of values

                """
                check(element, relation._predecessors)
                for value in iterable:
                    self.add(value)

        relation = self
        return MutablePredecessors()

    def _pop(self) -> tuple[Any, ...]:
        for source in self._successors:
            if self._successors[source]:
                destination = self._successors[source].pop()
                self._predecessors[destination].remove(source)
                return source, destination
        raise StopIteration

    @property
    def universes(self) -> tuple[MutableCollection[_E], MutableCollection[_F]]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[MutableCollection[_E], MutableCollection[_F]]
            The universes of this relation.

        """
        return cast(
            tuple[MutableCollection[_E], MutableCollection[_F]],
            self._universes,
        )

    @property
    def domain(self) -> MutableCollection[_E]:
        """
        Get the domain of this binary relation.

        It returns the first universe.

        Returns
        -------
        MutableCollection[_E]
            The domain of this binary relation.

        """
        return self.universes[0]

    @property
    def co_domain(self) -> MutableCollection[_F]:
        """
        Get the co-domain of this binary relation.

        It returns the second universe.

        Returns
        -------
        MutableCollection[_F]
            The co-domain of this binary relation.

        """
        return self.universes[1]
