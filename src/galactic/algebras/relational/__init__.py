"""
The :mod:`galactic.algebras.relational` module.

..  rubric:: Abstract classes

* :class:`AbstractRelation`;
* :class:`AbstractFiniteRelation`;
* :class:`AbstractBinaryRelation`;
* :class:`AbstractLeftFiniteBinaryRelation`;
* :class:`AbstractRightFiniteBinaryRelation`;
* :class:`AbstractFiniteBinaryRelation`;
* :class:`AbstractEndoRelation`;
* :class:`AbstractFiniteEndoRelation`;
* :class:`AbstractDirectedAcyclicRelation`;
* :class:`AbstractFiniteDirectedAcyclicRelation`.

..  rubric:: Concrete classes

* :class:`FrozenFiniteRelation`;
* :class:`MutableFiniteRelation`;
* :class:`FiniteSelection` which is a view on a finite relation;
* :class:`FiniteProjection` which is a view on a finite relation;
* :class:`FrozenFiniteBinaryRelation`;
* :class:`MutableFiniteBinaryRelation`;
* :class:`FrozenFiniteEndoRelation`;
* :class:`MutableFiniteEndoRelation`.

..  rubric:: Mixin classes

* :class:`FiniteRelationMixin`;
* :class:`ConcreteFiniteRelationMixin`;
* :class:`MutableFiniteRelationMixin`.
"""

__all__ = (
    "AbstractRelation",
    "AbstractFiniteRelation",
    "AbstractBinaryRelation",
    "AbstractLeftFiniteBinaryRelation",
    "AbstractRightFiniteBinaryRelation",
    "AbstractFiniteBinaryRelation",
    "AbstractEndoRelation",
    "AbstractFiniteEndoRelation",
    "AbstractDirectedAcyclicRelation",
    "AbstractFiniteDirectedAcyclicRelation",
    "FrozenFiniteRelation",
    "MutableFiniteRelation",
    "FiniteSelection",
    "FiniteProjection",
    "FrozenFiniteBinaryRelation",
    "MutableFiniteBinaryRelation",
    "FrozenFiniteEndoRelation",
    "MutableFiniteEndoRelation",
    "FiniteRelationMixin",
    "ConcreteFiniteRelationMixin",
    "MutableFiniteRelationMixin",
)

from ._abstract import (
    AbstractBinaryRelation,
    AbstractDirectedAcyclicRelation,
    AbstractEndoRelation,
    AbstractFiniteBinaryRelation,
    AbstractFiniteDirectedAcyclicRelation,
    AbstractFiniteEndoRelation,
    AbstractFiniteRelation,
    AbstractLeftFiniteBinaryRelation,
    AbstractRelation,
    AbstractRightFiniteBinaryRelation,
)
from ._binary import (
    FrozenFiniteBinaryRelation,
    MutableFiniteBinaryRelation,
)
from ._endo import (
    FrozenFiniteEndoRelation,
    MutableFiniteEndoRelation,
)
from ._finitary import (
    FrozenFiniteRelation,
    MutableFiniteRelation,
)
from ._mixins import (
    ConcreteFiniteRelationMixin,
    FiniteRelationMixin,
    MutableFiniteRelationMixin,
)
from ._operations import (
    FiniteProjection,
    FiniteSelection,
)
