"""The :mod:`_abstract` module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Collection, Container
from typing import Any, Protocol, TypeVar, runtime_checkable


@runtime_checkable
class AbstractRelation(Container[tuple[Any, ...]], Protocol):
    """
    It represents finitary relation.

    See https://en.wikipedia.org/wiki/Finitary_relation
    """

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractRelation) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractRelation) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractRelation) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractRelation) -> bool:
        raise NotImplementedError

    # AbstractRelation properties and methods

    @property
    @abstractmethod
    def universes(self) -> tuple[Container[Any], ...]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Container[Any], ...]
            The universes of this relation.

        """
        raise NotImplementedError


@runtime_checkable
class AbstractFiniteRelation(Collection[tuple[Any, ...]], Protocol):
    """
    It represents finitary relation.

    See https://en.wikipedia.org/wiki/Finitary_relation
    """

    __slots__ = ()

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractFiniteRelation) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractFiniteRelation) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractFiniteRelation) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractFiniteRelation) -> bool:
        raise NotImplementedError

    # AbstractFiniteRelation properties and methods

    @property
    @abstractmethod
    def universes(self) -> tuple[Collection[Any], ...]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Collection[Any], ...]
            The universes of this relation.

        """
        raise NotImplementedError


_E = TypeVar("_E")
_F = TypeVar("_F")


@runtime_checkable
class AbstractBinaryRelation(Container[tuple[_E, _F]], Protocol[_E, _F]):
    """
    It represents binary relation.

    See https://en.wikipedia.org/wiki/Binary_relation
    """

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    # AbstractBinaryRelation properties and methods

    @property
    @abstractmethod
    def universes(self) -> tuple[Container[_E], Container[_F]]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Container[_E], Container[_F]]
            The universes of this relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def domain(self) -> Container[_E]:
        """
        Get the domain of this binary relation.

        It returns the first universe.

        Returns
        -------
        Container[_E]
            The domain of this binary relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def co_domain(self) -> Container[_F]:
        """
        Get the co-domain of this binary relation.

        It returns the second universe.

        Returns
        -------
        Container[_F]
            The co-domain of this binary relation.

        """
        raise NotImplementedError

    @abstractmethod
    def successors(self, element: _E) -> Container[_F]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
        Container[_F]
            The successors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def predecessors(self, element: _F) -> Container[_E]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
        Container[_E]
            The predecessors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class AbstractLeftFiniteBinaryRelation(Container[tuple[_E, _F]], Protocol[_E, _F]):
    """
    It represents left finite binary relation.

    See https://en.wikipedia.org/wiki/Binary_relation
    """

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractLeftFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractLeftFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractLeftFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractLeftFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    # AbstractBinaryRelation properties and methods

    @property
    @abstractmethod
    def universes(self) -> tuple[Collection[_E], Container[_F]]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Collection[_E], Container[_F]]
            The universes of this relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def domain(self) -> Collection[_E]:
        """
        Get the domain of this binary relation.

        It returns the first universe.

        Returns
        -------
        Collection[_E]
            The domain of this binary relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def co_domain(self) -> Container[_F]:
        """
        Get the co-domain of this binary relation.

        It returns the second universe.

        Returns
        -------
        Container[_F]
            The co-domain of this binary relation.

        """
        raise NotImplementedError

    @abstractmethod
    def successors(self, element: _E) -> Container[_F]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
        Container[_F]
            The successors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def predecessors(self, element: _F) -> Collection[_E]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
        Collection[_E]
            The predecessors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class AbstractRightFiniteBinaryRelation(Container[tuple[_E, _F]], Protocol[_E, _F]):
    """
    It represents right finite binary relation.

    See https://en.wikipedia.org/wiki/Binary_relation
    """

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractRightFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractRightFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractRightFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractRightFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    # AbstractBinaryRelation properties and methods

    @property
    @abstractmethod
    def universes(self) -> tuple[Container[_E], Collection[_F]]:
        """
        Get the universes of this relation.

        Returns
        -------
        tuple[Container[_E], Collection[_F]]
            The universes of this relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def domain(self) -> Container[_E]:
        """
        Get the domain of this binary relation.

        It returns the first universe.

        Returns
        -------
        Container[_E]
            The domain of this binary relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def co_domain(self) -> Collection[_F]:
        """
        Get the co-domain of this binary relation.

        It returns the second universe.

        Returns
        -------
        Collection[_F]
            The co-domain of this binary relation.

        """
        raise NotImplementedError

    @abstractmethod
    def successors(self, element: _E) -> Collection[_F]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
        Collection[_F]
            The successors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def predecessors(self, element: _F) -> Container[_E]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
        Container[_E]
            The predecessors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class AbstractFiniteBinaryRelation(Collection[tuple[_E, _F]], Protocol[_E, _F]):
    """
    It represents binary relation.

    See https://en.wikipedia.org/wiki/Binary_relation
    """

    # Set comparison properties and methods

    @abstractmethod
    def __lt__(self, other: AbstractFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __le__(self, other: AbstractFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __gt__(self, other: AbstractFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __ge__(self, other: AbstractFiniteBinaryRelation[_E, _F]) -> bool:
        raise NotImplementedError

    # AbstractBinaryRelation properties and methods

    @property
    @abstractmethod
    def universes(self) -> tuple[Collection[_E], Collection[_F]]:
        """
        Get the universes of this relation.

        Returns
        -------
            The universes of this relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def domain(self) -> Collection[_E]:
        """
        Get the domain of this binary relation.

        It returns the first universe.

        Returns
        -------
            The domain of this binary relation.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def co_domain(self) -> Collection[_F]:
        """
        Get the co-domain of this binary relation.

        It returns the second universe.

        Returns
        -------
            The co-domain of this binary relation.

        """
        raise NotImplementedError

    @abstractmethod
    def successors(self, element: _E) -> Collection[_F]:
        """
        Get the successors of an element.

        Parameters
        ----------
        element
            The element whose successors are requested

        Returns
        -------
        Collection[_F]
            The successors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def predecessors(self, element: _F) -> Collection[_E]:
        """
        Get the predecessors of an element.

        Parameters
        ----------
        element
            The element whose predecessors are requested

        Returns
        -------
        Collection[_E]
            The predecessors.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class AbstractEndoRelation(AbstractBinaryRelation[_E, _E], Protocol[_E]):
    """
    It represents 2-relation from a universe to itself.
    """


# pylint: disable=too-few-public-methods
@runtime_checkable
class AbstractFiniteEndoRelation(AbstractFiniteBinaryRelation[_E, _E], Protocol[_E]):
    """
    It represents 2-relation from a universe to itself.
    """


@runtime_checkable
class AbstractDirectedAcyclicRelation(AbstractBinaryRelation[_E, _E], Protocol[_E]):
    """
    It represents directed acyclic graph.
    """

    # AbstractDirectedAcyclicRelation properties and methods

    @property
    @abstractmethod
    def sinks(self) -> Container[_E]:
        """
        Get the sinks of this DAG.

        Returns
        -------
        Container[_E]
            The sinks of this DAG.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def sources(self) -> Container[_E]:
        """
        Get the sources of this DAG.

        Returns
        -------
        Container[_E]
            The sources of this DAG.

        """
        raise NotImplementedError


# pylint: disable=duplicate-bases
@runtime_checkable
class AbstractFiniteDirectedAcyclicRelation(
    AbstractFiniteEndoRelation[_E],
    Protocol[_E],
):
    """
    It represents directed acyclic graph.
    """

    # AbstractDirectedAcyclicRelation properties and methods

    @property
    @abstractmethod
    def sinks(self) -> Collection[_E]:
        """
        Get the sinks of this DAG.

        Returns
        -------
        Collection[_E]
            The sinks of this DAG.

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def sources(self) -> Collection[_E]:
        """
        Get the sources of this DAG.

        Returns
        -------
        Collection[_E]
            The sources of this DAG.

        """
        raise NotImplementedError
