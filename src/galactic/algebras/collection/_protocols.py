"""The :mod:`_protocols` module."""

from abc import abstractmethod
from collections.abc import Collection, Iterable
from typing import Protocol, TypeVar, runtime_checkable

_E = TypeVar("_E")


# pylint: disable=too-few-public-methods
@runtime_checkable
class ExtensibleCollection(Collection[_E], Protocol):
    """
    It represents extensible collections.
    """

    @abstractmethod
    def extend(self, iterable: Iterable[_E]) -> None:
        """
        Extend the collection by adding the elements of the iterable.

        Other elements can be added by the method in order to maintain the algebraic
        consistency of the structures involved.

        Parameters
        ----------
        iterable
            An iterable of values

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class MutableCollection(ExtensibleCollection[_E], Protocol):
    """
    It represents mutable collections.
    """

    @abstractmethod
    def add(self, value: _E) -> None:
        """
        Add a value to the collection.

        Parameters
        ----------
        value
            The value to add.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def remove(self, value: _E) -> None:
        """
        Remove a value from the collection.

        This method can raise a KeyError if the value does not belong to the collection.

        Parameters
        ----------
        value
            The value to remove.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def discard(self, value: _E) -> None:
        """
        Discard a value from the collection.

        Parameters
        ----------
        value
            The value to discard.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def pop(self) -> _E:
        """
        Remove and return an arbitrary element from the collection.

        This method can raise a KeyError if the collection is empty.

        Returns
        -------
        _E
            The value removed.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def clear(self) -> None:
        """
        Clear the collection.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
