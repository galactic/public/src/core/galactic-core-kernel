from abc import abstractmethod
from collections.abc import Collection, Iterable
from typing import Protocol, TypeVar, runtime_checkable

_E = TypeVar("_E")

@runtime_checkable
class ExtensibleCollection(Collection[_E], Protocol):
    @abstractmethod
    def extend(self, iterable: Iterable[_E]) -> None: ...

@runtime_checkable
class MutableCollection(ExtensibleCollection[_E], Protocol):
    @abstractmethod
    def add(self, value: _E) -> None: ...
    @abstractmethod
    def remove(self, value: _E) -> None: ...
    @abstractmethod
    def discard(self, value: _E) -> None: ...
    @abstractmethod
    def pop(self) -> _E: ...
    @abstractmethod
    def clear(self) -> None: ...
