"""
The :mod:`galactic.algebras.collection` modules.

It defines some protocols for collections:

* :class:`ExtensibleCollection`
* :class:`MutableCollection`
"""

__all__ = (
    "ExtensibleCollection",
    "MutableCollection",
)

from ._protocols import ExtensibleCollection, MutableCollection
