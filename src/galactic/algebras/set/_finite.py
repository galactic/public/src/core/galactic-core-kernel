"""The :mod:`_finite` module."""

# pylint: disable=too-many-lines

from __future__ import annotations

import array
import bisect
import copy
import heapq
import itertools
import math
import reprlib
from collections import OrderedDict
from collections.abc import (
    Iterable,
    Iterator,
    MutableSequence,
    MutableSet,
    Sequence,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import Any, SupportsInt, TypeVar, cast, overload

from typing_extensions import Self

from galactic.helpers.core import default_repr

from ._fifo import FIFOSet

IntegerCouple = tuple[int, int]


class Intervals:
    """
    Intervals acting as a priority queue.

    Parameters
    ----------
    elements
        A mutable sequence of values
    complement
        A complement flag
    """

    __slots__ = ("_elements", "_complement", "_index")

    def __init__(
        self,
        elements: MutableSequence[int] | list[int | float],
        complement: bool = False,
    ) -> None:
        self._elements = elements
        self._complement = complement
        if self._complement:
            self._index = -1
        else:
            self._index = 0

    def __repr__(self) -> str:
        if self._index == -1:
            return reprlib.repr([-math.inf, *self._elements, math.inf])
        if self._complement:
            if self._index <= len(self._elements):
                return reprlib.repr([*self._elements[self._index :], math.inf])
            return repr([])
        return reprlib.repr([*self._elements[self._index :]])

    def __bool__(self) -> bool:
        if self._complement:
            return self._index <= len(self._elements)
        return self._index < len(self._elements)

    def __lt__(self, other: Intervals) -> bool:
        if self._index == -1:
            return True
        if other._index == -1:
            return False
        if self._complement and self._index == len(self._elements):
            return False
        if other._complement and other._index == len(other._elements):
            return True
        return self._elements[self._index] < other._elements[other._index]

    # pylint: disable=protected-access
    def union(self, *others: Intervals) -> Intervals:
        """
        Compute the union of this list of intervals and the others.

        Parameters
        ----------
        *others
            A sequence of list of intervals

        Returns
        -------
        Intervals
            The union of this list of intervals and the others

        """
        compacts: list[Intervals] = []
        result: list[int | float] = []
        if self:
            heapq.heappush(compacts, self)
        for other in others:
            if other:
                heapq.heappush(compacts, other)

        while compacts:
            current = heapq.heappop(compacts)
            inf = current._pop()
            sup = current._pop()
            if current:
                heapq.heappush(compacts, current)
            else:
                current._reset()
            while compacts and inf <= compacts[0]._first <= sup:
                compact = heapq.heappop(compacts)
                _ = compact._pop()
                sup = max(sup, compact._pop())
                if compact:
                    heapq.heappush(compacts, compact)
                else:
                    compact._reset()
            result.extend((inf, sup))
        return Intervals(elements=result)

    def _reset(self) -> None:
        """
        Reset the current list of intervals.
        """
        self._index = -1 if self._complement else 0

    def _pop(self) -> int | float:
        """
        Pop an element.

        Returns
        -------
        int | None
            The element.
        """
        result = self._first
        self._index += 1
        return result

    @property
    def _first(self) -> int | float:
        """
        Get the last element.

        Returns
        -------
        int | float
            The first element.
        """
        if self._index < 0:
            return -math.inf
        if self._complement and self._index == len(self._elements):
            return math.inf
        return self._elements[self._index]

    @property
    def complement(self) -> bool:
        """
        Get the complement flag.

        Returns
        -------
        bool
            The complement flag
        """
        return self._complement or (
            len(self._elements) > 0 and math.isinf(self._elements[0])
        )

    def __iter__(self) -> Iterator[int]:
        for element in self._elements:
            if not math.isinf(element):
                yield element  # type: ignore[misc]


# pylint: disable=protected-access
class IntegerSet(AbstractSet[int]):
    """
    It represents subsets of set of integers.

    Parameters
    ----------
    intervals
        An optional iterable of intervals.

    Raises
    ------
    ValueError
        If the intervals are not sorted.
    ValueError
        If an interval is empty.

    Instances store membership using a compact array of intervals represented by
    an array of int. It uses
    `inversion lists <https://en.wikipedia.org/wiki/Inversion_list>`_ as inner
    data structures.

    For example, the ``{0,1,3,4}`` subset will be represented by ``[0,2,3,5]`` meaning:

    * from element ``0`` (included) to element ``2`` (excluded)
    * from element ``3`` (included) to element ``5`` (excluded)

    Examples
    --------
    >>> from galactic.algebras.set import IntegerSet
    >>> set1 = IntegerSet([(0, 2), (3, 5)])
    >>> set1
    <galactic.algebras.set.IntegerSet object at 0x...>
    >>> list(set1)
    [0, 1, 3, 4]
    >>> len(set1)
    4
    >>> 0 in set1
    True
    >>> 5 in set1
    False
    >>> set2 = IntegerSet([(1, 2), (3, 6)])
    >>> list(set2)
    [1, 3, 4, 5]
    >>> list(set1 | set2)
    [0, 1, 3, 4, 5]
    >>> list(set1 & set2)
    [1, 3, 4]
    >>> list(set1 ^ set2)
    [0, 5]
    >>> set1 & set2 <= set1
    True
    >>> set1 | set2 >= set1
    True
    >>> set1.isdisjoint(set2)
    False

    """

    __slots__ = ("_intervals", "_length", "_hash_value")

    _intervals: MutableSequence[int]
    _length: int
    _hash_value: int | None

    def __init__(self, intervals: Iterable[IntegerCouple] | None = None) -> None:
        length = 0
        elements = []
        if intervals:
            last = None
            for inf, sup in intervals:
                if last is not None and inf <= last:
                    raise ValueError("Intervals are not sorted")
                if inf >= sup:
                    raise ValueError("An interval is empty")
                elements.extend([inf, sup])
                last = sup
                length += sup - inf

        self._intervals = self._compact(elements)
        self._length = length

        # Prepare the hash
        self._hash_value = None

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def from_iterable(cls, iterable: Iterable[int] | None = None) -> Self:
        """
        Create an instance from an iterable.

        Parameters
        ----------
        iterable
            An optional iterable of integers.

        Returns
        -------
        Self
            A new set.

        """
        if iterable is None:
            return cls()
        return cls._from_iterable(iterable)

    # pylint: disable=arguments-renamed
    @classmethod
    def _from_iterable(cls, iterable: Iterable[int]) -> Self:
        subset: MutableSet[int] = set()
        for item in iterable:
            subset.add(item)

        elements: list[int] = []
        if subset:
            last = -1
            for value in sorted(subset):
                if elements:
                    if value != last + 1:
                        elements.extend([last + 1, value])
                else:
                    elements.append(value)
                last = value
            elements.append(last + 1)

        result = cls()
        result._intervals = cls._compact(elements)
        result._length = len(subset)
        return result

    @classmethod
    def _compact(cls, elements: list[int]) -> MutableSequence[int]:
        # pylint: disable=too-many-return-statements
        # Transform the _intervals field by an array of adapted unsigned int
        if elements:
            if elements[0] < 0:
                if elements[0] >= -1 << 7 and elements[-1] < 1 << 7:
                    return array.array("b", elements)
                if elements[0] >= -1 << 15 and elements[-1] < 1 << 15:
                    return array.array("h", elements)
                if elements[0] >= -1 << 31 and elements[-1] < 1 << 31:
                    return array.array("l", elements)
                if elements[0] >= -1 << 63 and elements[-1] < 1 << 63:
                    return array.array("q", elements)
            if elements[-1] < 1 << 8:
                return array.array("B", elements)
            if elements[-1] < 1 << 16:
                return array.array("H", elements)
            if elements[-1] < 1 << 32:
                return array.array("L", elements)
            if elements[-1] < 1 << 64:
                return array.array("Q", elements)
            return elements
        return array.array("B")

    @classmethod
    def _create(cls, elements: Iterator[int]) -> Self:
        instance = cls()
        instance._intervals = cls._compact(list(elements))
        for index, item in enumerate(instance._intervals):
            if index % 2 == 0:
                instance._length -= item
            else:
                instance._length += item
        return instance

    @classmethod
    def _add(cls, elements: list[int], inf: int, sup: int) -> int:
        # Add an interval at the end
        if inf < sup:
            if elements:
                if sup > elements[-1]:
                    if inf <= elements[-1]:
                        increment = sup - elements[-1]
                        elements[-1] = sup
                        return increment
                    elements.extend([inf, sup])
                    return sup - inf
                return 0
            elements.extend([inf, sup])
            return sup - inf
        return 0

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            return other._length == self._length and other._intervals == self._intervals
        return super().__eq__(other)

    def __hash__(self) -> int:
        if self._hash_value is None:
            self._hash_value = hash(tuple(self._intervals))
        return self._hash_value

    def __copy__(self) -> Self:
        clone = self.__class__()
        clone._intervals = copy.copy(self._intervals)
        clone._length = self._length
        clone._hash_value = None
        return clone

    def copy(self) -> Self:
        """
        Compute a copy of this set.

        Returns
        -------
        Self
            A copy of this set.

        """
        return copy.copy(self)

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        if isinstance(item, SupportsInt):
            return bisect.bisect_right(self._intervals, int(item)) % 2 == 1
        return False

    def __len__(self) -> int:
        return self._length

    def __iter__(self) -> Iterator[int]:
        return itertools.chain.from_iterable(self.ranges())

    # Set comparison properties and methods

    def __ne__(self, other: object) -> bool:
        if isinstance(other, self.__class__):
            return other._length != self._length or other._intervals != self._intervals
        return super().__ne__(other)

    def __lt__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__):
            return self._length < other._length and self.__le__(other)
        return super().__lt__(other)

    def __le__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__):
            index = 0
            for inf, sup in self.intervals():
                index = bisect.bisect_right(other._intervals, inf, lo=index)
                if index % 2 == 0:
                    return False
                if sup > other._intervals[index]:
                    return False
                index -= 1
            return True
        return super().__le__(other)

    def __gt__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__):
            return self._length > other._length and self.__ge__(other)
        return super().__gt__(other)

    def __ge__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__):
            index = 0
            for inf, sup in other.intervals():
                index = bisect.bisect_right(self._intervals, inf, lo=index)
                if index % 2 == 0:
                    return False
                if sup > self._intervals[index]:
                    return False
                index -= 1
            return True
        return super().__ge__(other)

    def isdisjoint(self, other: Iterable[Any]) -> bool:
        """
        Test if the set is disjoint from the other.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the set is disjoint from the other.

        """
        if isinstance(other, self.__class__):
            intervals_self = self.intervals()
            intervals_other = other.intervals()
            try:
                inf_self, sup_self = next(intervals_self)
                inf_other, sup_other = next(intervals_other)
                while True:
                    if sup_self <= inf_other:
                        inf_self, sup_self = next(intervals_self)
                    elif sup_other <= inf_self:
                        inf_other, sup_other = next(intervals_other)
                    else:
                        return False
            except StopIteration:
                return True
        return super().isdisjoint(other)

    def issubset(self, other: Iterable[Any]) -> bool:
        """
        Test whether every element in the set is in other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        bool
            True if the set is a subset of the other.

        """
        if isinstance(other, self.__class__):
            return self <= other
        data = set(self)
        data.difference_update(other)
        return not data

    def issuperset(self, other: Iterable[Any]) -> bool:
        """
        Test whether every element in the other set is in the set.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        bool
            True if the set is a superset of the other.

        """
        if isinstance(other, self.__class__):
            return self >= other
        return all(element in self for element in other)

    # Set properties and methods

    def __and__(self, other: AbstractSet[Any]) -> Self:
        return self.intersection(other)

    def __or__(self, other: AbstractSet[Any]) -> Self:
        return self.union(other)

    def __sub__(self, other: AbstractSet[Any]) -> Self:
        return self.difference(other)

    def __xor__(self, other: AbstractSet[Any]) -> Self:
        return self.symmetric_difference(other)

    def union(self, *others: Iterable[int]) -> Self:
        """
        Compute the union of the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The union of the set and the others.

        """
        intervals = Intervals(self._intervals).union(
            *(
                Intervals(
                    (
                        other._intervals
                        if isinstance(other, self.__class__)
                        else self.from_iterable(other)._intervals
                    ),
                )
                for other in others
            ),
        )
        return self._create(iter(intervals))

    def intersection(self, *others: Iterable[Any]) -> Self:
        """
        Compute the intersection between the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The intersection between the set and the others

        Notes
        -----
        The intersection uses the de Morgan Laws and union operation.

        """
        intervals = Intervals(self._intervals, complement=True).union(
            *(
                Intervals(
                    (
                        other._intervals
                        if isinstance(other, self.__class__)
                        else self.from_iterable(other)._intervals
                    ),
                    complement=True,
                )
                for other in others
            ),
        )
        return self._create(iter(intervals))

    def difference(self, *others: Iterable[Any]) -> Self:
        """
        Compute the difference between the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The difference between the set and the others

        Notes
        -----
        The difference uses the de Morgan Laws and union operation.

        """
        intervals = Intervals(self._intervals, complement=True).union(
            *(
                Intervals(
                    (
                        other._intervals
                        if isinstance(other, self.__class__)
                        else self.from_iterable(other)._intervals
                    ),
                )
                for other in others
            ),
        )
        return self._create(iter(intervals))

    def symmetric_difference(self, other: Iterable[int]) -> Self:
        """
        Compute the symmetric difference between the set and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        Self
            The symmetric difference between the set and the other.

        Notes
        -----
        The symmetric_difference uses the de Morgan Laws and union operation.

        """
        intervals1 = Intervals(self._intervals, complement=True).union(
            Intervals(
                (
                    other._intervals
                    if isinstance(other, self.__class__)
                    else self.from_iterable(other)._intervals
                ),
            ),
        )
        intervals2 = Intervals(self._intervals).union(
            Intervals(
                (
                    other._intervals
                    if isinstance(other, self.__class__)
                    else self.from_iterable(other)._intervals
                ),
                complement=True,
            ),
        )
        intervals = Intervals(list(intervals1)).union(
            Intervals(list(intervals2)),
        )
        return self._create(iter(intervals))

    # IntegerSet properties and methods

    def intervals(self) -> Iterator[tuple[int, int]]:
        """
        Get an iterator over the intervals.

        Returns
        -------
        Iterator[tuple[int, int]]
            An iterator over the intervals.

        """
        iterator = iter(self._intervals)
        return zip(iterator, iterator, strict=False)

    def ranges(self) -> Iterator[range]:
        """
        Get an iterator over the ranges.

        Returns
        -------
        Iterator[range]
            An iterator over the ranges.

        """
        return itertools.starmap(range, self.intervals())


_T = TypeVar("_T")


class FiniteUniverse(AbstractSet[_T], Sequence[_T]):
    # noinspection PyUnresolvedReferences
    """
    It represents finite universes.

    Parameters
    ----------
    iterable
        An iterable of values.
    long
        A flag to indicate a long representation string

    Examples
    --------
    >>> from galactic.algebras.set import FiniteUniverse
    >>> universe = FiniteUniverse[str]("abc")
    >>> universe
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> list(universe)
    ['a', 'b', 'c']
    >>> list(reversed(universe))
    ['c', 'b', 'a']
    >>> universe.index('c')
    2
    >>> universe.count('c')
    1
    >>> universe.count('g')
    0
    >>> list(universe.whole)
    ['a', 'b', 'c']
    >>> [list(singleton) for singleton in universe.singletons()]
    [['a'], ['b'], ['c']]
    >>> [list(subset) for subset in universe.parts()]
    [[], ['a'], ['b'], ['c'], ['a', 'b'], ['a', 'c'], ['b', 'c'], ['a', 'b', 'c']]

    """

    __slots__ = ("_elements", "_index", "_hash_value", "_empty", "_whole")

    _elements: Sequence[_T]
    _index: OrderedDict[_T, int]
    _hash_value: int | None
    _empty: FiniteSubSet[_T]
    _whole: FiniteSubSet[_T]

    def __init__(self, iterable: Iterable[_T]) -> None:
        self._index = OrderedDict((key, count) for count, key in enumerate(iterable))
        self._elements = tuple(self._index)
        self._hash_value = None
        self._empty = FiniteSubSet[_T](self)
        if self._elements:
            # noinspection PyTypeChecker
            self._whole = FiniteSubSet[_T].from_intervals(
                self,
                [(0, len(self._elements))],
            )
        else:
            self._whole = self._empty

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __hash__(self) -> int:
        if self._hash_value is None:
            self._hash_value = hash(self._elements)
        return self._hash_value

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        try:
            return item in self._index
        except TypeError:
            return False

    def __len__(self) -> int:
        return len(self._index)

    def __iter__(self) -> Iterator[_T]:
        return iter(self._index)

    # Reversible properties and methods

    def __reversed__(self) -> Iterator[_T]:
        return reversed(self._index)

    # Sequence properties and methods

    @overload
    def __getitem__(self, value: int) -> _T: ...  # coverage: ignore

    @overload
    def __getitem__(self, value: slice) -> Sequence[_T]: ...  # coverage: ignore

    def __getitem__(self, value: Any) -> object:
        return self._elements[value]

    def index(
        self,
        value: _T,
        start: int | None = None,
        stop: int | None = None,
    ) -> int:
        """
        Get the index of a value.

        Parameters
        ----------
        value
            The value whose index is requested.
        start
            Where to start.
        stop
            Where to stop.

        Returns
        -------
        int
            The index requested.

        Raises
        ------
        ValueError
            If the value is not in the universe or not found in the interval.

        """
        try:
            index = self._index[value]
            if start is not None and index < int(start):
                raise ValueError(f"{value} is not found in the interval")
            if stop is not None and index >= int(stop):
                raise ValueError(f"{value} is not found in the interval")
        except KeyError as error:
            raise ValueError(f"{value} is not in this universe") from error
        return index

    def count(self, value: object) -> int:
        """
        Get the total number of occurrences of value.

        Parameters
        ----------
        value
            The value whose count is requested.

        Returns
        -------
        int
            1 if the value is in the universe, else 0.

        """
        if value in self._index:
            return 1
        return 0

    # FiniteUniverse properties and methods

    def _parts(self, length: int) -> Iterator[FiniteSubSet[_T]]:
        if length == 0:
            yield self._empty
        elif length == len(self):
            yield self._whole
        else:
            for part in itertools.combinations(self._elements, length):
                yield FiniteSubSet[_T](self, part)

    def parts(
        self,
        inf: int | None = None,
        sup: int | None = None,
    ) -> Iterator[FiniteSubSet[_T]]:
        """
        Get an iterator over the parts of this universe.

        Parameters
        ----------
        inf
            The lower cardinality of the produced parts.
        sup
            The upper cardinality of the produced parts.

        Returns
        -------
        Iterator[FiniteSubSet[_T]]
            An iterator over the selected parts of this universe

        """
        if inf is None:
            inf = 0
        if sup is None:
            sup = len(self)
        return itertools.chain.from_iterable(
            self._parts(cardinality) for cardinality in range(inf, sup + 1)
        )

    def singletons(self) -> Iterator[FiniteSubSet[_T]]:
        """
        Get an iterator over the singletons of this universe.

        Returns
        -------
        Iterator[FiniteSubSet[_T]]
            An iterator over the singletons of this universe

        """
        return self.parts(1, 1)

    @property
    def empty(self) -> FiniteSubSet[_T]:
        """
        Get the empty subset of this universe.

        Returns
        -------
        FiniteSubSet[_T]
            The empty subset.

        """
        return self._empty

    @property
    def whole(self) -> FiniteSubSet[_T]:
        """
        Get the whole subset of this universe.

        Returns
        -------
        FiniteSubSet[_T]
            The whole subset.

        """
        return self._whole


class FiniteSubSet(AbstractSet[_T]):
    # noinspection PyUnresolvedReferences
    """
    It represents subsets of a finite universe.

    Instances stores membership using a compact array of intervals represented by
    an array of int. For example, using the ``abcdef`` universe, the ``abde`` subset
    will be represented by ``[0,2,3,5]`` meaning:

    * from element ``0`` (included) to element ``2`` (excluded) of universe
    * from element ``3`` (included) to element ``5`` (excluded) of universe

    Parameters
    ----------
    universe
        A finite universe.
    elements
        An optional iterable.

    Examples
    --------
    >>> from galactic.algebras.set import FiniteUniverse, FiniteSubSet
    >>> from pprint import pprint
    >>> universe = FiniteUniverse[str]("abcdef")
    >>> universe
    <galactic.algebras.set.FiniteUniverse object at 0x...>
    >>> set1 = FiniteSubSet[str].from_intervals(universe, [(0, 2), (3, 5)])
    >>> set1
    <galactic.algebras.set.FiniteSubSet object at 0x...>
    >>> list(set1.universe)
    ['a', 'b', 'c', 'd', 'e', 'f']
    >>> len(set1)
    4
    >>> "a" in set1
    True
    >>> "f" in set1
    False
    >>> list(~set1)
    ['c', 'f']
    >>> set2 = FiniteSubSet[str](universe, "bdef")
    >>> set2
    <galactic.algebras.set.FiniteSubSet object at 0x...>
    >>> list(set2.intervals())
    [(1, 2), (3, 6)]
    >>> list(set1 | set2)
    ['a', 'b', 'd', 'e', 'f']
    >>> list((set1 | set2).intervals())
    [(0, 2), (3, 6)]
    >>> list(set1 & set2)
    ['b', 'd', 'e']
    >>> list(set1 ^ set2)
    ['a', 'f']
    >>> set1 & set2 <= set1
    True
    >>> set1 | set2 >= set1
    True
    >>> set1.isdisjoint(set2)
    False
    >>> pprint([list(subset) for subset in set1.subsets()])
    [[],
     ['a'],
     ['b'],
     ['d'],
     ['e'],
     ['a', 'b'],
     ['a', 'd'],
     ['a', 'e'],
     ['b', 'd'],
     ['b', 'e'],
     ['d', 'e'],
     ['a', 'b', 'd'],
     ['a', 'b', 'e'],
     ['a', 'd', 'e'],
     ['b', 'd', 'e'],
     ['a', 'b', 'd', 'e']]
    >>> pprint([list(superset) for superset in set1.supersets(strict=True)])
    [['a', 'b', 'c', 'd', 'e'],
     ['a', 'b', 'd', 'e', 'f'],
     ['a', 'b', 'c', 'd', 'e', 'f']]

    """

    __slots__ = ("_universe", "_integers", "_hash_value")

    _universe: FiniteUniverse[_T]
    _integers: IntegerSet
    _hash_value: int | None

    def __init__(
        self,
        universe: FiniteUniverse[_T],
        elements: Iterable[_T] | None = None,
    ) -> None:
        # Store the universe
        self._universe = universe

        # Prepare the hash
        self._hash_value = None

        # Fill the integers
        if elements is None:
            self._integers = IntegerSet()
        else:
            # noinspection PyTypeChecker
            self._integers = IntegerSet.from_iterable(
                universe.index(item) for item in elements
            )

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __copy__(self) -> Self:
        result = self.__class__(self._universe)
        result._integers = copy.copy(self._integers)
        result._hash_value = None
        return result

    def copy(self) -> Self:
        """
        Compute a copy of this set.

        Returns
        -------
        Self
            A copy of this set.

        """
        return copy.copy(self)

    # pylint: disable=arguments-differ
    def _from_iterable(self, iterable: Iterable[_T]) -> Self:
        return self.__class__(self._universe, iterable)

    # noinspection PyProtectedMember
    # pylint: disable=protected-access
    @classmethod
    def from_intervals(
        cls,
        universe: FiniteUniverse[_T],
        intervals: Iterable[IntegerCouple] | None = None,
    ) -> Self:
        """
        Create an instance.

        Parameters
        ----------
        universe
            A finite universe.
        intervals
            An optional iterable of intervals.

        Returns
        -------
        Self
            The new set.

        Raises
        ------
        ValueError
            If a mark is outside of the universe.

        """
        result = cls(universe)
        result._integers = IntegerSet(intervals)
        if result._integers._intervals[-1] > len(universe):
            raise ValueError("A mark is outside of the universe")
        if result._integers._intervals[0] < 0:
            raise ValueError("A mark is outside of the universe")
        return result

    def __hash__(self) -> int:
        if self._hash_value is None:
            self._hash_value = hash((self._universe, self._integers))
        return self._hash_value

    def __eq__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return other._integers == self._integers
        return super().__eq__(other)

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        try:
            return self._universe.index(item) in self._integers  # type: ignore[arg-type]
        except (ValueError, TypeError):
            return False

    def __len__(self) -> int:
        return len(self._integers)

    def __iter__(self) -> Iterator[_T]:
        return (self._universe[index] for index in self._integers)

    # Set comparison properties and methods

    def __ne__(self, other: object) -> bool:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return other._integers != self._integers
        return super().__ne__(other)

    def __lt__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers < other._integers
        return super().__lt__(other)

    def __le__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers <= other._integers
        return super().__le__(other)

    def __gt__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers > other._integers
        return super().__gt__(other)

    def __ge__(self, other: AbstractSet[object]) -> bool:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers >= other._integers
        return super().__ge__(other)

    def isdisjoint(self, other: Iterable[object]) -> bool:
        """
        Test if the set is disjoint from the other.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the set is disjoint from the other.

        """
        # pylint: disable=protected-access
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers.isdisjoint(other._integers)
        return super().isdisjoint(other)

    def issubset(self, other: Iterable[object]) -> bool:
        """
        Test if the set is a subset of the other.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the set is a subset of the other.

        """
        # pylint: disable=protected-access
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers.issubset(other._integers)
        data = set(self)
        data.difference_update(other)
        return not data

    def issuperset(self, other: Iterable[object]) -> bool:
        """
        Test if the set is a superset of the other.

        Parameters
        ----------
        other
            An iterable of values.

        Returns
        -------
        bool
            True if the set is a superset of the other.

        """
        # pylint: disable=protected-access
        if isinstance(other, self.__class__) and other._universe is self._universe:
            return self._integers.issuperset(other._integers)
        return not any(element not in self for element in other)

    # Set properties and methods

    def __or__(self, other: AbstractSet[Any]) -> Self:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            result = self.__class__(self._universe)
            result._integers = self._integers | other._integers
            return result
        return cast(Self, super().__or__(other))

    def __and__(self, other: AbstractSet[Any]) -> Self:
        if isinstance(other, self.__class__) and other._universe is self._universe:
            result = self.__class__(self._universe)
            result._integers = self._integers & other._integers
            return result
        return cast(Self, super().__and__(other))

    def __sub__(self, other: AbstractSet[Any]) -> Self:
        if isinstance(other, self.__class__):
            result = self.__class__(self._universe)
            result._integers = self._integers - other._integers
            return result
        return cast(Self, super().__sub__(other))

    def __xor__(self, other: AbstractSet[Any]) -> Self:
        if isinstance(other, self.__class__):
            result = self.__class__(self._universe)
            result._integers = self._integers ^ other._integers
            return result
        return cast(Self, super().__xor__(other))

    # noinspection PyProtectedMember
    def __invert__(self) -> Self:
        if len(self) == 0:
            return cast(Self, self._universe.whole)

        if len(self) == len(self._universe):
            return cast(Self, self._universe.empty)

        result = copy.copy(self)

        if result._integers._intervals[0] == 0:
            del result._integers._intervals[0]
        else:
            result._integers._intervals.insert(0, 0)
        if result._integers._intervals[-1] == len(self._universe):
            del result._integers._intervals[-1]
        else:
            result._integers._intervals.append(len(self._universe))
        result._integers._length = len(self._universe) - len(self)

        return result

    def union(self, *others: Iterable[_T]) -> Self:
        """
        Compute the union of the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The union of the set and the others.

        """
        if all(
            isinstance(other, self.__class__) and other._universe is self._universe
            for other in others
        ):
            result = self.__class__(self._universe)
            result._integers = self._integers.union(
                *(other._integers for other in others),  # type: ignore[attr-defined]
            )
            return result
        return self.__class__(
            self._universe,
            elements=itertools.chain(
                self,
                itertools.chain.from_iterable(others),
            ),
        )

    def intersection(self, *others: Iterable[Any]) -> Self:
        """
        Compute the intersection between the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The intersection between the set and the others

        """
        if all(
            isinstance(other, self.__class__) and other._universe is self._universe
            for other in others
        ):
            result = self.__class__(self._universe)
            result._integers = self._integers.intersection(
                *(other._integers for other in others),  # type: ignore[attr-defined]
            )
            return result
        elements = FIFOSet[_T](self)
        for other in others:
            data = FIFOSet[_T]()
            for element in other:
                if element in elements:
                    data.add(element)
            elements = data
        return self.__class__(self._universe, elements=elements)

    def difference(self, *others: Iterable[Any]) -> Self:
        """
        Compute the difference between the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The difference between the set and the others

        """
        if all(
            isinstance(other, self.__class__) and other._universe is self._universe
            for other in others
        ):
            result = self.__class__(self._universe)
            result._integers = self._integers.difference(
                *(other._integers for other in others),  # type: ignore[attr-defined]
            )
            return result
        elements = FIFOSet[_T](self)
        for other in others:
            for element in other:
                elements.discard(element)
        return self.__class__(self._universe, elements=elements)

    def symmetric_difference(self, other: Iterable[_T]) -> Self:
        """
        Compute the symmetric difference between the set and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        Self
            The symmetric difference between the set and the other.

        """
        if isinstance(other, self.__class__) and other._universe is self._universe:
            result = self.__class__(self._universe)
            result._integers = self._integers.symmetric_difference(other._integers)
            return result
        elements = FIFOSet[_T](self)
        for element in other:
            if element in elements:
                elements.remove(element)
            else:
                elements.add(element)
        return self.__class__(self._universe, elements=elements)

    # FiniteSubset properties and methods

    def intervals(self) -> Iterator[tuple[int, int]]:
        """
        Get an iterator over the intervals.

        Returns
        -------
        Iterator[tuple[int, int]]
            An iterator over the intervals.

        """
        return self._integers.intervals()

    def ranges(self) -> Iterator[range]:
        """
        Get an iterator over the ranges.

        Returns
        -------
        Iterator[range]
            An iterator over the ranges.

        """
        return self._integers.ranges()

    def subsets(self, strict: bool | None = None) -> Iterator[Self]:
        """
        Get an iterator over the subset of this set (including itself).

        Parameters
        ----------
        strict
            Are the subsets strict?

        Returns
        -------
        Iterator[Self]
            An iterator over the subsets.

        """
        return (
            self.__class__(self._universe, elements)
            for elements in itertools.chain.from_iterable(
                itertools.combinations(self, length)
                for length in range(len(self) + (1 - int(bool(strict))))
            )
        )

    def supersets(self, strict: bool | None = None) -> Iterator[Self]:
        """
        Get an iterator over the supersets of this set (including itself).

        Parameters
        ----------
        strict
            Are the subsets strict?

        Returns
        -------
        Iterator[Self]
            An iterator over the supersets.

        """
        complement = ~self
        return (
            self.__class__(self._universe, itertools.chain(self, elements))
            for elements in itertools.chain.from_iterable(
                itertools.combinations(complement, length)
                for length in range(int(bool(strict)), len(self._universe) + 1)
            )
        )

    @property
    def universe(self) -> FiniteUniverse[_T]:
        """
        Get the underlying universe.

        Returns
        -------
        FiniteUniverse[_T]
            The underlying universe.

        """
        return self._universe
