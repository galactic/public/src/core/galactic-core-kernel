"""
The :mod:`galactic.algebras.set` module.

It defines several set classes:

* :class:`FrozenFIFOSet` for creating frozen FIFO sets;
* :class:`FIFOSet` for creating FIFO sets;
* :class:`IntegerSet` for creating compact sets of integers;
* :class:`FiniteUniverse` for creating finite universes;
* :class:`FiniteSubSet` for creating subset of finite universes;
"""

__all__ = (
    "FrozenFIFOSet",
    "FIFOSet",
    "IntegerSet",
    "FiniteUniverse",
    "FiniteSubSet",
)

from ._fifo import FIFOSet, FrozenFIFOSet
from ._finite import FiniteSubSet, FiniteUniverse, IntegerSet
