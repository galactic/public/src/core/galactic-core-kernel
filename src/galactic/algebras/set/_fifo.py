"""The :mod:`_fifo` module."""

from __future__ import annotations

import contextlib
import copy
import itertools
from collections import OrderedDict
from collections.abc import (
    Iterable,
    Iterator,
    MutableSet,
)
from collections.abc import (
    Set as AbstractSet,
)
from typing import Any, TypeVar, cast

from typing_extensions import Self

from galactic.helpers.core import default_repr

_T = TypeVar("_T")


class FrozenFIFOSet(AbstractSet[_T]):
    """
    It represents frozen sets that keep the FIFO order.

    Initialise an instance using 0 or 1 argument that should be iterable.

    Parameters
    ----------
    *args
        An iterable.

    Raises
    ------
    TypeError
        If two or more arguments are given.
    TypeError
        If the argument is not iterable.

    Examples
    --------
    >>> from galactic.algebras.set import FrozenFIFOSet
    >>> a = FrozenFIFOSet[int]([3, 5, 1, 2, 6, 2])
    >>> a
    <galactic.algebras.set.FrozenFIFOSet object at 0x...>
    >>> list(a)
    [3, 5, 1, 2, 6]
    >>> list(a | FrozenFIFOSet[int]([1, 5, 7 ,8, 9]))
    [3, 5, 1, 2, 6, 7, 8, 9]

    """

    __slots__ = ("_data", "_hash_value")

    _data: OrderedDict[_T, None]
    _hash_value: int | None

    def __init__(self, *args: Iterable[_T]) -> None:
        if len(args) > 1:
            raise TypeError(
                f"{self.__class__.__name__} expected at most 1 arguments, "
                f"got {len(args)}",
            )
        if len(args) == 1:
            self._data = OrderedDict((value, None) for value in args[0])
        else:
            self._data = OrderedDict()
        self._hash_value = None

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __hash__(self) -> int:
        if self._hash_value is None:
            self._hash_value = self._hash()
        return self._hash_value

    def __copy__(self) -> Self:
        clone = self.__class__()
        clone._data = copy.copy(self._data)
        return clone

    def copy(self) -> Self:
        """
        Compute a copy of this set.

        Returns
        -------
        Self
            A copy of this set.

        """
        return self.__class__(self)

    # Collection properties and methods

    def __contains__(self, item: object) -> bool:
        try:
            return item in self._data
        except TypeError:
            return False

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[_T]:
        return iter(self._data)

    @property
    def head(self) -> _T:
        """
        Return the first value of the set.

        Returns
        -------
        _T
            The first value.

        Raises
        ------
        KeyError
            If the set is empty.

        """
        try:
            return next(itertools.islice(self._data.keys(), 1))
        except StopIteration as error:
            raise KeyError from error

    @property
    def tail(self) -> _T:
        """
        Return the last value of the set.

        Returns
        -------
        _T
            The last value.

        Raises
        ------
        KeyError
            If the set is empty.

        """
        try:
            return next(itertools.islice(reversed(self._data.keys()), 1))
        except StopIteration as error:
            raise KeyError from error

    # Reversible properties and methods

    def __reversed__(self) -> Iterator[_T]:
        return reversed(self._data)

    # Set properties and methods

    def isdisjoint(self, other: Iterable[Any]) -> bool:
        """
        Test if the set is disjoint from the other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        bool
            True if the set is disjoint from the other.

        """
        return all(element not in self._data for element in other)

    def issubset(self, other: Iterable[Any]) -> bool:
        """
        Test whether every element in the set is in other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        bool
            True if the set is a subset of the other.

        """
        data = set(self._data)
        data.difference_update(other)
        return not data

    def issuperset(self, other: Iterable[Any]) -> bool:
        """
        Test whether every element in the other set is in the set.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        bool
            True if the set is a superset of the other.

        """
        return all(element in self._data for element in other)

    def union(self, *others: Iterable[_T]) -> Self:
        """
        Compute the union of the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The union of the set and the others.

        """
        result: Self = self.copy()
        for other in others:
            for element in other:
                result._data[element] = None  # pylint: disable=protected-access
        return result

    def intersection(self, *others: Iterable[Any]) -> Self:
        """
        Compute the intersection between the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The intersection between the set and the others

        """
        result = self.copy()
        for other in others:
            data: OrderedDict[_T, None] = OrderedDict()
            for element in other:
                if element in result._data:  # pylint: disable=protected-access
                    data[element] = None
            result._data = data  # pylint: disable=protected-access
        return result

    def difference(self, *others: Iterable[Any]) -> Self:
        """
        Compute the difference between the set and the others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        Returns
        -------
        Self
            The difference between the set and the others

        """
        result: Self = self.copy()
        for other in others:
            for element in other:
                with contextlib.suppress(KeyError):
                    del result._data[element]  # pylint: disable=protected-access
        return result

    def symmetric_difference(self, other: Iterable[_T]) -> Self:
        """
        Compute the symmetric difference between the set and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        Returns
        -------
        Self
            The symmetric difference between the set and the other.

        """
        result: Self = self.copy()
        for element in other:
            if element in result._data:  # pylint: disable=protected-access
                del result._data[element]  # pylint: disable=protected-access
            else:
                result._data[element] = None  # pylint: disable=protected-access
        return result


class FIFOSet(FrozenFIFOSet[_T], MutableSet[_T]):
    """
    it represents mutable sets that keep the FIFO order.

    Examples
    --------
    >>> from galactic.algebras.set import FIFOSet
    >>> a = FIFOSet[int]([3, 5, 1, 2, 6, 2])
    >>> a
    <galactic.algebras.set.FIFOSet object at 0x...>
    >>> list(a)
    [3, 5, 1, 2, 6]
    >>> list(a | FIFOSet[int]([1, 5, 7 ,8, 9]))
    [3, 5, 1, 2, 6, 7, 8, 9]

    """

    __hash__ = None  # type: ignore[assignment]

    # MutableSet properties and methods

    def add(self, value: _T) -> None:
        """
        Add the value to the set.

        Parameters
        ----------
        value
            The value to add.

        """
        self.additem(value)

    def remove(self, value: _T) -> None:
        """
        Remove value from the set if it is present.

        Parameters
        ----------
        value
            The value to remove.

        """
        del self._data[value]

    def discard(self, value: _T) -> None:
        """
        Discard value from the set if it is present.

        Parameters
        ----------
        value
            The value to remove.

        """
        with contextlib.suppress(KeyError):
            self.remove(value)

    def pop(self) -> _T:
        """
        Remove and return the first value of the set.

        Returns
        -------
        _T
            The value removed.

        """
        return self.popitem()

    def clear(self) -> None:
        """
        Clear the set.
        """
        self._data.clear()

    # FIFOSet properties and methods

    def update(self, *others: Iterable[_T]) -> None:
        """
        Update a set with the union of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            for element in other:
                self._data[element] = None

    def intersection_update(self, *others: Iterable[Any]) -> None:
        """
        Update a set with the intersection of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            data: OrderedDict[_T, None] = OrderedDict()
            for element in other:
                if element in self._data:
                    data[element] = None
            self._data = data

    def difference_update(self, *others: Iterable[Any]) -> None:
        """
        Update a set with the difference of itself and others.

        Parameters
        ----------
        *others
            A sequence of iterable.

        """
        for other in others:
            for element in other:
                with contextlib.suppress(KeyError):
                    del self._data[element]

    def symmetric_difference_update(self, other: Iterable[_T]) -> None:
        """
        Update a set with the symmetric difference of itself and the other.

        Parameters
        ----------
        other
            An iterable of elements.

        """
        for element in other:
            if element in self._data:
                del self._data[element]
            else:
                self._data[element] = None

    def move_to_end(self, value: _T, last: bool = True) -> None:
        """
        Move an existing value to either end of the set.

        The value is moved to the right end if last is True (the
        default) or to the beginning if last is False.

        Parameters
        ----------
        value
            The value to move
        last
            Where to move

        """
        self._data.move_to_end(value, last=last)

    def additem(self, value: _T, last: bool = True) -> None:
        """
        Add value to the set either at the beginning or at the end.

        Parameters
        ----------
        value
            The value to add.
        last
            Where to add.

        """
        if value not in self._data:
            self._data[value] = None
            self._data.move_to_end(value, last)

    def popitem(self, last: bool = False) -> _T:
        """
        Remove and return an existing value from either end of the set.

        Parameters
        ----------
        last
            Does the remove take place at the end or the beginning?

        Returns
        -------
        _T
            The value removed.

        Raises
        ------
        KeyError
            if the set is empty.

        """
        try:
            (key, _) = self._data.popitem(last=last)
        except KeyError as error:
            raise KeyError from error
        return key
