"""The :mod:`_algebras` module."""

from __future__ import annotations

from collections import OrderedDict
from typing import SupportsInt

from typing_extensions import Self

from galactic.algebras.lattice import Element

from ._helper import factors, gcd, lcm


class PrimeFactors(Element):
    r"""
    It represents a partially ordered relation between integers.

    :math:`a \leq b`: is equivalent to :math:`a` a divisor of :math:`b`.

    Parameters
    ----------
    value
        An integer value.

    Raises
    ------
    ValueError
        If the value is not a positive number.

    """

    __slots__ = ("_value",)

    _value: int | None

    def __init__(self, value: SupportsInt | None = None) -> None:
        super().__init__()
        if value is None:
            self._value = None
        else:
            value = int(value)
            if value < 0:
                raise ValueError(f"{value} must be a positive number")
            self._value = value

    @property
    def value(self) -> int | None:
        """
        Get the value of the integer.

        Returns
        -------
        int | None
            The integer value

        """
        return self._value

    def __hash__(self) -> int:
        return hash(self._value)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self._value == other.value

    def __ne__(self, other: object) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self._value != other.value

    def __lt__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return (
            self.value != 0
            and self.value is not None
            and other.value != self.value
            and other.value % self.value == 0  # type: ignore[operator]
        )

    def __gt__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return (
            other.value != 0
            and self.value is not None
            and other.value != self.value
            and self._value % other.value == 0  # type: ignore[operator]
        )

    def __le__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self == other or self < other

    def __ge__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self == other or self > other

    def __and__(self, other: Self) -> Self:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.__class__(gcd(self.value, other.value))

    def __or__(self, other: Self) -> Self:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.__class__(lcm(self.value, other.value))

    def __int__(self) -> int | None:
        return self._value

    def __str__(self) -> str:
        return str(self._value)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self._value})"

    def latex(self) -> str:
        """
        Get a LateX representation of a number using a prime number decomposition.

        Returns
        -------
        str
            A LaTeX representation

        """
        if self._value == 0:
            return "$0$"
        if self._value == 1:
            return "$1$"
        elements: OrderedDict[int | None, int] = OrderedDict()
        for item in factors(self._value):
            if item in elements:
                elements[item] += 1
            else:
                elements[item] = 1
        powers = (
            f"{number}^{power}" if power > 1 else str(number)
            for number, power in elements.items()
        )
        return f"${self._value}={'·'.join(powers)}$"

    def _repr_latex_(self) -> str:
        return self.latex()
