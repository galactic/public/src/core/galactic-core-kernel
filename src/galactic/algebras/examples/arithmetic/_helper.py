"""The :mod:`_helper` module."""

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Iterator


def gcd(*elements: int | None) -> int | None:
    """
    Compute the greatest common divisor of a collection of integers.

    Parameters
    ----------
    *elements
        A sequence of integers

    Returns
    -------
    int | None
        The greatest common divisor of args

    Note
    ----
    * The greatest common divisor of 0 and 0 is computed as 0.
    * The greatest common divisor of an empty set is computed as 1.

    Raises
    ------
    ValueError
        If a value cannot be converted to an integer or if the converted
        value is less than 0.

    """
    result = 0
    for value in elements:
        if value is None:
            return None
        integer = int(value)
        if integer < 0:
            raise ValueError
        while integer > 0:
            result, integer = integer, result % integer
    return result


def lcm(*elements: int | None) -> int | None:
    """
    Compute the least common multiple of a collection of integers.

    Parameters
    ----------
    *elements
        A sequence of integers

    Returns
    -------
    int | None
        The least common multiple of args

    Note
    ----
    The least common multiple of an empty set is computed as 1.

    Raises
    ------
    ValueError
        If a value cannot be converted to an integer or if the converted
        value is less than 0.

    """
    result = 1
    for value in elements:
        if value is None:
            return None
        integer = int(value)
        if integer < 0:
            raise ValueError
        if integer == 0:
            return 0
        result *= integer // gcd(result, integer)  # type: ignore[operator]
    return result


def factors(number: int | None = None) -> Iterator[int]:
    """
    Compute the prime factors of an integer.

    Parameters
    ----------
    number
        The number for which decomposition is requested

    Yields
    ------
    int
        A prime factor of the number.

    Raises
    ------
    ValueError
        If the number is negative

    Notes
    -----
    * 0 is yielded if the number is 0.
    * nothing is yielded is the number is None or 1
    """
    if number is not None:
        if number < 0:
            raise ValueError(f"{number} is negative")
        if number == 0:
            yield 0
        else:
            divisor = 2
            while number != 1:
                if number % divisor == 0:
                    yield divisor
                    number //= divisor
                elif divisor * divisor > number:
                    yield number
                    number = 1
                else:
                    divisor += 1
