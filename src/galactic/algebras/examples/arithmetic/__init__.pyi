from typing import TYPE_CHECKING, SupportsInt

from galactic.algebras.lattice import Element

if TYPE_CHECKING:
    from collections.abc import Iterator

    from typing_extensions import Self

class PrimeFactors(Element):
    def __init__(self, value: SupportsInt | None = None) -> None: ...
    def __lt__(self, other: Self) -> bool: ...
    def __gt__(self, other: Self) -> bool: ...
    def __le__(self, other: Self) -> bool: ...
    def __ge__(self, other: Self) -> bool: ...
    def __and__(self, other: Self) -> Self: ...
    def __or__(self, other: Self) -> Self: ...
    def __int__(self) -> int | None: ...
    @property
    def value(self) -> int | None: ...
    def latex(self) -> str: ...

def gcd(*args: int | None) -> int | None: ...
def lcm(*args: int | None) -> int | None: ...
def factors(number: int | None = None) -> Iterator[int]: ...
