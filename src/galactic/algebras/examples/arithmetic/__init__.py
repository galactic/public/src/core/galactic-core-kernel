"""
The :mod:`galactic.algebras.examples.arithmetic` module.

It defines several functions:

* :func:`gcd` See https://en.wikipedia.org/wiki/Greatest_common_divisor;
* :func:`lcm` See https://en.wikipedia.org/wiki/Least_common_multiple;
* :func:`factors` to get the prime factors of a number.

and one classe:

* :class:`PrimeFactors`;
"""

__all__ = (
    "gcd",
    "lcm",
    "factors",
    "PrimeFactors",
)

from ._algebras import PrimeFactors
from ._helper import factors, gcd, lcm
