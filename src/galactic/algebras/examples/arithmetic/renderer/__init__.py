"""
The :mod:`galactic.algebras.examples.arithmetic.renderer` module.

It define a class for renderering elements:

* :class:`PrimeFactorsRenderer`.
"""

__all__ = ("PrimeFactorsRenderer",)

from ._main import PrimeFactorsRenderer
