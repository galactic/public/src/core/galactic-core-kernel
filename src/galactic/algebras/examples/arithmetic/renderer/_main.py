"""The :mod:`_main` module."""

from __future__ import annotations

import re
from typing import TYPE_CHECKING

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.relational.renderer import CellRenderer, NodeRenderer

if TYPE_CHECKING:
    from collections.abc import Collection


# pylint: disable=too-many-instance-attributes
class PrimeFactorsRenderer(
    NodeRenderer[PrimeFactors, PrimeFactors],
    CellRenderer[PrimeFactors],
):
    """
    It renders prime factors.

    * graphviz node attributes when rendering a Hasse Diagram
    * header and row title when rendering a reduced context

    Parameters
    ----------
    meet_irreducible_color
        The meet irreducible color.
    join_irreducible_color
        The join irreducible color.
    meet_semi_lattice
        Is it displayed in a meet semi-lattice context?
    join_semi_lattice
        Is it displayed in a join semi-lattice context?
    meet_irreducible
        Is it displayed as a meet irreducible?
    join_irreducible
        Is it displayed as a join irreducible?

    """

    __slots__ = (
        "_meet_semi_lattice",
        "_join_semi_lattice",
        "_meet_irreducible",
        "_join_irreducible",
        "_meet_irreducible_color",
        "_join_irreducible_color",
    )

    _meet_semi_lattice: bool
    _join_semi_lattice: bool
    _meet_irreducible: bool
    _join_irreducible: bool
    _meet_irreducible_color: str
    _join_irreducible_color: str

    _regex = re.compile(r"^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$")

    # pylint: disable=too-many-arguments, too-many-positional-arguments
    def __init__(
        self,
        meet_irreducible_color: str = "#fcaf3e",
        join_irreducible_color: str = "#8ae234",
        meet_semi_lattice: bool = False,
        join_semi_lattice: bool = False,
        meet_irreducible: bool = False,
        join_irreducible: bool = False,
    ) -> None:
        self.meet_irreducible_color = meet_irreducible_color
        self.join_irreducible_color = join_irreducible_color
        self.meet_semi_lattice = meet_semi_lattice
        self.join_semi_lattice = join_semi_lattice
        self.meet_irreducible = meet_irreducible
        self.join_irreducible = join_irreducible

    # pylint: disable=unused-argument
    def render(self, element: PrimeFactors, index: int) -> str:
        """
        Render a prime factors for a cell element.

        Parameters
        ----------
        element
            The prime factors to render.
        index
            The element index.

        Returns
        -------
        str
            The markdown representation of a prime factors.

        """
        if self._meet_irreducible:
            return str(element)
        return f"{element.latex()} "

    # pylint: disable=too-many-positional-arguments
    def attributes(
        self,
        element: PrimeFactors,
        index: int | None = None,
        current: bool = False,
        successors: Collection[PrimeFactors] | None = None,
        predecessors: Collection[PrimeFactors] | None = None,
    ) -> dict[str, str]:
        """
        Produce a dictionary of graphviz attributes for a node.

        Parameters
        ----------
        element
            The prime factors to render.
        index
            The element index.
        current
            Is `element` the current element?
        successors
            The successors of `element`.
        predecessors
            The predecessors of `element`.

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes.

        """
        if self._meet_irreducible:
            return {
                "shape": "circle",
                "label": str(element),
                "style": "filled",
                "fillcolor": self._meet_irreducible_color,
            }
        if self._join_irreducible:
            return {
                "shape": "circle",
                "label": str(element),
                "style": "filled",
                "fillcolor": self._join_irreducible_color,
            }

        if self._meet_semi_lattice and self._join_semi_lattice:
            is_join_irreducible = predecessors is not None and len(predecessors) == 1
            is_meet_irreducible = successors is not None and len(successors) == 1
        elif self._meet_semi_lattice:
            is_join_irreducible = False
            is_meet_irreducible = (
                successors is not None
                and predecessors is not None
                and (
                    len(successors) == 1
                    or (len(successors) == 0 and len(predecessors) != 0)
                )
            )
        elif self._join_semi_lattice:
            is_join_irreducible = (
                successors is not None
                and predecessors is not None
                and (
                    len(predecessors) == 1
                    or (len(predecessors) == 0 and len(successors) != 0)
                )
            )
            is_meet_irreducible = False
        else:
            is_join_irreducible = False
            is_meet_irreducible = False

        if is_join_irreducible:
            if is_meet_irreducible:
                fillcolor = (
                    f"{self._join_irreducible_color};0.5:{self._meet_irreducible_color}"
                )
                return {
                    "shape": "circle",
                    "label": str(element),
                    "style": "filled",
                    "fillcolor": fillcolor,
                    "gradientangle": "90",
                }
            return {
                "shape": "circle",
                "label": str(element),
                "style": "filled",
                "fillcolor": self._join_irreducible_color,
            }
        if is_meet_irreducible:
            return {
                "shape": "circle",
                "label": str(element),
                "style": "filled",
                "fillcolor": self._meet_irreducible_color,
            }
        return super().attributes(
            element=element,
            index=index,
            current=current,
            successors=successors,
            predecessors=predecessors,
        )

    @property
    def meet_irreducible_color(self) -> str:
        """
        Get or set the meet irreducible color.

        Returns
        -------
        str
           The meet irreducible color.

        """
        return self._meet_irreducible_color

    @meet_irreducible_color.setter
    def meet_irreducible_color(self, value: str) -> None:
        """
        Set the meet irreducible color.

        Parameters
        ----------
        value
            The new meet irreducible color.

        Raises
        ------
        ValueError
            If the value is not a valid color.

        """
        if not self._regex.match(value):
            raise ValueError(f"{value} is not a valid color")
        self._meet_irreducible_color = value

    @property
    def join_irreducible_color(self) -> str:
        """
        Get or set the join irreducible color.

        Returns
        -------
        str
           The join irreducible color.

        """
        return self._join_irreducible_color

    @join_irreducible_color.setter
    def join_irreducible_color(self, value: str) -> None:
        """
        Set the join irreducible color.

        Parameters
        ----------
        value
            The new join irreducible color.

        Raises
        ------
        ValueError
            If the value is not a valid color.

        """
        if not self._regex.match(value):
            raise ValueError(f"{value} is not a valid color")
        self._join_irreducible_color = value

    @property
    def meet_semi_lattice(self) -> bool:
        """
        Get or set the meet semi-lattice mode.

        Returns
        -------
        bool
            If the display is in meet semi-lattice mode.

        """
        return self._meet_semi_lattice

    @meet_semi_lattice.setter
    def meet_semi_lattice(self, value: bool) -> None:
        """
        Set the meet semi-lattice mode.

        Parameters
        ----------
        value
            The new meet semi-lattice mode.

        """
        self._meet_semi_lattice = value

    @property
    def join_semi_lattice(self) -> bool:
        """
        Get or set the join semi-lattice mode.

        Returns
        -------
        bool
            If the display is in join semi-lattice mode.

        """
        return self._join_semi_lattice

    @join_semi_lattice.setter
    def join_semi_lattice(self, value: bool) -> None:
        """
        Set the join semi-lattice mode.

        Parameters
        ----------
        value
            The new join semi-lattice mode.

        """
        self._join_semi_lattice = value

    @property
    def meet_irreducible(self) -> bool:
        """
        Get or set the meet irreducible mode.

        Returns
        -------
        bool
            If the display is in meet irreducible mode.

        """
        return self._meet_irreducible

    @meet_irreducible.setter
    def meet_irreducible(self, value: bool) -> None:
        """
        Set the meet irreducible mode.

        Parameters
        ----------
        value
            The new meet irreducible mode.

        """
        self._meet_irreducible = value

    @property
    def join_irreducible(self) -> bool:
        """
        Get or set the join irreducible mode.

        Returns
        -------
        bool
            If the display is in join irreducible mode.

        """
        return self._join_irreducible

    @join_irreducible.setter
    def join_irreducible(self, value: bool) -> None:
        """
        Set the join irreducible mode.

        Parameters
        ----------
        value
            The new join irreducible mode.

        """
        self._join_irreducible = value
