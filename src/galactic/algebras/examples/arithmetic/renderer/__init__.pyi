from typing import TYPE_CHECKING

from galactic.algebras.examples.arithmetic import PrimeFactors
from galactic.algebras.relational.renderer import CellRenderer, NodeRenderer

if TYPE_CHECKING:
    from collections.abc import Collection

# noinspection PyUnresolvedReferences
class PrimeFactorsRenderer(
    NodeRenderer[PrimeFactors, PrimeFactors],
    CellRenderer[PrimeFactors],
):
    def __init__(
        self,
        meet_irreducible_color: str = "#fcaf3e",
        join_irreducible_color: str = "#8ae234",
        meet_semi_lattice: bool = False,
        join_semi_lattice: bool = False,
        meet_irreducible: bool = False,
        join_irreducible: bool = False,
    ) -> None: ...
    def render(self, element: PrimeFactors, index: int) -> str: ...
    def attributes(
        self,
        element: PrimeFactors,
        index: int | None = None,
        current: bool = False,
        successors: Collection[PrimeFactors] | None = None,
        predecessors: Collection[PrimeFactors] | None = None,
    ) -> dict[str, str]: ...
    @property
    def meet_irreducible_color(self) -> str: ...
    @meet_irreducible_color.setter
    def meet_irreducible_color(self, value: str) -> None: ...
    @property
    def join_irreducible_color(self) -> str: ...
    @join_irreducible_color.setter
    def join_irreducible_color(self, value: str) -> None: ...
    @property
    def meet_semi_lattice(self) -> bool: ...
    @meet_semi_lattice.setter
    def meet_semi_lattice(self, value: bool) -> None: ...
    @property
    def join_semi_lattice(self) -> bool: ...
    @join_semi_lattice.setter
    def join_semi_lattice(self, value: bool) -> None: ...
    @property
    def meet_irreducible(self) -> bool: ...
    @meet_irreducible.setter
    def meet_irreducible(self, value: bool) -> None: ...
    @property
    def join_irreducible(self) -> bool: ...
    @join_irreducible.setter
    def join_irreducible(self, value: bool) -> None: ...
