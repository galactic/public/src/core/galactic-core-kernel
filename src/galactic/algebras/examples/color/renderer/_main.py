"""The :mod:`_main` module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from galactic.algebras.examples.color import Color

if TYPE_CHECKING:
    from collections.abc import Collection


# pylint: disable=too-few-public-methods
class ColorRenderer:
    """
    It renders graphviz node and edge attributes.
    """

    __slots__ = ()

    # pylint: disable=too-many-arguments, unused-argument, too-many-positional-arguments
    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def attributes(
        self,
        element: Color,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Color] | None = None,
        predecessors: Collection[Color] | None = None,
    ) -> dict[str, str]:
        """
        Produce a dictionary of graphviz attributes for a node.

        Parameters
        ----------
        element
            The color to render.
        index
            The element index.
        current
            Is `element` the current element?
        successors
            The successors of the `element`.
        predecessors
            The predecessors of the `element`.

        Returns
        -------
        dict[str, str]
            A dictionary of graphviz attributes.

        """
        return {
            "shape": "circle",
            "label": "",
            "style": "filled",
            "fillcolor": element.hexadecimal(),
        }
