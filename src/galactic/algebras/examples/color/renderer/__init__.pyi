from typing import TYPE_CHECKING

from galactic.algebras.examples.color import Color

if TYPE_CHECKING:
    from collections.abc import Collection

class ColorRenderer:
    def attributes(
        self,
        element: Color,
        index: int | None = None,
        current: bool = False,
        successors: Collection[Color] | None = None,
        predecessors: Collection[Color] | None = None,
    ) -> dict[str, str]: ...
