"""
The :mod:`galactic.algebras.examples.color.renderer` module.

It defines a class for rendering elements:

* :class:`ColorRenderer` for rendering colors into jupyter notebooks.
"""

__all__ = ("ColorRenderer",)

from ._main import ColorRenderer
