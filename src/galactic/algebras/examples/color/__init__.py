"""
The :mod:`galactic.algebras.examples.color` module defines several classes.

* :class:`Color` which represents partially comparable colors;
"""

__all__ = ("Color",)

from ._algebras import Color
