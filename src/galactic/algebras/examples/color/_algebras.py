"""The :mod:`_algebras` module."""

from __future__ import annotations

from typing import SupportsFloat

from typing_extensions import Self

from galactic.algebras.lattice import Element


# pylint: disable=too-many-ancestors
class Color(Element):
    r"""
    It implements a partially ordered relation between colors.

    :math:`(r_a,g_a,b_a) \leq (r_b,g_b,b_b)`: is equivalent to
    :math:`r_a\leq r_b \wedge g_a\leq g_b \wedge b_a\leq b_b`.

    Parameters
    ----------
    red
        The red color.
    green
        The green color.
    blue
        The blue color.

    Raises
    ------
    ValueError
        If a color is not between 0 and 1.

    """

    __slots__ = ("_red", "_green", "_blue")

    def __init__(
        self,
        red: SupportsFloat = 0.0,
        green: SupportsFloat = 0.0,
        blue: SupportsFloat = 0.0,
    ) -> None:
        super().__init__()
        red = float(red)
        if red < 0 or red > 1:
            raise ValueError("The red value must be between 0 and 1")
        self._red = red

        green = float(green)
        if green < 0 or green > 1:
            raise ValueError("The green value must be between 0 and 1")
        self._green = green

        blue = float(blue)
        if blue < 0 or blue > 1:
            raise ValueError("The blue value must be between 0 and 1")
        self._blue = blue

    @property
    def red(self) -> float:
        """
        Get the red value of the color.

        Returns
        -------
        float
            The red value.

        """
        return self._red

    @property
    def green(self) -> float:
        """
        Get the green value of the color.

        Returns
        -------
        float
            The green value.

        """
        return self._green

    @property
    def blue(self) -> float:
        """
        Get the blue value of the color.

        Returns
        -------
        float
            The blue value.

        """
        return self._blue

    def __hash__(self) -> int:
        return hash((self._red, self._green, self._blue))

    def __and__(self, other: Self) -> Self:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.__class__(
            red=min(self._red, other.red),
            green=min(self._green, other.green),
            blue=min(self._blue, other.blue),
        )

    def __or__(self, other: Self) -> Self:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.__class__(
            red=max(self._red, other.red),
            green=max(self._green, other.green),
            blue=max(self._blue, other.blue),
        )

    def __le__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return (
            self._red <= other.red
            and self._green <= other.green
            and self._blue <= other.blue
        )

    def __gt__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self >= other and self != other

    def __lt__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self <= other and self != other

    def __ge__(self, other: Self) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return (
            self._red >= other.red
            and self._green >= other.green
            and self._blue >= other.blue
        )

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return (
            self._red == other.red
            and self._green == other.green
            and self._blue == other.blue
        )

    def __ne__(self, other: object) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return (
            self._red != other.red
            or self._green != other.green
            or self._blue != other.blue
        )

    def __str__(self) -> str:
        return str((self._red, self._green, self._blue))

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            f"(red={self._red}, green={self._green}, blue={self._blue})"
        )

    def _repr_svg_(self) -> str:
        return (
            "<svg "
            'width="30" '
            'height="30"  '
            'viewBox="0 0 30 30" '
            'xmlns="http://www.w3.org/2000/svg">'
            '<rect width="30" height="30" '
            f'style="fill:{self.hexadecimal()};stroke:#000000;stroke-width:1" />'
            "</svg>"
        )

    def hexadecimal(self) -> str:
        """
        Convert a color into HTML notation.

        Returns
        -------
        str
            The HTML color notation.

        """
        return (
            f"#"
            f"{format(int(self._red * 255), '02X')}"
            f"{format(int(self._green * 255), '02X')}"
            f"{format(int(self._blue * 255), '02X')}"
        )
