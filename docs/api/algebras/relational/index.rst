Relation
========

Classes and functions
---------------------

.. automodule:: galactic.algebras.relational
    :members:
    :inherited-members:  __init__

Renderers
---------

.. automodule:: galactic.algebras.relational.renderer
    :members:
    :inherited-members:
