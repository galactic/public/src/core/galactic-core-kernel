Arithmetic example
==================

.. automodule:: galactic.algebras.examples.arithmetic
    :members:
    :inherited-members:  __init__

.. automodule:: galactic.algebras.examples.arithmetic.renderer
    :members:
    :inherited-members:  __init__

Color example
=============

.. automodule:: galactic.algebras.examples.color
    :members:
    :inherited-members:  __init__

.. automodule:: galactic.algebras.examples.color.renderer
    :members:
    :inherited-members:  __init__
