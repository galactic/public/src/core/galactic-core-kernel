Lattices
========

Classes and functions
---------------------

.. automodule:: galactic.algebras.lattice
    :members:
    :inherited-members:  __init__

Renderers
---------

.. automodule:: galactic.algebras.lattice.renderer
    :members:
    :inherited-members:
