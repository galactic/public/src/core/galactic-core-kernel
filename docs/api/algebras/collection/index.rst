Collections
===========

.. automodule:: galactic.algebras.collection
    :members:
    :inherited-members:
