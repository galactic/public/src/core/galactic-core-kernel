Posets
======

Classes and functions
---------------------

.. automodule:: galactic.algebras.poset
    :members:
    :inherited-members:  __init__

Renderers
---------

.. automodule:: galactic.algebras.poset.renderer
    :members:
    :inherited-members:
