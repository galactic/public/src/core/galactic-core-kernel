..  toctree::
    :hidden:
    :maxdepth: 1

============
Architecture
============

Modules
=======

The **GALACTIC** kernel contains several mathematical algebra modules:

* a :mod:`galactic.algebras.set` module for dealing with sets;
* a :mod:`galactic.algebras.relational` module for dealing with
  relational data;
* a :mod:`galactic.algebras.poset` module for dealing with partially
  ordered sets;
* a :mod:`galactic.algebras.lattice` module for dealing with lattice
  and semi-lattices;

Class diagrams
==============

.. uml:: lattice.uml
    :align: center
    :caption: :class:`~galactic.algebras.lattice.Lattice` class

