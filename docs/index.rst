========================
**GALACTIC** core kernel
========================

*galactic-core-kernel* [#logo]_ [#logokernel]_ is the core library for studying
lattices.

**GALACTIC** stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

..  toctree::
    :maxdepth: 1
    :caption: Getting started

    install/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Learning

    theory/index.rst
    architecture/index.rst
    notebooks

..  toctree::
    :maxdepth: 1
    :caption: Reference

    api/algebras/collection/index.rst
    api/algebras/set/index.rst
    api/algebras/relational/index.rst
    api/algebras/poset/index.rst
    api/algebras/lattice/index.rst
    api/algebras/examples/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Release Notes

    release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
        The **GALACTIC** icon has been constructed using icons present in the
        `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
        designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
        The product or characters depicted in these icons
        are © by Disney / Lucasfilm.

        It's a tribute to the french mathematician
        `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
        died at age 20 in a duel.
        In France, Concept Lattices are also called *Galois Lattices*.

.. [#logokernel]
        The *galactic-core-kernel* icon has been constructed using the main
        **GALACTIC** icon and the
        `Atom, blockchain, coins icon <https://www.iconfinder.com/icons/9297480/atom_blockchain_coins_cryptocurrency_crypto_cosmos_icon>`_
        designed by
        `LAFS <https://www.iconfinder.com/nandiny>`_
