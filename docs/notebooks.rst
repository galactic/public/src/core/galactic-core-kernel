Notebooks
=========

..  nbgallery::

    share/galactic/core/kernel/notebooks/poset/notebook
    share/galactic/core/kernel/notebooks/relational/notebook
    share/galactic/core/kernel/notebooks/lattice/notebook
    share/galactic/core/kernel/notebooks/lattice/extensions
