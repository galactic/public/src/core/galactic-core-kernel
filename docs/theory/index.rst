..  toctree::
    :hidden:
    :maxdepth: 1

Theory
======
Before presenting the functionalities of this framework,
we need to introduce some important theoretical concepts.

The aim of this library is to propose operations on
lattice theory.

Lattice theory was introduced by Birkhoff in his
book `Trends in LATTICE THEORY`_

Let's first introduce the notion of an order relation.

A binary relation :math:`R` on a set :math:`E` is a
set of pairs :math:`(x,y)`, which is a subset of all
the pairs :math:`\in E^2`, We can also write
:math:`xRy` to say :math:`x` has :math:`R`
relation  with :math:`y`.

We say that :math:`R` is an order relation
and we note it :math:`\leq` if it is:

* reflexive: :math:`a \leq a`
* transitive: :math:`a \leq b` and :math:`b\leq c` implies :math:`a \leq c`
* anti-symmetric: :math:`a \leq b` and :math:`b \leq a` implies :math:`a = b`

An ordered set is a pair
:math:`(E,\leq)`, with :math:`E` being a
set and :math:`\leq` an
order relation on :math:`E`.

:math:`a` is called lower neighbour of :math:`b`, if :math:`a < b`
and there is no element :math:`c` such as :math:`a < c < b`,
so in this case :math:`b` is an upper neighbour of :math:`a`.
(we write: :math:`a \prec b`).

Every finite ordered set :math:`(E,\leq)` can be
represented by a line diagram called a `Hasse diagram`_.

Two elements :math:`x`, :math:`y` of an ordered set :math:`(E,\leq)`
are considered comparable if :math:`x \leq y` or :math:`y \leq x`,
otherwise they are incomparable.

Let :math:`(E,\leq)` be an ordered set,
and :math:`X` a subset of :math:`E`. A lower bound
of :math:`X` is an element :math:`s`
of :math:`E` with :math:`s < x` for all :math:`x \in X`.
An upper bound of :math:`X` is defined dually.
If there is a largest element in the set of all lower
bounds of :math:`X`, it is called the *infimum*
of :math:`X` and is denoted by :math:`\inf X` or
:math:`\bigwedge X`, dually, a least upper bound is called
*supremum* and denoted by :math:`\sup X` or :math:`\bigvee X`.

Infimum and supremum are frequently also called meet and join.

An ordered set :math:`V = (L,\leq)` is a lattice if
for any two elements :math:`x, y \in L`, the supremum
and the infimum exists.

:math:`L` is called a complete lattice, if the supremum
:math:`\bigvee X` and the infimum :math:`\bigwedge X` exist
for any subset :math:`X` of :math:`L`. Every complete
lattice :math:`L` has a largest element,
:math:`\bigvee L`, called the **unit element**
of the lattice, denoted by :math:`1_L`. Dually,
the smallest element :math:`0_L` is called
the **zero element**\.

**Irreducible element**: element that can’t be
obtained as the join or the meet of any subset
of other elements:

.. _Trends in LATTICE THEORY:  https://bit.ly/2t8NweD
.. _Hasse diagram: https://en.wikipedia.org/wiki/Hasse_diagram
